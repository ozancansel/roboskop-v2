#include <Arduino.h>
#include <SimpleSerial.h>
#include <TaskScheduler.h>
#include <lesson.h>
#include <lesson2.h>
#include <lesson3.h>
#include <lesson4.h>
#include <lesson5.h>
#include <lesson6.h>
#include <lesson7.h>
#include <lesson8.h>
#include <lesson9.h>
#include <lesson10.h>
#include <lesson11.h>
#include <lesson12.h>
#include <lesson13.h>
#include <lesson14.h>
#include <lesson15.h>
#include <switchgame.h>
#include <BlankLesson.h>
#include <EventContext.h>
#include <context.h>
#include <robogezgin.h>
#include <MemoryFree.h>

// #define   DEBUG
#define   START_EXAMPLE_COMMAND     83      //{S<lessonNum>}
#define   END_EXAMPLE_COMMAND       69      //{E}
#define   ATTACH_ANALOG_READ        65      //{A<enable>,<pin>,<period>}
#define   FREE_MEMORY_COMMAND       70       //{F}

void      onCommandReceived(uint8_t code);
void      onAnalogReadCallback(int pin, int val);
void      startExample(int id);
void      endExample();

Context*  createContext(int idx);
Context*  createEmptyLesson();

SimpleSerial    comSerial(&Serial , onCommandReceived);
Scheduler      os;
EventContext   eventContext(&os , onAnalogReadCallback);

Context      *currentLesson = NULL;

void setup(){

  Serial.begin(9600);

  currentLesson = createEmptyLesson();

  startExample(16);
}

void loop(){

  os.execute();
  comSerial.check();

  currentLesson->loop();
}

void onCommandReceived(uint8_t code){

  switch(code){
    case START_EXAMPLE_COMMAND :{

        int   exampleId = comSerial.readIntArg();

        startExample(exampleId);

        break;
    }
    case END_EXAMPLE_COMMAND :     {

         endExample();

        break;
    }
    case ATTACH_ANALOG_READ   :    {
         //{A<enable>,<pin>,<period>}
         bool enable = comSerial.readIntArg();
         int  pin   = comSerial.readIntArg();

         if(enable){
              int period = comSerial.readIntArg();

             eventContext.attachAnalogRead(pin, period);
         }
         else{
              eventContext.detachAnalogRead(pin);
         }

         break;
    }
    case FREE_MEMORY_COMMAND  :    {

         Serial.print(SimpleSerial::COMMAND_START_CH);
         Serial.write(FREE_MEMORY_COMMAND);
         Serial.print(freeMemory());
         Serial.print(SimpleSerial::COMMAND_END_CH);
         break;
    }
    default : {

      currentLesson->onCommandReceived(code);

      break;
    }
  }
}

void onAnalogReadCallback(int pin, int val){
     //{A<pin>,<value>}
     Serial.print(SimpleSerial::COMMAND_START_CH);
     Serial.write(ATTACH_ANALOG_READ);
     Serial.print(pin);
     Serial.print(SimpleSerial::COMMAND_ARGS_SEPERATOR);
     Serial.print(val);
     Serial.print(SimpleSerial::COMMAND_END_CH);
}

void startExample(int id){

     currentLesson->end();
     free(currentLesson);

     currentLesson = createContext(id);

     currentLesson->start();

}

void endExample(){
     currentLesson->end();

     free(currentLesson);

     currentLesson = createEmptyLesson();

     currentLesson->start();
}

Context*  createContext(int idx){

     Context *ctx = NULL;

     switch(idx){
               case      0    :
                    ctx = new BlankLesson();
                    break;
               case      1    :
                    ctx = new SwitchGame();
                    break;
               case      2    :
                    ctx = new Lesson2();
                    break;
               case      3    :
                    ctx = new Lesson3();
                    break;
               case      4    :    {
                    ctx = new Lesson4();
                    break;
               }
               case      5    :
                    ctx = new Lesson5();
                    break;
               case      6    :
                    ctx = new Lesson6();
                    break;
               case      7    :
               ctx = new Lesson7();
                    break;
               case      8    :    {
                    ctx = new Lesson8();
                    break;
               }
               case      9    :    {
                    ctx = new Lesson9();
                    break;
               }
               case      10    :    {
                    ctx = new Lesson10();
                    break;
               }
               case      11    :    {
                    ctx = new Lesson11();
                    break;
               }
               case      12    :    {
                    ctx = new Lesson12();
                    break;
               }
               case      13    :    {
                    ctx = new Lesson13();
                    break;
               }
               case      14    :    {
                    ctx = new Lesson14();
                    break;
               }
               case      15    :    {
                    ctx = new Lesson15();
                    break;
               }
               case      16    :    {
                    ctx = new RoboGezgin();
                    break;
               }
     }

     ctx->setOs(&os);
     ctx->setSerial(&comSerial);

     return ctx;
}

Context*  createEmptyLesson(){
     return new BlankLesson();
}
