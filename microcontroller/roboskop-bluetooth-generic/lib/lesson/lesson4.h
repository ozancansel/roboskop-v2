#ifndef LESSON_4_H
#define LESSON_4_H

#include <Arduino.h>
#include <lesson.h>
#include <TaskScheduler.h>
#include <SimpleSerial.h>
#include <analogread.h>

#define   _START_POT_READ_COMMAND       115    //{s}
#define   _PAUSE_READ_COMMAND           112    //{p}

#define   _LESSON_4_ANALOG_READ_PIN          A0

class Lesson4 : public Lesson{

public:

 Lesson4() : Lesson(
      static_cast<Context::loopFunc>(&Lesson4::_loop),
      static_cast<Context::startFunc>(&Lesson4::_start),
      static_cast<Context::endFunc>(&Lesson4::_end),
      static_cast<Context::commandReceived>(&Lesson4::_commandReceived)) {
      set_derivedPtr(this);
}

  void onAnalogRead(void *self, int key , int analogVal);

protected:

    void    _loop();
    void    _start();
    void    _end();
    void    _commandReceived( uint8_t code );

private:

    AnalogRead      *_reader;

};

void Lesson4::_loop(){
}

void Lesson4::_start(){
    pinMode(A0 , INPUT);
    _reader = new AnalogRead();
    _reader->setFuncPtr(reinterpret_cast<AnalogRead::callbackFunc>(&Lesson4::onAnalogRead));
}

void Lesson4::_end()     {
     delete _reader;
}

void Lesson4::_commandReceived(uint8_t code){

     switch(code){
       case _START_POT_READ_COMMAND :
           if(_reader->getStatus() == AnalogRead::NONE)
             _reader->readPeriodically(0 , _LESSON_4_ANALOG_READ_PIN , 50 ,  os() , (void*)this);
           else if(_reader->getStatus() == AnalogRead::READ_PAUSED)
             _reader->resume();

       break;
       case _PAUSE_READ_COMMAND    :
           _reader->pause();
       break;
     }
}


void Lesson4::onAnalogRead(void *self, int key , int val){

       int potVal = analogRead(A0);

       Serial.print(SimpleSerial::COMMAND_START_CH);
       Serial.write(_START_POT_READ_COMMAND);
       Serial.print(potVal);
       Serial.print(SimpleSerial::COMMAND_END_CH);
}

#endif
