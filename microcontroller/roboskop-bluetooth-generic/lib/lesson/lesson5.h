#ifndef LESSON_5_H
#define LESSON_5_H

#include <lesson.h>
#include <analogread.h>
#include <SimpleSerial.h>

#define     _LESSON_5_ANALOG_PIN      A0
#define     _LESSON_5_START_TO_READ   115 //{s}
#define     _LESSON_5_PAUSE_READ      112 //{p}

class Lesson5 : public Lesson{

public:

     Lesson5() : Lesson(
          static_cast<Context::loopFunc>(&Lesson5::_loop),
          static_cast<Context::startFunc>(&Lesson5::_start),
          static_cast<Context::endFunc>(&Lesson5::_end),
          static_cast<Context::commandReceived>(&Lesson5::_commandReceived)) {  set_derivedPtr(this);
          }
  void   onAnalogRead(void* self, int key , int analogVal);

protected:


  void  _loop();
  void  _start();
  void _end();
  void _commandReceived(uint8_t code);

private:

  AnalogRead  *_reader;

};

void Lesson5::onAnalogRead(void *self, int key, int analogVal){

  Serial.print(SimpleSerial::COMMAND_START_CH);
  Serial.write(_LESSON_5_START_TO_READ);
  Serial.print(analogVal);
  Serial.print(SimpleSerial::COMMAND_END_CH);

}

void Lesson5::_loop(){

}

void Lesson5::_start(){
  pinMode(A0 , INPUT);
  _reader = new AnalogRead();
  _reader->setFuncPtr(reinterpret_cast<AnalogRead::callbackFunc>(&Lesson5::onAnalogRead));
}

void Lesson5::_end(){
     delete _reader;
}

void Lesson5::_commandReceived(uint8_t code){

  switch(code){
    case _LESSON_5_START_TO_READ :

        if(_reader->getStatus() == AnalogRead::NONE)
          _reader->readPeriodically(0 , _LESSON_5_ANALOG_PIN , 50 ,  os() , this);
        else if(_reader->getStatus() == AnalogRead::READ_PAUSED)
          _reader->resume();

    break;
    case _LESSON_5_PAUSE_READ    :
        _reader->pause();
    break;
  }

}

#endif
