#ifndef LESSON_H
#define LESSON_H

#include <Arduino.h>
#include <TaskScheduler.h>
#include <SimpleSerial.h>
#include <context.h>

class Lesson : public Context{

public:

     Lesson(Context::loopFunc loopFuncPtr, Context::startFunc startFuncPtr , Context::endFunc endFuncPtr , Context::commandReceived commandReceivedFuncPtr)
     : Context(loopFuncPtr , startFuncPtr , endFuncPtr , commandReceivedFuncPtr){    }

     Lesson() {}

};

#endif
