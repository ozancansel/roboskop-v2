#ifndef LESSON_10_H
#define LESSON_10_H

#include <Arduino.h>
#include <lesson.h>
#include <buzzer.h>

#define _LESSON_10_BUZZER_PIN      9
#define _LESSON_10_POT_PIN         A0

class Lesson10 : public Lesson{

public:

     Lesson10();

protected:

    void    _loop();
    void    _start();
    void    _end();
    void    _commandReceived( uint8_t code );

private:

     Buzzer    _buzzer = Buzzer(_LESSON_10_BUZZER_PIN);

     int       _lastNote;

};

Lesson10::Lesson10() : Lesson(
     static_cast<Context::loopFunc>(&Lesson10::_loop),
     static_cast<Context::startFunc>(&Lesson10::_start),
     static_cast<Context::endFunc>(&Lesson10::_end),
     static_cast<Context::commandReceived>(&Lesson10::_commandReceived)){
          set_derivedPtr(this);
     _lastNote = 0;
}

void Lesson10::_loop(){

     // int analogVal = analogRead(_LESSON_10_POT_PIN);
     //
     // int note = map(analogVal , 0 , 1000 , 0 , 11);
     //
     // if(note == _lastNote)
     //      return;
     //
     // _buzzer.playNote(note , 4);
     //
     // _lastNote = note;

     int analogVal = analogRead(_LESSON_10_POT_PIN);

     int val = map(analogVal , 0 , 1023 , 0 , 255);

     analogWrite(_LESSON_10_BUZZER_PIN , val);

}

void Lesson10::_start(){
     pinMode(_LESSON_10_POT_PIN , INPUT);
     _buzzer.stopNote();
}

void Lesson10::_end(){
     _buzzer.stopNote();
}

void Lesson10::_commandReceived( uint8_t code ){

}

#endif
