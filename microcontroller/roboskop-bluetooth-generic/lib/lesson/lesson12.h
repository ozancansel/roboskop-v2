#ifndef LESSON_12_H
#define LESSON_12_H

#include <lesson.h>
#include <acoustic.h>
#include <buzzer.h>

#define LESSON_12_ACOUSTIC_ECHO_PIN          12
#define LESSON_12_ACOUSTIC_TRIGGER_PIN       11
#define LESSON_12_BUZZER_PIN                 8

#define LESSON_12_START_SENSOR               114 // {r}
#define LESSON_12_PAUSE_SENSOR               112 // {p}

class Lesson12 : public Lesson{

public:

     void onDistanceMeasure(void *self, int key , int val);
     Lesson12();

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);

private:

     Acoustic       *_acoustic;
     Buzzer         *_buzzer;

     unsigned long _beforeMappedVal;

};

Lesson12::Lesson12()  : Lesson(
     static_cast<Context::loopFunc>(&Lesson12::_loop),
     static_cast<Context::startFunc>(&Lesson12::_start),
     static_cast<Context::endFunc>(&Lesson12::_end),
     static_cast<Context::commandReceived>(&Lesson12::_commandReceived)){
     set_derivedPtr(this);
     _beforeMappedVal = 0;
     _acoustic        = NULL;
     _buzzer          = NULL;
}

void Lesson12::_loop(){

}

void Lesson12::_start(){
     _acoustic = new Acoustic(LESSON_12_ACOUSTIC_TRIGGER_PIN , LESSON_12_ACOUSTIC_ECHO_PIN );
     _buzzer = new Buzzer(LESSON_12_BUZZER_PIN);
     _buzzer->setOs(os());

     _acoustic->setCallbackPtr(reinterpret_cast<Acoustic::callbackFunc>(&Lesson12::onDistanceMeasure));
     _buzzer->stopNote();
}

void Lesson12::_end(){
     _buzzer->stopNote();
     _acoustic->pause();

     delete _buzzer;
     delete _acoustic;
}

void Lesson12::_commandReceived(uint8_t code){

     switch(code){
          case LESSON_12_START_SENSOR :

          if(_acoustic->getStatus() == Acoustic::READ_PAUSED)
          _acoustic->resume();
          else
          _acoustic->readPeriodically(0 , 70 , os() , this);

          break;
          case LESSON_12_PAUSE_SENSOR:
          _acoustic->pause();
          break;
     }

}

void Lesson12::onDistanceMeasure(void *self, int key , int val){

    Lesson12  *_th = (Lesson12*)self;

     if(val < 0)
      return;

          Serial.print(SimpleSerial::COMMAND_START_CH);
          Serial.write(LESSON_12_START_SENSOR);
          Serial.print(val);
          Serial.print(SimpleSerial::COMMAND_END_CH);

     if(val > 40){
          _th->_buzzer->pause();
          return;
     }

     if(val < 2){
          _th->_buzzer->pause();
          _th->_buzzer->on();

          return;
     }

     int mappedVal =  map(val , 0 , 40 , 50 , 500);

     if(abs(mappedVal - _th->_beforeMappedVal) < 30)
     return;

     _th->_buzzer->blink(mappedVal , os());

     _th->_beforeMappedVal = mappedVal;

}

#endif
