#ifndef LESSON_3_H
#define LESSON_3_H

#include <TaskScheduler.h>
#include <SimpleSerial.h>
#include <circularindex.h>
#include "lesson.h"

#define   _TRAFFIC_LIGHT_RED_PIN      11
#define   _TRAFFIC_LIGHT_YELLOW_PIN   10
#define   _TRAFFIC_LIGHT_GREEN_PIN    9

#define   _SET_RED_INTERVAL_CODE          114 // "r"
#define   _SET_YELLOW_INTERVAL_CODE       121 //  "y"
#define   _SET_GREEN_INTERVAL_CODE        103 //  "g"

#define   _STOP_TRAFFIC_LIGHT_CODE        115

class Lesson3 : public Lesson {

public:

  Lesson3(): Lesson(
       static_cast<Context::loopFunc>(&Lesson3::_loop),
       static_cast<Context::startFunc>(&Lesson3::_start),
       static_cast<Context::endFunc>(&Lesson3::_end),
       static_cast<Context::commandReceived>(&Lesson3::_commandReceived)) {

       set_derivedPtr(this);
 }

  static void onPeriod(void *sender , void** args);

protected:

  CircularIndex   _lightIdx = CircularIndex(2);

  void    _loop();
  void    _start();
  void    _end();
  void    _commandReceived( uint8_t code );

  void    stop();
  void    start();

private:
                                // G     R     Y
 unsigned long  intervals[3] = { 2000 , 3500 , 800 };
  int            pins[3] = { _TRAFFIC_LIGHT_RED_PIN , _TRAFFIC_LIGHT_YELLOW_PIN , _TRAFFIC_LIGHT_GREEN_PIN };

  Task  *_periodTask;

};

void Lesson3::_loop(){

}

void Lesson3::_start(){

  //Pinler ayarlanıyor
    for(int i = 0; i < 3 ; i++){
      pinMode(pins[i] , OUTPUT);
      digitalWrite(pins[i] , LOW);
    }

    _periodTask = new Task( 1 , TASK_ONCE, Lesson3::onPeriod , os() , true, NULL, NULL , (void*) this, NULL);

}

void Lesson3::_end(){
  os()->deleteTask(*_periodTask);
  delete _periodTask;

  for(int i = 0; i < 3; i++){
    digitalWrite(pins[i] , LOW);
  }

}

void Lesson3::_commandReceived( uint8_t code ){

    int interval = 0;

  switch(code){
    case _SET_RED_INTERVAL_CODE:

       interval = serial()->readIntArg();

       intervals[1] = interval;

    break;

    case _SET_YELLOW_INTERVAL_CODE:

      interval = serial()->readIntArg();

      intervals[2] = interval;

    break;
    case _SET_GREEN_INTERVAL_CODE:

      interval = serial()->readIntArg();

      intervals[0] = interval;

    break;

    case _STOP_TRAFFIC_LIGHT_CODE:


    break;
  }

}

void Lesson3::stop(){
     _periodTask->disable();

     for(int i = 0; i < 3; i++)
          digitalWrite(pins[i] , LOW);

}

void Lesson3::start(){
     _periodTask->enableIfNot();
}

void Lesson3::onPeriod(void* sender , void** args){

  Lesson3 *lesson = (Lesson3*) sender;

  for(int i = 0; i < 3; i++){

    if(lesson->_lightIdx.idx() == i){
        digitalWrite(lesson->pins[lesson->_lightIdx.idx()] , HIGH);
    }
    else{
        digitalWrite(lesson->pins[i] , LOW);
    }
  }

  lesson->_lightIdx.increment();

  lesson->_periodTask->set(lesson->intervals[lesson->_lightIdx.idx()] , TASK_ONCE , Lesson3::onPeriod , NULL , NULL);
  lesson->_periodTask->enableIfNot();

}

#endif
