#ifndef LESSON_7_H
#define LESSON_7_H

#include <Arduino.h>
#include <lesson.h>
#include <rgb.h>
#include <analogread.h>

#define _LESSON_7_ANALOG_PIN       A0
#define _LESSON_7_R_PIN            11
#define _LESSON_7_G_PIN            10
#define _LESSON_7_B_PIN            9

#define _LESSON_7_SET_R            114 //{r<density>}
#define _LESSON_7_SET_G            103 //{g<density>}
#define _LESSON_7_SET_B            98  //{b<density>}

class Lesson7 : public Lesson{

public:

     Lesson7() : Lesson(
          static_cast<Context::loopFunc>(&Lesson7::_loop),
          static_cast<Context::startFunc>(&Lesson7::_start),
          static_cast<Context::endFunc>(&Lesson7::_end),
          static_cast<Context::commandReceived>(&Lesson7::_commandReceived)) {
          set_derivedPtr(this);
     }

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);

private:

     Rgb       _rgb = Rgb(_LESSON_7_R_PIN , _LESSON_7_G_PIN , _LESSON_7_B_PIN );

     int  _r;
     int  _g;
     int  _b;

};

void Lesson7::_loop(){

     int analogVal = analogRead(_LESSON_7_ANALOG_PIN);

     float multiplier = analogVal / 1023.0;

     _rgb.rgb(_r * multiplier , _g * multiplier , _b * multiplier);

}

void Lesson7::_start(){
     pinMode(_LESSON_7_ANALOG_PIN , INPUT);
     _r = 255;
     _g = 255;
     _b = 255;
}

void Lesson7::_end(){
     _rgb.rgb(0 , 0 , 0);
}

void Lesson7::_commandReceived(uint8_t code){
     switch(code){
          case _LESSON_7_SET_R:{
               int density = serial()->readIntArg();

               _r = density;
               break;
          }
          case _LESSON_7_SET_G:{
               int density = serial()->readIntArg();

               _g = density;
               break;
          }
          case _LESSON_7_SET_B:{
               int density = serial()->readIntArg();

               _b = density;
               break;
          }
     }
}

#endif
