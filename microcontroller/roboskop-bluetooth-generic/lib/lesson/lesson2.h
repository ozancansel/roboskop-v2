#ifndef LESSON_2_H
#define LESSON_2_H

#include <lesson.h>
#include <led.h>

#define _LESSON_2_LED_PIN     8
#define _LED_ON_COMMAND       111 //{o}
#define _LED_OFF_COMMAND      99  //{c}
#define _LED_TOGGLE_COMMAND   116 //{t}
#define _LED_BLINK_COMMAND    98 //{b<interval>}

class Lesson2 : public Lesson{

public    :

     Lesson2() : Lesson(
          static_cast<Context::loopFunc>(&Lesson2::_loop),
          static_cast<Context::startFunc>(&Lesson2::_start),
          static_cast<Context::endFunc>(&Lesson2::_end),
          static_cast<Context::commandReceived>(&Lesson2::_commandReceived)) {
          set_derivedPtr(this);
          _led = NULL;
     }

protected:

     void    _loop();
     void    _start();
     void    _end();
     void    _commandReceived(uint8_t code);

private:

     Led      *_led;
     Led       _anotherLed;

};

void Lesson2::_loop(){

}

void Lesson2::_start(){
      _led = new Led(_LESSON_2_LED_PIN);
}

void Lesson2::_end(){
     delete _led;
}

void Lesson2::_commandReceived(uint8_t code){
     switch(code){
          case _LED_ON_COMMAND :
          if(_led->status() == Led::BLINKING)
               _led->pause();
          _led->on();
          break;
          case _LED_OFF_COMMAND:
          if(_led->status() == Led::BLINKING)
               _led->pause();
          _led->off();
          break;
          case _LED_TOGGLE_COMMAND:

          if(_led->status() == Led::BLINKING)
               _led->pause();

          _led->toggle();
          break;
          case _LED_BLINK_COMMAND:{
               int interval = serial()->readIntArg();
               _led->blink(interval , os());

               if(_led->status() == Led::PAUSED)
                    _led->resume();

               break;
          }
     }
}

#endif
