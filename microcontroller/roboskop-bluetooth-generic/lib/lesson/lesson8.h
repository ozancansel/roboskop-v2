#ifndef LESSON_8_H
#define LESSON_8_H

#include <Arduino.h>
#include <lesson.h>
#include <rgb.h>
#include <analogread.h>

#define _LESSON_8_ANALOG_PIN       A0
#define _LESSON_8_R_PIN            11
#define _LESSON_8_G_PIN            10
#define _LESSON_8_B_PIN            9

#define _LESSON_8_SET_R            114 //{r<density>}
#define _LESSON_8_SET_G            103 //{g<density>}
#define _LESSON_8_SET_B            98  //{b<density>}

class Lesson8 : public Lesson{

public:

     Lesson8();

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);

private:

     Rgb       _rgb = Rgb(_LESSON_8_R_PIN , _LESSON_8_G_PIN , _LESSON_8_B_PIN );

     float  _max;
     float  _beforeMultiplier;
     int  _r;
     int  _g;
     int  _b;

};

Lesson8::Lesson8() : Lesson(
     static_cast<Context::loopFunc>(&Lesson8::_loop),
     static_cast<Context::startFunc>(&Lesson8::_start),
     static_cast<Context::endFunc>(&Lesson8::_end),
     static_cast<Context::commandReceived>(&Lesson8::_commandReceived)){
          set_derivedPtr(this);
     _max = 0;
     _beforeMultiplier = 0;
}

void Lesson8::_loop(){

     int analogVal = analogRead(_LESSON_8_ANALOG_PIN);

     if(analogVal > _max)
          _max = analogVal;

     float multiplier = 1.0 - (analogVal / _max);

     if(abs( (multiplier - _beforeMultiplier) * 100.0 ) < 9){

          if(multiplier < 0.1){
               _rgb.rgb(0 , 0 , 0);
          }

          return;
     }

     _rgb.rgb(_r * multiplier , _g * multiplier , _b * multiplier);

     _beforeMultiplier = multiplier;

}

void Lesson8::_start(){
     pinMode(_LESSON_8_ANALOG_PIN , INPUT);
    _r = 0;
    _g = 0;
    _b = 0;
}

void Lesson8::_end(){
     _rgb.rgb(0 , 0 , 0);
}

void Lesson8::_commandReceived(uint8_t code){
     switch(code){
          case _LESSON_8_SET_R:{
               int density = serial()->readIntArg();

               _r = density;
               break;
          }
          case _LESSON_8_SET_G:{
               int density = serial()->readIntArg();

               _g = density;
               break;
          }
          case _LESSON_8_SET_B:{
               int density = serial()->readIntArg();

               _b = density;
               break;
          }
     }
}

#endif
