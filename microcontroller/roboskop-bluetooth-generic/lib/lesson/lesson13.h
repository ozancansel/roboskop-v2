#ifndef LESSON_13_H
#define LESSON_13_H

#include <lesson.h>
#include <analogread.h>

class Lesson13 : public Lesson{

public:

     Lesson13() : Lesson(
          static_cast<Context::loopFunc>(&Lesson13::_loop),
          static_cast<Context::startFunc>(&Lesson13::_start),
          static_cast<Context::endFunc>(&Lesson13::_end),
          static_cast<Context::commandReceived>(&Lesson13::_commandReceived)) {
          set_derivedPtr(this);
      }

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);


private:

     void onAnalogTick(void *sender, int key , int val);

     AnalogRead analog;

};

void Lesson13::onAnalogTick(void *self , int key , int val){

}

void Lesson13::_loop(){

}

void Lesson13::_start(){

}

void Lesson13::_end(){
}

void Lesson13::_commandReceived(uint8_t code){

}

#endif
