#ifndef LESSON_6_H
#define LESSON_6_H

#include <lesson.h>
#include <rgb.h>

#define _LESSON_6_R_PIN            11
#define _LESSON_6_G_PIN            10
#define _LESSON_6_B_PIN            9

#define _LESSON_6_SET_R            114 //{r<density>}
#define _LESSON_6_SET_G            103 //{g<density>}
#define _LESSON_6_SET_B            98  //{b<density>}

class Lesson6 : public  Lesson{

public:

     Lesson6() : Lesson(
          static_cast<Context::loopFunc>(&Lesson6::_loop),
          static_cast<Context::startFunc>(&Lesson6::_start),
          static_cast<Context::endFunc>(&Lesson6::_end),
          static_cast<Context::commandReceived>(&Lesson6::_commandReceived)) {
          set_derivedPtr(this);
     }

protected:

     void      _loop();
     void      _start();
     void      _end();
     void      _commandReceived(uint8_t code);

private:

     Rgb       _rgb = Rgb(_LESSON_6_R_PIN , _LESSON_6_G_PIN , _LESSON_6_B_PIN );

};

void Lesson6::_loop(){

}

void Lesson6::_start(){
     _rgb.rgb(0 , 0 , 0);
}

void Lesson6::_end(){
     _rgb.rgb(0 , 0 , 0);
}

void Lesson6::_commandReceived(uint8_t code){
     switch(code){
          case _LESSON_6_SET_R:{
               int density = serial()->readIntArg();

               _rgb.setR(density);
               break;
          }
          case _LESSON_6_SET_G:{
               int density = serial()->readIntArg();

               _rgb.setG(density);
               break;
          }
          case _LESSON_6_SET_B:{
               int density = serial()->readIntArg();

               _rgb.setB(density);
               break;
          }
     }
}

#endif
