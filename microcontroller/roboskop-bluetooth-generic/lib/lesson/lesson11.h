#ifndef LESSON_11_H
#define LESSON_11_H

#include <lesson.h>
#include <acoustic.h>
#include <SimpleSerial.h>

#define _LESSON_11_TRIGGER_PIN          11
#define _LESSON_11_ECHO_PIN             12

#define _LESSON_11_START_READ_DISTANCE       114 //{r}
#define _LESSON_11_PAUSE_READ_DISTANCE       112 //{p}

class Lesson11 : public Lesson{

public:

     Lesson11() : Lesson(
          static_cast<Context::loopFunc>(&Lesson11::_loop),
          static_cast<Context::startFunc>(&Lesson11::_start),
          static_cast<Context::endFunc>(&Lesson11::_end),
          static_cast<Context::commandReceived>(&Lesson11::_commandReceived)) {
          set_derivedPtr(this);
      }
     void onDistanceMeasure(void *sender, int key , int val);

protected:

     void    _loop();
     void    _start();
     void    _end();
     void    _commandReceived( uint8_t code );

private:

     Acoustic  *_acoustic;

};

void Lesson11::_loop(){

}

void Lesson11::_start(){
     _acoustic = new Acoustic(_LESSON_11_TRIGGER_PIN , _LESSON_11_ECHO_PIN );

     _acoustic->setCallbackPtr(reinterpret_cast<Acoustic::callbackFunc>(&Lesson11::onDistanceMeasure));
}

void Lesson11::_end(){
     delete _acoustic;
}

void Lesson11::_commandReceived( uint8_t code ){
     switch(code){
          case _LESSON_11_START_READ_DISTANCE:{

               if(_acoustic->getStatus() == Acoustic::READ_PAUSED)
                    _acoustic->resume();
               else
                    _acoustic->readPeriodically(0 , 70 , os() , this);

               break;
          }
          case _LESSON_11_PAUSE_READ_DISTANCE :
               _acoustic->pause();
          break;
     }
}

void Lesson11::onDistanceMeasure(void *sender , int key , int val){
     Serial.print(SimpleSerial::COMMAND_START_CH);
     Serial.write(_LESSON_11_START_READ_DISTANCE);
     Serial.print(val);
     Serial.print(SimpleSerial::COMMAND_END_CH);
}

#endif
