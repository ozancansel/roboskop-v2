#ifndef LESSON_15_H
#define LESSON_15_H

#include <Arduino.h>
#include <lesson.h>
#include <analogread.h>
#include <rgb.h>

#define THRESHOLD   410
#define STEP_1      50
#define STEP_2      80
#define STEP_3      105

#define LESSON_15_MICROPHONE_PIN   A0
#define LESSON_15_R_PIN            11
#define LESSON_15_G_PIN            10
#define LESSON_15_B_PIN            9

#define _LESSON_15_ANALOG_PIN      A0

#define _LESSON_15_START_TO_READ   115  //{s}
#define _LESSON_15_PAUSE_READ      112  //{p}

class Lesson15 : public Lesson{

public:

     static void onIgnoreTick(void *sender , void **args);
     void onAnalogRead(void *self , int key , int analogVal);

     Lesson15() : Lesson(
          static_cast<Context::loopFunc>(&Lesson15::_loop),
          static_cast<Context::startFunc>(&Lesson15::_start),
          static_cast<Context::endFunc>(&Lesson15::_end),
          static_cast<Context::commandReceived>(&Lesson15::_commandReceived)) {
          set_derivedPtr(this);
      }

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);

private:

     bool      _ignoreFlag;

     Rgb       _rgb = Rgb(LESSON_15_R_PIN , LESSON_15_G_PIN , LESSON_15_B_PIN);
     Task      *_ignoreTask;
     AnalogRead     *_reader;

};

void Lesson15::_loop(){

     if(_ignoreFlag)
          return;

     int mic = analogRead(LESSON_15_MICROPHONE_PIN);

     _rgb.rgb(0 , 0 , 0);

     if(mic < THRESHOLD && mic >= (THRESHOLD - STEP_1)){

         _rgb.rgb(0 , 255 , 0);
         _ignoreTask->restartDelayed(2000);
         _ignoreTask->enableIfNot();
         _ignoreFlag = true;
     }else if(mic < (THRESHOLD - STEP_1) && mic > (THRESHOLD - STEP_2)){
       _rgb.rgb(0 , 0 , 255);
       _ignoreTask->restartDelayed(2000);
       _ignoreTask->enableIfNot();
       _ignoreFlag = true;
     }
     else if(mic <= (THRESHOLD - STEP_2) && mic > (THRESHOLD - STEP_3)){
       _rgb.rgb(255 , 0 ,0 );
       _ignoreTask->restartDelayed(2000);
       _ignoreTask->enableIfNot();
       _ignoreFlag = true;
     }else if(mic <= 250){

       _rgb.rgb(255 , 255 , 255);

       _ignoreTask->restartDelayed(2000);
       _ignoreTask->enableIfNot();
       _ignoreFlag = true;
     }
}

void Lesson15::_start(){
     _reader = new AnalogRead();
     _reader->setFuncPtr(reinterpret_cast<AnalogRead::callbackFunc>(&Lesson15::onAnalogRead));
     _ignoreFlag = false;
     _ignoreTask = new Task(0 , TASK_ONCE , Lesson15::onIgnoreTick , os() , false , NULL , NULL , (void*)this , NULL );
}

void Lesson15::_end(){
     os()->deleteTask(*_ignoreTask);

     delete _ignoreTask;
     delete _reader;

     _rgb.rgb(0 , 0 , 0);
}

void Lesson15::_commandReceived(uint8_t code){

     switch(code){
          case _LESSON_15_START_TO_READ :
          if(_reader->getStatus() == AnalogRead::NONE)
            _reader->readPeriodically(0 , _LESSON_15_ANALOG_PIN , 50 ,  os() , (void*)this);
          else if(_reader->getStatus() == AnalogRead::READ_PAUSED)
            _reader->resume();
            break;

            case _LESSON_15_PAUSE_READ  :
            _reader->pause();
            break;
     }

}

void Lesson15::onIgnoreTick(void *sender, void **args){
     Lesson15 *obj = (Lesson15*)sender;

     obj->_ignoreFlag = false;
}

void Lesson15::onAnalogRead(void *self, int key , int analogVal){
     Serial.print("{");
     Serial.write(_LESSON_15_START_TO_READ);
     Serial.print(analogVal);
     Serial.print("}");
}

#endif
