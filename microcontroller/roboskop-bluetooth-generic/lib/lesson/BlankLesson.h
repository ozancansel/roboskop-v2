#ifndef BLANK_LESSON_H
#define BLANK_LESSON_H

#include <lesson.h>

class BlankLesson : public Lesson {

public:

     BlankLesson() : Lesson(
          static_cast<Context::loopFunc>(&BlankLesson::_loop),
          static_cast<Context::startFunc>(&BlankLesson::_start),
          static_cast<Context::endFunc>(&BlankLesson::_end),
          static_cast<Context::commandReceived>(&BlankLesson::_commandReceived)) {


     }


       void    _start() { Serial.println("Blank started");  }
       void    _loop() {
       }
       void    _end() { }
       void    _commandReceived(uint8_t code) { }

};

#endif
