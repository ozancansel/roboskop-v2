#ifndef LESSON_9_H
#define LESSON_9_H

#include <lesson.h>
#include <buzzer.h>

#define _LESSON_9_BUZZER_PIN       9

#define _LESSON_9_PLAY_NOTE        112 // {p<noteIdx>,<pitchIdx>}
#define _LESSON_9_STOP_NOTE        115 // {s}

class Lesson9 : public Context{

public:

     Lesson9() : Context(
          static_cast<Context::loopFunc>(&Lesson9::_loop),
          static_cast<Context::startFunc>(&Lesson9::_start),
          static_cast<Context::endFunc>(&Lesson9::_end),
          static_cast<Context::commandReceived>(&Lesson9::_commandReceived)) {

          set_derivedPtr(this);
     }

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);

private:

     Buzzer    _buzzer = Buzzer(_LESSON_9_BUZZER_PIN);

};

void Lesson9::_loop(){

}

void Lesson9::_start(){

}

void Lesson9::_end(){
     _buzzer.stopNote();
}

void Lesson9::_commandReceived(uint8_t code){

          switch(code){
          case _LESSON_9_PLAY_NOTE :{

               int noteIdx = serial()->readIntArg();
               int pitchIdx = serial()->readIntArg();

               _buzzer.playNote(noteIdx , pitchIdx);


               break;
          }
          case _LESSON_9_STOP_NOTE:{
               _buzzer.stopNote();
               break;
          }
     }
}

#endif
