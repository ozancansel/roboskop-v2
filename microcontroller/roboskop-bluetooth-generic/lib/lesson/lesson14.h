#ifndef LESSON_14_H
#define LESSON_14_H

#include <Arduino.h>
#include <lesson.h>
#include <analogread.h>

#define _LESSON_14_ANALOG_PIN      A0

#define _LESSON_14_START_TO_READ   115  //{s}
#define _LESSON_14_PAUSE_READ      112  //{p}

class Lesson14 : public Lesson{

public:

     void onAnalogRead(void *sender , int key , int analogVal);

     Lesson14() : Lesson(
          static_cast<Context::loopFunc>(&Lesson14::_loop),
          static_cast<Context::startFunc>(&Lesson14::_start),
          static_cast<Context::endFunc>(&Lesson14::_end),
          static_cast<Context::commandReceived>(&Lesson14::_commandReceived)) {
          set_derivedPtr(this);
      }

protected:

     void  _loop();
     void  _start();
     void _end();
     void _commandReceived(uint8_t code);

private:

     AnalogRead     *_reader;

};

void Lesson14::onAnalogRead(void* sender, int key , int analogVal){

     Serial.print("{");
     Serial.write(_LESSON_14_START_TO_READ);
     Serial.print(analogVal);
     Serial.print("}");

}

void Lesson14::_loop(){

}

void Lesson14::_start(){
     _reader   = new AnalogRead();

     _reader->setFuncPtr(reinterpret_cast<AnalogRead::callbackFunc>(&Lesson14::onAnalogRead));
}

void Lesson14::_end(){
     delete _reader;
}

void Lesson14::_commandReceived(uint8_t code){

     switch(code){
       case _LESSON_14_START_TO_READ :

           if(_reader->getStatus() == AnalogRead::NONE)
             _reader->readPeriodically(0 , _LESSON_14_ANALOG_PIN , 50 ,  os() , (void*)this);
           else if(_reader->getStatus() == AnalogRead::READ_PAUSED)
             _reader->resume();

       break;
       case _LESSON_14_PAUSE_READ    :
           _reader->pause();
       break;
     }
}

#endif
