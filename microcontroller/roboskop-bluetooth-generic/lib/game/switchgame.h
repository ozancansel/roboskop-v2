#ifndef SWITCH_GAME_H
#define SWITCH_GAME_H

#include <TaskScheduler.h>
#include <lesson.h>

#define _SWITCH_PRESSED_CODE       115
#define _SWITCH_PIN                A0

class SwitchGame : public Lesson   {

public:

     SwitchGame();

     static void onIgnoreTick(void *sender , void **args);

protected:

     void    _loop();
     void    _start();
     void    _end();
     void    _commandReceived( uint8_t code );

private:

     bool      _previousState;
     bool      _ignoreFlag;
     int       _scaleVal;
     int       _scaleCounter;
     int       _scaleSum;

     Task      *_ignoreTask;

};

SwitchGame::SwitchGame(){
     _previousState = false;
     _ignoreFlag = false;
     _scaleVal = 5;
     _scaleCounter = 0;
     _scaleSum = 0;
}

void SwitchGame::_loop(){

     if(_ignoreFlag){
          return;
     }

     int analogVal  = analogRead(_SWITCH_PIN);

     if(_scaleCounter < _scaleVal){
          if(analogVal < 1000)
               analogVal = 0;

          _scaleSum += analogVal;

          _scaleCounter++;
     }
     else{

          bool state = (_scaleSum / _scaleVal) > 1000;

          if(!_previousState && state){

               _ignoreTask->restartDelayed(150);
               _ignoreTask->enableIfNot();

               _ignoreFlag = true;
          }

          _previousState = state;
          _scaleSum = 0;
          _scaleCounter = 0;
     }
}

void SwitchGame::_start(){
     pinMode(_SWITCH_PIN , OUTPUT);
     digitalWrite(_SWITCH_PIN , LOW);
     pinMode(_SWITCH_PIN , INPUT);

     _ignoreTask = new Task(0 , TASK_ONCE , SwitchGame::onIgnoreTick , os() , false , NULL , NULL , (void*)this , NULL );

}

void SwitchGame::_end(){
     if(os() != NULL){
          os()->deleteTask(*_ignoreTask);

          delete _ignoreTask;
     }
}

void SwitchGame::_commandReceived( uint8_t  code){

}

void SwitchGame::onIgnoreTick(void *sender, void **args){

     SwitchGame *obj = (SwitchGame*)sender;

     obj->_ignoreFlag = false;
}


#endif
