#ifndef INT_BUFFER
#define INT_BUFFER

class IntBuffer{

private:

  unsigned int      *_buffer;
  int               _length;
  int               _currentIdx;
  int               _dequeueIdx;

public:

  IntBuffer(unsigned int *buffer , int length);

  void            enqueue(unsigned int val);
  unsigned int    dequeue();
  void            clear();
  bool            empty();
  bool            full();

};

IntBuffer::IntBuffer(unsigned int *buffer , int length){
    _currentIdx = 0;
    _buffer = buffer;
    _length = length;
}

void IntBuffer::enqueue(unsigned int val){
    _buffer[_currentIdx] = val;
    _currentIdx++;
}

unsigned int IntBuffer::dequeue(){
  if(_dequeueIdx >= _currentIdx)
    return 0;

  unsigned int val = _buffer[_dequeueIdx];
  _dequeueIdx++;

  return val;
}

void IntBuffer::clear(){
  _currentIdx = 0;
  _dequeueIdx = 0;
}

bool IntBuffer::empty(){
  return _currentIdx <= _dequeueIdx;
}

bool IntBuffer::full(){
  return _currentIdx >= _length;
}

#endif
