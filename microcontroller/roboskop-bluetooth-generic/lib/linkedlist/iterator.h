#ifndef ITERATOR_H
#define ITERATOR_H

#include <linkedlist.h>

template<class T>
class Iterator{

public:

     Iterator(Node<T> *p);
     void next();
     T    data();
     bool hasNext();
     bool isNull();
     void reset();

private:

     Node<T>   *_temp;
     Node<T>   *_p;

};

template<class T>
Iterator<T>::Iterator(Node<T> *p){
     _p = p;
     _temp = p;
}

template<class T>
void Iterator<T>::next(){

     if(_temp == NULL)
          return;

     _temp = _temp->next;
}

template<class T>
void Iterator<T>::reset(){
     _temp = _p;
}

template<class T>
T    Iterator<T>::data(){
     return _temp->data;
}

template<class T>
bool Iterator<T>::hasNext(){
     return _temp->next != NULL;
}

template<class T>
bool Iterator<T>::isNull(){
     return _temp == NULL;
}

#endif
