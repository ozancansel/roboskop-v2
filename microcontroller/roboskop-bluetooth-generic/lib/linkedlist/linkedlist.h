#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <Arduino.h>
#include <node.h>
#include <iterator.h>

template<class T>
class LinkedList{

public:

     LinkedList();
     ~LinkedList();
     void insertToHead(T val);
     void insertToTail(T val);
     void print();
     void remove(T val);
     T    pop();
     int  length();
     T    first();
     T    last();
     Iterator<T>*    iterator();

private:

     Node<T>   *head;
     Node<T>   *tail;

     int       _length;
};

template<class T>
LinkedList<T>::LinkedList(){
     head      = NULL;
     tail      = NULL;
     _length   = 0;
}

template<class T>
LinkedList<T>::~LinkedList(){
     Node<T> * curr = head;

     while (head)
     {
          head = head->next;
          delete curr;
          curr = head;
     }
}

template<class T>
int LinkedList<T>::length(){
     return _length;
}

template<class T>
T LinkedList<T>::first(){
     return head->data;
}

template<class T>
T LinkedList<T>::last(){
     return tail->data;
}

template<class T>
void LinkedList<T>::remove(T val){
     Node<T> *curr = head;
     Node<T> *before = head;

     while(curr){

          if(curr->data == val){
               //Silinecek eğer en sonuncuysa
               if(curr == head){
                    if(head == tail)
                         tail = NULL;
                    head = curr->next;

                    delete curr;
               }
               else if(curr == tail){
                    before->next = NULL;
                    tail = before;
                    delete curr;
               }
               else     {
                    before->next = curr->next;

                    delete curr;
               }

               _length--;
          }

          before = curr;
          curr = curr->next;
     }

}

template<class T>
T    LinkedList<T>::pop(){
     T    val;
     if(length() == 0)
          return NULL;
     else if(length() == 1){
          val = head->data;
          head = NULL;
          tail = NULL;

          _length--;
     }else{
          Node<T> *f = head;
          val = head->data;
          head = head->next;
          delete f;

          _length--;
     }

     return val;
}

template<class T>
void LinkedList<T>::insertToHead(T val){

     Node<T>   *newNode = new Node<T>(val);
     newNode->next = head;
     head = newNode;
     if(head->next == NULL)
          tail = newNode;

          _length++;

}

template<class T>
void LinkedList<T>::insertToTail(T val){

     Node<T> * newNode = new Node<T>(val);
       if (tail == NULL)
       {
           newNode->next = tail;
           tail = newNode;
           head = newNode;
           _length++;

           return;
       }
       tail->next = newNode;
       tail = tail->next;

       _length++;
}

template<class T>
void LinkedList<T>::print()
{

    Node<T> * curr = head;

    while (curr)
    {
        Serial.println(curr->data);
        curr = curr->next;
    }
}

template<class T>
Iterator<T>* LinkedList<T>::iterator(){
     Node<T>   *it = head;

     return new Iterator<T>(it);
}


#endif
