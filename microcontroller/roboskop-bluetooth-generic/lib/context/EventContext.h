#ifndef EVENT_CONTEXT_H
#define EVENT_CONTEXT_H

#include <QueueList.h>
#include <analogread.h>
#include <TaskScheduler.h>

class EventContext{

public:

     EventContext(Scheduler *os , void(*analogCb)(int,int));
     void      attachAnalogRead(int pin , int period);
     void      detachAnalogRead(int pin);
     void      onAnalogRead(void *self , int key , int analogVal);
     void      setOs(Scheduler *os);

private:

     Scheduler                *_os;
     QueueList<AnalogRead*>   _analogs;
     void                     (*_analogCallback)(int , int);
     int                      getAnalogActualPin(int idx);
};

EventContext::EventContext(Scheduler *os , void(*analogCb)(int,int)){
     _os = os;
     _analogCallback = analogCb;
}

void EventContext::attachAnalogRead(int pin, int period){

     AnalogRead *read = new AnalogRead();

     read->readPeriodically(pin , getAnalogActualPin(pin), period , _os , (void*)this);
     read->setFuncPtr(reinterpret_cast<AnalogRead::callbackFunc>(&EventContext::onAnalogRead));
     _analogs.push(read);
}

void EventContext::setOs(Scheduler *os){
     _os = os;
}

void EventContext::detachAnalogRead(int pin){

     int loopCount = _analogs.count();

     for(int i = 0; i < loopCount; i++){
          AnalogRead *it = _analogs.pop();

          if(it->getKey() == pin){
               delete it;
          }else{
               _analogs.push(it);
          }
     }
}

void EventContext::onAnalogRead(void* self_, int key , int analogVal){
     EventContext *self = (EventContext*)self_;
     self->_analogCallback(key , analogVal);
}

int EventContext::getAnalogActualPin(int idx){
     switch(idx){
          case 0    :    return A0;
          case 1    :    return A1;
          case 2    :    return A2;
          case 3    :    return A3;
          case 4    :    return A4;
          case 5    :    return A5;
     }
}

#endif
