#ifndef CONTEXT_H
#define CONTEXT_H

#include <Arduino.h>
#include <TaskScheduler.h>
#include <SimpleSerial.h>

class Context;
template<typename T>    Context* createItemInstance() { return new T; }

class Context{

  public:

       typedef void(Context::*loopFunc)(void);
       typedef void(Context::*startFunc)(void);
       typedef void(Context::*endFunc)(void);
       typedef void(Context::*commandReceived)(uint8_t);
       typedef void(Context::*destructor)(void);

    Context(loopFunc loopFuncPtr, startFunc startFuncPtr , endFunc endFuncPtr , commandReceived commandReceivedFuncPtr);
    Context(loopFunc loopFuncPtr, startFunc startFuncPtr , endFunc endFuncPtr , commandReceived commandReceivedFuncPtr , destructor destructorFuncPtr);

    Context(){
         _loopFuncPtr = NULL;
         _startFuncPtr = NULL;
         _endFuncPtr = NULL;
         _commandReceivedFuncPtr = NULL;
         _derivedPtr = NULL;
         _destructorFuncPtr = NULL;
         _serial = NULL;
         _os = NULL;
    }

    ~Context();

    void            setSerial(SimpleSerial *serial);
    void            setOs(Scheduler *os);
    void            start();
    void            loop();
    void            end();
    void            onCommandReceived( uint8_t code );
    void            setId(int id);
    void            set_derivedPtr(Context *ctx);
    bool            running();
    bool            ended();
    int             id();

  protected:

    int             _id;

    Scheduler*      os();
    SimpleSerial*   serial();
    void    _loop();
    void    _start();
    void    _end();
    void    _commandReceived( uint8_t code );

    private:

      Scheduler          *_os;
      SimpleSerial       *_serial;
      Context            *_derivedPtr;
      loopFunc           _loopFuncPtr;
      startFunc          _startFuncPtr;
      endFunc            _endFuncPtr;
      commandReceived    _commandReceivedFuncPtr;
      destructor         _destructorFuncPtr;

      bool               _running;
      bool               _ended;

};

Context::~Context(){
     free(_derivedPtr);
}

Context::Context(loopFunc loopFuncPtr, startFunc startFuncPtr , endFunc endFuncPtr , commandReceived commandReceivedFuncPtr){
     _loopFuncPtr             =    loopFuncPtr;
     _startFuncPtr            =    startFuncPtr;
     _endFuncPtr              =    endFuncPtr;
     _commandReceivedFuncPtr  =    commandReceivedFuncPtr;
     _destructorFuncPtr       =    NULL;
     _running                 =    false;
     _ended                   =    false;
}

Context::Context(loopFunc loopFuncPtr, startFunc startFuncPtr , endFunc endFuncPtr , commandReceived commandReceivedFuncPtr , destructor destructorFuncPtr){
     _loopFuncPtr             =    loopFuncPtr;
     _startFuncPtr            =    startFuncPtr;
     _endFuncPtr              =    endFuncPtr;
     _commandReceivedFuncPtr  =    commandReceivedFuncPtr;
     _destructorFuncPtr       =    destructorFuncPtr;
     _running                 =    false;
     _ended                   =    false;
}

void Context::start(){
    _start();
    _running   =    true;
    _ended     =    false;
}

void Context::loop(){
    _loop();
}

void Context::end(){
    _end();
    _ended     =    true;
    _running   =    false;
}

void Context::_loop(){
     //Eğer context başlatılmadıysa geri dönülüyor
     if(!_running)
          return;

     if(_loopFuncPtr)
          (_derivedPtr->*_loopFuncPtr)();
}

void Context::_end(){
     if(_endFuncPtr)
          (_derivedPtr->*_endFuncPtr)();
}

void Context::_start(){
     if(_startFuncPtr)
          (_derivedPtr->*_startFuncPtr)();
}

void Context::_commandReceived(uint8_t code){
     //Eğer context başlatılmadıysa gelen komutları dinlemesi önleniyor
     if(!_running)
          return;

     if(_commandReceivedFuncPtr)
          (_derivedPtr->*_commandReceivedFuncPtr)(code);
}

void  Context::onCommandReceived(uint8_t code){
  _commandReceived(code);
}

Scheduler*    Context::os(){
  return _os;
}

SimpleSerial* Context::serial(){
  return _serial;
}

void Context::setSerial(SimpleSerial *serial){
  _serial = serial;
}

void Context::setOs(Scheduler *os){
  _os = os;
}

void Context::setId(int id){
     _id = id;
}

int Context::id(){
     return _id;
}

void Context::set_derivedPtr(Context *ctx){
     _derivedPtr = ctx;
}

bool Context::running(){
     return _running;
}

bool Context::ended(){
     return _ended;
}

#endif
