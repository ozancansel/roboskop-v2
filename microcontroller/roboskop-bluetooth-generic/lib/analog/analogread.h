#ifndef ANALOG_READ_H
#define ANALOG_READ_H

#include <TaskScheduler.h>

class AnalogRead{


public:

  AnalogRead();
  ~AnalogRead();

  typedef void(AnalogRead::*callbackFunc)(void*,int,int);

 enum AnalogReadStatus { NONE = 0 ,  READ_ACTIVE = 1, READ_PAUSED = 2 };

  static void onTick(void *sender , void** args);
  void    readPeriodically(int key , int pin , unsigned long period , Scheduler *os , void *listener);
  void    pause();
  void    resume();
  void    changeInterval(unsigned long period);
  int     getStatus();
  int     getPin();
  int     getKey();
  void    trigger(void *sender , int key , int val);
  void    setFuncPtr(callbackFunc f);

private:

  unsigned long   _currentInterval;
  int             _pin;
  int             _key;
  int             _status;
  void            *_listener;
  callbackFunc                func;

  Task            *_periodicTask;
  Scheduler       *_os;
};

void AnalogRead::setFuncPtr(callbackFunc f){
     func = f;
}

void    AnalogRead::trigger(void *sender , int key , int val){
     (this->*func)(sender,key,val);
}

AnalogRead::AnalogRead(){
  _status           = NONE;
  _periodicTask     = NULL;
}

AnalogRead::~AnalogRead(){

  if(_os != NULL && _periodicTask != NULL){

    _os->deleteTask(*_periodicTask);

    delete _periodicTask;
  }
}

int AnalogRead::getStatus(){
  return _status;
}

int AnalogRead::getPin(){
     return _pin;
}

int AnalogRead::getKey(){
     return _key;
}

void AnalogRead::readPeriodically(int key , int pin, unsigned long period , Scheduler *os,  void *listener){

  _pin = pin;
  _key = key;
  _os = os;
  _listener = listener;


  //Eğer daha önceden okuma işlemi başlamadıysa
    if(_periodicTask == NULL){
        _periodicTask = new Task(period , TASK_FOREVER , AnalogRead::onTick , os , true , NULL , NULL , (void*)this , NULL);
    }
    else{
      changeInterval(period);
    }

    _status = READ_ACTIVE;
}

void AnalogRead::pause(){

  //Eğer ki task NULL'sa geri dönülüyor
  if(_periodicTask == NULL)
    return;

    _periodicTask->disable();
    _status = READ_PAUSED;

}

void AnalogRead::resume(){
  //Eğer ki task NULL'sa geri dönülüyor
  if(_periodicTask == NULL)
    return;

  _periodicTask->enableIfNot();
  _status = READ_ACTIVE;
}

void AnalogRead::changeInterval(unsigned long period){
    _periodicTask->set(period , TASK_FOREVER , AnalogRead::onTick , NULL , NULL );
}

void AnalogRead::onTick(void *sender , void** args){
  AnalogRead *obj = (AnalogRead*)sender;

  int analogVal = analogRead(obj->_pin);

  // (obj)->*(func))(obj->_listener , obj->_key , analogVal);

  obj->trigger(obj->_listener , obj->_key, analogVal);
}

#endif
