arduino-simple-serial
===========
  It is a simple Arduino serial library for listening and processing commands which comes from another computer, microcontroller etc.
  It is handy for those who are looking for fast and simple communication library.
  


Basic Explanation
================
SimpleSerial library listens Serial classes and process the commands by incoming bytes. It doesnt use binary communication. It communicates over string. The library defines CommandStart, CommandEnd and ArgumentSeperator characters whose default values are '{' , '}' and ',' in ordered. </br>
Default pattern is : {COMMAND_CODEARG1,ARG2,ARG3,ARG4,....} </br></br>
Some command parsing examples are shown at below :  </br>
<b>INPUT  -></b> {1LEFT,255,14}  </br>
Code : 49 (which is ASCII code equivalent of '1') </br>
ARG1 : "LEFT" </br>
ARG2 : 255  </br>
ARG3 : 14   </br>
<b>INPUT -></b> {2}  </br>
Code : 50 </br>
<b>INPUT -></b> {214,253,1444,1555,1666,1777} </br>
CODE : 50 (ASCII code equivalent of '2' which is second byte of command text)</br>
ARG1 : 14 </br>
ARG2 : 253  </br>
ARG3 : 1444 </br>
ARG4 : 1555 </br>
ARG6 : 1666 </br>
ARG7 : 1777 </br>

Benchmark
==================
Processing a command which has five arguments such as {1234,1555,155,243,14} takes 104us approximately. It means a microcontroller can process 9615 command in a second if we ignore serial transmission latency.
Processing another command which has no arguments such as {Z} takes 8us approximately and it means 125000 command in a second.

Of course we dont expect that it is as fast as binary communication but it is sufficient to create a prototype swiftly and easy to implement in client side.

 Usage
======
   
        
        /*HardwareSerial example*/
        
        #include <SimpleSerial.h>

        void commandCallback(uint8_t);

        SimpleSerial serial(&Serial,commandCallback);

        void setup() {
            // put your setup code here, to run once:
            Serial.begin(115200);
        }

        void loop() {
            // put your main code here, to run repeatedly:

            serial.check();
  
        }

        void commandCallback(uint8_t code){
            Serial.print("Command received : ");
            Serial.println(code);
        }
        
        /*
            This example simply listens Serial and as soon as command receives it fires 
            the specified callBack method with the command code. check() function should be 
            called in a loop to make it work.
        */
  

=========
        /* SoftwareSerial example */
        
        #include <SoftwareSerial.h>
        #include <SimpleSerial.h>

        void commandCallback(uint8_t);
        SoftwareSerial sSerial(10,11);
        SimpleSerial serial(&sSerial,commandCallback);

        void setup() {
            // put your setup code here, to run once:
            Serial.begin(115200);
            mySerial.begin(115200);
        }

        void loop() {
            // put your main code here, to run repeatedly:

            serial.check();
  
        }

        void commandCallback(uint8_t code){
            Serial.print("Command received : ");
            Serial.println(code);
        }
        
        /*
          This example listens sSerial and writes incoming command to built-in Serial
        */
        
==============
        /*Hardware Serial With Arguments Example*/
        
        #include <SimpleSerial.h>

        void commandCallback(uint8_t);

        SimpleSerial serial(&Serial,commandCallback);

        void setup() {
            // put your setup code here, to run once:
            Serial.begin(115200);
        }

        void loop() {
            // put your main code here, to run repeatedly:

            serial.check();
  
        }

        void commandCallback(uint8_t code){
            Serial.print("Command received : ");
            Serial.println(code);
            
            int arg1 = serial.readIntArg();
            int arg2 = serial.readIntArg();
            String arg3 = serial.readStrArg();
            
            Serial.print("Arg1 : ");
            Serial.print(arg1);
            Serial.print(" Arg2 : ");
            Serial.print(arg2);
            Serial.print(" Arg3 : ");
            Serial.println(arg3);
        }
        
        /*
            This example listen Serial and reads the arguments from Serial input.
            
            Input -> {1233,144,HELLO}
            Output From Serial Monitor -> Command received : 49 Arg1 : 233 Arg2 : 144 Arg3 : HELLO

        */
        

==============
API Documentation : 
===============

<b>SimpleSerial(HardwareSerial *serial, void(*callback)(uint8_t), char argSeperator)</b></br>
Constructor for built-in serial. </br>
1-serial - Serial reference which will be listened </br>
2-callback - Callback method which will be fired when command is received </br>
3-argSeperator - it is ',' as default but it could be changed.</br></br>
<b>SimpleSerial(SoftwareSerial *serial, void(*callback)(uint8_t), char argSeperator);</b></br>
1-serial - SoftwareSerial reference which will be listened </br>
2-callback - Callback method which will be fired when command is received </br>
3-argSeperator - it is ',' as default but it could be changed. </br></br>
<b>void setCommandStart(char ch)</b></br>
Changes the command start character with specified character. It is '{' as default </br></br>
<b>void setCommandEnd(char ch)</b></br>
Changes the command end character with specified character. It is '}' as default </br></br>
<b>void setTimeout(uint16_t timeout)</b></br>
Sets the timeout. It is 5ms as default.</br></br>
<b>int readIntArg()</b></br>
Reads the integer arg from received command and returns as an integer</br></br>
<b>String readStrArg()</b></br>
Reads the string arg from received command and returns as a string</br></br>
<b>void check()</b></br>
Checks whether command is received. It is better to call in loop().




  
    
