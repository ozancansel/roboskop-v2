/*Hardware Serial With Arguments Example*/

#include <SimpleSerial.h>

void commandCallback(uint8_t);

SimpleSerial serial(&Serial, commandCallback);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:

  serial.check();

}

void commandCallback(uint8_t code) {
  Serial.print("Command received : ");
  Serial.print(code);

  int arg1 = serial.readIntArg();
  int arg2 = serial.readIntArg();
  String arg3 = serial.readStrArg();

  Serial.print(" Arg1 : ");
  Serial.print(arg1);
  Serial.print(" Arg2 : ");
  Serial.print(arg2);
  Serial.print(" Arg3 : ");
  Serial.println(arg3);
}
