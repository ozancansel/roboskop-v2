#include <SimpleSerial.h>

void commandCallback(uint8_t);

SimpleSerial serial(&Serial,commandCallback);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:

  serial.check();
  
}

void commandCallback(uint8_t code){
  Serial.print("Command received : ");
  Serial.println(code);
}

