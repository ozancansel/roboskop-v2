/*Hardware Serial With Arguments Example*/

#include <SimpleSerial.h>

void commandCallback(uint8_t);

SimpleSerial serial(&Serial, commandCallback);


unsigned long start;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:

  start = micros();
  serial.check();

}

void commandCallback(uint8_t code) {

  int arg1 = serial.readIntArg();
  int arg2 = serial.readIntArg();
  int arg3 = serial.readIntArg();
  int arg4 = serial.readIntArg();
  int arg5 = serial.readIntArg();
  
  unsigned long dur = micros() - start;

  Serial.print("Processing time : ");
  Serial.print(dur);
  Serial.println("us");
}

/*Send a command from Serial Monitor then it will tell you how long the processing is taken
* For example send {1234,1555,155,243,14} to test 
*/
