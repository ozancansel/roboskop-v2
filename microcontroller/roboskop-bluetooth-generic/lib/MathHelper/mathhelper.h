#ifndef MATH_HELPER_H
#define MATH_HELPER_H

class MathHelper{

public:

     static int sign(int val);

};

int MathHelper::sign(int val){
     if(val < 0)
          return -1;
     else if(val > 0)
          return 1;
     else
          return 0;
}

#endif
