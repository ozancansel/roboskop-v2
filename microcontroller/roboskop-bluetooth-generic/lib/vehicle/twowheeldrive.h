#ifndef TWO_WHEEL_DRIVE_H
#define TWO_WHEEL_DRIVE_H

#include <motordriver.h>
#define DEFAULT_MIN_THROTTLE       40

class TwoWheelDrive{

public:

     TwoWheelDrive(int lin1, int lin2, int rin1, int rin2);
     ~TwoWheelDrive();
     void calculateThrottle(int th , int steering , int *left , int *rigt);
     void directThrottle(int th , int steering);
     void thWithAcceleration(int th , int steering , int step);
     void throttle(int th);
     void setLeftTh(int th);
     void setRightTh(int th);
     void brake();
     void setOs(Scheduler *os);
     void setMinPwm(int val);
     int  getSteering();
     int  getThrottle();

private:

     int _throttle;
     int _steering;
     int _minPwm;

     Scheduler      *_os;
     MotorDriver    *_left;
     MotorDriver    *_right;

};

TwoWheelDrive::TwoWheelDrive(int lin1 , int lin2 , int rin1 , int rin2){
     _left = new MotorDriver();
     _right = new MotorDriver();
     _left->setIn1(lin1);
     _left->setIn2(lin2);
     _right->setIn1(rin1);
     _right->setIn2(rin2);
     _left->setMinThrottle(DEFAULT_MIN_THROTTLE);
     _right->setMinThrottle(DEFAULT_MIN_THROTTLE);

     _os = NULL;

     _throttle = 0;
     _steering = 0;
}

TwoWheelDrive::~TwoWheelDrive(){
     delete _left;
     delete _right;
}

int TwoWheelDrive::getSteering(){
     return _steering;
}

int TwoWheelDrive::getThrottle(){
     return _throttle;
}

void TwoWheelDrive::setOs(Scheduler *os){
     _os = os;
}

void TwoWheelDrive::thWithAcceleration(int th, int steering, int step){
     int l = 0, r = 0;

     calculateThrottle(th, steering, &l, &r);

     _left->throttleWithAcceleration(l , step , _os);
     _right->throttleWithAcceleration(r, step, _os);

     _throttle = th;
     _steering = steering;
}

void TwoWheelDrive::directThrottle(int th, int steering){
     int l , r;

     calculateThrottle(th, steering, &l, &r);

     _left->directThrottle(l);
     _right->directThrottle(r);

     _throttle = th;
     _steering = steering;
}

void TwoWheelDrive::calculateThrottle(int th , int steering , int *left , int *right){

     if(th > 255)
          th = 255;
     else if(th < -255)
          th = -255;

     if(steering > 255)
          steering = 255;
     else if(steering < -255)
          steering = -255;

     bool forward = th > 0;

     float turningFactor = th / 255.0;

     float leftThrottle = abs(th) + (steering * turningFactor);
     float rightThrottle =  abs(th) - (steering * turningFactor);


     if(leftThrottle > 255){
          rightThrottle -= (leftThrottle - 255);
          leftThrottle = 255;
     }

     if(rightThrottle > 255){
          leftThrottle -= (rightThrottle - 255);
          rightThrottle = 255;
     }

     if(!forward){
          leftThrottle *= -1;
          rightThrottle *= -1;
     }

     *left = (int)leftThrottle;
     *right = (int)rightThrottle;
}

void TwoWheelDrive::setLeftTh(int th){
     _left->directThrottle(th);
}

void TwoWheelDrive::setRightTh(int th){
     _right->directThrottle(th);
}

void TwoWheelDrive::throttle(int th){
     directThrottle(th , _steering);
}

void TwoWheelDrive::brake(){
     //Not implemented
}

void TwoWheelDrive::setMinPwm(int val){
     _left->setMinThrottle(val);
     _right->setMinThrottle(val);
}

#endif
