#ifndef MOTOR_DRIVER_H
#define MOTOR_DRIVER_H

#include <Arduino.h>
#include <TaskScheduler.h>
#include <mathhelper.h>

#define _DEFAULT_ACCELERATION_INTERVAL  20

class MotorDriver {

public:

     static void onAccelerationTick(void *sender , void **args);
     enum MotorStatus { FREE = 0, BRAKE = 1, ACCELERATING = 2, FIXED_THROTTLE = 3};
     MotorDriver(int in1, int in2);       //Initializes DcMotor
     MotorDriver();                                //Empty constructor for assign required pins dynamically
     ~MotorDriver();
     void setIn1(int in2);                         //Sets input pin 1
     void setIn2(int in1);                         //Sets input pin 2
     void setAccelerationInterval(int interval);
     void brake();                                 //Arranges pin to make motor none direction and sets throttle level as zero
     void directThrottle(int throttle);                 //Throttles dc motor according the value. It takes a value which is between [-100 , 100]
     void throttleWithAcceleration(int target , int agressivity, Scheduler *os);
     void setMinThrottle(int val);
     void throttleStep();
     int  motorState();
     int  mappedThrottle(int th);

private:

     int _in1;
     int _in2;
     int _currentThrottle;
     int _stepSize;
     int _accelerationInterval;
     int _motorState;
     int _targetThrottle;
     int _minThrottle;
     bool _accelerationRunning;
     bool _braking;

     void throttle(int throttle);                 //Throttles dc motor according the value. It takes a value which is between [-100 , 100]
     void accelerationPause();
     void accelerationResume();
     void setAccTaskInterval(int interval);
     void determineState();

     Task *_accelerationTask;
     Scheduler *_os;


};

MotorDriver::~MotorDriver(){
     if(_accelerationTask != NULL && _os != NULL){
          _os->deleteTask(*_accelerationTask);

          delete _accelerationTask;
     }
}

MotorDriver::MotorDriver(int in1 , int in2){
     _in1 = in1;
     _in2 = in2;
     _accelerationRunning = false;
     _braking =     false;
     _accelerationTask = NULL;
     _motorState = FREE;
     _currentThrottle = 0;
     _minThrottle = 80;
     _accelerationInterval = _DEFAULT_ACCELERATION_INTERVAL;

     pinMode(_in1 , OUTPUT);
     pinMode(_in2 , OUTPUT);
}

MotorDriver::MotorDriver(){
     _in1 = -1;
     _in2 = -1;
     _minThrottle = 80;
     _braking = false;
     _accelerationRunning = false;
     _accelerationTask = NULL;
     _motorState = FREE;
     _currentThrottle = 0;
     _accelerationInterval = _DEFAULT_ACCELERATION_INTERVAL;
}

void MotorDriver::setIn1(int i1){
     _in1 = i1;
     pinMode(_in1 , OUTPUT);
}

void MotorDriver::setIn2(int i2){
     _in2 = i2;
     pinMode(_in2 , OUTPUT);
}

void MotorDriver::directThrottle(int th){
     throttle(th);
}

void MotorDriver::throttle(int throttle){

     int mapTh = mappedThrottle(throttle);

     if(mapTh >= 0){
          analogWrite(_in1 , abs(mapTh));
          digitalWrite(_in2 , LOW);
     } else {
          digitalWrite(_in1 , LOW);
          analogWrite(_in2 , abs(mapTh));
     }

     _currentThrottle = throttle;
}

int MotorDriver::mappedThrottle(int throttle){

     if(throttle > 255)
          throttle = 255;
     else if(throttle < -255)
          throttle = -255;

     return throttle;
}

int MotorDriver::motorState(){
     return _motorState;
}

void MotorDriver::brake(){
     digitalWrite(_in1 , HIGH);
     digitalWrite(_in2 , HIGH);

     //Flag değiştiriliyor
     _braking            =    true;
     _currentThrottle    =    0;

     //Eğer ki hızlanma devam ediyorsa yarıda kesilip fren yapılıyor
     if(_motorState == ACCELERATING){
          _accelerationRunning = false;
          accelerationPause();
     }

     determineState();
}

void MotorDriver::throttleWithAcceleration(int target , int stepSize, Scheduler *os){

     _os = os;
     _stepSize = stepSize;
     _targetThrottle = target;

     if(_accelerationTask == NULL){
          _accelerationTask = new Task(_accelerationInterval , TASK_FOREVER , MotorDriver::onAccelerationTick , _os , true , NULL , NULL , (void*)this , NULL);
     } else {

          accelerationResume();
     }

     //Flag değiştiriliyor
     _accelerationRunning = true;

     if(_motorState == BRAKE)
          _braking = false;

     determineState();
}

void MotorDriver::accelerationPause(){
     if(_accelerationTask != NULL)
          _accelerationTask->disable();
}

void MotorDriver::accelerationResume(){
     if(_accelerationTask != NULL){
          _accelerationTask->enableIfNot();
     }
}

void MotorDriver::setAccelerationInterval(int interval){
     _accelerationInterval = interval;

     setAccTaskInterval(_accelerationInterval);
}

void MotorDriver::setAccTaskInterval(int interval){
     if(_accelerationTask != NULL){
          _accelerationTask->set(interval , TASK_FOREVER , MotorDriver::onAccelerationTick , NULL , NULL);
     }
}

void MotorDriver::determineState(){
     if(_accelerationRunning){
          _motorState = ACCELERATING;
     }
     else if(_currentThrottle == 0){
          if(_braking)
               _motorState = BRAKE;
          else
               _motorState = FREE;
     } else if(_currentThrottle != 0){
               _motorState = FIXED_THROTTLE;
     }
}

void MotorDriver::setMinThrottle(int val){
     _minThrottle = val;
}

void MotorDriver::throttleStep(){
     int stepSign = MathHelper::sign(_targetThrottle - _currentThrottle);
     int difference = _targetThrottle - _currentThrottle;

     //Eğer ki fark step büyüklüğünden azsa direk target'a eşitleniyor
     if(difference == 0){
          _accelerationTask->disable();
          _accelerationRunning = false;
          determineState();

     } else if(abs(difference) <= _stepSize){
          throttle(_targetThrottle);
     } else {
          throttle(_currentThrottle + (_stepSize) * stepSign);
     }
}

void MotorDriver::onAccelerationTick(void *sender , void **args){
     MotorDriver *obj = (MotorDriver*)sender;

     obj->throttleStep();
}

#endif
