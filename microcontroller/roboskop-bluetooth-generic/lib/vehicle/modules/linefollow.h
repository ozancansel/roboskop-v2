#ifndef LINE_FOLLOW_H
#define LINE_FOLLOW_H

#include <context.h>
#include <twowheeldrive.h>
#include <mathhelper.h>
#include <fivetcrt.h>

#define START_FOLLOWING_COMMAND_CH      0    //{0}
#define START_CALIBRATING_COMMAND_CH    1    //{1}
#define DISABLE_MODULE_COMMAND_CH       2    //{2}
#define SENSOR_READ_COMMAND_CH          3    //{3}
#define SET_MAX_SPEED                   4    //{4}
#define SET_MAX_TURNING                 5    //{5}

class LineFollow : public Context  {

public:

     LineFollow();
     enum States { NONE = 0 , CALIBRATING = 1 , FOLLOWING = 2 , SENSOR_READ = 3};

     void      setDrive(TwoWheelDrive *driver);
     void      calibrate();
     void      startToFollow();
     void      setNone();


protected:

     void      _commandReceived(uint8_t code);
     void      _start();
     void      _end();
     void      _loop();


private:

     int _leftPin;
     int _rightPin;
     int _midPin;
     int _state;
     int _step = 5;
     int _lastDir = 1;
     int _lastPos = 0;
     int _beforePwm = 0;
     int _lastSteering;
     int _avgSpeed = 180;
     int _maxTurning = 255;

     bool      _toplama;

     FiveTcrt       *_sensor;
     TwoWheelDrive  *_driver;
};

LineFollow::LineFollow() : Context(
     static_cast<Context::loopFunc>(&LineFollow::_loop),
     static_cast<Context::startFunc>(&LineFollow::_start),
     static_cast<Context::endFunc>(&LineFollow::_end),
     static_cast<Context::commandReceived>(&LineFollow::_commandReceived)){
     set_derivedPtr(this);
     _leftPin = -1;
     _rightPin = -1;
     _midPin = -1;
     _state = NONE;
     _driver = NULL;
     _toplama = false;
}

void LineFollow::startToFollow(){
     _state = FOLLOWING;
     Serial.print(SimpleSerial::COMMAND_START_CH);
     Serial.write(CONSOLE_OUTPUT_COMMAND);
     Serial.print(F("Çizgi izleme başladı"));
     Serial.print(SimpleSerial::COMMAND_END_CH);
}

void LineFollow::calibrate(){
     _state = CALIBRATING;
}

void LineFollow::setNone(){
     if(_state == FOLLOWING){
          Serial.print(SimpleSerial::COMMAND_START_CH);
          Serial.write(CONSOLE_OUTPUT_COMMAND);
          Serial.print(F("Çizgi izleme durduruldu"));
          Serial.print(SimpleSerial::COMMAND_END_CH);
     }
     _state = NONE;
     _driver->directThrottle(0 , 0);
}

void LineFollow::setDrive(TwoWheelDrive *driver){
     _driver = driver;
}

void LineFollow::_start(){
     _sensor = new FiveTcrt();
     _lastPos = 0;
     _lastSteering = 0;
}

void LineFollow::_loop(){
     if(_state == NONE){

     } else if(_state == SENSOR_READ){

     }
     else if(_state == CALIBRATING) {
          Serial.print(SimpleSerial::COMMAND_START_CH);
          Serial.write(CONSOLE_OUTPUT_COMMAND);
          Serial.print(F("Kalibre ediliyor"));
          Serial.print(SimpleSerial::COMMAND_END_CH);
          _sensor->calibrate(1000);
          setNone();
          Serial.print(SimpleSerial::COMMAND_START_CH);
          Serial.write(CONSOLE_OUTPUT_COMMAND);
          Serial.print(F("Hazır!"));
          Serial.print(SimpleSerial::COMMAND_END_CH);
     } else if(_state == FOLLOWING) {
          _sensor->check();
          int pos = _sensor->pos();

          pos = map(pos , -100 , 100 , -2 , 2);

          int leftThrottle = 0;
          int rightThrottle = 0;

          _lastSteering += abs(pos) == 1 ? abs(pos) * 20 : abs(pos) * 50;

          if(_lastSteering > _maxTurning)
               _lastSteering = _maxTurning;

          if(_lastSteering < -_maxTurning)
               _lastSteering = -_maxTurning;

          if(pos > 0){
               leftThrottle = _avgSpeed - _lastSteering;
               rightThrottle = _avgSpeed;
          }
          else if(pos < 0){
               leftThrottle = _avgSpeed;
               rightThrottle = _avgSpeed - _lastSteering;
          }
          else if(abs(_lastPos) > 0 && pos == 0){
               leftThrottle = _avgSpeed + _lastPos < 0 ? abs(_lastSteering) * 0.4 : 0;
               rightThrottle = _avgSpeed + _lastPos > 0 ? abs(_lastSteering) * 0.4 : 0;
               delay(6);
          } else {
               leftThrottle = _avgSpeed;
               rightThrottle = _avgSpeed;
          }

          if(leftThrottle > 255)
               leftThrottle = 255;
          if(leftThrottle < -255)
               leftThrottle = -255;

          if(rightThrottle > 255)
               rightThrottle = 255;
          if(rightThrottle < -255)
               rightThrottle = -255;


          _driver->setLeftTh(leftThrottle);
          _driver->setRightTh(rightThrottle);

          _lastPos = pos;
     }
}

void LineFollow::_end(){
     delete _sensor;
}

void LineFollow::_commandReceived(uint8_t code){

     Serial.println(code);

     switch(code){
          case START_FOLLOWING_COMMAND_CH    :    {
               _state = FOLLOWING;
               break;
          }
          case START_CALIBRATING_COMMAND_CH       :    {
               Serial.println("calibrating");
               _state = CALIBRATING;
               break;
          }
          case DISABLE_MODULE_COMMAND_CH     :    {
               setNone();
               break;
          }
          case SENSOR_READ_COMMAND_CH   :    {
               _state = SENSOR_READ;
               break;
          }
          case SET_MAX_SPEED            :    {
               _avgSpeed      =    serial()->readIntArg();
               break;
          }
          case SET_MAX_TURNING          :    {
               _maxTurning    =    serial()->readIntArg();
               break;
          }
     }
}

#endif
