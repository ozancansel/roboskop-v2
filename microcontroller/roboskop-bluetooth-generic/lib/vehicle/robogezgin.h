#ifndef ROBOGEZGIN_H
#define ROBOGEZGIN_H

#include <Arduino.h>
#include <context.h>
#include <twowheeldrive.h>
#include <modules/linefollow.h>
#include <linkedlist.h>
#include <rgb.h>
#include <mpu6050util.h>
#include <fivetcrt.h>
#include <acs712.h>

#define LEFT_MOTOR_IN1        5
#define LEFT_MOTOR_IN2        3
#define RIGHT_MOTOR_IN1       9
#define RIGHT_MOTOR_IN2       6
#define RED_PIN               4
#define GREEN_PIN             7
#define BLUE_PIN              8

#define COMMAND_GO            94   //{^<throttle> , <angle>}
#define ACTIVATE_MODULE       97   //{a<moduleId>,<enable>}
#define COMMAND_TO_MODULE     109  //{m<moduleId>,args...}
#define RGB_SET               114  //{r<red>,<green>,<blue>}
#define SET_LIGHTS_ENABLED    108  //{l<enable>}
#define ACCEL_STD_COMMAND     103  //{g<xStd>,<yStd>,<zStd>}
#define MODULE_COMMAND        99   //{c<moduleKey>,<code>,args}

#define LINE_FOLLOW_MODULE_KEY     0
#define LIGHTS_MODULE_KEY          1


class RoboGezgin : public Context{

public:

     RoboGezgin() : Context(
          static_cast<Context::loopFunc>(&RoboGezgin::_loop),
          static_cast<Context::startFunc>(&RoboGezgin::_start),
          static_cast<Context::endFunc>(&RoboGezgin::_end),
          static_cast<Context::commandReceived>(&RoboGezgin::_commandReceived),
          static_cast<Context::destructor>(&RoboGezgin::destructorFunc)) {
               set_derivedPtr(this);
          };

     ~RoboGezgin();
     void      destructorFunc();

protected:

     void _loop();
     void _start();
     void _end();
     void _commandReceived(uint8_t code);
     void initializeModules();
     Context*  getModule(int id);

private:

     TwoWheelDrive                 *_driver;
     LinkedList<Context*>          *_modules;
     Rgb                           *_rgb;
     LineFollow                    *_lineFollowerModule;
     Mpu6050Util                   *_gyro;
     Acs712                        *_currentSens;


     bool                          _lightsEnabled;
     int                           _beforeDrop;
     bool                          _dropped;
     void                          determineLights();

     Context*                      enableModule(int key , bool enabled);
};

RoboGezgin::~RoboGezgin(){

}

void RoboGezgin::destructorFunc(){
     Iterator<Context *> *it = _modules->iterator();

     while(!it->isNull()){
          delete it->data();
          it->next();
     }

     delete it;
}

void RoboGezgin::_loop(){

     if(_modules->length() > 0){
     Iterator<Context *> *it = _modules->iterator();

          while(!it->isNull()){
               it->data()->loop();
               it->next();
          }

          delete it;
     }

     determineLights();
}

void RoboGezgin::_start(){
     Serial.println("Gezgin aktif");
     _modules = new LinkedList<Context*>();
     _driver = new TwoWheelDrive(LEFT_MOTOR_IN1 , LEFT_MOTOR_IN2 , RIGHT_MOTOR_IN1 , RIGHT_MOTOR_IN2);
     _driver->setOs(os());
     _rgb = new Rgb(RED_PIN , GREEN_PIN , BLUE_PIN);
     _currentSens = new Acs712(A0);
     _gyro = NULL;
     _lineFollowerModule = NULL;
     _beforeDrop = 0;
     _dropped = false;

     _rgb->rgb(0 , 0 , 0);

     //Default parametreler ayarlanıyor
     _lightsEnabled = true;
     _driver->directThrottle(0 , 0);
}

void RoboGezgin::_end(){
     delete _modules;
     delete _rgb;
     delete _driver;

     if(_lineFollowerModule != NULL)
          delete _lineFollowerModule;

     if(_gyro != NULL)
          delete _gyro;
}

void RoboGezgin::_commandReceived(uint8_t code){

     switch(code){
          case COMMAND_GO     :    {
               int throttle = serial()->readIntArg();
               int steering = serial()->readIntArg();

               _driver->directThrottle(throttle, steering);

               break;
          }
          case ACTIVATE_MODULE     :    {
               //{a<moduleId>,<enable>}

               //Modulün keyi alınıyor
               int  moduleKey = serial()->readIntArg();
               bool enabled   = serial()->readIntArg();

               //Modül key'e göre üretiliyor
               enableModule(moduleKey , enabled);

               break;
          }
          case RGB_SET   :    {

//{l<red>,<green>,<blue>}
               int r = serial()->readIntArg();
               int g = serial()->readIntArg();
               int b = serial()->readIntArg();

               _rgb->rgb(r, g, b);

               break;
          }
          case SET_LIGHTS_ENABLED  :    {

               int enabled = serial()->readIntArg();

               _lightsEnabled = enabled;

               break;
          }
          case MODULE_COMMAND      :    {
               int key = serial()->readIntArg();

               Context* ctx = getModule(key);
               if(ctx != NULL){
                    int code = serial()->readIntArg();
                    ctx->onCommandReceived(code);
               }

               break;
          }
          default   :    {
               Iterator<Context *> *it = _modules->iterator();

               while(!it->isNull()){
                    it->data()->onCommandReceived(code);
                    it->next();
               }

               delete it;
          }
     }
}

void RoboGezgin::initializeModules(){

}

Context*  RoboGezgin::enableModule(int key , bool enabled){

     Context   *ctx = NULL;

     switch(key){
          case LINE_FOLLOW_MODULE_KEY   :    {

               if(enabled && _lineFollowerModule == NULL){

                    _lineFollowerModule = new LineFollow();

                    _lineFollowerModule->setOs(os());
                    _lineFollowerModule->setSerial(serial());
                    _lineFollowerModule->start();
                    _lineFollowerModule->setId(LINE_FOLLOW_MODULE_KEY);
                    _modules->insertToTail(_lineFollowerModule);

                    return _lineFollowerModule;
               } else if(!enabled && _lineFollowerModule != NULL){
                    _lineFollowerModule->end();

                    _modules->remove(_lineFollowerModule);
                    delete _lineFollowerModule;
               }

               return NULL;
          }

          case LIGHTS_MODULE_KEY   :    {
               if(enabled && _rgb == NULL){

               }
          }
     }

     return NULL;
}

void RoboGezgin::determineLights(){

     if(_lightsEnabled){
          if(_driver->getThrottle() < 120 && _driver->getThrottle() > 20){
               _rgb->rgb(0 , 255 , 0);
          } else if(_driver->getThrottle() < 180 && _driver->getThrottle() > 120){
               _rgb->rgb(0 , 0 , 255);
          }
          else if(_driver->getThrottle() > 180)
               _rgb->rgb(255 , 0 , 0);
          else
               _rgb->rgb(0 , 0 , 0);
     }
     else{
          _rgb->rgb(0 , 0 , 0);
     }
}

Context *RoboGezgin::getModule(int id){
     Iterator<Context *> *it = _modules->iterator();

     Context   *ctx = NULL;

     while(!it->isNull()){
          if(it->data()->id() == id){
               ctx = it->data();
          }
          it->next();
     }

     delete it;

     return ctx;
}

#endif
