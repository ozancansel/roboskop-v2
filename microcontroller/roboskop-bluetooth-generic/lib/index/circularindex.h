#ifndef CIRCULAR_INDEX_H
#define CIRCULAR_INDEX_H

class CircularIndex{

public:

    CircularIndex(long from , long to);
    CircularIndex(long max);

    void    increment();
    void    increment(long num);
    void    setIdx(long idx);
    void    setFrom(long from);
    void    setTo(long to);
    long    idx();

private:

    long    _idx;
    long    _from;
    long    _to;
};

CircularIndex::CircularIndex(long from , long to){
  _from = from;
  _idx = from;
  _to = to;
}

CircularIndex::CircularIndex(long max){
  _from = 0;
  _idx = 0;
  _to = max;
}

void CircularIndex::setIdx(long idx){
  _idx = idx;
}

void CircularIndex::setFrom(long from){
    _from = from;
}

void CircularIndex::setTo(long to){
  _to = to;
}

long CircularIndex::idx(){
  return _idx;
}

void CircularIndex::increment(){
  increment(1);
}

void CircularIndex::increment(long num){
  _idx += num;

  if(_idx > _to){
    _idx = _from;
  }

}

#endif
