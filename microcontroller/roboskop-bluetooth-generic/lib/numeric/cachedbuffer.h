#ifndef CACHED_BUFFER_H
#define CACHED_BUFFER_H

#include <linkedlist.h>

template<class T>
class CachedBuffer{

public:

     CachedBuffer(int maxSize);
     ~CachedBuffer();
     void      append(T val);
     bool      reachedMaxSize();
     void      print();
     long      variance();
     long      standardDeviation();

private:

     LinkedList<T>  *_l;
     int       _maxSize;

};

template<class T>
CachedBuffer<T>::CachedBuffer(int maxSize){
     _l = new LinkedList<T>();
     _maxSize = maxSize;
}

template<class T>
CachedBuffer<T>::~CachedBuffer(){
     delete _l;
}

template<class T>
void CachedBuffer<T>::append(T val){

     //Eğer maksimum büyüklük aşılmışsa en eski veri siliniyor
     while(_maxSize && _maxSize <= _l->length()){
          _l->pop();
     }

     //Değer ekleniyor
     _l->insertToTail(val);

}

template<class T>
bool      CachedBuffer<T>::reachedMaxSize(){
     return _maxSize <= _l->length();
}

template<class T>
void      CachedBuffer<T>::print(){
     _l->print();
}

template<class T>
long    CachedBuffer<T>::variance(){
     Iterator<T>    *it = _l->iterator();

     long       sum = 0;
     long       varianceSum = 0;
     long       average = 0;
     long       var = 0;

     while(!it->isNull()){
          sum += (long)it->data();

          it->next();
     }



     //Mean hesaplanıyor
     average = sum / (long)_l->length();

     //Listenin başına dönülüyor
     it->reset();

     while(!it->isNull()){

          long difference = ((long)it->data()) - average;

          varianceSum += difference * difference;

          it->next();
     }

     var = varianceSum / (long)_l->length();

     delete it;

     return var;
}

template<class T>
long CachedBuffer<T>::standardDeviation(){
     long val = variance();

     long deviation = sqrt(val);
     return deviation;
}

#endif
