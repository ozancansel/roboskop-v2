#ifndef FIVE_TCRT_H
#define FIVE_TCRT_H

#include <Arduino.h>

class FiveTcrt{

public:

     FiveTcrt();
     void      check();
     int       pos();
     void      calibrate(int time);
     void      read(int *i);
     bool      lineLost();

private:

     int  _pos;
     int  _whiteLineThres;
     int  _blackAreaThres;
     int  _sensError;

     int  _lineLostCount;
     int  _lineLostCounter;
     bool _lineLost;

     void      findMinorPos(int *arr);

};

FiveTcrt::FiveTcrt(){
     _whiteLineThres = 1025;
     _blackAreaThres = 0;
     _sensError      = 40;
     _lineLostCount  = 20;
     _lineLostCounter= 0;
}

void      FiveTcrt::check(){
     int  sensValues[5];

     sensValues[0] = analogRead(A0);
     sensValues[1] = analogRead(A1);
     sensValues[2] = analogRead(A2);
     sensValues[3] = analogRead(A3);
     sensValues[4] = analogRead(A4);

     bool lineFound = false;

     for(int i = 0; i < 5;i++){


          if(sensValues[i] < _whiteLineThres){
               _pos = map(i , 0 , 4 , -100 , 100);
               lineFound = true;
               // findMinorPos(sensValues);
          }
     }

     if(lineFound){
          _lineLostCounter = 0;
          _lineLost = false;
     }else if(_lineLost >= _lineLostCount){
          _lineLostCounter = 0;
          _lineLost = true;
     } else {
          _lineLostCounter++;
     }

}

int       FiveTcrt::pos(){
     return _pos;
}

void      FiveTcrt::calibrate(int time){
     int  values[5];
     unsigned long  t1 = millis();

     while(millis() - t1 < time){
          read(values);

          for(int i = 0; i < 5; i++){
               if(values[i] < _whiteLineThres){
                    _whiteLineThres = values[i];
               }

               if(values[i] > _blackAreaThres){
                    _blackAreaThres = values[i];
               }
          }
     }

     _whiteLineThres +=  max(_whiteLineThres * 0.2 , _sensError);
     _blackAreaThres -=  max(_blackAreaThres * 0.5 , _sensError);

     Serial.println("Calibrating finished");
     Serial.print("W : ");
     Serial.print(_whiteLineThres); Serial.print("\t"); Serial.print("\t");
     Serial.print(" B : ");
     Serial.print(_blackAreaThres);
}

void      FiveTcrt::read(int *arr){
     arr[0] = analogRead(A0);
     arr[1] = analogRead(A1);
     arr[2] = analogRead(A2);
     arr[3] = analogRead(A3);
     arr[4] = analogRead(A4);
}

void      FiveTcrt::findMinorPos(int *arr){

     for(int i = 0; i < 5; i++){
          if(arr[i] < _blackAreaThres && arr[i] > _whiteLineThres){
               //Beyaz çizginin üzerindeki sensörün indexi alınıyor
               int idx = map(_pos , -100 , 100 , 0 , 4);
               int direction = i - idx;
               if(abs(direction) == 1) {
                    float interpolationMin = (arr[i] - _whiteLineThres) / (float)(_blackAreaThres - arr[i]);
                    if(interpolationMin > 1){
                         interpolationMin =  1.0 / interpolationMin;
                         _pos += direction * interpolationMin * 50;
                    }
               }
          }
     }
}

bool      FiveTcrt::lineLost(){
     return _lineLost;
}

#endif
