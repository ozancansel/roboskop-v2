#ifndef ACS_712_H
#define ACS_712_H

class Acs712{

public:

     Acs712(int pin);
     double readMiliamp();

private:

     int _pin;

};

Acs712::Acs712(int pin){
     _pin = pin;
}

double Acs712::readMiliamp(){
     int sens = analogRead(_pin);
     int volt = map(sens , 0 , 1023 , 0 , 5000);
     int difference = volt - 2500;

     double miliAmp = difference / 0.185;

     return miliAmp;
}

#endif
