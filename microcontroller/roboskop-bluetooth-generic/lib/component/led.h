#ifndef LED_H
#define LED_H

#include <Arduino.h>
#include <TaskScheduler.h>

class Led{

public:

     static void onTick(void *sender , void** args);

     enum BlinkStatus { NONE = 0, BLINKING = 1 , PAUSED = 2 };

     Led();
     Led(int pin);
     ~Led();

     void      setPin(int pin);
     void      blink(unsigned long period , Scheduler *os);
     void      resume();
     void      pause();
     void      changeInterval(unsigned long period);
     void      toggle();
     void      on();
     void      off();
     void      pwm(int val);
     int       status();

private:

     int       _pin;
     int       _status;
     bool      _ledState;

     Task      *_blinkTask;
     Scheduler *_os;

};

Led::Led(){
     _blinkTask     = NULL;
     _status        = NONE;
     _ledState      = false;
     _os            = NULL;
}

Led::Led(int pin){
     _blinkTask     = NULL;
     _pin           = pin;
     _status        = NONE;
     _ledState      = false;
     _os            = NULL;
     pinMode(_pin , OUTPUT);
}

Led::~Led(){
     off();

     if(_os != NULL){
       _os->deleteTask(*_blinkTask);

       delete _blinkTask;
     }
}

void Led::setPin(int pin){
     _pin = pin;

     pinMode(_pin , OUTPUT);
}

void Led::blink(unsigned long period , Scheduler *os){
     _os = os;

     //Eğer ki task daha önceden başlatılmamışsa
     if(_blinkTask == NULL){
          _blinkTask = new Task(period , TASK_FOREVER , Led::onTick , os , true , NULL , NULL , (void*) this , NULL );
     }else{
          changeInterval(period);
     }

     resume();
}

void Led::resume(){
     if(_blinkTask == NULL)
     return;

     _blinkTask->enableIfNot();
     _status = BLINKING;
}

void Led::pause(){

     if(_blinkTask == NULL)
     return;

     _blinkTask->disable();
     _status = PAUSED;
}

void Led::changeInterval(unsigned long period){
     _blinkTask->set(period , TASK_FOREVER , Led::onTick , NULL , NULL);
}

void Led::toggle(){
     _ledState = !_ledState;
     digitalWrite(_pin , _ledState);
}

void Led::on(){
     _ledState = true;
     digitalWrite(_pin , HIGH);
}
void Led::off(){
     _ledState = false;
     digitalWrite(_pin , LOW);
}
void Led::pwm(int val){
     _ledState = val != 0;
     analogWrite(_pin , val);
}

int Led::status(){
     return _status;
}

void Led::onTick(void *sender,  void **args){
     Led *obj = (Led*)sender;

     obj->toggle();
}


#endif
