#ifndef ACOUSTIC_H
#define ACOUSTIC_H

#include <Arduino.h>
#include <TaskScheduler.h>

class IAcousticSensorListener{
     public:
          virtual void onDistanceMeasure(int key , int val) = 0;
};

class Acoustic{

public:

     typedef void(Acoustic::*callbackFunc)(void*,int,int);

     Acoustic(int trigger , int echo);
     ~Acoustic();
     enum AcousticReadStatus { NONE = 0 ,  READ_ACTIVE = 1, READ_PAUSED = 2 };

     static void         onTick(void *sender , void **args);

     int       distance();
     int       getStatus();
     void      readPeriodically(int key , unsigned long period , Scheduler *os , void *listener);
     void      changeInterval(unsigned long period);
     void      pause();
     void      resume();
     void      setCallbackPtr(callbackFunc f);
     void      triggerCb(void *sender, int key , int val);

private:

     int                 _triggerPin;
     int                 _echoPin;
     unsigned long       _currentInterval;
     int                 _status;
     int                 _key;

     Task      *_periodicTask;
     Scheduler *_os;
     void       *_listener;

     callbackFunc        cbFunc;

};

void Acoustic::setCallbackPtr(callbackFunc f){
     cbFunc = f;
}

void Acoustic::triggerCb(void *sender , int key , int val){
     if(cbFunc)
          (this->*cbFunc)(sender , key , val);
}

int Acoustic::getStatus(){
     return _status;
}

Acoustic::Acoustic(int trigger , int echo){
     _triggerPin = trigger;
     _echoPin = echo;
     _status = NONE;

     _periodicTask = NULL;
     _os = NULL;
     _listener = NULL;
     cbFunc = NULL;

     pinMode(_triggerPin , OUTPUT);
     pinMode(_echoPin , INPUT);

}

Acoustic::~Acoustic(){
     if(_os != NULL && _periodicTask != NULL){
       _os->deleteTask(*_periodicTask);

       delete _periodicTask;
     }
}

int Acoustic::distance(){
     digitalWrite(_triggerPin , LOW);
     delayMicroseconds(5);
     digitalWrite(_triggerPin , HIGH);
     delayMicroseconds(10);
     digitalWrite(_triggerPin , LOW);

     long duration = pulseIn(_echoPin , HIGH , 5000);

     if(duration == 0)
          return -1;

     long distance = (duration / 2) / 29.1;

     return distance;
}

void Acoustic::readPeriodically(int key , unsigned long period , Scheduler *os , void *listener){
     _key = key;
     _os = os;
     _listener = listener;

     if(_periodicTask == NULL){
          _periodicTask = new Task(period , TASK_FOREVER , Acoustic::onTick , os , true , NULL , NULL , (void*)this , NULL );
     }
     else{
          changeInterval(period);
     }

}

void Acoustic::pause(){

     //Eğer ki task atanmamışsa geri dönülüyor
     if(_periodicTask == NULL)
          return;

          _periodicTask->disable();
          _status = READ_PAUSED;

}

void Acoustic::resume(){

     //Eğer ki task atanmamışsa geri dönülüyor
     if(_periodicTask == NULL)
     return;

     _periodicTask->enableIfNot();
     _status = READ_ACTIVE;
}

void Acoustic::changeInterval(unsigned long period){
     _periodicTask->set(period , TASK_FOREVER , Acoustic::onTick , NULL , NULL );
}

void Acoustic::onTick(void *sender , void **args){
     Acoustic *obj = (Acoustic*)sender;

     int dist = obj->distance();

     obj->triggerCb(obj->_listener , obj->_key , dist);
}

#endif
