#ifndef MPU6050_UTI_H
#define MPU6050_UTI_H

#include <I2Cdev.h>
#include <MPU6050.h>
#include <cachedbuffer.h>
#include <MemoryFree.h>
#include <mathhelper.h>

#define   CALIBRATING_COUNT        1000

class Mpu6050Util{

public:

     Mpu6050Util(int cache = 7);
     ~Mpu6050Util();

     bool      initialize();
     void      calibrate();
     void      getAccelerometer(int *xStd , int *yStd , int *zStd);

private:

     MPU6050   *_sens;
     int16_t      _xOffset;
     int16_t   _yOffset;
     int16_t   _zOffset;
     int       _cacheSize;

     int16_t   _ax;
     int16_t   _ay;
     int16_t   _az;

     CachedBuffer<long>    *_xBuffer;
     CachedBuffer<long>    *_yBuffer;
     CachedBuffer<long>    *_zBuffer;

};

Mpu6050Util::Mpu6050Util(int cache){
     _sens = NULL;
     _xBuffer = new CachedBuffer<long>(cache);
     _yBuffer = new CachedBuffer<long>(cache);
     _zBuffer = new CachedBuffer<long>(cache);
     _ax = 0;
     _ay = 0;
     _az = 0;
}

Mpu6050Util::~Mpu6050Util(){
     if(_sens != NULL)
          delete _sens;
     delete _xBuffer;
     delete _yBuffer;
     delete _zBuffer;
}

void      Mpu6050Util::getAccelerometer(int *xStd , int *yStd , int *zStd){
     _sens->getAcceleration(&_ax, &_ay, &_az);

     long ax = _ax;
     long ay = _ay;
     long az = _az;

     _xBuffer->append(ax);
     _yBuffer->append(ay) ;
     _zBuffer->append(az);

     if(_xBuffer->reachedMaxSize() && _yBuffer->reachedMaxSize() && _zBuffer->reachedMaxSize()){
          *xStd = _xBuffer->standardDeviation();
          *yStd = _yBuffer->standardDeviation();
          *zStd = _zBuffer->standardDeviation();
     }
}

bool Mpu6050Util::initialize(){
     _sens = new MPU6050();

     Wire.begin();

     _sens->initialize();

     bool success = _sens->testConnection();

     return success;
}

void Mpu6050Util::calibrate(){

     int       x,y,z;
     int       calibratingStep                    =    200;
     int       maxOffsetChange                    =    200;
     int       acceptedLimit                      =    50;
     int       frsOrdercalibrationIteration       =    10;
     double    xStepSum = 0 , yStepSum = 0 , zStepSum = 0;
     double    xPerStep , yPerStep , zPerStep;
     bool      xCalibrated = false, yCalibrated = false, zCalibrated = false;

     //Offsetler sıfırlanıyor
     _sens->setXAccelOffset(0);
     _sens->setYAccelOffset(0);
     _sens->setZAccelOffset(0);

     //Kalibrasyon için gerekli parametreler hesaplanıyor
     //Bir offseting değer olarak ne kadar değiştirdiği hesaplanıyor
     //-200              |              +200
     //Önce +calibratingStep değer okunur, sonra -calibratingStep'deki değer okunur
     //Daha sonra aradaki farktan step değeri bulunur
     for(int i = 0; i < frsOrdercalibrationIteration; i++){
          int beforeX , beforeY , beforeZ;
          int afterX , afterY , afterZ;
          _sens->setXAccelOffset(calibratingStep);
          _sens->setYAccelOffset(calibratingStep);
          _sens->setZAccelOffset(calibratingStep);

          _sens->getAcceleration(&beforeX, &beforeY, &beforeZ);
          delay(5);
          _sens->setXAccelOffset(-calibratingStep);
          _sens->setYAccelOffset(-calibratingStep);
          _sens->setZAccelOffset(-calibratingStep);

          _sens->getAcceleration(&afterX, &afterY , &afterZ);
          delay(5);

          xStepSum += abs(afterX - beforeX) / ((double) calibratingStep);
          yStepSum += abs(afterY - beforeY) / ((double) calibratingStep);
          zStepSum += abs(afterZ - beforeZ) / ((double) calibratingStep);
     }

     xPerStep = xStepSum / (double)frsOrdercalibrationIteration;
     yPerStep = yStepSum / (double)frsOrdercalibrationIteration;
     zPerStep = zStepSum / (double)frsOrdercalibrationIteration;

     //Offsetler sıfırlanıyor
     _sens->setXAccelOffset(0);
     _sens->setYAccelOffset(0);
     _sens->setZAccelOffset(0);

     while(!xCalibrated || !yCalibrated || !zCalibrated){
          _sens->getAcceleration(&x, &y, &z);

          if(abs(x) < acceptedLimit){
               xCalibrated = true;
          } else if(!xCalibrated){

               int xOffset = abs(x) / xPerStep;

               if(abs(xOffset) > maxOffsetChange)
               xOffset = maxOffsetChange;

               xOffset = _sens->getXAccelOffset() + (xOffset * -MathHelper::sign(x));

               _sens->setXAccelOffset(xOffset);

          }

          delay(5);

          if(abs(y) < acceptedLimit){
               yCalibrated = true;
          } else if(!yCalibrated){

               int yOffset = abs(y) / yPerStep;

               if(abs(yOffset) > maxOffsetChange)
               yOffset = maxOffsetChange;

               yOffset = _sens->getYAccelOffset() + (yOffset * -MathHelper::sign(y));

               _sens->setYAccelOffset(yOffset);

          }

          delay(5);

          if(abs(z) < acceptedLimit){
               zCalibrated = true;
          } else if(!zCalibrated){

               int zOffset = abs(z) / zPerStep;

               if(abs(zOffset) > maxOffsetChange)
               zOffset = maxOffsetChange;

               zOffset = _sens->getZAccelOffset() + (zOffset * -MathHelper::sign(z));

               _sens->setZAccelOffset(zOffset);

          }

          delay(5);
     }
}

#endif
