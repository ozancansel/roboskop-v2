#ifndef BUZZER_H
#define BUZZER_H

#include <Arduino.h>
#include <TaskScheduler.h>

class Buzzer{


public:

     enum noteNames{ NOTE_B = 10 , NOTE_C = 0, NOTE_CS = 1, NOTE_D = 2 , NOTE_DS = 3, NOTE_E = 4  , NOTE_F = 5 , NOTE_FS = 6 , NOTE_G = 7 , NOTE_GS = 8 , NOTE_A = 9 , NOTE_REST = 11 };
     enum BlinkStatus { NONE = 0, BLINKING = 1 , PAUSED = 2 };
     Buzzer();
     Buzzer(int pin);
     ~Buzzer();

     static void onBlinkTick(void *sender , void **args);
     void      playNote(int noteIdx ,  int pitchIdx);
     void      stopNote();
     void      blink(unsigned long period , Scheduler *os);
     void      resume();
     void      pause();
     void      changeInterval(unsigned long period);
     void      off();
     void      on();
     void      setOs(Scheduler *os);

private:

     unsigned int   _toneFactors[13];
     unsigned int   _pitchFactors[11];
     unsigned int   _blinkTickInterval;
     bool      _isHigh;
     int       _pin;
     int       _status;

     Task      *_blinkTask;
     Scheduler *_os;

     void      fillNotes();

};

Buzzer::Buzzer(){
     fillNotes();
     _status        = NONE;
     _blinkTask     = NULL;
     _os            = NULL;
     _isHigh        = false;
}

Buzzer::Buzzer(int pin){
     _pin           = pin;
     _status        = NONE;
     _blinkTask     = NULL;
     _os            = NULL;
     _isHigh        = false;

     pinMode(_pin , OUTPUT);
     fillNotes();
}

Buzzer::~Buzzer(){
     if(_os != NULL){
       _os->deleteTask(*_blinkTask);

       delete _blinkTask;
     }
}

void Buzzer::setOs(Scheduler *os){
    _os = os;
}

void Buzzer::blink(unsigned long period , Scheduler *os){

     _os = os;
     _blinkTickInterval = period;

     if(_blinkTask == NULL){
        Serial.println("Blink Not NULL");
          _blinkTask = new Task(period , TASK_ONCE , Buzzer::onBlinkTick , os , true , NULL , NULL , (void*)this , NULL);
     }

     Serial.println("Blink Start");

     resume();
     _status = BLINKING;
}

void Buzzer::changeInterval(unsigned long period){
     _blinkTask->set(period , TASK_FOREVER , Buzzer::onBlinkTick , NULL , NULL);
}

void Buzzer::resume(){

     //Eğer task daha önceden atanmamışsa geri dönülüyor
     if(_blinkTask == NULL)
     return;

     _blinkTask->enableIfNot();
     _status = BLINKING;
}

void Buzzer::pause(){

     //Eğer task daha önceden atanmamışsa geri dönülüyor
     if(_blinkTask == NULL)
        return;

     off();

     if(_blinkTask->isEnabled())
        _blinkTask->disable();
        Serial.println("Disabled");

     _status = PAUSED;
}

void Buzzer::playNote(int noteIdx , int pitchIdx){
     if(noteIdx == NOTE_REST){
          stopNote();
          return;
     }

     tone(_pin , _toneFactors[noteIdx] * _pitchFactors[pitchIdx]);
}

void Buzzer::stopNote(){
     noTone(_pin);
}

void Buzzer::fillNotes(){
     // _toneFactors[0] = 33;
     // _toneFactors[1] = 35;
     // _toneFactors[2] = 37;
     // _toneFactors[3] = 39;
     // _toneFactors[4] = 41;
     // _toneFactors[5] = 44;
     // _toneFactors[6] = 46;
     // _toneFactors[7] = 49;
     // _toneFactors[8] = 52;
     // _toneFactors[9] = 55;
     // _toneFactors[10] = 58;
     // _toneFactors[11] = 0;

     _toneFactors[0] = 261;
     _toneFactors[1] = 273;
     _toneFactors[2] = 294;
     _toneFactors[3] = 312;
     _toneFactors[4] = 329;
     _toneFactors[5] = 349;
     _toneFactors[6] = 370;
     _toneFactors[7] = 390;
     _toneFactors[8] = 416;
     _toneFactors[9] = 440;
     _toneFactors[10] = 493;
     _toneFactors[11] = 0;

     //{ 1 , 2 , 4 , 8 , 16 ,  32 , 64 , 128 , 256 , 512 , 1024};

     _pitchFactors[0] = 1;
     _pitchFactors[1] = 2;
     _pitchFactors[2] = 4;
     _pitchFactors[3] = 8;
     _pitchFactors[4] = 16;
     _pitchFactors[5] = 32;
     _pitchFactors[6] = 64;
     _pitchFactors[7] = 128;
     _pitchFactors[8] = 256;
     _pitchFactors[9] = 512;
     _pitchFactors[10] = 1024;
}

void Buzzer::onBlinkTick(void *sender , void **args){

     Buzzer *obj = (Buzzer*)sender;

     obj->_isHigh = !obj->_isHigh;

     digitalWrite(obj->_pin , obj->_isHigh);

     obj->_blinkTask->set(obj->_blinkTickInterval , TASK_ONCE , Buzzer::onBlinkTick , NULL , NULL);
     obj->_blinkTask->enableIfNot();

}

void Buzzer::on(){
     digitalWrite(_pin , HIGH);
}

void Buzzer::off(){
     digitalWrite(_pin , LOW);
}

#endif
