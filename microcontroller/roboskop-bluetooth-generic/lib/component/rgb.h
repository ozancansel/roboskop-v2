#ifndef RGB_H
#define RGB_H

#include <Arduino.h>

class Rgb{

public:

     void      setR(int val);
     void      setG(int val);
     void      setB(int val);
     void      rgb(int r , int g , int b);
     int       r();
     int       g();
     int       b();
     Rgb(int r, int g ,int b);

private:

     int  _rPin;
     int  _gPin;
     int  _bPin;

     int  _r;
     int  _g;
     int  _b;

};

Rgb::Rgb(int r , int g , int b){
     _rPin = r;
     _gPin = g;
     _bPin = b;

     pinMode(_rPin , OUTPUT);
     pinMode(_gPin , OUTPUT);
     pinMode(_bPin , OUTPUT);
}

void Rgb::setR(int val){
     _r = val;
     analogWrite(_rPin , 255 - val);
}

void Rgb::setG(int val){
     _g = val;
     analogWrite(_gPin , 255 - val);
}

void Rgb::setB(int val){
     _b = val;
     analogWrite(_bPin , 255 - val);
}

void Rgb::rgb(int r , int g , int b){
     setR(r);
     setG(g);
     setB(b);
}

int Rgb::r(){
     return _r;
}

int Rgb::b(){
     return _b;
}
int Rgb::g(){
     return _g;
}


#endif
