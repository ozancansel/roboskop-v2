#include "arduinoconstants.h"
#include "arduinoproxy.h"

void ArduinoConstants::registerQmlType(){
    qmlRegisterType<ArduinoConstants>("Roboskop" , 1 , 0 , "ArduinoConstants");
}

ArduinoConstants::ArduinoConstants(QQuickItem *parent) : QQuickItem(parent)
{

}

int ArduinoConstants::a0(){
    return A0;
}

int ArduinoConstants::a1(){
    return A1;
}

int ArduinoConstants::a2(){
    return A2;
}

int ArduinoConstants::a3(){
    return A3;
}

int ArduinoConstants::a4(){
    return A4;
}

int ArduinoConstants::a5(){
    return A5;
}

int ArduinoConstants::high(){
    return HIGH;
}

int ArduinoConstants::low(){
    return LOW;
}

int ArduinoConstants::output(){
    return OUTPUT;
}

int ArduinoConstants::input(){
    return INPUT;
}
