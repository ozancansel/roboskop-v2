#ifndef GLOBALNOTIFIER_H
#define GLOBALNOTIFIER_H

#include <QObject>

class GlobalNotifier : public QObject
{

    Q_OBJECT

public:

    GlobalNotifier();

    void        setDebug(bool enabled = false);

public slots:

    void        notify(QString name , QStringList notifyArgs);

signals:

    void        broadcast(QString name , QStringList notifyArgs);

private:

    bool        _debug;


};

#endif // GLOBALNOTIFIER_H
