import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import Roboskop 1.0
import "."
import "qml/controls"
import "qml/form"
import "qml/dialog"
import "qml/theme"
import "qml/parts"
import "qml/enum"

ApplicationWindow {

    id          :   root
    visible     :   true
    visibility  :   Window.FullScreen
    title       :   qsTr("Roboskop")

    readonly property real navbarEmptySpaceLeftOffset           :   0.02
    readonly property real navbarEmptySpaceTopOffset            :   0.07

    toolBar: Rectangle {
        id              :   toolbar
        color          :   "#2286fb"
        width           :   parent.width
        height          :   SizeBinding.size(root.height * 0.1 , SizeBinding.mm(15) , SizeBinding.size(6))

        Row {
            id              :   iconsRow
            anchors.top     :   parent.top
            anchors.bottom  :   parent.bottom
            anchors.left    :   parent.left
            anchors.leftMargin  :   (height * 0.2)
            spacing         :   height * 0.4

            Item    {
                id      :   appMenuIconContainerr
                height  :   parent.height
                width   :   height

                MouseArea{
                    anchors.fill    :   parent
                    onClicked       :   {
                        if(appMenu.position == 1)
                            appMenu.close()
                        else
                            appMenu.open()
                    }
                }

                visible     :   false
            }

            Item    {
                id          :   backButton2
                width       :   opacity * height
                height      :   parent.height
                x           :   height / 2
                opacity                     :   stackView.depth > 1 ? 1 : 0


                Image{
                    anchors.verticalCenter      :   parent.verticalCenter
                    source                      :   "/res/img/navigation_previous_item.png"
                    fillMode                    :   Image.PreserveAspectFit
                    anchors.fill                :   parent
                }

                MouseArea {
                    id: backmouse
                    anchors.fill    :   parent
                    onClicked       :   {

                        while(stackView.depth > 1){
                            stackView.pop()
                            stackView.currentItem.refreshLessons();
                        }

                        while(navbarEmptySpace.depth > 1)
                            navbarEmptySpace.pop()
                    }
                }
            }

            Text {
                id              :   headerText
                x               :   height / 2
                font.pixelSize  :   parent.height * 0.56
                Behavior on x { NumberAnimation{ easing.type: Easing.OutCubic} }
                anchors.verticalCenter  :   parent.verticalCenter
                color       :   "white"
                text        :   "Roboskop"
                font.family :   FontCollection.headerFontName
                visible     :   navbarEmptySpace.depth === 1
            }
        }

        StackView{
            id                  :   navbarEmptySpace
            anchors.left        :   iconsRow.right
            anchors.leftMargin  :   parent.width * navbarEmptySpaceLeftOffset
            anchors.right       :   parent.right
            anchors.rightMargin :   anchors.leftMargin
            anchors.bottom      :   parent.bottom
            anchors.bottomMargin:   parent.height * navbarEmptySpaceTopOffset
            anchors.top         :   parent.top
            anchors.topMargin   :   anchors.bottomMargin

            initialItem         :   Item {

            }
        }
    }

    LoginDialog {
        id          :   dialog
        visible     :   false
        height      :   parent.height * 0.4
        width       :   height * 2
    }

    StackView   {
        id              :   stackView

        anchors.fill    :   parent
        focus           :   true
        Keys.onReleased: if (event.key === Qt.Key_Back && stackView.depth > 1) {
                             stackView.pop();
                             event.accepted = true;
                         }

        initialItem     : ExampleForm   {
            width       :   parent.width
            height      :   parent.height
        }

        Component.onCompleted   :   {
//            navbarEmptySpace.push({ item : Qt.resolvedUrl("/qml/parts/ExampleStateNavbarComponent.qml") , properties : { lessonIdx : 6} })
//            stackView.push({ item : Qt.resolvedUrl("/qml/form/LessonForm.qml") , properties : { lessonIdx : 6 , kit : 101}})
//             stackView.currentItem.setCurrentIndex(0)
        }
    }

    Connections {
        target      :   stackView.currentItem
        onOpenExampleRequired: {
            //Değişkenler <idx><kitCode>
            navbarEmptySpace.push({ item : Qt.resolvedUrl("/qml/parts/ExampleStateNavbarComponent.qml") , properties : { lessonIdx : idx } })
            stackView.push({ item : Qt.resolvedUrl("/qml/form/LessonForm.qml") , properties : { lessonIdx : idx , kit : kitCode}})
        }
    }
}
