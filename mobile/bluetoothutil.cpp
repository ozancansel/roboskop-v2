#include "bluetoothutil.h"
#include <QDebug>
#include <QtQml>
#include <QTime>

void BluetoothUtil::init(){
    qRegisterMetaType<BluetoothUtil::ConnectionState>("bluetooth::BtState");
    qmlRegisterType<BluetoothUtil>("BluetoothUtil" , 1 , 0 , "BluetoothUtil");
}

BluetoothUtil::BluetoothUtil()
{

    _socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol, this);
    m_socket = _socket;
    _agent = new QBluetoothDeviceDiscoveryAgent(this);


    setSocket(_socket);
    _commandQueued = false;

    connect(_socket , SIGNAL(connected()) , this , SLOT(btConnected()));
    connect(_socket , SIGNAL(disconnected()) , this , SLOT(btDisconnected()));
    connect(_agent , SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)) , this , SLOT(btDeviceDiscovered(QBluetoothDeviceInfo)));
    connect(_agent , SIGNAL(finished()) , this , SLOT(btScanFinished()));
    connect(_socket , SIGNAL(stateChanged(QBluetoothSocket::SocketState)) , this , SLOT(btStateChanged(QBluetoothSocket::SocketState)));


    setDebugEnabled(false);
}

void BluetoothUtil::btConnected(){

    if(_debug)
        qDebug() << "Bluetooth bağlandı.";

    setState(Connected);
}

void BluetoothUtil::btDisconnected(){

    if(_debug)
        qDebug() << "Bluetooth bağlantısı koptu.";

    setState(Disconnected);
}

//Private slots
void BluetoothUtil::btDeviceDiscovered(const QBluetoothDeviceInfo &device){
    qDebug() << device.name() << " " << device.address() << " discovered" << " Service : ";

    QString name = device.name();
    QString addr = device.address().toString();
    appendIfNotExists(device);
    sendDeviceDiscoveredSignal(name , addr);
}

void BluetoothUtil::btScanFinished(){

    if(getState() == Discovering)
        setState(None);

    if(debug())
        qDebug() << "Bluetooth tarama bitti";

    sendScanStatusChangedSignal();
}

//Public slots
void BluetoothUtil::connectTo(QString address){

    if(debug())
        qDebug() << address << "'a bağlanılıyor.";

    _agent->stop();

    //Eğer soket açıksa kapatılıyor
    if(_socket->isOpen())
        _socket->close();

    QBluetoothDeviceInfo info = byAddress(address);

    QVariantMap map;

    map["name"] = info.name();
    map["address"] = info.address().toString();
    setConnectingDevice(map);
    _socket->connectToService(QBluetoothAddress(address) , QBluetoothUuid(QBluetoothUuid::SerialPort));
}

void BluetoothUtil::startScan(){

    if(debug())
        qDebug() << "Tarama başladı";

    setState(Discovering);

    _agent->stop();
    _agent->start();
}

bool BluetoothUtil::debug(){
    return _debug;
}

void BluetoothUtil::setDebugEnabled(bool enabled){
    _debug = enabled;
}

void BluetoothUtil::btStateChanged(QBluetoothSocket::SocketState state){

    if(state == QBluetoothSocket::UnconnectedState && (getState() != Disconnected || getState() != Error)){
        setState(None);
    }else if(state == QBluetoothSocket::ConnectingState){
        setState(Connecting);
    }else if(state == QBluetoothSocket::ConnectedState){
        setState(Connected);
    }

    if(_debug)
        qDebug() << "Soket durumu değişti : " << state;
}

void BluetoothUtil::appendIfNotExists(QBluetoothDeviceInfo newOne){

    foreach (QBluetoothDeviceInfo info, m_discoveredDevices) {
        if(info.address() == newOne.address()){
            if(info.name() != newOne.name()){
                m_discoveredDevices.removeOne(info);
                m_discoveredDevices.append(newOne);
                return;
            }
        }
    }

    m_discoveredDevices.append(newOne);
}

QBluetoothDeviceInfo BluetoothUtil::byAddress(QString addr){
    foreach (QBluetoothDeviceInfo info, m_discoveredDevices) {
        if(info.address().toString() == addr)
            return info;
    }

    return QBluetoothDeviceInfo();
}
