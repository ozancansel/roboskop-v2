pragma Singleton
import QtQuick 2.0
import "qml/controls"

Item {

    property Toast  mainToast
    readonly property string kidFontName        :   kidFont.name
    readonly property string headerFontName     :   kidFont.name
    readonly property string labelFontName      :   kidFont.name

    FontLoader{
        id      :   kidFont
        source  :   "/res/font/kid-font.otf"
    }

    function displayMessage(message){

        if(mainToast === undefined)
            return

    }
}
