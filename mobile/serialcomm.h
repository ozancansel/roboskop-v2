#ifndef ISERIALINTERFACE_H
#define ISERIALINTERFACE_H

#include <QObject>
#include <QIODevice>
#include <QTimer>
#include <QQueue>
#include <QTime>
#include <QQuickItem>

class SerialComm : public QObject{

    Q_OBJECT
    Q_ENUMS(ConnectionState)
    Q_PROPERTY(ConnectionState state READ getState NOTIFY stateChanged)
    Q_PROPERTY(int bufferedCommandsLength READ getBufferedCommandsLength)
    Q_PROPERTY(QVariantMap connectingDevice READ connectingDevice NOTIFY connectingDeviceChanged)

public:

    enum ConnectionState{ Disconnected = 0, Connecting = 1 , Connected = 2, Discovering = 3 , Error = 4 , None = 5 };

    static void         registerQmlType();
    SerialComm(QObject* parent = nullptr);
    int                 getBufferedCommandsLength();
    ConnectionState     getState();
    QVariantMap         connectingDevice();

protected:

    void    sendDeviceDiscoveredSignal(QString &name , QString &address);
    void    sendConnectionStatusChangedSignal();
    void    sendConnectingStatusChangedSignal();
    void    sendScanStatusChangedSignal();
    void    sendDataIncomeSignal(QString &data);
    void    sendStateChangedSignal();
    void    setSocket(QIODevice *dev);
    void    setState(ConnectionState state);
    void    setConnectingDevice(QVariantMap name);

signals:

    void    deviceDiscovered(QString name , QString address);
    void    connectionStatusChanged();
    void    connectingStatusChanged();
    void    scanStatusChanged();
    void    dataIncome(QString data);
    void    stateChanged();
    void    connectingDeviceChanged();

public slots:

    virtual void    connectTo(QString address);
    virtual void    closeSocket();
    virtual void    startScan();
    virtual void    send(QString text);
    QIODevice*      device();

private slots:

    void sendTimerTick();

protected:

    QIODevice*      m_socket;

private:

    QQueue<QString>     m_buffer;
    QTimer              m_senderTimer;
    QTime               m_lastTick;
    ConnectionState     m_state;
    QVariantMap         m_connectingDevice;

};

#endif // ISERIALINTERFACE_H
