import QtQuick 2.0

Item {

    readonly property ListModel   notes     :   ListModel{  }
    property alias  symbol                  :   symbols
    property int bpm                        :   120
    property int noteIdx                    :   0
    property bool noteTransition            :   false
    property bool melodyPlaying             :   durationCalculator.running

    signal  nextNote(int tone , string note)
    signal  melodyStart()
    signal  melodyEnd()

    NoteSymbols {
        id      :   symbols
    }

    function add(tone , note , beat , modifier){
        notes.append({ "tone" : tone, "note" : note , "beat" : beat , "modifier" : modifier})
    }

    function    setBpm(bp){
        item.bpm = bp
    }

    function play(){
        durationCalculator.start()
    }

    function noteDuration(idx){
        if(idx > notes.count)
            return -1
        else{
            var note = notes.get(idx)
            return symbols.duration(bpm , note.beat , note.modifier)
        }
    }

    function    noteText(idx){
        if(idx >= notes.count)
            return ""
        else{
            console.log(idx)
            var note = notes.get(idx)

            return note.note
        }
    }

    function pause(){
        durationCalculator.stop()
    }

    function stop(){
        durationCalculator.stop()
        noteIdx = 0
        melodyEnd()
    }

    id              :   item

    Timer{
        id          :   durationCalculator
        repeat      :   true
        interval    :   1
        onTriggered :   {
            if(noteIdx >= notes.count)  {
                durationCalculator.stop()
                noteIdx = 0
                melodyEnd()
            } else if (noteTransition)   {
                nextNote(0 , symbols.rest)
                interval = 10
                noteTransition = false
            }
            else    {
                if(noteIdx == 0)
                    melodyStart()
                var note = notes.get(noteIdx)
                var duration = symbols.duration(bpm , note.beat , note.modifier)
                console.log("Note " + note)
                nextNote(note.tone , note.note)

                interval        =   duration
                noteTransition  =   true
                noteIdx += 1
            }
        }
    }
}
