import QtQuick 2.0

Item {
    readonly property int   c       :   0
    readonly property int   cDies   :   1
    readonly property int   d       :   2
    readonly property int   dDies   :   3
    readonly property int   e       :   4
    readonly property int   f       :   5
    readonly property int   fDies   :   6
    readonly property int   g       :   7
    readonly property int   gDies   :   8
    readonly property int   a       :   9
    readonly property int   aDies   :   10
    readonly property int   b       :   11
    readonly property int   rest    :   12

    readonly property int   semibreve       :   0   //4 beats
    readonly property int   minim           :   1   //2 beats
    readonly property int   crochet         :   2   //1 beat
    readonly property int   quaver          :   3   //1/2 beat
    readonly property int   semiquaver      :   4   //1/4 beat
    readonly property int   demisemiquaver  :   5   //1/8 beat

    readonly property int   beatHalfModifier    :   1
    readonly property int   beatNoModifier      :   0

    readonly property variant beatsMap  :   [ 4 , 2 , 1 , 0.5 , 0.25 , 0.125 ]
    readonly property variant notesText :   [ "do" , "do#" , "re" , "re#" , "mi" , "fa" , "fa#" , "sol" , "sol#" , "la" , "si" , "rest"]
    readonly property variant beatsText :   [ "semibreve - 4b" , "minim - 2b" , "crochet - 1b" , "quaver - 1/2b" , "semiquaver - 1/4b" , "demisemiquaver - 1/8b"]

    readonly property variant hertzMapping : {
        "c" : [ 0 , 0 , 65 , 130 , 262 ] ,
        "c#" : [0 , 0 , 69 , 138 , 277 ],
        "d" : [0 , 0 , 73 , 146 , 294 ],
        "d#" : [0 , 0 , 77 , 155 , 311 ],
        "e" : [0 , 0 , 82 , 164  , 330],
        "f" : [0 , 0 , 87 , 174 , 349 ],
        "f#" : [0 , 0 , 92 , 185 , 369 ],
        "g" : [0 , 0 , 98 , 196 , 392 ],
        "g#" : [0 , 0 , 103 , 207 , 415],
        "a" : [0 , 0 , 110 , 220 , 440 ],
        "a#" : [0 , 0 , 116 , 233 , 466 ],
        "b" : [ 0 , 0 , 123 , 247 , 494 ] ,
        "rest":  [0 , 0 , 0 , 0 , 0]
    }

    function    duration(tempo , beat , modifier){
        return (beatsMap[beat] + (beatsMap[beat] * 0.5 * modifier)) * (60 / tempo) * 1000
    }

    function    getNoteText(note){
        return notesText[note]
    }

    function    getBeatText(beat){
        return beatsText[beat]
    }
}

