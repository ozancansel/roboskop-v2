import QtQuick 2.0
import "../"

Melody {

    Component.onCompleted   :   {
        setBpm(120)
        var tone = 3
        add(tone , "e" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "e" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "e", symbol.minim , symbol.beatNoModifier)
        add(tone , "e" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "e", symbol.crochet , symbol.beatNoModifier)
        add(tone , "e" , symbol.minim , symbol.beatNoModifier)
        add(tone , "e" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "g" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "c", symbol.crochet , symbol.beatHalfModifier)
        add(tone , "d" , symbol.quaver , symbol.beatHalfModifier)
        add(tone , "e" , symbol.minim , symbol.beatNoModifier)
        add(tone , "f" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "f" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "f" , symbol.minim , symbol.beatNoModifier)
        add(tone , "e" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "e", symbol.crochet , symbol.beatNoModifier)
        add(tone , "e" , symbol.minim , symbol.beatNoModifier)
        add(tone , "d" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "c" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "d" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "e" , symbol.crochet , symbol.beatNoModifier)
        add(tone , "d" , symbol.minim , symbol.beatNoModifier)
        add(tone , "g" , symbol.crochet , symbol.beatNoModifier)
    }
}
