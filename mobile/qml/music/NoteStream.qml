import QtQuick 2.0

Item {

    property Melody melody  :   Melody{ }
    property real widthPSec :   100
    property real shiftStep :    widthPSec / (1000 / shiftingTimer.interval)
    readonly property variant   backgroundColors    :   [ "#33cc33" , "#ffcc33" , "#ff3366" ]
    readonly property variant   fontColors          :   [ "ffcc33" , "#ff3366" , "#f2f1ef"]

    function    play(){
        shiftingTimer.start()
    }

    function    pause(){
        shiftingTimer.stop()
    }

    function    stop(){
        shiftingTimer.stop()
        streamRow.x = 0
    }

    FontLoader{
        id      :   kidFont
        source  :   "/res/font/kid-font.otf"
    }

    clip            :   true

    Row {

        id          :   streamRow
        width       :   parent.width
        height      :   parent.height

        Repeater{
            model   :   melody.notes.count
            Rectangle{
                width   :   ((melody.noteDuration(index) + 16) / 1000) * widthPSec
                height  :   parent.height
                color   :   backgroundColors[index % backgroundColors.length]
                border.width    :   height  *   0.03
                border.color    :   "#111111"
                Text {
                    id      :   noteName
                    text    :   melody.noteText(index)
                    color   :   "white"
                    font.pointSize  :   parent.height * 0.4
                    font.bold       :   true
                    font.family     :   kidFont.name
                    anchors.centerIn    :   parent
                }
            }
        }
    }

    Timer{
        id          :   shiftingTimer
        interval    :   16
        repeat      :   true
        onTriggered :   {
            streamRow.x -= shiftStep
        }
    }
}
