import QtQuick 2.2
import QtQuick.Controls 2.2
import "../theme"
import "../controls"

Dialog {

    property int    score                       :   50
    property string text1                       :   "Tebrikler !!!\nBu dersi başarıyla tamamladın.\nPuan"
    property bool   scoreEnabled                :   false

    id          :   dialog
    modal       :   true
    enter           :   Transition {
        ParallelAnimation{
            NumberAnimation{
                property    :   "opacity"; from : 0; to : 1.0
            }
            ScaleAnimator{
                from        :   0
                to          :   1
            }
        }
    }

    signal okClicked()

    contentItem :   Item    {
        readonly property real congratTextHeight    :   height * 0.8
        readonly property real congratTextWidth     :   width * 0.6

        id              :   contentContainer

        Column  {

            id                  :   textList
            anchors.centerIn    :   parent
            width               :   childrenRect.width
            height              :   childrenRect.height
            z                   :   5
            spacing             :   height * 0.05

            Text    {
                id                  :   congratulationsText
                text                :   text1
                wrapMode            :   Text.Wrap
                verticalAlignment   :   Text.AlignVCenter
                horizontalAlignment :   Text.AlignHCenter
                font.family         :   FontCollection.labelFontName
                width               :   contentContainer.width
                height              :   contentContainer.height * (scoreEnabled ? 0.7 : 1)
                fontSizeMode        :   Text.Fit
                font.pixelSize          :   height
                color               :   Color.colors[Color.black]
            }

            Text    {
                id                      :   scoreText
                text                    :   dialog.score
                verticalAlignment       :   Text.AlignVCenter
                horizontalAlignment     :   Text.AlignHCenter
                font.bold               :   true
                font.family             :   FontCollection.unitedKingdomFont
                anchors.horizontalCenter:   parent.horizontalCenter
                color                   :   Color.colors[Color.green]
                height                  :   contentContainer.height * 0.3
                width                   :   contentContainer.width
                fontSizeMode            :   Text.Fit
                font.pixelSize          :   height
                visible                 :   scoreEnabled
            }


            RoboskopButton{
                id                          :   okeyButton
                height                      :   contentContainer.height * 0.15
                width                       :   contentContainer.width * 0.4
                text                        :   "Tamam!"
                controlColor                :   okeyButton.green
                anchors.horizontalCenter    :   parent.horizontalCenter
                onClicked                   :   {
                    okClicked()
                    dialog.close()
                }
            }
        }

        Rectangle   {
            id                  :   textListbackground
            anchors.fill        :   textList
            anchors.margins     :   - (textList.height * 0.1)
            anchors.centerIn    :   textList
            z                   :   textList.z - 1 //text list background
            color               :   Color.colors[Color.yellow]
            radius              :   height * 0.05
            border.width        :   height * 0.01
        }
    }
}
