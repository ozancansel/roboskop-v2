import QtQuick 2.0
import QtBluetooth 5.3
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.2
import "../theme"
import "../controls"
import "../model"
import "../parts"
import "../enum"

Rectangle {


    property alias      dialogWidth         :   container.width
    property alias      dialogHeight        :   container.height
    property double     textHeight          :   0.35
    property double     headerTextHeight    :   0.40
    readonly property color     backgroundColor  :   "white"
    readonly property color     textColor   :   "black"

    readonly property real buttonsContainerBottomMargin         :   0.9

    signal  connected()
    signal  disconnected()

    function    connectTo(btAdress){
        bluetooth.connectTo(btAdress)
    }

    function    startScan(){
        bluetoothModel.clear()
        bluetooth.startScan()
    }

    id                  :   container
    color               :   backgroundColor

    BluetoothModel{
        id      :   bluetoothModel
    }

    Toast   {
        id          :   toast
        container   :   parent
    }

    Item{
        width               :   parent.width * 0.6
        height              :   parent.height * 0.6
        anchors.centerIn    :   parent
        z                   :   2

        Text {
            id              :   statusText
            text            :   bluetooth.state == BluetoothStateEnum.discovering ? "Cihaz aranıyor..." : "Cihaz arayın"
            font.pixelSize  :   parent.height * 0.4
            font.family     :   FontCollection.headerFontName
            anchors.centerIn:   parent
            color           :   Color.colors[Color.purple]
        }

        MouseArea{
            id              :   mouseArea
            anchors.fill    :   parent
            onClicked       :   startScan()
        }

        visible             :   !bluetoothModel.count
        enabled             :   !bluetoothModel.count
    }

    ListView {

        id                  :   bluetoothListView
        model               :   bluetoothModel
        anchors.top         :   parent.top
        anchors.bottom      :   controlButtonsContainer.top
        height              :   parent.height * 0.8
        width               :   parent.width
        spacing             :   SizeBinding.mm(3)

        delegate :  Rectangle {

            id                      :   btListItemContainer
            width                   :   bluetoothListView.width
            height                  :   bluetoothListView.height * 0.2
            scale                   :   ListView.view.currentIndex === index ? 1.5 : 0.5
            color                   :   "transparent"

            Text {
                id                  :   bluetoothNameText
                text                :   name + " - " + address
                anchors.centerIn    :   parent
                font.pixelSize      :   parent.height * textHeight
                font.family         :   FontCollection.labelFontName
                color               :   bluetoothListView.currentIndex === index ?  Color.colors[Color.white] : Color.colors[Color.brown]
            }

            MouseArea {
                anchors.fill        :   parent
                onClicked           :   bluetoothListView.currentIndex = index
            }

            Behavior on scale {                    // added to smooth the scaling
                NumberAnimation {
                    duration        :   150
                }
            }

            Text {
                id                      :   newItemText
                text                    :   "   +1"
                anchors.left            :   bluetoothNameText.right
                anchors.verticalCenter  :   btListItemContainer.verticalCenter
                color                   :   bluetoothListView.currentIndex === index ?  Color.colors[Color.pink] : Color.colors[Color.pink]
                font.pixelSize          :   parent.height * textHeight * 0.75
                font.family             :   FontCollection.labelFontName
            }

            SequentialAnimation{

                id              :   fadeOutAnimation

                PauseAnimation{
                    duration    :   1500
                }

                NumberAnimation{
                    property    :   "opacity"
                    from        :   1
                    to          :   0
                    target      :   newItemText
                    duration    :   1000
                }
            }

            Component.onCompleted   :   {
                if(!fadeOutAnimation.complete())
                    fadeOutAnimation.start()
            }
        }

        header: Component{
            id          :   bluetoothHeading

            Rectangle{
                width               :   bluetoothListView.width
                height              :   bluetoothListView.height * 0.2
                color               :   "transparent"
                Text {
                    id              :   name
                    text            :   qsTr("Bluetooth Listesi")
                    font.pointSize  :   parent.height * headerTextHeight
                    font.family     :   FontCollection.headerFontName
                    anchors.centerIn:   parent
                    color           :   Color.colors[Color.purple]
                }
            }
        }

        highlight                   :   Rectangle{color : "#2286fb"}
        preferredHighlightBegin     :   0
        preferredHighlightEnd       :   delegate.width
        highlightRangeMode          :   ListView.ApplyRange
        cacheBuffer                 :   400
    }


    Rectangle {

        id                      :   controlButtonsContainer
        anchors.bottom          :   parent.bottom
        width                   :   SizeBinding.size(parent.width * 0.45 , SizeBinding.mm(180) , SizeBinding.mm(70))
        height                  :   parent.height * 0.2
        anchors.horizontalCenter:   bluetoothListView.horizontalCenter
        color                   :   "transparent"

        RoboskopButton{
            id                      :   scanButton
            height                  :   parent.height * 0.6
            width                   :   parent.width * 0.5
            anchors.verticalCenter  :   parent.verticalCenter
            text                    :   "Tara"
            controlColor            :   scanButton.yellow
            onClicked               :   {
                startScan()
            }
        }

        RoboskopButton{
            id                      :   connectButton
            anchors.left            :   scanButton.right
            anchors.leftMargin      :   SizeBinding.mm(6)
            height                  :   scanButton.height
            width                   :   scanButton.width
            anchors.verticalCenter  :   parent.verticalCenter
            text                    :   "Bağlan"
            controlColor            :   connectButton.pink
            onClicked               :   {

                if(bluetoothListView.currentIndex < 0)
                    return

                connectTo(bluetoothModel.get(bluetoothListView.currentIndex).address)
            }
        }
    }

    Connections{
        target                      :   bluetooth
        onDeviceDiscovered          :   {
            bluetoothModel.append({"name" : name , "address" : address})
        }

        onStateChanged              :   {
            if(bluetooth.state == BluetoothStateEnum.connected){
                toast.displayMessage("Bluetooth bağlantısı kuruldu.")
                connected()
            }
        }
    }
}

