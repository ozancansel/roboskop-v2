import QtQuick 2.2
import QtQuick.Controls 2.2
import "../controls"
import "../theme"
import "../enum"

Dialog {

    property string deviceName      :  "Deneme 12"//bluetooth.connectingDevice.name
    property real dialogWidth        :   0
    property real dialogHeight       :   0

    id          :   dialog
    modal       :   true
    visible     :   bluetooth.state === BluetoothStateEnum.connecting

    contentItem :   Item {

        id          :   contentItem

//        Rectangle   {
//            id              :   backRect
//            anchors.fill    :   parent
//            color           :   "#333333"
//        }

        Item    {
            id                  :   dialogContainer
            width               :   parent.width * dialogWidth
            height              :   parent.height * dialogHeight
            anchors.centerIn    :   parent

            Rectangle{
                anchors.fill:parent
                color: "red"
            }

            Column  {

                id                  :   contentColumn
                z                   :   5
                anchors.centerIn    :   parent
                spacing             :   dialogContainer.height * 0.02

                Text    {
                    id                  :   connectingText
                    verticalAlignment   :   Text.AlignVCenter
                    horizontalAlignment :   Text.AlignHCenter
                    font.family         :   FontCollection.labelFontName
                    width               :   dialogContainer.width
                    height              :   dialogContainer.height * 0.4
                    text                :   deviceName + "\nBaglaniliyor..."
                    fontSizeMode        :   Text.Fit
                    wrapMode            :   Text.Wrap
                    font.pixelSize      :   1000
                    color               :   Color.colors[Color.white]
                }

                BusyIndicator   {
                    id: control
                    running :   true
                    z       :   5
                    width   :   parent.width
                    height  :   parent.height

                    contentItem: Item {
                        implicitWidth: Math.min(control.width , control.height)
                        implicitHeight: Math.min(control.width , control.height)

                        Item {
                            id: item
                            width: parent.implicitWidth
                            height: parent.implicitHeight
                            x:   (parent.width / 2) - (width / 2)
                            y: (parent.height / 2) - (height / 2)
                            opacity: control.running ? 1 : 0

                            Behavior on opacity {
                                OpacityAnimator {
                                    duration: 250
                                }
                            }

                            RotationAnimator {
                                target: item
                                running: control.visible && control.running
                                from: 0
                                to: 360
                                loops: Animation.Infinite
                                duration: 1250
                                easing.type :   Easing.InSine
                            }

                            Repeater {
                                id: repeater
                                model: 6

                                Rectangle {
                                    id  :   dot
                                    x: item.width / 2 - width / 2
                                    y: item.height / 2 - height / 2
                                    implicitWidth: item.width / 6
                                    implicitHeight: item.height / 6
                                    radius: height / 2
                                    color: Color.colors[Color.orange]
                                    transform: [
                                        Translate {
                                            y: -Math.min(item.width, item.height) * 0.5 + dot.radius
                                        },
                                        Rotation {
                                            angle: index / repeater.count * 360
                                            origin.x: dot.radius
                                            origin.y: dot.radius
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }

            Rectangle   {
                id          :   background
                z           :   contentColumn.z -1
                anchors.fill:   dialogContainer
                color       :   Color.colors[Color.green]
                radius      :   height * 0.05
                border.width:   height * 0.01
            }
        }
    }

    background      :   Item {  }
}
