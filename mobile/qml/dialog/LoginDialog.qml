import QtQuick 2.0
import QtQuick.Controls 2.2
import "../form"

Dialog {

    id      :   dialog
    modal   :   true
    x           :   (parent.width / 2) - (width / 2)
    y           :   (parent.height / 2) - (height / 2)
    closePolicy :   Popup.CloseOnReleaseOutside
    background  :   Item {
    }
    contentItem :   LoginForm   {
    }
}
