import QtQuick 2.0
import Bacon2D 1.0
import "../bacon2dlib"

PhysicsEntity {

    id              :   entity
    bodyType        :   BoxBody.Dynamic
    sleepingAllowed :   false


    readonly property real  boxWidth        :   0.75
    property alias fixture    :   fixture

    body.fixedRotation  :   true

    fixtures        :   Box {
        id                  :   fixture
        width               :   target.width * entity.boxWidth
        height              :   target.height * entity.boxWidth
        density             :   1
        friction            :   0.9
        restitution         :   0
        x                   :   target.width * ((1 - entity.boxWidth) * 0.5)
        y                   :   target.height * ((1 - entity.boxWidth) * 0.5)
    }

//    Sprite {
//        id      :   spriteItem

//        anchors.centerIn    :   parent

//        animation   : "falling"

//        animations: SpriteAnimation {
//            name: "falling"
//            source: "/res/img/astronaut.png"
//            frames: 3
//            duration: 450
//            loops: Animation.Infinite
//        }
//    }

    Image{
        source      :   "/res/img/spaceship-256x.png"
        fillMode    :   Image.PreserveAspectFit
        anchors.fill:   parent
        smooth      :   true
    }
}
