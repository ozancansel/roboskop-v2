import QtQuick 2.0
import QtQuick.Particles 2.0

ParticleSystem {

    id          :   sys
    property real   flameAngle       :   200

    Turbulence{
        id      :   turbulance
        enabled :   true
        height  :   parent.height
        width   :   parent.width
        x       :   parent. width / 4
        anchors.fill    : parent
        strength    : 200
        NumberAnimation on strength{from: 16; to: 64; easing.type: Easing.InOutBounce; duration: 1800; loops: -1}
    }

    ImageParticle {
        groups: ["smoke"]
        source: "qrc:///particleresources/glowdot.png"
        color: "#11111111"
        colorVariation: 0
    }

    ImageParticle {
        groups: ["flame"]
        source: "qrc:///particleresources/glowdot.png"
        color: "#11ff400f"
        colorVariation: 0.1
    }

    Emitter {
        id              :   emitter
        anchors.centerIn:   parent
        group           : "flame"
        emitRate        : 120
        lifeSpan        : 800
        size            : parent.width * 0.8
        endSize         : size * 0.5
        sizeVariation   : endSize
        acceleration: PointDirection { y    :   parent.height * 0.2  }
        velocity: AngleDirection { angle    : sys.flameAngle; magnitude: emitter.size * 6; angleVariation: 0; magnitudeVariation: magnitude * 0.1 }
    }

    TrailEmitter {
        id                  :   smoke1
        group               :   "smoke"
        follow              :   "flame"
        y                   :   emitter.size * 0.5

        emitRatePerParticle :   1
        lifeSpan            :   2400
        lifeSpanVariation   :   400
        size                :   emitter.size
        endSize             :   emitter.endSize
        sizeVariation       :   emitter.sizeVariation
        acceleration        :   PointDirection { y: 40 }
        velocity            :   AngleDirection { angle: sys.flameAngle; magnitude: emitter.size * 6; angleVariation: 10; magnitudeVariation: 5 }
    }
}
