import QtQuick 2.0
import Bacon2D 1.0
import "../bacon2dlib"

PhysicsEntity{

    id                      :   target

    property alias          fixture :   boxFixture

    Rectangle{
        anchors.fill        :   parent
        color               :   "black"
    }

//    fixtures    :   [
//        Edge{
//            vertices    :   [
//                Qt.point(0 , 0) ,
//                Qt.point(0 , target.height)
//            ]
//        }   ,
//        Edge{
//            vertices    :   [
//                Qt.point(0 , target.height) ,
//                Qt.point(target.width , target.height)
//            ]
//        }   ,
//        Edge{
//            vertices    :   [
//                Qt.point(target.width , target.height)  ,
//                Qt.point(target.width , 0)
//            ]
//        }   ,
//        Edge{
//            id          :   line
//            vertices    :   [
//                Qt.point(target.width , 0)  ,
//                Qt.point(0 , 0)
//            ]
//        }
//    ]

    fixtures    :   Box{
        id      :   boxFixture
        width   :   target.width
        height  :   target.height
    }
}
