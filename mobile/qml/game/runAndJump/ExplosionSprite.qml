import QtQuick 2.0
import QtQuick.Particles 2.0

ParticleSystem{

    id      :   sys

    property alias  mainEmitter :   fireEmitter

    ImageParticle {
        groups: ["explosionStart"]
        source: "qrc:///particleresources/glowdot.png"
        color: "#11ff400f"
        colorVariation: 0.2
    }

    ImageParticle{
        groups: ["smoke"]
        source: "qrc:///particleresources/glowdot.png"
        color: "#11111111"
        colorVariation: 0.1
    }

    Turbulence{
        id              :   turbulence
        enabled         :   true
        anchors.fill    :   parent
        strength        :   400
    }

    Emitter {
        id              :   fireEmitter
        anchors.centerIn:   parent
        group           :   "explosionStart"
        lifeSpan        :   900
        size            :   parent.width * 0.5
        endSize         :   size * 0.5
        sizeVariation   :   endSize
        acceleration: PointDirection { y    :   -parent.height * 0.2  }
        velocity        :   AngleDirection{
            angle               :   -90
            angleVariation      :   45
            magnitude           :   400
            magnitudeVariation  :   20
        }
    }

    TrailEmitter {
        id                  :   smoke1
        group               :   "smoke"
        follow              :   "explosionStart"

        startTime           :   fireEmitter.lifeSpan * 0.7
        emitRatePerParticle :   2
        lifeSpan            :   1000
        lifeSpanVariation   :   300
        size                :   fireEmitter.size
        endSize             :   fireEmitter.endSize
        sizeVariation       :   fireEmitter.sizeVariation
        acceleration        :   PointDirection { y  :   -parent.height * 0.2 }
        velocity            :   AngleDirection { angle: -90; magnitude: fireEmitter.size * 2; angleVariation: 10; magnitudeVariation: 5 }
    }
}
