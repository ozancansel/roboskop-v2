import QtQuick 2.0
import Bacon2D 1.0
import "../bacon2dlib"

Component{
    PhysicsEntity {
        id      :   bullet

        bodyType        :   BoxBody.Dynamic
        bullet          :   true
        fixedRotation   :   true

        fixtures    :   Box {
            id          :   spaceshipBulletBody
            width       :   target.width
            height      :   target.height
            density     :   100
            restitution :   0
            friction    :   1
            objectName  :   "bullet"
        }

        Rectangle{
            width       :   target.width
            height      :   target.height
            color       :   "red"
            radius      :   target.height * 0.5
        }
    }
}
