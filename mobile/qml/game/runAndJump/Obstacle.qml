import QtQuick 2.7
import Bacon2D 1.0
import "../bacon2dlib"

Component{
    PhysicsEntity {

        property bool rotating          :   true
        readonly property real  boxWidth        :   0.68

        id      :   entity

        fixtures    :   Box {
            width   :   target.width * entity.boxWidth
            height  :   target.height * entity.boxWidth
            density :   1
            friction:   1
            restitution :   0
            objectName  :   "obstacle"
            x       :   target.width * ((1 - entity.boxWidth) / 2)
            y       :   target.height * ((1 - entity.boxWidth) / 2)
        }

        Image{
            id              :   texture
            width           :   target.width
            height          :   target.height
            fillMode        :   Image.PreserveAspectFit
            source          :   "/res/img/meteor-512x512.png"
            z               :   4

            RotationAnimator{
                id          :   rotatingMeteor
                target      :   texture
                from        :   0
                to          :   360
                duration    :   3000
                running     :   entity.rotating
                loops       :   Animation.Infinite
            }
        }
    }
}
