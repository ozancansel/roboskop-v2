﻿import QtQuick 2.7
import QtQml 2.2
import Bacon2D 1.0
import QtQuick.Controls 2.1
import QtQuick.Particles 2.0
import QtMultimedia 5.0
import "../controls"
import "bacon2dlib"
import "runAndJump"

Game {

    id              :   spaceshipGame
    currentScene    :   gameOverScene
    readonly property real baseLineThickness    :   0.01
    readonly property real baseLineBottomMargin :   0.25

    readonly property real obstacleWidth        :   0.2
    readonly property real bulletWidth          :   0.02
    readonly property real bulletHeight         :   0.2
    readonly property real obstacleHeight       :   0.25
    readonly property real scoreWidth           :   0.2
    readonly property real scoreHeight          :   0.2
    readonly property real buttonWidth          :   0.2
    readonly property real buttonHeight         :   0.1

    signal          gameOver(int score)

    function        shipUp(step){
        var newY = spaceShip.y - (gamePlayScene.height * step)
        spaceShip.y = newY < 0 ? 0 : newY
    }

    function        shipDown(step){
        var newY = spaceShip.y + (gamePlayScene.height * step)
        spaceShip.y = newY > (gamePlayScene.height - spaceShip.height) ? gamePlayScene.height - spaceShip.height : newY
    }

    function        setShipPos(portion){
        spaceShip.y = (gamePlayScene.height - spaceShip.height) * portion
    }

    function        start(){
        pushScene(gamePlayScene)
        gamePlayScene.restart()
    }

    function        restart(){
        pushScene(gamePlayScene)
        gamePlayScene.restart()
    }

    function        getScaledX(val){
        return (gamePlayScene.width / 1200) * val
    }

    Settings{
        id                      :   settings
        property int    score   :   gamePlayScene.sceneScore
        property bool   noSound :   false
    }

    Scene   {

        id      :   gamePlayScene
        physics :   true
        width   :   parent.width
        height  :   parent.height
        debug   :   true
        gravity :   Qt.point(0 , 0)
        state   :   "gameOver"

        onPreSolve  :   {

            var     targetA =   contact.fixtureA.getBody().target
            var     targetB =   contact.fixtureB.getBody().target

            console.log("A : " + contact.fixtureA.objectName + " B : " + contact.fixtureB.objectName)

            if(contact.fixtureA.objectName  ===  "obstacle" && contact.fixtureB.objectName   ===  "bullet"){
                targetA.destroy()
                targetB.destroy()

                obstacleExplosion.x                 =       targetA.x
                obstacleExplosion.y                 =       targetA.y + (targetA.height * 0.5)
                obstacleExplosion.mainEmitter.burst(200)
                obstacleExplosion.mainEmitter.emitRate  =   0

                gamePlayScene.sceneScore    +=  200

                if(!asteroidExplosionSound.playing)
                    asteroidExplosionSound.play()
            }
            else if(contact.fixtureB.objectName === "obstacle" && contact.fixtureB.objectName === "bullet"){
                targetA.destroy()
                targetB.destroy()

                obstacleExplosion.x                 =       targetA.x
                obstacleExplosion.y                 =       targetA.y + (targetA.height * 0.5)
                obstacleExplosion.mainEmitter.burst(200)
                obstacleExplosion.mainEmitter.emitRate  =   0

                gamePlayScene.sceneScore    +=  200

                if(!asteroidExplosionSound.playing)
                    asteroidExplosionSound.play()
            }
        }

        function        restart(){
            sceneScore                      =   0
            gamePlayScene.state             =   ""
            spaceShip.visible               =   true
            rocketFlameParticleSys.visible  =   true
            sceneLoop.restart()
        }


        function        fire(){

            if(spaceshipGame.gameState !== Bacon2D.Active){
                return
            }

            if(spaceshipGame.currentScene != gamePlayScene)
                return;

            var newObstacle = bulletComponent.createObject(gamePlayScene)

            newObstacle.width = gamePlayScene.width * bulletWidth
            newObstacle.height = newObstacle.width * bulletHeight
            newObstacle.x = spaceShip.x + spaceShip.width
            newObstacle.y = spaceShip.y + (spaceShip.height * 0.5)
            newObstacle.z = 2
            newObstacle.behavior = bulletBehaviour
            newObstacle.linearVelocity  =   Qt.point(getScaledX(40) , 0)
        }

        readonly property real obstacleHeight               :   gamePlayScene.height * obstacleHeight
        readonly property real obstacleWidth                :   gamePlayScene.width * obstacleWidth
        readonly property real asteroidDefaultSpawnPeriod   :   2000
        readonly property real asteroidVelocity             :   getAsteroidSpeed()
        readonly property real scoreLowerThreshold          :   400
        readonly property real scoreUpperThreshold          :   20000
        readonly property real asteroidDefaultVelocity      :   getScaledX(15)

        property real   doubleChancePortion                 :   {

            var portion
            if(sceneScore < scoreLowerThreshold)
                portion     =   0
            else
                portion     =   (sceneScore - scoreLowerThreshold) * 0.01

            if(portion > 0.5)
                portion     =   0.5

            return portion
        }

        property real   tripleChancePortion                 :   {
            var portion
            if(sceneScore < scoreLowerThreshold)
                portion     =   0
            else
                portion     =   (sceneScore - scoreLowerThreshold) * 0.002

            if(portion > 0.5)
                portion     =   0.5

            return portion
        }

        property int    sceneScore                          :   0

        function    getAsteroidSpeed(){

            var velocity;
            var plus;

            if(settings.score < scoreLowerThreshold){

                velocity    =   asteroidDefaultVelocity
                plus        =   0
            }
            else if(settings.score < scoreUpperThreshold){
                velocity    =   asteroidDefaultVelocity
                plus        =   (gamePlayScene.sceneScore - scoreLowerThreshold) * 0.01
            }

            plus            =   getScaledX(plus)
            velocity        +=  plus

            return -velocity
        }

        function    getAsteroidSpawnPeriod(){

            var period  =   2000
            var minus   =   0

            if(settings.score < scoreLowerThreshold){
                period  =   asteroidDefaultSpawnPeriod
                minus   =   0
            }
            else {
                period  =   asteroidDefaultSpawnPeriod
                minus   =   (gamePlayScene.sceneScore - scoreLowerThreshold) * 0.10
            }


            period  -=  minus


            return period
        }

        states      :   [
            State{
                name        :   "gameOver"
            }
        ]

        transitions :   [

            Transition {
                to          :   "gameOver"
                SequentialAnimation{
                    ScriptAction{
                        script  :   {
                            spaceShip.visible               =   false
                            rocketFlameParticleSys.visible  =   false
                            explosionEffect.x               =   spaceShip.x
                            explosionEffect.y               =   spaceShip.y + spaceShip.height / 2
                            explosionEffect.mainEmitter.burst(500)
                            explosionEffect.mainEmitter.emitRate    =   0
                            explosionSound.play()
                            sceneLoop.stop()
                            obstacleCreator.stop()

                            sceneTransitionTimer.start()
                        }
                    }
                }
            }
        ]

        Item    {
            id              :   rect
            width           :   parent.width * scoreWidth
            height          :   parent.height * scoreHeight
            anchors.right   :   parent.right
            anchors.top     :   parent.top

            Text {
                id      :   name
                text    :   "Skor : " + gamePlayScene.sceneScore
                font.pixelSize  :   parent.height * 0.3
                anchors.centerIn    :   parent
                color   :   "white"
            }

            z   :   2
        }

        Hero    {
            id      :   spaceShip
            x       :   parent.width * 0.1
            width   :   parent.width * 0.1
            height  :   width
            z       :   2
            behavior:   ScriptBehavior{
                script  :  {
                    if(gamePlayScene.state != "gameOver")
                        gamePlayScene.sceneScore++
                }
            }

            fixture.onBeginContact    :   {
                if(other.objectName == "obstacle"){
                    gamePlayScene.state = "gameOver"

                    spaceshipGame.gameOver(settings.score)
                }
            }

            NumberAnimation on y{
                duration    :   1000
            }
        }

        Keys.onUpPressed    :   shipUp(0.05)
        Keys.onDownPressed    :   shipUp(0.05)

        ImageLayer {
            id              :   imgLayer
            source          :   "/res/img/sky-1500.png"
            layerType       :   Layer.Infinite
            anchors.fill    :   parent
            behavior        :   ScrollBehavior{
                horizontalStep  :   getScaledX(-15)
            }
        }

        Column{

            visible     :   false

            Button{
                id      :   upButton
                width   :   150
                height  :   50
                text    :   "Yukarı"
                onClicked   :   {
                    shipUp(0.1)
                }
            }

            Button{
                id      :   downButton
                width   :   150
                height  :   50
                text    :   "Aşağı"
                onClicked   :   {
                    shipDown(0.1)
                }
            }

            Button{
                id      :   centerButton
                width   :   150
                height  :   50
                text    :   "Ortala"
                onClicked   :   {
                    setShipPos(0.5)
                }
            }

            Button{
                id      :   explodeButton
                width   :   150
                height  :   50
                text    :   "Patlat"
                onClicked   :   {
                    gamePlayScene.state = "gameOver"
                }
            }

            Button{
                id      :   fireButton
                width   :   150
                height  :   50
                text    :   "Ateş"
                onClicked   :   {
                    gamePlayScene.fire()
                }
            }
        }

        GroundLine{
            anchors.left    :   parent.left
            anchors.top     :   parent.top
            anchors.bottom  :   parent.bottom
        }

        SpaceshipBullet{
            id      :   bulletComponent
        }

        Obstacle{
            id      :   obstacle
        }

        ScriptBehavior{
            id      :   bulletBehaviour
            script  :   {

                if(gamePlayScene.state == "gameOver"){
                    target.destroy()
                    return
                }

                if(target.linearVelocity != getScaledX(40))
                    target.linearVelocity = Qt.point(getScaledX(40) , 0)

                if(target.x > gamePlayScene.width)
                    target.destroy()

            }
        }

        ScriptBehavior{
            id      :   obstacleBehaviour
            script  :   {
                if(gamePlayScene.state == "gameOver"){
                    target.destroy()
                    return
                }

                var newPos = target.x + gamePlayScene.asteroidVelocity
                target.x = newPos

                if(target.x < -target.width)
                    target.destroy()

            }
        }

        ScriptBehavior{
            id      :   scoreUpdate
            script  :   {
            }
        }

        RocketFlame{
            id                          :   rocketFlameParticleSys
            height                      :   spaceShip.height *   0.5
            width                       :   spaceShip.width  *   0.5
            anchors.right               :   spaceShip.left
            anchors.rightMargin         :   width * -0.7
            anchors.verticalCenter      :   spaceShip.verticalCenter
            flameAngle                  :   185
            z                           :   spaceShip.z + 1
        }


        ExplosionSprite{
            id              :   explosionEffect
            height          :   width
            width           :   spaceShip.width
            z               :   5
            running         :   true
        }

        ExplosionSprite{
            id              :   obstacleExplosion
            height          :   gamePlayScene.width * obstacleWidth
            width           :   gamePlayScene.height * obstacleHeight
            z               :   4
            running         :   true
        }

        Timer{
            id          :   obstacleCreator
            interval    :   gamePlayScene.getAsteroidSpawnPeriod()
            running     :   false
            repeat      :   false
            onTriggered    :   {
                if(spaceshipGame.gameState !== Bacon2D.Active){
                    return
                }

                var doubleChance    =       Math.random() < gamePlayScene.doubleChancePortion
                var tripleChance    =       Math.random() < gamePlayScene.tripleChancePortion
                var objCount        =       1

                if(tripleChance){
                    objCount        =   3
                    console.log("Triple Chance")
                }
                else if(doubleChance){
                    objCount        =   2

                    console.log("Double Chance")
                }


                for(var i = 0; i < objCount; i++){
                    var newObstacle = obstacle.createObject(gamePlayScene)

                    newObstacle.width = gamePlayScene.width * obstacleWidth
                    newObstacle.height = gamePlayScene.height * obstacleHeight
                    newObstacle.x = gamePlayScene.width
                    newObstacle.y = (Math.random() * (gamePlayScene.height - newObstacle.height))
                    newObstacle.z = 2
                    newObstacle.behavior = obstacleBehaviour
                    newObstacle.rotating = Qt.binding(function(){   return gamePlayScene.state != "gameOver"})
                }
            }
        }

        SoundEffect{
            id      :   explosionSound
            source  :   "/res/sound/explosion.wav"
        }

        SoundEffect{
            id      :   laserGunSound
            source  :   "/res/sound/laser-sound-2.wav"
        }

        SoundEffect{
            id      :   asteroidExplosionSound
            source  :   "/res/sound/explosion-asteroid-2.wav"
        }

        Timer{
            id          :   sceneTransitionTimer
            interval    :   2000
            onTriggered :   {
                spaceshipGame.pushScene(gameOverScene)
            }
        }

        Timer{
            id          :   sceneLoop
            interval    :   20
            running     :   true
            repeat      :   true
            onTriggered :   {
                //Engel zamanlayıcısı düzenleniyor
                if(!obstacleCreator.running){
                    obstacleCreator.interval    =   gamePlayScene.getAsteroidSpawnPeriod()
                    obstacleCreator.start()
                }
            }
        }

        Timer{
            id          :   cannonTimer
            interval    :   200
            running     :   true
            repeat      :   true
            onTriggered :   {

                if(gamePlayScene.state  === "gameOver")
                    return

                gamePlayScene.fire()
                //                if(!laserGunSound.playing)
                //                    laserGunSound.play()
            }
        }
    }

    Scene   {

        id          :   gameOverScene
        width       :   parent.width
        height      :   parent.height

        Image {
            id              :   background
            source              :   "/res/img/sky.png"
            fillMode            :   Image.Tile
            anchors.fill    :   parent
        }

        Text {
            id                  :   gameOverText
            text                :   "Skor\n" + settings.score
            horizontalAlignment :   Text.AlignHCenter
            anchors.centerIn    :   parent
            font.pixelSize      :   parent.height * 0.2
            color               :   "white"
            z                   :   2

            Behavior    on  x{
                NumberAnimation{
                    duration    :   500
                }
            }
        }
        
        Row {
            id                          :   buttonsRow
            anchors.horizontalCenter    :   gameOverText.horizontalCenter
            anchors.top                 :   gameOverText.bottom
            height                      :   gamePlayScene.height * buttonHeight

            LessonControlButton{
                id              :   restartButton
                width           :   gameOverScene.width * spaceshipGame.buttonWidth
                height          :   parent.height
                text            :   "Yeniden Başlat"
                fillColor       :   "transparent"
                stroke.color    :   "white"
                stroke.width    :   height * 0.035
                textColor       :   "white"
                onClicked       :   {
                    spaceshipGame.restart()
                }
            }
        }

        enterAnimation  :   ParallelAnimation   {
            ScaleAnimator {
                target      :   gameOverText
                from        :   0.6
                to          :   1
                duration    :   800
                easing.type      :   Easing.OutBounce
            }
        }
    }
}
