import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    id      :   effect

    ParticleSystem{
        id      :   sys
    }

    ImageParticle {
        source          : "/res/img/star-particle.png"
        color           :   "#11ff40ff"
        colorVariation  : 0.6
        system          :   sys
        anchors.fill    :   parent
    }

    Gravity{
        id          :   gravity
        angle       :   270
        magnitude   :   parent.height * 1.2
        system      :   sys
    }

    Emitter {
        id              :   fireEmitter
        anchors.centerIn:   parent
        system          :   sys
        lifeSpan        :   1000
        size            :   parent.height * 0.9
        sizeVariation   :   endSize
        emitRate        :   10
        velocity        :   AngleDirection{
            angle           :   0
            angleVariation  :   360
            magnitude       :   effect.height * 0.7
        }
    }
}
