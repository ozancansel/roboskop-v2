import QtQuick 2.7
import "template"
import "../model"
import "../theme"

Template25x25{

    property double ledVisualLineThickness      :   0.04
    property double ledCenterLineThickness      :   0.1

    mainText: "BUZZER"
    subText:  "HOPARLÖR"

    component:  Component{
        id : componentVisual

        Image{
//            source  :  "/res/img/buzzer-icon-512x512.png"
            source  :   "/res/img/robologo-512-.png"
            fillMode: Image.PreserveAspectFit
            scale   : 1
            smooth  :   true
            mipmap  :   true
        }
    }

    code            :   "#16p4"

    pins : ListModel{
        Component.onCompleted: {
            append( {"pinOutColor" : Color.red , "label" : "Güç"} )
            append( {"pinOutColor" : Color.black , "label" : "Toprak"} )
        }
    }
}
