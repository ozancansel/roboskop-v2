import QtQuick 2.0
import "template"
import "../model"
import "../theme"

Template25x25{

    property double ledVisualLineThickness      :   0.04
    property double ledCenterLineThickness      :   0.1

    mainText: "MİKROFON"
    subText:  "SES ALICISI"

    component:  Component{
        id : componentVisual

        Image{
            source  :   "/res/img/robologo-512-.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    code            :   "#16p1"

    pins : ListModel{
        Component.onCompleted: {
            append( {"pinOutColor" : Color.red , "label" : "Güç"} )
            append( {"pinOutColor" : Color.yellow , "label" : "Sinyal"} )
            append( {"pinOutColor" : Color.black , "label" : "Toprak"} )
        }
    }
}
