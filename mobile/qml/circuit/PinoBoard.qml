import QtQuick 2.0
import "../theme"

Item {

    readonly property real  xScaleFactor    :    width / 1280
    readonly property real  yScaleFactor    :    height / 720
    readonly property int   pinWidth        :   40
    readonly property int   digitalPinWidth :   170
    readonly property int   powerPinWidth   :   65

    readonly property int   lemon           :   0
    readonly property int   orange          :   1
    readonly property int   banana          :   2
    readonly property int   strawberry      :   3
    readonly property int   grape           :   4
    readonly property int   cherry          :   5
    readonly property int   power1          :   6
    readonly property int   power2          :   7
    readonly property int   power3          :   8
    readonly property int   power4          :   9
    readonly property int   ground1         :   10
    readonly property int   ground2         :   11
    readonly property int   ground3         :   12
    readonly property int   ground4         :   13

    readonly property variant   posMapping  :   [
        Qt.point(x + lemonIcon.x + lemonIcon.width / 2 , y + lemonIcon.y + lemonIcon.height / 2), //Lemon
        Qt.point(x + digitalPinsRow.x + orangePin.x + orangePin.width / 2 , y + digitalPinsRow.y + orangePin.y + orangePin.height), //Orange
        Qt.point(x + digitalPinsRow.x + bananaPin.x + bananaPin.width / 2 , y + digitalPinsRow.y + bananaPin.y + bananaPin.height), //Banana
        Qt.point(x + digitalPinsRow.x + strawberryPin.x + strawberryPin.width / 2 , y + digitalPinsRow.y + strawberryPin.y + strawberryPin.height) , //strawberry
        Qt.point(x + digitalPinsRow.x + grapePin.x + grapePin.width / 2 , y + digitalPinsRow.y + grapePin.y + grapePin.height),  //grape
        Qt.point(x + cherryIcon.x + cherryIcon.width/ 2 , y + cherryIcon.y + cherryIcon.height ) , //Cherry
        Qt.point(x + powerColumns.x + applePin1.x + applePin1.width , y + powerColumns.y + applePin1.y + powerPinColumn1.y + applePin1.height / 2), //Power1
        Qt.point(x + powerColumns.x + applePin2.x + applePin2.width , y + powerColumns.y + applePin2.y + powerPinColumn2.y + applePin2.height / 2), //Power2
        Qt.point(x + powerColumns.x + applePin3.x + applePin3.width , y + powerColumns.y + applePin3.y + powerPinColumn3.y + applePin3.height / 2), //Power3
        Qt.point(x + powerColumns.x + applePin4.x + applePin4.width , y + powerColumns.y + applePin4.y + powerPinColumn4.y + applePin4.height / 2), //Power4
        Qt.point(x + powerColumns.x + melonPin1.x + melonPin1.width , y + powerColumns.y + powerPinColumn1.y + melonPin1.y + melonPin1.height / 2), //Ground1
        Qt.point(x + powerColumns.x + melonPin2.x + melonPin2.width , y + powerColumns.y + powerPinColumn2.y + melonPin2.y + melonPin2.height / 2), //Ground2
        Qt.point(x + powerColumns.x + melonPin3.x + melonPin3.width , y + powerColumns.y + powerPinColumn3.y +melonPin3.y + melonPin3.height / 2), //Ground3
        Qt.point(x + powerColumns.x + melonPin4.x + melonPin4.width , y + powerColumns.y + powerPinColumn4.y + melonPin4.y + melonPin4.height / 2)   //Ground4
    ]

    function    getScaledX(xP){
        return xScaleFactor * xP
    }

    function    getScaledY(yP){
        return yScaleFactor * yP;
    }


    function getVerticalDiffAbs(otherComponent){
        //        return Math.abs(end().y - otherComponent.y - (1.5 * centerOffset))
        return Math.abs(end().y - otherComponent.y)
    }

    Item{
        anchors.fill    :   parent
        Rectangle{
            id              :   backRect
            anchors.fill    :   parent
            border.width    :   getScaledX(20)
            radius          :   getScaledY(50)
            z               :   2
            color           :   "transparent"
        }

        Image {
            id              :   pinoBackground
            source          :   "/res/img/pino-background.png"
            anchors.fill    :   parent
        }
    }

    Item{
        id              :   contentItem
        anchors.fill    :   parent
        z               :   2

        Image {
            id          :   cherryIcon
            source      :   "/res/img/fruits/cherry.png"
            width       :   getScaledX(digitalPinWidth)
            fillMode    :   Image.PreserveAspectFit
            x           :   getScaledX(420)
            y           :   getScaledY(60)
        }

        Item{
            id      :   btBackContainerItem
            width   :   getScaledX(80)
            height  :   getScaledY(200)
            x       :   getScaledX(100)
            y       :   getScaledY(270)

            Rectangle   {
                id              :   btPlaceRect
                anchors.fill    :   parent
                color           :   "#007fff"
                radius          :   getScaledY(4)
            }
        }

        Image {
            id          :   lemonIcon
            source      :   "/res/img/fruits/lemon.png"
            fillMode    :   Image.PreserveAspectFit
            width       :   getScaledX(200)
            anchors.top :   btBackContainerItem.bottom
            anchors.horizontalCenter    :   btBackContainerItem.horizontalCenter
        }

        Text {
            id                  :   topLeftText
            y                   :   getScaledY(80)
            x                   :   getScaledX(40)
            text                :   "4-9 Yaş\nÖzel Üretim"
            font.pixelSize      :   getScaledY(35)
            rotation            :   -45
            font.family         :   FontCollection.aclonicaFont
            verticalAlignment   :   Text.AlignVCenter
            horizontalAlignment :   Text.AlignHCenter
        }

        Text {
            id                  :   roboskopText
            text                :   qsTr("www.roboskop.com")
            font.pixelSize      :   getScaledY(30)
            x                   :   getScaledX(640)
            y                   :   getScaledY(70)
            font.family         :   FontCollection.unitedKingdomFont
        }

        Row{
            id                  :   digitalPinsRow
            x                   :   getScaledX(400)
            y                   :   getScaledY(550)
            Image {
                id                  :   orangePin
                source              :   "/res/img/fruits/orange.png"
                width               :   getScaledX(digitalPinWidth)
                height              :   width
                fillMode            :   Image.PreserveAspectFit
            }

            Image {
                id                  :   bananaPin
                source              :   "/res/img/fruits/banana.png"
                width               :   getScaledX(digitalPinWidth)
                height              :   width
                fillMode            :   Image.PreserveAspectFit
            }

            Image {
                id                  :   strawberryPin
                source              :   "/res/img/fruits/strawberry.png"
                width               :   getScaledX(digitalPinWidth)
                height              :   width
                fillMode            :   Image.PreserveAspectFit
            }

            Image {
                id                  :   grapePin
                source              :   "/res/img/fruits/grape.png"
                width               :   getScaledX(digitalPinWidth)
                height              :   width
                fillMode            :   Image.PreserveAspectFit
            }
        }

        Image {
            id              :   pinoLogo
            source          :   "/res/img/pino-logo.png"
            width           :   getScaledX(400)
            fillMode        :   Image.PreserveAspectFit
            x               :   getScaledX(450)
            y               :   getScaledY(200)
        }

        Column  {
            id                      :   powerColumns
            x                       :   getScaledX(1100)
            y                       :   getScaledY(120)
            spacing                 :   getScaledY(20)
            height                  :   childrenRect.height

            Column {
                id                      :   powerPinColumn1
                height                  :   childrenRect.height
                Image {
                    id                  :   applePin1
                    source              :   "/res/img/fruits/apple.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                    scale               :   1.25
                }

                Image {
                    id                  :   melonPin1
                    source              :   "/res/img/fruits/melon.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                }
            }


            Column {
                id                      :   powerPinColumn2
                height                  :   childrenRect.height
                Image {
                    id                  :   applePin2
                    source              :   "/res/img/fruits/apple.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                    scale               :   1.25
                }

                Image {
                    id                  :   melonPin2
                    source              :   "/res/img/fruits/melon.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                }
            }


            Column {

                id                      :   powerPinColumn3
                height                  :   childrenRect.height

                Image {
                    id                  :   applePin3
                    source              :   "/res/img/fruits/apple.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                    scale               :   1.25
                }

                Image {
                    id                  :   melonPin3
                    source              :   "/res/img/fruits/melon.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                }
            }


            Column {
                id                      :   powerPinColumn4
                height                  :   childrenRect.height
                Image {
                    id                  :   applePin4
                    source              :   "/res/img/fruits/apple.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                    scale               :   1.25
                }

                Image {
                    id                  :   melonPin4
                    source              :   "/res/img/fruits/melon.png"
                    width               :   getScaledX(powerPinWidth)
                    height              :   width
                    fillMode            :   Image.PreserveAspectFit
                }
            }
        }
    }
}
