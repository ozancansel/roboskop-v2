import QtQuick 2.0

Rectangle{

    //Width
    readonly property real ledContainerWidth            :   0.2
    readonly property real ledHeaderContainerWidth      :   0.85

    //Height
    readonly property real ledContainerHeight           :   0.4
    readonly property real ledMiddleLineHeight          :   0.14

    //radius
    readonly property real ledMiddleLineRadius          :   0.2
    readonly property real ledLegsRadius                :   0.3

    //Length
    readonly property real ledHeaderHeight              :   0.56

    property alias ledHeaderFillColor                   :   ledWhiteSpace.color
    property alias ledHeaderStrokeColor                 :   ledHead.color
    property alias ledLeftLegColor                      :   leftLeg.color
    property alias ledRightLegColor                     :   rightLeg.color

    color           :   "transparent"

    Rectangle{

        id      :   ledHeaderContainer
        width   :   parent.width * ledHeaderContainerWidth
        height  :   width * ledHeaderHeight
        clip    :   true
        anchors.horizontalCenter        :   parent.horizontalCenter
        color   :   "transparent"

        Rectangle{
            id                          :   ledHead
            width                       :   parent.width
            height                      :   width
            anchors.horizontalCenter    :   parent.horizontalCenter
            color                       :   "black"
            clip                        :   true
            radius                      :   width * 0.5

            Rectangle{
                id                      :   ledWhiteSpace
                anchors.fill            :   parent
                anchors.margins         :   parent.width * 0.13
                radius                  :   width * 0.5
            }
        }
    }

    Rectangle{
        id              :   ledMiddleHorizontalLine
        anchors.top     :   ledHeaderContainer.bottom
        anchors.left    :   parent.left
        anchors.right   :   parent.right
        height          :   parent.height * ledMiddleLineHeight
        color           :   "black"
        radius          :   height * ledMiddleLineRadius
        z               :   2
    }

    Rectangle{
        id                  :   leftLeg
        anchors.left        :   ledHeaderContainer.left
        anchors.leftMargin  :   width
        anchors.top         :   ledMiddleHorizontalLine.bottom
        anchors.topMargin   :   -radius
        anchors.bottom      :   parent.bottom
        width               :   (ledHead.width - ledWhiteSpace.width) * 0.7
        radius              :   width * 0.3
        color               :   "black"
    }

    Rectangle{
        id                      :   rightLeg
        anchors.right           :   ledHeaderContainer.right
        anchors.rightMargin     :   width
        anchors.top             :   ledMiddleHorizontalLine.bottom
        anchors.topMargin       :   -radius
        anchors.bottom          :   parent.bottom
        width                   :   (ledHead.width - ledWhiteSpace.width) * 0.7
        color                   :   "black"
        radius                  :   width * 0.3
    }
}
