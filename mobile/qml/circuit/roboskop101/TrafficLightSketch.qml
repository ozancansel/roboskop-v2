import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(0)
    readonly property real wireThickness    :   getScaledX(3.5)

    ArduinoUno{
        id          :   uno
        x           :   getScaledX(50) + slideX
        y           :   getScaledY(350) + slideY
        width       :   getScaledX(380)
        height      :   getScaledY(250)
    }

    TrafficLightKit{
        id          :   trafficLightKit
        x           :   getScaledX(190) + slideX
        y           :   getScaledY(10) + slideY
        width       :   getScaledX(190)
        height      :   getScaledY(190 * 1.4)
    }

    BluetoothKit{
        id          :   bluetoothKit
        x           :   getScaledX(450) + slideX
        y           :   getScaledY(30 + slideY)
        width       :   getScaledX(150)
        height      :   getScaledY(220)
    }


    //pin11'den trafik lambası kırmızıya

    Wire{
        id          :   pin11ToRedPin1
        alignment   :   Qt.Vertical
        length      :   getScaledY(80)
        centerX     :   uno.getDigitalPinPos(11).x
        centerY     :   uno.getDigitalPinPos(11).y
        z           :   4
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   pin11ToRedPin3
        alignment   :   Qt.Vertical
        length      :   pin11ToRedPin1.getVerticalDiffAbs(trafficLightKit.getPinPos(0))
        centerX     :   trafficLightKit.getPinPos(0).x
        centerY     :   trafficLightKit.getPinPos(0).y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   pin11ToRedPin2
        alignment   :   Qt.Horizontal
        length      :   pin11ToRedPin1.getHorizontalDiffAbs(pin11ToRedPin3.end())
        centerX     :   pin11ToRedPin1.end().x
        centerY     :   pin11ToRedPin1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    //pin11 trafik lambası bitiş

    //pin10'dan trafik lambası sarıya
    Wire{
        id          :   pin10ToYellowPin1
        alignment   :   Qt.Vertical
        length      :   getScaledY(70)
        centerX     :   uno.getDigitalPinPos(10).x
        centerY     :   uno.getDigitalPinPos(10).y
        z           :   4
        wireColor   :   Color.yellow
        thickness   :   wireThickness
        reverse     :   true
    }


    Wire{
        id          :   pin10ToYellowPin3
        alignment   :   Qt.Vertical
        length      :   pin10ToYellowPin1.getVerticalDiffAbs(trafficLightKit.getPinPos(1))
        centerX     :   trafficLightKit.getPinPos(1).x
        centerY     :   trafficLightKit.getPinPos(1).y
        z           :   4
        wireColor   :   Color.yellow
        thickness   :   wireThickness
    }

    Wire{
        id          :   pin10ToYellowPin2
        alignment   :   Qt.Horizontal
        length      :   pin10ToYellowPin1.getHorizontalDiffAbs(pin10ToYellowPin3.end())
        centerX     :   pin10ToYellowPin1.end().x
        centerY     :   pin10ToYellowPin1.end().y
        z           :   4
        wireColor   :   Color.yellow
        thickness   :   wireThickness
    }

    //pin10 bitiş

    //pin9'dan trafik lambası yeşile

    Wire{
        id          :   pin10ToGreenPin1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(9).x
        centerY     :   uno.getDigitalPinPos(9).y
        z           :   4
        wireColor   :   Color.green
        thickness   :   wireThickness
        reverse     :   true
    }


    Wire{
        id          :   pin10ToGreenPin3
        alignment   :   Qt.Vertical
        length      :   pin10ToGreenPin1.getVerticalDiffAbs(trafficLightKit.getPinPos(2))
        centerX     :   trafficLightKit.getPinPos(2).x
        centerY     :   trafficLightKit.getPinPos(2).y
        z           :   4
        wireColor   :   Color.green
        thickness   :   wireThickness
    }

    Wire{
        id          :   pin10ToGreenPin2
        alignment   :   Qt.Horizontal
        length      :   pin10ToGreenPin1.getHorizontalDiffAbs(pin10ToGreenPin3.end())
        centerX     :   pin10ToGreenPin1.end().x
        centerY     :   pin10ToGreenPin1.end().y
        z           :   4
        wireColor   :   Color.green
        thickness   :   wireThickness
    }

    //pin9'dan trafik lambası yeşile bitiş ------

    //Arduino gnd'den trafik lambası gnd'ye

    Wire{
        id          :   gndToTrafficLightGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(40)
        centerX     :   uno.getDigitalPinPos(14).x
        centerY     :   uno.getDigitalPinPos(14).y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
        reverse     :   true
    }


    Wire{
        id          :   gndToTrafficLightGnd3
        alignment   :   Qt.Vertical
        length      :   gndToTrafficLightGnd1.getVerticalDiffAbs(trafficLightKit.getPinPos(3))
        centerX     :   trafficLightKit.getPinPos(3).x
        centerY     :   trafficLightKit.getPinPos(3).y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   gndToTrafficLightGnd2
        alignment   :   Qt.Horizontal
        length      :   gndToTrafficLightGnd1.getHorizontalDiffAbs(gndToTrafficLightGnd3.end())
        centerX     :   gndToTrafficLightGnd1.end().x
        centerY     :   gndToTrafficLightGnd1.end().y
        z           :   2
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    //Arduino tx -> bluetooth->rx

    Wire{
        id          :   txToRx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(1).x
        centerY     :   uno.getDigitalPinPos(1).y
        z           :   4
        wireColor   :   Color.green
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   txToRx3
        alignment   :   Qt.Vertical
        length      :   txToRx1.getVerticalDiffAbs(bluetoothKit.getPinPos(0))
        centerX     :   bluetoothKit.getPinPos(0).x
        centerY     :   bluetoothKit.getPinPos(0).y
        z           :   4
        wireColor   :   Color.green
        thickness   :   wireThickness
    }

    Wire{
        id          :   txToRx2
        alignment   :   Qt.Horizontal
        length      :   txToRx1.getHorizontalDiffAbs(txToRx3.end())
        centerX     :   txToRx1.end().x
        centerY     :   txToRx1.end().y
        z           :   2
        wireColor   :   Color.green
        thickness   :   wireThickness
    }

    //Arduino tx -> bluetooth->rx bitiş

    //Arduino rx -> bluetooth->tx

    Wire{
        id          :   rxToTx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getDigitalPinPos(0).x
        centerY     :   uno.getDigitalPinPos(0).y
        z           :   4
        wireColor   :   Color.blue
        thickness   :   wireThickness
        reverse     :   true
    }


    Wire{
        id          :   rxToTx3
        alignment   :   Qt.Vertical
        length      :   rxToTx1.getVerticalDiffAbs(bluetoothKit.getPinPos(1))
        centerX     :   bluetoothKit.getPinPos(1).x
        centerY     :   bluetoothKit.getPinPos(1).y
        z           :   4
        wireColor   :   Color.blue
        thickness   :   wireThickness
    }

    Wire{
        id          :   rxToTx2
        alignment   :   Qt.Horizontal
        length      :   rxToTx1.getHorizontalDiffAbs(rxToTx3.end())
        centerX     :   rxToTx1.end().x
        centerY     :   rxToTx1.end().y
        z           :   2
        wireColor   :   Color.blue
        thickness   :   wireThickness
    }

    //Arduino rx -> bluetooth->tx bitiş

    //Arduino gnd -> bluetooth->gnd başlangıç

    Wire{
        id          :   ardGndToBtGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(27)
        centerX     :   uno.getPowerPinPos(4).x
        centerY     :   uno.getPowerPinPos(4).y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
    }



    Wire{
        id          :   ardGndToBtGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToBtGnd1.getVerticalDiffAbs(bluetoothKit.getPinPos(2))
        centerX     :   bluetoothKit.getPinPos(2).x
        centerY     :   bluetoothKit.getPinPos(2).y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
    }


    Wire{
        id          :   ardGndToBtGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToBtGnd1.getHorizontalDiffAbs(ardGndToBtGnd3.end())
        centerX     :   ardGndToBtGnd1.end().x
        centerY     :   ardGndToBtGnd1.end().y
        z           :   2
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    //Arduino gnd -> bluetooth->gnd başlangıç

    //Arduino 5v -> bluetooth->5v başlangıç


    Wire{
        id          :   ard5vToBtPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(47)
        centerX     :   uno.getPowerPinPos(3).x
        centerY     :   uno.getPowerPinPos(3).y
        z           :   4
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard5vToBtPower3
        alignment   :   Qt.Vertical
        length      :   ard5vToBtPower1.getVerticalDiffAbs(bluetoothKit.getPinPos(3))
        centerX     :   bluetoothKit.getPinPos(3).x
        centerY     :   bluetoothKit.getPinPos(3).y
        z           :   4
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard5vToBtPower2
        alignment   :   Qt.Horizontal
        length      :   ard5vToBtPower1.getHorizontalDiffAbs(ard5vToBtPower3.end())
        centerX     :   ard5vToBtPower1.end().x
        centerY     :   ard5vToBtPower1.end().y
        z           :   2
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    //Arduino 5v -> bluetooth->5v başlangıç
}
