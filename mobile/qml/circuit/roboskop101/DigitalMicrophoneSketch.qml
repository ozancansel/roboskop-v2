import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real wireThickness    :   getScaledX(3.2)

    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(0)

    ArduinoUno{
        id      :   uno
        x       :   getScaledX(0) + slideX
        y       :   getScaledY(320) + slideY
        width   :   getScaledX(380)
        height  :   getScaledY(250)
    }


    BluetoothKit{
        id      :   bluetoothKit
        x       :   getScaledX(490) + slideX
        y       :   getScaledY(10) + slideY
        width   :   getScaledX(150)
        height  :   getScaledY(150 * (40 / 25))
    }

    MicrophoneKit{
        id      :   microphoneKit
        x       :   getScaledX(150) + slideX
        y       :   getScaledY(20) + slideY
        width   :   getScaledX(200)
        height  :   getScaledY(200)
    }

    //Ard 5v -> Mic power

    Wire{
        id          :   ard5vToMicPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(55)
        centerX     :   uno.getPowerPinPos(3).x
        centerY     :   uno.getPowerPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard5vToMicPower2
        alignment   :   Qt.Horizontal
        length      :   getScaledX(260)
        centerX     :   ard5vToMicPower1.end().x
        centerY     :   ard5vToMicPower1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ard5vToMicPower3
        alignment   :   Qt.Vertical
        length      :   getScaledY(370)
        centerX     :   ard5vToMicPower2.end().x
        centerY     :   ard5vToMicPower2.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ard5vToMicPower5
        alignment   :   Qt.Vertical
        length      :   ard5vToMicPower3.getVerticalDiffAbs(microphoneKit.getPinPos(0))
        centerX     :   microphoneKit.getPinPos(0).x
        centerY     :   microphoneKit.getPinPos(0).y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard5vToMicPower4
        alignment   :   Qt.Horizontal
        length      :   ard5vToMicPower3.getHorizontalDiffAbs(ard5vToMicPower5.end())
        centerX     :   ard5vToMicPower3.end().x
        centerY     :   ard5vToMicPower3.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    //Ard 5v -> Mic power bitiş

    //Ard A0 -> Mic Signal

    Wire{
        id          :   ardA0ToMicSignal1
        alignment   :   Qt.Vertical
        length      :   getScaledY(35)
        centerX     :   uno.getAnalogPinPos(0).x
        centerY     :   uno.getAnalogPinPos(0).y
        wireColor   :   Color.yellow
        thickness   :   wireThickness
    }

    Wire{
        id          :   ardA0ToMicSignal2
        alignment   :   Qt.Horizontal
        length      :   getScaledX(320)
        centerX     :   ardA0ToMicSignal1.end().x
        centerY     :   ardA0ToMicSignal1.end().y
        wireColor   :   Color.yellow
        thickness   :   wireThickness
        reverse     :true
    }

    Wire{
        id          :   ardA0ToMicSignal3
        alignment   :   Qt.Vertical
        length      :   getScaledY(330)
        centerX     :   ardA0ToMicSignal2.end().x
        centerY     :   ardA0ToMicSignal2.end().y
        wireColor   :   Color.yellow
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ardA0ToMicSignal5
        alignment   :   Qt.Vertical
        length      :   ardA0ToMicSignal3.getVerticalDiffAbs(microphoneKit.getPinPos(1))
        centerX     :   microphoneKit.getPinPos(1).x
        centerY     :   microphoneKit.getPinPos(1).y
        wireColor   :   Color.yellow
        thickness   :   wireThickness
    }

    Wire{
        id          :   ardA0ToMicSignal4
        alignment   :   Qt.Horizontal
        length      :   ardA0ToMicSignal3.getHorizontalDiffAbs(ardA0ToMicSignal5.end())
        centerX     :   ardA0ToMicSignal3.end().x
        centerY     :   ardA0ToMicSignal3.end().y
        wireColor   :   Color.yellow
        thickness   :   wireThickness
    }

    //Ard A0 -> Mic Signal bitiş

    //Ard GND -> Mic Gnd

    Wire{
        id          :   ardGndToMicGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(55)
        centerX     :   uno.getDigitalPinPos(14).x
        centerY     :   uno.getDigitalPinPos(14).y
        wireColor   :   Color.black
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ardGndToMicGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToMicGnd1.getVerticalDiffAbs(microphoneKit.getPinPos(2))
        centerX     :   microphoneKit.getPinPos(2).x
        centerY     :   microphoneKit.getPinPos(2).y
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   ardGndToMicGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToMicGnd1.getHorizontalDiffAbs(ardGndToMicGnd3.end())
        centerX     :   ardGndToMicGnd1.end().x
        centerY     :   ardGndToMicGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    //Ard GND -> Mic Gnd Bitiş

    //Bt Rx ->Uno Tx

    Wire{
        id          :   ardTxToBtRx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(1).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.green
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx3
        alignment   :   Qt.Vertical
        length      :   ardTxToBtRx1.getVerticalDiffAbs(bluetoothKit.getPinPos(0))
        centerX     :   bluetoothKit.getPinPos(0).x
        centerY     :   bluetoothKit.getPinPos(0).y
        wireColor   :   Color.green
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx2
        alignment   :   Qt.Horizontal
        length      :   ardTxToBtRx1.getHorizontalDiffAbs(ardTxToBtRx3.end())
        centerX     :   ardTxToBtRx1.end().x
        centerY     :   ardTxToBtRx1.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness


    }

    //Bt Rx ->Uno Tx Bitiş

    //Uno Rx -> Bt Tx

    Wire{
        id          :   ardRxToBtTx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getDigitalPinPos(0).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.blue
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx3
        alignment   :   Qt.Vertical
        length      :   ardRxToBtTx1.getVerticalDiffAbs(bluetoothKit.getPinPos(1))
        centerX     :   bluetoothKit.getPinPos(1).x
        centerY     :   bluetoothKit.getPinPos(1).y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx2
        alignment   :   Qt.Horizontal
        length      :   ardRxToBtTx1.getHorizontalDiffAbs(ardRxToBtTx3.end())
        centerX     :   ardRxToBtTx1.end().x
        centerY     :   ardRxToBtTx1.end().y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    //Uno Rx -> Bt Tx bitiş


    //Uno gnd -> Bt gnd

    Wire{
        id          :   ardGndToBtGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(55)
        centerX     :   uno.getPowerPinPos(4).x
        centerY     :   uno.getPowerPinPos(4).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToBtGnd1.getVerticalDiffAbs(bluetoothKit.getPinPos(2))
        centerX     :   bluetoothKit.getPinPos(2).x
        centerY     :   bluetoothKit.getPinPos(2).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToBtGnd1.getHorizontalDiffAbs(ardGndToBtGnd3.end())
        centerX     :   ardGndToBtGnd1.end().x
        centerY     :   ardGndToBtGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    //Uno gnd -> Bt gnd bitiş

    //Uno 3.3 -> Bt güç

    Wire{
        id          :   ard33VToBtPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(65)
        centerX     :   uno.getPowerPinPos(2).x
        centerY     :   uno.getPowerPinPos(2).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard33VToBtPower3
        alignment   :   Qt.Vertical
        length      :   ard33VToBtPower1.getVerticalDiffAbs(bluetoothKit.getPinPos(3))
        centerX     :   bluetoothKit.getPinPos(3).x
        centerY     :   bluetoothKit.getPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard33VToBtPower2
        alignment   :   Qt.Horizontal
        length      :   ard33VToBtPower1.getHorizontalDiffAbs(ard33VToBtPower3.end())
        centerX     :   ard33VToBtPower1.end().x
        centerY     :   ard33VToBtPower1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    //Uno 3.3 -> Bt güç bitiş

}
