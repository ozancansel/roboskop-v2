pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.0

QtObject {

    readonly property var lessonMapping : [{ } ,
                               /*Lesson 1*/     {  "sketch" : "/qml/circuit/roboskop101/LedIntroductionSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/EmptyControl.qml" ,
                                                    "id"        :   1   },     //"/qml/circuit/roboskop101/control/SpaceshipWithPotGame.qml"},
                                /*Lesson 2*/     {  "sketch"    :   "/qml/circuit/roboskop101/LedBlinkSketch.qml",
                                                    "control"   :   "/qml/circuit/roboskop101/control/LedBlinkControl.qml",
                                                    "id"        :   2   } ,
                                /*Lesson 3*/     {  "id"        :   3,
                                                    "sketch"    :   "/qml/circuit/roboskop101/TrafficLightSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/TrafficLightControl.qml" } ,
                                /*Lesson 4*/    {   "id"        :   4 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/PotSignalSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/PotReadControl.qml"} ,
                                /*Lesson 5*/    {   "id"        :   5,
                                                    "sketch"    :   "/qml/circuit/roboskop101/LdrSignalSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/LdrReadControl.qml"  }   ,
                                /*Lesson 6*/    {   "id"        :   6 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/RgbLedSketch.qml"    ,
//                                                  "sketch"    :   "/qml/circuit/preschool/RgbLedSketch.qml"    ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/RgbControl.qml"}   ,
                                /*Lesson 7*/    {   "id"        :   7 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/RgbLedAndPotSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/RgbControl.qml" ,
                                                    "conf"      :   {   "gameDisabled"  :   true , "withPot" : true }} ,
                                /*Lesson 8*/    {   "id"        :   8,
                                                    "sketch"    :   "/qml/circuit/roboskop101/RgbLedAndLdrSketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/RgbControl.qml"  ,
                                                    "conf"      :   {   "gameDisabled"  :   true , "withLdr"  : true}} ,
                                /*Lesson 9*/    {   "id"        :   9 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/RoboticMusicSketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/PianoControl.qml" ,
                                                    "conf"      :   {   "melody"    :   "none"  }} ,
                                /*Lesson 10*/   {   "id"        :   10,
                                                    "sketch"    :   "/qml/circuit/roboskop101/RoboticMusicSketch.qml"    ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/PianoControl.qml" ,
                                                    "conf"      :   {   "melody"    :   "jingleBells"}} ,
                                /*Lesson 11*/   {   "id"        :   11 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/AcousticRulerSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/AcousticDistanceControl.qml"  } ,
                                /*Lesson 12*/   {   "id"        :   12 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/ParkSensorSketch.qml"    ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/AcousticDistanceControl.qml"  ,
                                                    "conf"      :   {   "gameDisabled"  :   true , "buzzerEnabled" : true } }   ,
                                /*Lesson 13*/   {   "id"        :   13 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/Lesson13Sketch.qml",
                                                    "control"   :   "/qml/circuit/roboskop101/control/SpaceshipWithPotGame.qml"  }   ,
                                /*Lesson 14*/   {   "id"        :   14 ,
                                                    "sketch"    :   "/qml/circuit/roboskop101/DigitalMicrophoneSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/MicrophoneReadControl.qml",
                                                    "conf"      :   {   "faceDisabled"  :   true}
                                                }   ,
                                /*Lesson 15*/   {   "id"        :   15,
                                                    "sketch"    :   "/qml/circuit/roboskop101/RgbLedAndMicSketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/MicrophoneReadControl.qml"  }   ,
                                /*Lesson 16*/   {   "sketch"    :   "/qml/circuit/roboskop101/EmptySketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/EmptyControl.qml"}
    ]

    function    getSketchPath(idx){
        return lessonMapping[idx].sketch
    }

    function    getInteractivePath(index){
        console.log(lessonMapping[index])
        return lessonMapping[index].control
    }

    function    getConf(index){
        return lessonMapping[index].conf
    }

    function    getId(index){
        return lessonMapping[index].id
    }

}
