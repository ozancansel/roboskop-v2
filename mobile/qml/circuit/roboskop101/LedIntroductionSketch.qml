import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real wireThickness    :   getScaledX(3.2)

    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(0)

    ArduinoUno{
        id      :   uno
        x       :   getScaledX(50) + slideX
        y       :   getScaledY(310) + slideY
        width   :   getScaledX(380)
        height  :   getScaledY(250)



    }

    LedKit{
        id      :   led
        x       :   getScaledX(165) + slideX
        y       :   getScaledY(50) + slideY
        width   :   getScaledX(200)
        height  :   getScaledY(200)
    }

    Wire{
        id                  :   fiveVoltToLed1
        alignment           :   Qt.Vertical
        length              :   getScaledY(65)
        centerX             :   uno.getPowerPinPos(3).x
        centerY             :   uno.getPowerPinPos(3).y
        rotation            :   180
        z                   :   20
        wireColor           :   Color.red
        thickness           :   wireThickness
    }

    Wire {
        id                  :   fiveVoltToLed2
        alignment           :   Qt.Horizontal
        length              :   getScaledX(233)
        centerX             :   fiveVoltToLed1.end().x
        centerY             :   fiveVoltToLed1.end().y
        reverse             :   true
        wireColor           :   Color.red
        thickness           :   wireThickness
        z                   :   19
    }

    Wire {
        id                  :   fiveVoltToLed3
        alignment           :   Qt.Vertical
        length              :   getScaledY(337)
        centerX             :   fiveVoltToLed2.end().x
        centerY             :   fiveVoltToLed2.end().y
        reverse             :   true
        wireColor           :   Color.red
        thickness           :   wireThickness
        z                   :   18
    }

    Wire{
        id                  :   fiveVoltToLed5
        alignment           :   Qt.Vertical
        length              :   fiveVoltToLed3.getVerticalDiffAbs(led.getPinPos(0))
        centerX             :   led.getPinPos(0).x
        centerY             :   led.getPinPos(0).y
        wireColor           :   Color.red
        thickness           :   wireThickness
        z                   :   17
    }


    Wire{
        id                  :   fiveVoltToLed4
        alignment           :   Qt.Horizontal
        length              :   fiveVoltToLed3.getHorizontalDiffAbs(fiveVoltToLed5.end())
        centerX             :   fiveVoltToLed3.end().x
        centerY             :   fiveVoltToLed3.end().y
        wireColor           :   Color.red
        thickness           :   wireThickness
        z                   :   17
    }

    Wire{
        id                  :   gndToLed1
        alignment           :   Qt.Vertical
        length              :   getScaledY(65)
        centerX             :   uno.getPowerPinPos(5).x
        centerY             :   uno.getPowerPinPos(5).y
        wireColor           :   Color.black
        thickness           :   wireThickness
        z                   :   5
    }

    Wire{
        id                  :   gndToLed2
        alignment           :   Qt.Horizontal
        length              :   getScaledX(190)
        centerX             :   gndToLed1.end().x
        centerY             :   gndToLed1.end().y
        wireColor           :   Color.black
        thickness           :   wireThickness
        z                   :   20
    }

    Wire{
        id                  :   gndToLed3
        alignment           :   Qt.Vertical
        length              :   getScaledY(337)
        centerX             :   gndToLed2.end().x
        centerY             :   gndToLed2.end().y
        wireColor           :   Color.black
        thickness           :   wireThickness
        z                   :   20
        reverse             :   true
    }


    Wire{
        id                  :   gndToLed5
        alignment           :   Qt.Vertical
        length              :   gndToLed3.getVerticalDiffAbs(led.getPinPos(1))
        centerX             :   led.getPinPos(1).x
        centerY             :   led.getPinPos(1).y
        wireColor           :   Color.black
        thickness           :   wireThickness
        z                   :   18
    }

    Wire{
        id                  :   gndToLed4
        alignment           :   Qt.Horizontal
        length              :   gndToLed3.getHorizontalDiffAbs(gndToLed5.end())
        centerX             :   gndToLed3.end().x
        centerY             :   gndToLed3.end().y
        wireColor           :   Color.black
        thickness           :   wireThickness
        z                   :   19
        reverse             :   true
    }


}
