import QtQuick 2.0
import Roboskop 1.0
import "../../../chart"
import "../../../controls"
import "../../../theme"
import "../../../parts"
import "../../template"

Item {

    property    variant     values          :   []
    readonly property int   maxCache        :   15
    property real           deviation       :   0
    property bool           faceDisabled    :   false
    property bool           exampleStarted  :   false
    property bool           readActive      :   false

    id          :   ctl

    function    addToValues(val){

        if(values.length > 14){
            values.shift()
        }

        values.push(val)
    }

    function    startExample(){
        exampleStarted  =   true
        readActive = true
    }

    function    stopExample(){
        exampleStarted  =   false
        readActive = false
    }

    ArduinoProxy{
        id      :   proxy
        socket  :   bluetooth
    }

    ArduinoConstants{
        id      :   constants
    }

    AnalogReadTemplate{
        id                  :   chart
        anchors.fill        :   parent

        chart.title         :   "Mikrofon Sinyal Ölçüm"
        axisY.titleText     :   "Desibel (db)"
        axisY.tickCount     :   10
        splineSeries.name   :   "Mic"
        visible             :   true

        Component.onCompleted   :   {
            axisY.applyNiceNumbers()
        }
    }

    SmileyFace  {
        id                  :   smiley
        anchors.centerIn    :   parent
        width               :   parent.height * 0.6
        height              :   width
        vibrationEnabled    :   true
        visible             :   !faceDisabled
        emotionalState      :   {
            if(!exampleStarted)
                return smiley.sleeping
            if( deviation < 20 )
                return smiley.fine
            else if(deviation > 20 && deviation < 50)
                return smiley.medium
            else
                return smiley.angry
        }
    }

    RStatistics{
        id      :   statistics
    }

//    Timer{
//        id          :   mock
//        running     :   false
//        interval    :   50
//        repeat      :   true
//        onTriggered :   {


//            addToValues(Math.floor(Math.random() * 400))
//            deviation = statistics.calculateStdDeviation(values)
//            console.log(deviation)
//        }
//    }

    Timer{
        id          :   analogTimer
        running     :   readActive
        repeat      :   true
        interval    :   70
        onTriggered :   {
            var val = proxy.analogRead(constants.a0)
            chart.splineSeries.newPoint(val)

            addToValues(val)
            deviation = statistics.calculateStdDeviation(values)
        }
    }
}
