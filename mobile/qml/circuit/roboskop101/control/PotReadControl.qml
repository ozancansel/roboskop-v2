import QtQuick 2.0
import QtQuick.Controls 2.1
import QtCharts 2.1
import QtMultimedia 5.0
import Roboskop 1.0
import "../../../chart"
import "../../../controls"
import "../../../theme"
import "../../template"
import "../../../enum"
import "../../../../"
import "../../../dialog"

Rectangle{

    readonly property real chartWidth               :   0.6

    readonly property real controlButtonHeight      :   0.08
    readonly property real currentStateTextHeight   :   0.05
    readonly property real controlSpacing           :   0.02
    readonly property real widthPortion             :   0.16
    readonly property real heightPortion            :   0.23
    readonly property real   textHeight             :   0.4
    readonly property real moveStep                 :   potReadCont.width * 0.022
    property real   splinePosX                      :   chart.width + chart.x
    property real   splinePosY                      :   0
    property variant rectangles                     :   []
    property bool   gameIsRunning                   :   false
    property int    score                           :   0
    property int    remainingTime                   :   60

    readonly property variant   lineColors  :   [   Color.green , Color.pink , Color.yellow]

    property bool isReading                :   false

    signal newScore(int score)

    function    startExample(){
        startToRead()
        startGame()
    }

    function    stopExample(){
        stopToRead()
        if(gameIsRunning)
            stopGame()
    }

    function    startToRead(){
        startGame()
    }

    function    stopToRead(){

    }

    function    startGame(){
        gameIsRunning = true
        score           =   0
        remainingTime   =   60
    }

    function   stopGame(){

        if(!gameIsRunning)
            return

        gameIsRunning   =   false

        for(var i = 0; i <rectangles.length; i++)
            rectangles.pop().visible = false

        endSound.play()
        newScore(score)
        gameEndDialog.open()
        InternalNotifyEnum.setRunning(false)
    }

    function    addScore(){
        congratSound.play()
//        congratSound.muted
        score += 1;
        chart.splineColor   =   lineColors[score % lineColors.length]
    }

    function    contains(rect , point){
        if(rect.x <= point.x && rect.x + rect.width >= point.x && rect.y < point.y && rect.y + rect.height >= point.y)
            return true
        else
            return false
    }

    function    calculateYPos(portion){
        var topMargin   =   chart.height * 0.22
        var plotHeight  =   chart.height - topMargin

        var pos = topMargin + (plotHeight * portion)

        return pos
    }

    id      :   potReadCont


    ArduinoProxy{
        id      :   proxy
        socket  :   bluetooth
    }

    ArduinoConstants{
        id      :   constants
    }

    Column{
        z       :   2
        anchors.right       :   parent.right
        anchors.rightMargin :   parent.height * 0.05
        anchors.top         :   parent.top
        anchors.topMargin   :   parent.height * 0.05


        Text {
            id      :   scoreText
            text    :   "Skor " + score
            font.pixelSize  :   potReadCont.height * 0.05
            font.family     :   FontCollection.labelFontName
        }

        Text {
            id              :   durationText
            text            :   "Süre " + remainingTime + "s"
            font.pixelSize  :   potReadCont.height * 0.05
            font.family     :   FontCollection.labelFontName
            color           :   {
                if(remainingTime < 7)
                    return Color.colors[Color.pink]
                else if(remainingTime < 15)
                    return Color.colors[Color.yellow]
                else
                    return Color.colors[Color.black]
            }
        }
    }

    AnalogReadTemplate{
        id                  :   chart
        width               :   parent.width * 0.7
        anchors.left        :   parent.left
        anchors.top         :   parent.top
        anchors.bottom      :   parent.bottom

        chart.title         :   "Potansiyometre Sinyal Ölçüm"
        axisY.titleText     :   "Volt (V)"
        axisY.tickCount     :   10
        splineSeries.name   :   "Potentiometer"

        Component.onCompleted   :   {
            axisY.applyNiceNumbers()
        }
    }

    Timer{
        id          :   creator
        interval    :   1200
        repeat      :   true
        running     :   gameIsRunning
        onTriggered :   {
            var portion =   Math.random()
            var voltage =   portion * 5
            var width   =   potReadCont.width * widthPortion
            var height  =   potReadCont.height * heightPortion
            var x       =   potReadCont.width
            var y       =   calculateYPos(1 - portion) - (height * 0.5)
            var createdObj  =   voltageRectComponent.createObject(potReadCont , { "width" : width , "height"  : height , "x" : x , "y" : y , "voltage" : voltage})

            rectangles.push(createdObj)
        }
    }

    Timer{
        id          :   remainingTimeTimer
        interval    :   1000
        repeat      :   true
        running     :   gameIsRunning
        onTriggered :   {
            remainingTime -= 1
            if(remainingTime <= 0){
                stopGame()
            }
        }
    }

    Timer{
        id          :   rectMove
        interval    :   16
        repeat      :   true
        running     :   gameIsRunning
        onTriggered :   {
            for(var i = 0; i < rectangles.length; i++){
                var item = rectangles[i];

                item.x -= moveStep

                var rect = Qt.rect(item.x , item.y , item.width , item.height)
                var p    = Qt.point(splinePosX , splinePosY)

                if(contains(rect , p)){
                    if(!item.accepted){
                        item.accepted = true
                        addScore()
                    }
                }

                if(item.x < 0 && Math.abs(item.x) > item.width){
                    item.destroy()
                }
            }
        }
    }

    Component   {
        id      :       voltageRectComponent

        Item {

            property real   voltage     :   0
            property bool   accepted    :   false
            id  :   voltItem

            Rectangle   {
                anchors.fill    :   parent
                color           :   voltItem.accepted ? Color.colors[Color.green] : Color.colors[Color.pink]
                radius          :   height * 0.3
            }

            Text {
                id                  :   name
                text                :    voltItem.voltage.toFixed(2) + "V"
                anchors.centerIn    :   parent
                color               :   "white"
                z                   :   2
                font.pixelSize      :   parent.height * textHeight
                font.family         :   Global.kidFontName
            }
        }
    }

    GameEndDialog{
        id                  :   gameEndDialog
        width               :   parent.width * 0.4
        height              :   parent.height * 0.4
        x                   :   (parent.width / 2) - (width / 2)
        y                   :   (parent.height / 2) - (height / 2)
        scoreEnabled        :   true
        score               :   potReadCont.score
    }

    SoundEffect{
        id      :   corretSound
        source  :   SoundCollection.newPointSoundPath    }

    SoundEffect{
        id      :   endSound
        source  :   SoundCollection.timeOverSoundPath
    }

    SoundEffect{
        id      :   congratSound
       source   :   SoundCollection.congratSoundPath
    }

    Timer{
        id          :   analogReadTimer
        repeat      :   true
        interval    :   16
        running     :   gameIsRunning
        onTriggered :   {
            var analogVolt  =   proxy.analogRead(constants.a0)
            chart.splineSeries.newPoint(analogVolt)
            splinePosY = calculateYPos(1 - (analogVolt / 1023))
        }
    }
}
