import QtQuick 2.0

Item {

    function    startExample(){
    }

    function    stopExample(){

    }

    Text {
        id                  :   emptyControlText
        text                :   "Bu derste IoT kontrolü yoktur."
        font.pixelSize      :   parent.height * 0.1
        anchors.centerIn    :   parent
        clip                :   true
    }
}
