import QtQuick 2.0
import Roboskop 1.0
import "../../"
import "../../template"
import "../../../controls"
import "../../../theme"

Rectangle {


    //Width
    readonly property real ledOnOffButtonWidth              :   0.6
    readonly property real ledVisualWidth                   :   0.8
    readonly property real ledBlinkButtonsInternalWidth     :   0.8

    //Height
    readonly property real ledInfoTextContainerHeight       :   0.2
    readonly property real ledOnOffButtonContainerHeight    :   0.2
    readonly property real ledOnOffButtonHeight             :   0.7
    readonly property real ledBlinkButtonsContainerHeight   :   0.2
    readonly property real ledBlinkButtonsInternalHeight    :   0.8

    //Font
    readonly property real ledInfoTextFontSize              :   0.7


    //Configuration
    readonly property int   blinkPin                        :   8

    property bool ledIsOn                                   :   false
    property bool blinkLedStatus                            :   true
    property int  blinkInterval                             :   200

    function    startExample(){
        proxy.pinMode(blinkPin , constants.output)
    }

    function    stopExample(){
//        off()
    }

    ArduinoProxy{
        id          :   proxy
        socket      :   bluetooth
    }

    ArduinoConstants{
        id          :   constants
    }

    InteractiveControlContainer {

        id          :   interactiveControlContainer
        width       :   parent.width * 0.8
        height      :   parent.height * 0.7
        title       :   "Led Kontrol"
        anchors.centerIn    :   parent
        controlsComponent   :   Component   {

            Rectangle   {

                anchors.fill    :   parent
                color           :   "#1bb7ea"


                function    blink(interval){
                    blinkInterval = interval
                    blinkAnimation.restart();
                    blinkAnimation.running = false
                    blinkAnimation.running = true
                }

                function    on(){
                    blinkAnimation.stop()
                    proxy.digitalWrite(blinkPin , constants.high)
                    ledIsOn = true
                }

                function    off(){
                    blinkAnimation.running = false
                    proxy.digitalWrite(blinkPin , constants.low)
                    ledIsOn = false
                }

                Rectangle{
                    id                      :   ledVisualContainer
                    anchors.top             :   parent.top
                    anchors.bottom          :   parent.bottom
                    anchors.left            :   parent.left
                    width                   :   parent.width * 0.2
                    color                   :   parent.color

                    Image {
                        id                  :   ledImg
                        source              :   "/res/img/led-1.png"
                        anchors.fill        :   parent
                        anchors.margins     :   parent.width * 0.15
                        fillMode            :   Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    anchors.left    :   ledVisualContainer.right
                    anchors.right   :   parent.right
                    anchors.top     :   parent.top
                    anchors.bottom  :   parent.bottom
                    color           :   "transparent"

                    Item{
                        id              :   ledInfoTextContainer
                        anchors.left    :   parent.left
                        anchors.right   :   parent.right
                        height          :   parent.height * ledInfoTextContainerHeight

                        Text {
                            id                      :   name
                            text                    :   ledIsOn ? "Durum : Açık" : "Durum : Kapalı"
                            anchors.centerIn        :   parent
                            font.pixelSize          :   parent.height * ledInfoTextFontSize
                            font.family             :   FontCollection.headerFontName
                        }
                    }

                    Rectangle{

                        id                  :   ledOnOffContainer
                        anchors.top         :   ledInfoTextContainer.bottom
                        anchors.left        :   parent.left
                        anchors.right       :   parent.right
                        height              :   parent.height * ledOnOffButtonContainerHeight
                        color               :   "transparent"

                        RoboskopButton{
                            id                  :   openCloseLedButton
                            text                :   ledIsOn ? "Led Söndür" : "Led Yak"
                            width               :   parent.width * ledOnOffButtonWidth
                            anchors.centerIn    :   parent
                            height              :   parent.height * ledOnOffButtonHeight
                            onClicked           :   {

                                if(ledIsOn)
                                    off()
                                else
                                    on()
                            }
                            controlColor        :   openCloseLedButton.green
                        }
                    }

                    Row{

                        anchors.top         :   ledOnOffContainer.bottom
                        anchors.left        :   parent.left
                        anchors.right       :   parent.right
                        height              :   parent.height * ledBlinkButtonsContainerHeight



                        Item{

                            width           :   parent.width * (1 / 3)
                            height          :   parent.height


                            RoboskopButton{
                                id                          :   slowBlink
                                anchors.centerIn            :   parent
                                text                        :   "Yavaş"
                                width                       :   parent.width * ledBlinkButtonsInternalWidth
                                height                      :   parent.height * ledBlinkButtonsInternalHeight
                                anchors.verticalCenter      :   parent.verticalCenter
                                onClicked                   :   blink(1000)
                                controlColor                :   slowBlink.green
                            }
                        }

                        Item{

                            width           :   parent.width * (1 / 3)
                            height          :   parent.height

                            RoboskopButton{
                                id                          :   normalBlink
                                anchors.centerIn            :   parent
                                text                        :   "Normal"
                                width                       :   parent.width * ledBlinkButtonsInternalWidth
                                height                      :   parent.height * ledBlinkButtonsInternalHeight
                                anchors.verticalCenter      :   parent.verticalCenter
                                onClicked                   :   blink(400)
                                controlColor                :   normalBlink.yellow
                            }
                        }


                        Item{

                            width           :   parent.width * (1 / 3)
                            height          :   parent.height

                            RoboskopButton{
                                id                          :   fastBlink
                                anchors.centerIn            :   parent
                                text                        :   "Hızlı"
                                width                       :   parent.width * ledBlinkButtonsInternalWidth
                                height                      :   parent.height * ledBlinkButtonsInternalHeight
                                anchors.verticalCenter      :   parent.verticalCenter
                                onClicked                   :   blink(40)
                                controlColor                :   fastBlink.pink
                            }
                        }
                    }
                }

                SequentialAnimation {
                    property int delayDuration  :   blinkInterval / 2

                    onDelayDurationChanged      :   {

                        if(running)
                            blinkAnimation.restart()
                    }

                    id                          :   blinkAnimation
                    loops                       :   Animation.Infinite
                    running                     :   false

                    ScriptAction {
                        script  :   {
                            proxy.digitalWrite(blinkPin , constants.high)
                        }
                    }

                    PauseAnimation {
                        duration: blinkAnimation.delayDuration
                    }

                    ScriptAction    {
                        script  :   proxy.digitalWrite(blinkPin , constants.low)
                    }

                    PauseAnimation {
                        duration    :   blinkAnimation.delayDuration
                    }
                }
            }
        }
    }
}
