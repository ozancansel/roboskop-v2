import QtQuick 2.6
import QtQuick.Controls 2.1
import Roboskop 1.0
import "../../../music"
import "../../../music/notearchive"
import "../../../controls"
import "../../../theme"

Item {

    function    startExample(){
        bluetooth.send("{r}")
    }

    function    stopExample(){
        bluetooth.send("{p}")

        if(melody !== "none" )
            jingleBells.stop()
    }


    readonly property real  toneControlContainerWidth       :   0.4
    readonly property real  toneControlContainerHeight      :   0.2
    readonly property real  toneControlDescTextHeight       :   0.4
    readonly property real  toneControlContainerTopMargin   :   0.2
    property string         melody                          :   "none"

    readonly property variant borderColorMappings       :   [   Color.black,  Color.green , Color.yellow , Color.pink ]
    property variant colorIdx                  :   0

    id          :   pianoControlCont

    ArduinoProxy{
        id          :   arduinoProxy
        socket      :   bluetooth
    }

    ArduinoConstants{
        id          :   constants
    }

    JingleBells {
        id          :   jingleBells
        onBpmChanged:   console.log(jingleBells.bpm)

        onNextNote  :    {
            //<tone> <note>
            if(playAuto.checked){
                arduinoProxy.noTone(9)
                arduinoProxy.tone(9 , noteSymbols.hertzMapping[note][tone])
            }
        }

        onMelodyEnd :   {
            arduinoProxy.noTone(9)
            stream.stop()
        }

        onMelodyStart  :    stream.play()
    }

    NoteSymbols{
        id      :   noteSymbols
    }

    Item{
        visible                     :   false
        id                          :   toneControlContainer
        width                       :   parent.width *   toneControlContainerWidth
        height                      :   parent.height * toneControlContainerHeight
        anchors.horizontalCenter    :   parent.horizontalCenter
        anchors.topMargin           :   parent.height * toneControlContainerTopMargin

        Column{
            anchors.fill            :   parent
            spacing                 :   10

            Text {
                id      :   toneDesc
                text    :   {
                    if(toneSlider.value == 1)
                        return "Kalın"
                    else if(toneSlider.value == 2)
                        return "Orta"
                    else if (toneSlider.value == 3)
                        return "İnce"
                }

                font.pixelSize  :   parent.height * toneControlDescTextHeight
                anchors.horizontalCenter    :   parent.horizontalCenter
            }

            Slider{
                id      :   toneSlider
                from    :   1
                to      :   3
                value   :   3
                snapMode:   Slider.SnapAlways
                stepSize:   1
                width   :   parent.width
            }
        }
    }

    Row{
        id                      :   controlButtons
        visible                 :   melody  !== "none"
        width                   :   parent.width * 0.8
        height                  :   Math.max(parent.height * 0.12 , playButton.height)
        anchors.top             :   parent.top
        anchors.topMargin       :   10
        anchors.horizontalCenter:   pianoMainContainer.horizontalCenter
        spacing         :   10

        RoboskopButton{
            id          :   playButton
            text        :   "Cal"
            width       :   parent.width * 0.2
            height      :   parent.height
            onClicked   :   jingleBells.play()
            controlColor:   playButton.green
        }

        RoboskopButton{
            id          :   stopButton
            text        :   "Durdur"
            width       :   parent.width * 0.2
            height      :   parent.height
            onClicked   :   jingleBells.stop()
            controlColor:   playButton.yellow
        }

        Slider  {
            id          :   bpmSlider
            stepSize    :   1
            from        :   60
            to          :   500
            value       :   120
            width       :   parent.width * 0.4
            onValueChanged  :    jingleBells.setBpm(bpmSlider.value)
            anchors.verticalCenter  :   parent

            background  :   Item    {

                anchors.fill    :   bpmSlider

                Rectangle{
                    width                   :   bpmSlider.width - bpmSlider.handle.width
                    height                  :   bpmSlider.handle.height * 0.3
                    color                   :   "green"
                    anchors.verticalCenter  :   parent.verticalCenter
                    radius                  :   height * 0.5
                    x                       :   bpmSlider.handle.width / 2
                }
            }
        }

        RoboskopCheckButton    {
            id                      :   playAuto
            width                   :   parent.width * 0.2
            text                    :   "Otomatik Cal!"
            buttonColor             :   "#ff3366"
            anchors.verticalCenter  :   parent
        }
    }

    NoteStream{
        id                  :   stream
        visible             :   pianoControlCont.melody !== "none"
        widthPSec           :   jingleBells.bpm
        melody              :   jingleBells
        anchors.top         :   controlButtons.bottom
        anchors.topMargin   :   10
        anchors.bottom      :   pianoMainContainer.top
        anchors.bottomMargin:   10
        anchors.left        :   pianoMainContainer.left
        anchors.leftMargin  :   noteIndicator.width
        width               :   parent.width * 0.9
    }

    Rectangle   {
        id                  :   noteIndicator
        anchors.top         :   stream.top
        anchors.topMargin   :   -radius
        anchors.bottom      :   stream.bottom
        anchors.bottomMargin:   -height * 0.5
        anchors.right       :   stream.left
        width               :   height * 0.13
        visible             :   pianoControlCont.melody !== "none"
        color               :   Color.colors[borderColorMappings[colorIdx % borderColorMappings.length]]
        radius              :   width * 0.3
        Behavior on color   {
            ColorAnimation {

            }
        }
    }

    Timer   {
        id      :   playingAnimationTimer
        repeat  :   true
        running :   jingleBells.melodyPlaying
        interval:   400
        onTriggered :   {
            colorIdx++
        }
    }


    Rectangle   {

        id                      :   pianoMainContainer
        width                   :   parent.width * 0.8
        height                  :   parent.height * 0.6
        anchors.horizontalCenter:   parent.horizontalCenter
        anchors.bottom          :   parent.bottom
        anchors.bottomMargin    :   20
        color                   :   Color.colors[Color.black]
        radius                  :   height * 0.1

        readonly property variant notesToTextMap : ["c" , "c#" , "d" , "d#" , "e" , "f" , "f#" , "g" , "g#" , "a" , "a#" , "b" ]


        Rectangle{
            anchors.fill            :   parent
            anchors.margins         :   parent.width * 0.025

            color                   :   "black"

            Rectangle   {
                id                  :   pianoControl
                anchors.top         :   parent.top
                anchors.left        :   parent.left
                anchors.right       :   parent.right
                height              :   parent.height * 0.3
                color               :   "red"
                Text {
                    id              :   pianoControlText
                    text            :   qsTr("Roboskop Piano")
                    color           :   "white"
                    font.pixelSize  :   parent.height * 0.35
                    font.family     :   FontCollection.headerFontName
                    font.bold       :   true
                    anchors.centerIn:   parent
                }
            }

            Rectangle{

                id                  :   pianoBorder
                anchors.left        :   pianoControl.left
                anchors.right       :   pianoControl.right
                anchors.top         :   pianoControl.bottom
                height              :   parent.height * 0.2
            }

            Row{

                id                  :   pianoKeyboardRow
                anchors.top         :   pianoControl.bottom
                anchors.left        :   pianoControl.left
                anchors.right       :   pianoControl.right
                height      :   parent.height * 0.65

                Repeater{
                    model   :   11
                    Rectangle{
                        readonly property string noteName      :   pianoMainContainer.notesToTextMap[index]
                        id      :   noteRectangle
                        width   :   parent.width * (1 / 11)
                        height  :   parent.height
                        color   :   {
                            if(index === 0 || index === 4 || index === 7)
                                "white"
                            else
                                "white"
                        }

                        border.color    :   "black"
                        border.width    :   2
                        scale           :   pianoButtons.pressed ? 0.94 : 1

                        Behavior on scale {                    // added to smooth the scaling
                            NumberAnimation {
                                duration        :   100
                            }
                        }

                        Text {
                            id                  :   noteText
                            text                :   pianoMainContainer.notesToTextMap[index]
                            font.pixelSize      :   parent.width * 0.2
                            font.family         :   FontCollection.labelFontName
                            anchors.centerIn    :   parent
                        }

                        MouseArea{
                            id          :   pianoButtons
                            anchors.fill:   parent
                            onPressed   :   {
                                var hertz = noteSymbols.hertzMapping[noteRectangle.noteName][4]
                                arduinoProxy.tone( 9 , hertz )
                            }
                            onReleased  :   arduinoProxy.noTone(9)
                        }
                    }
                }
            }
        }
    }
}
