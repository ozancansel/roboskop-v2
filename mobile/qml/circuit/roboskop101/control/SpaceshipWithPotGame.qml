import QtQuick 2.0
import Roboskop 1.0
import "../../../game"

Rectangle {

    property bool readActive        :   false

    function    startExample(){
        readActive = true
    }

    function    stopExample(){
        readActive = false
    }

    signal newScore(int score)


    RunAndJumpGameScene {
        id              :   game
        anchors.fill    :   parent
        onGameOver      :   newScore(score)
    }

    ArduinoProxy{
        id              :   arduinoProxy
        socket          :   bluetooth
    }

    ArduinoConstants{
        id              :   constants
    }

    Connections {
        target              :   commandInterpreter
        onCommandIncome     :   {
            if(code === 65){
                var value = arduinoProxy.analogRead(constants.a0)
                game.setShipPos(commandArgs[1] / 1023)
            }
        }
    }

    Timer{
        property bool pinModed  :   false
        id          :   timer
        repeat      :   true
        running     :   readActive
        interval    :   50
        onTriggered :   {

            if(!timer.pinModed)
                arduinoProxy.pinMode(constants.a0 , constants.output)

            var value = arduinoProxy.analogRead(constants.a0)

            game.setShipPos(value / 1023)
        }
    }
}
