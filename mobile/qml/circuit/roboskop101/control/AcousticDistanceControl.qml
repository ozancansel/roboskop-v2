import QtQuick 2.0
import QtMultimedia 5.0
import Roboskop 1.0
import "../../../controls"
import "../../../theme"

Rectangle {

    //Top Margin
    readonly property real acousticVisualTopMargin          :   0.6
    readonly property real acousticInfoContainerTopMargin   :   0.05

    //Left Margin
    readonly property real acousticVisualLeftMargin         :   0.05

    readonly property real acousticRoadLineMargin           :   0.05

    //Width
    readonly property real acousticVisualContainerWidth     :   0.09
    readonly property real distanceInfoContainerWidth       :   0.2

    //Height
    readonly property real acousticVisualContainerHeight    :   0.32
    readonly property real acousticCircuitBaseWidth         :   0.25
    readonly property real trigEchoVisualHeight             :   0.60
    readonly property real acousticRoadLineHeight           :   0.015
    readonly property real obstacleHeight                   :   0.3
    readonly property real distanceInfoContainerHeight      :   0.1
    readonly property real distanceInfoTextHeight           :   0.6

    //Radius
    readonly property real acousticCircuitBaseRadius        :   0.3

    //Color
    readonly property color acousticCircuitBaseColor        :   "#127ab7"
    readonly property color acousticEchoColor               :   "#848484"
    readonly property color acousticTriggerColor            :   "#7a3a73"

    //Pin Conf
    readonly property int buzzerPin                         :   9
    readonly property int triggerPin                        :   11
    readonly property int echoPin                           :   12

    //Conf
    readonly property real  maxDist                         :   80
    property int acousticId                                 :   -1

    //Duration
    readonly property int maxInterval                       :   500
    readonly property int minInterval                       :   0
    readonly property int maxDistance                       :   50
    readonly property int minDistance                       :   0

    property real   currentDistance                         :   30
    property bool   readDistance                            :   false
    property int    remainingTime       :   30
    property int    score               :   0
    property int    targetDist          :   20
    property bool   gameIsActive        :   false
    property bool   gameDisabled        :   false
    property bool   buzzerEnabled       :   false

    signal newScore(int score)

    function calculateX(dist){
        var pixelDist = (acousticDistanceControlContainer.width - (acousticVisualContainer.x + acousticVisualContainer.width + obstacle.width)) * (dist / maxDist)

        return pixelDist + acousticVisualContainer.x + acousticVisualContainer.width - 10
    }

    function    startExample(){
        if(!gameDisabled)
            startGame()

        readDistance = true
        if(acousticId <= 0){
            acousticId = proxy.attachAcoustic(triggerPin , echoPin);
        }

        readDistance = true
    }

    function    startGame(){
        remainingTime   =   30;
        score           =   0;
        timer.start()
        gameIsActive    =   true
        startSound.play()
    }

    function    stopGame(){
        timer.stop()
        readDistance = false
        gameIsActive = false
        newScore(score)
    }

    function    stopExample(){
        if(!gameDisabled)
            stopGame()

        readDistance = false
    }

    signal toastMessageRequired(string message);

    onCurrentDistanceChanged    :   {
        if(gameIsActive && targetDist == currentDistance){
            score += 1;

            targetDist = Math.random() * 25 + 7
            corretSound.play()
        }
    }

    id      :   acousticDistanceControlContainer

    ArduinoProxy{
        id      :   proxy
        socket  :   bluetooth
    }

    ArduinoConstants{
        id      :   constants
    }

    Rectangle   {
        id                      :   distanceInfoContainer
        anchors.horizontalCenter:   parent.horizontalCenter
        anchors.top             :   parent.top
        anchors.topMargin       :   parent.height * acousticInfoContainerTopMargin
        height                  :   parent.height * distanceInfoContainerHeight
        width                   :   parent.width * distanceInfoContainerWidth

        Text {
            id                  :   distanceInfoText
            text                :   currentDistance <= maxDist ? currentDistance + " cm" : "Bilinmiyor"
            font.pixelSize      :   parent.height * distanceInfoTextHeight
            font.family         :   FontCollection.labelFontName
            anchors.centerIn    :   parent
        }
    }

    Rectangle   {

        id                      :   gameContainer
        width                   :   parent.width * 0.4
        height                  :   parent.height * 0.2
        x                       :   10
        y                       :   10
        visible                 :   !gameDisabled

        Column  {

            anchors.fill        :   parent

            Text {
                id              :   remainingTimeText
                text            :   "Kalan Süre    : " +   remainingTime + "s"
                font.pixelSize  :   gameContainer.height * 0.3
                font.family     :   FontCollection.labelFontName
            }

            Text {
                id              :   name
                text            :   "Skor            : " + score
                font.pixelSize  :   gameContainer.height * 0.3
                font.family     :   FontCollection.labelFontName
            }

            Text {
                id              :   target
                text            :   "Hedef          :  " + targetDist + " cm"
                font.pixelSize  :   gameContainer.height * 0.3
                font.family     :   FontCollection.labelFontName
            }
        }

        Timer{
            id          :   timer
            interval    :   1000
            repeat      :   true
            onTriggered :   {
                remainingTime -= 1

                if(remainingTime === 0){
                    timer.stop()
                    gameIsActive    =   false

                    toastMessageRequired("Tebrikler. Skor -> " + score)
                    endSound.play()
                }
            }
        }

        SoundEffect{
            id      :   startSound
            source  :   SoundCollection.startGameSoundPath
        }

        SoundEffect{
            id      :   corretSound
            source  :   SoundCollection.newPointSoundPath
        }

        SoundEffect{
            id      :   endSound
            source  :   SoundCollection.timeOverSoundPath
        }
    }

    Rectangle{
        id                      :   obstacle
        height                  :   parent.height  *    obstacleHeight
        width                   :   height * 0.1
        radius                  :   width * 0.4
        x                       :   calculateX(currentDistance)
        anchors.verticalCenter  :   parent.verticalCenter
        color                   :   "black"
        visible                 :   currentDistance <= 50 || currentDistance > 0

        Behavior on x{
            SmoothedAnimation { velocity    :   600 }
        }
    }

    Rectangle{
        height                  :   parent.height * acousticRoadLineHeight
        anchors.right           :   parent.right
        anchors.left            :   parent.left
        anchors.bottom          :   acousticVisualContainer.top
        anchors.bottomMargin    :   parent.height * acousticRoadLineMargin
        color                   :   "black"
        rotation                :   -3
        antialiasing            :   true
    }

    Rectangle{
        height              :   parent.height * acousticRoadLineHeight
        anchors.right       :   parent.right
        anchors.left        :   parent.left
        anchors.top         :   acousticVisualContainer.bottom
        anchors.topMargin   :   parent.height * acousticRoadLineMargin
        color               :   "black"
        rotation            :   3
        antialiasing        :   true
    }

    Rectangle{

        id                          :   acousticVisualContainer
        anchors.left                :   parent.left
        anchors.leftMargin          :   parent.width  * acousticVisualLeftMargin
        anchors.verticalCenter      :   parent.verticalCenter
        width                       :   parent.width * acousticVisualContainerWidth
        height                      :   parent.height * acousticVisualContainerHeight

        Item{

            id              :   triggerContainer
            height          :   parent.height * 0.5
            anchors.left    :   acousticCircuitBaseVisual.right
            anchors.right   :   parent.right
            anchors.top     :   parent.top

            Rectangle{
                id                          :   triggerVisual
                color                       :   acousticTriggerColor
                height                      :   parent.height * trigEchoVisualHeight
                anchors.left                :   parent.left
                anchors.right               :   parent.right
                anchors.verticalCenter      :   parent.verticalCenter
            }
        }

        Item{
            id              :   echoContainer
            anchors.left    :   acousticCircuitBaseVisual.right
            anchors.right   :   parent.right
            anchors.top     :   triggerContainer.bottom
            anchors.bottom  :   parent.bottom

            Rectangle{
                id                          :   echoVisual
                color                       :   acousticEchoColor
                height                      :   parent.height * trigEchoVisualHeight
                anchors.left                :   parent.left
                anchors.right               :   parent.right
                anchors.verticalCenter      :   parent.verticalCenter
            }
        }

        Rectangle{
            id              :   acousticCircuitBaseVisual
            anchors.left    :   parent.left
            anchors.bottom  :   parent.bottom
            anchors.top     :   parent.top
            width           :   parent.width * acousticCircuitBaseWidth
            radius          :   height * acousticCircuitBaseRadius
            color           :   acousticCircuitBaseColor
        }
    }

    Connections{
        target              :   commandInterpreter
        onCommandIncome     :   {
            if(code === 114){
                if(parseInt(commandArgs[0]) > 0)
                    currentDistance = commandArgs[0]
            }
        }
    }

    Timer{
        id          :   distanceTimer
        repeat      :   true
        interval    :   50
        running     :   readDistance
        onTriggered :   {
            var distance = proxy.readDistance( acousticId , 5000 );
            if(distance >= 0){
                currentDistance = distance;
                var interval = ((maxInterval - minInterval) * ((distance / (maxDistance - minDistance))))
                if(Math.abs(interval - buzzerTimer.interval) > 60){
                    buzzerTimer.interval = interval;
                }
            }
        }
    }

    Timer{
        property bool ledState  :   false
        property bool pinModed  :   false
        id          :   buzzerTimer
        repeat      :   true
        interval    :   100
        running     :   buzzerEnabled && readDistance
        onTriggered :   {

            if(!buzzerTimer.pinModed){
                proxy.pinMode( buzzerPin , constants.output )
            }

            if(currentDistance < 5){
                proxy.digitalWrite(buzzerPin , constants.high)
                buzzerTimer.ledState = constants.high
                return
            }

            if(buzzerTimer.ledState){
                proxy.digitalWrite(buzzerPin , constants.low)
            } else {
                proxy.digitalWrite(buzzerPin , constants.high)
            }

            buzzerTimer.ledState = !buzzerTimer.ledState
        }
    }
}
