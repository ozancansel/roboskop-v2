import QtQuick 2.0
import QtQuick.Controls 2.1
import QtMultimedia 5.0
import QtQuick.Layouts 1.1
import Roboskop 1.0
import "../../../proxy"
import "../../../theme"
import "../../template"
import "../../../controls"

Rectangle {

    //Width
    readonly property real controlsContainerWidth           :   0.6
    readonly property real sliderTextWidth                  :   0.3
    readonly property real currentColorWidth                :   0.8

    //Height
    readonly property real sliderContainersHeight           :   0.75
    readonly property real controlsContainerHeight          :   0.5
    readonly property real currentColorHeight               :   0.8

    //Border
    readonly property real currentColorLabelBorder          :   0.005

    //Radius
    readonly property real currentColorLabelRadius          :   0.15

    //FontSize
    readonly property real sliderTextFontSize               :   0.3

    readonly property int   tolerance               :   10
    property bool           gameDisabled            :   false
    property int            remainingTime           :   60
    property bool           gameIsActive            :   false
    property int            targetR                 :   255
    property int            targetG                 :   0
    property int            targetB                 :   255
    property color          targetColor             :   Qt.rgba(targetR / 255 , targetG / 255 . targetB / 255 , 1)
    property int            score                   :   0
    property bool           redChannelChecked       :   gameIsActive && checkRedChannel()
    property bool           greenChannelChecked     :   gameIsActive && checkGreenChannel()
    property bool           blueChannelCheked       :   gameIsActive && checkBlueChannel()
    property bool           withPot                 :   false
    property bool           withLdr                 :   false
    property double         analogMultiplier        :   0

    signal newScore(int score)

    function    startExample(){
        applyCurrentColor()

        if(gameDisabled)
            return

        startGame()
    }

    function    stopExample(){
        if(!gameDisabled)
            stopGame()
    }

    function    checkRedChannel()   {
        var res =   Math.abs(targetR - interactiveContainer.controlsItem.red.value) < tolerance
        return  res
    }

    function    checkGreenChannel() {
        var res  =  Math.abs(targetG - interactiveContainer.controlsItem.green.value) < tolerance
        return  res
    }

    function    checkBlueChannel()  {
        var res =   Math.abs(targetB - interactiveContainer.controlsItem.blue.value) < tolerance
        return  res
    }

    function    applyCurrentColor(){

        rgbLed.rgb(interactiveContainer.controlsItem.red.value ,
                   interactiveContainer.controlsItem.green.value ,
                   interactiveContainer.controlsItem.blue.value)
    }

    function r(value){
        rgbLed.r(value)
    }

    function g(value){
        rgbLed.g(value)
    }

    function b(value){
        rgbLed.b(value)
    }

    function    checkColor(){
        if( Math.abs(targetR - interactiveContainer.controlsItem.red.value) < tolerance &&
                Math.abs(targetG - interactiveContainer.controlsItem.green.value) < tolerance &&
                Math.abs(targetB - interactiveContainer.controlsItem.blue.value) < tolerance){
            return true
        }

        return false
    }

    function    incrementScore(){
        score++
        congratSound.play()
    }

    function    resetScore(){
        score = 0
    }

    function    resetTime(){
        remainingTime   =   60
    }

    function    startGame(){
        resetScore()
        resetTime()
        //nextColor()
        gameIsActive    =   true
    }

    function    stopGame(){
        gameIsActive =   false
        newScore(score)
        endSound.play()
    }

    function    nextColor(){
        targetR = Math.round(Math.random() * 255)
        targetG = Math.round(Math.random() * 255)
        targetB = Math.round(Math.random() * 255)
    }

    //Events
    onRedChannelCheckedChanged  : {
        if(redChannelChecked)
            corretSound.play()
    }

    onGreenChannelCheckedChanged:   {
        if(greenChannelChecked)
            corretSound.play()
    }

    onBlueChannelChekedChanged  :   {
        if(blueChannelCheked)
            corretSound.play()
    }

    id          :   rgbControl

    Timer   {
        id      :   remainingTimeTimer
        repeat  :   true
        running :   gameIsActive && !gameDisabled
        interval:       1000
        onTriggered     :   {
            //Eger ki sure bitmisse oyun durduruluyor
            if(remainingTime <= 0){
                stopGame()
                return
            }

            //Sure azaltiliyor
            remainingTime -= 1
        }
    }

    ArduinoProxy{
        id              :   proxy
        socket          :   bluetooth
    }

    ArduinoConstants{
        id              :   constants
    }

    RgbProxy{
        id              :   rgbLed
        arduino         :   proxy
        redPin          :   11
        greenPin        :   10
        bluePin         :   9
    }

    Item {


        id              :   scoreBoard
        visible         :   !gameDisabled
        anchors.top     :   interactiveContainer.top
        anchors.bottom  :   interactiveContainer.bottom
        anchors.left    :   parent.left
        anchors.right   :   interactiveContainer.left
        anchors.rightMargin :   width   *   0.1
        anchors.leftMargin  :   anchors.rightMargin

        Rectangle{
            anchors.fill    :   targetColorContainer
            anchors.centerIn:   parent
            anchors.margins :   width * - 0.02
            border.width    :   width * 0.01
            border.color    :   targetColor
            radius          :   targetColorRect.radius
        }

        Column  {
            id                  :   targetColorContainer
            width               :   parent.width
            anchors.centerIn    :   parent

            Item {
                id      :   scoreContainer
                width   :   parent.width
                height  :   scoreBoard.height * 0.2
                Text {
                    id                      :   scoreText
                    text                    :   "Skor   :   " + score
                    font.pixelSize          :   parent.height * 0.4
                    anchors.centerIn        :   parent
                }
            }

            Item {
                id      :   remainingTimeTextCont
                width   :   parent.width
                height  :   scoreBoard.height * 0.2
                Text {
                    id                      :   remainingTimeText
                    text                    :   "Sure   :   " + remainingTime
                    font.pixelSize          :   parent.height * 0.4
                    anchors.centerIn        :   parent
                    color                   :   {
                        if(remainingTime < 5)
                            return "red"
                        else if(remainingTime < 15)
                            return "green"
                        else
                            return "black"
                    }
                }
            }

            Rectangle{
                id      :   targetColorRect
                color   :   Qt.rgba(targetR / 255 , targetG / 255 , targetB / 255 , 1)
                width   :   parent.width
                height  :   scoreBoard.height * 0.2
                radius  :   height * 0.2
            }
        }
    }

    SoundEffect{
        id      :   corretSound
        source  :   SoundCollection.newPointSoundPath
    }

    SoundEffect{
        id      :   endSound
        source  :   SoundCollection.timeOverSoundPath
    }

    SoundEffect{
        id      :   congratSound
        source  :   SoundCollection.congratSoundPath
    }

    InteractiveControlContainer {
        id                  :   interactiveContainer
        width               :   parent.width * 0.6
        height              :   parent.height * 0.9
        anchors.centerIn    :   parent
        title               :   "RGB Kontrol"

        controlsComponent   :   Component{

            Column{

                property alias  red     :   redSlider
                property alias  green   :   greenSlider
                property alias  blue    :   blueSlider

                width       :   parent.width
                height      :   parent.height

                Row {
                    id          :   sliderContainers
                    width       :   parent.width
                    height      :   parent.height * sliderContainersHeight

                    Rectangle{
                        id      :   redSliderContainer
                        width   :   parent.width * 0.33
                        height  :   parent.height


                        Slider{
                            id              :   redSlider
                            anchors.top     :   redSliderContainer.top
                            height          :   parent.height * 0.7
                            width           :   parent.width
                            orientation     :   Qt.Vertical
                            from            :   0
                            to              :   255
                            value           :   10
                            onValueChanged  :   {
                                if(gameIsActive && checkColor()){
                                    incrementScore()
                                    nextColor()
                                }

                                if(!gameDisabled)
                                    rgbControl.r(value / 255)
                            }
                            background      :   Item   {
                                Rectangle{
                                    id                          :   redRectPath
                                    width                       :   redSlider.handle.width / 3
                                    height                      :   parent.height - redSlider.handle.height / 2
                                    border.width                :   width * 0.2
                                    border.color                :   "red"
                                    color                       :   "transparent"
                                    radius                      :   width * 0.5
                                    anchors.centerIn            :   parent
                                }

                                Rectangle{
                                    width                       :   redRectPath.width
                                    height                      :   redSlider.value / 255 * redRectPath.height
                                    color                       :   redRectPath.border.color
                                    radius                      :   redRectPath.radius
                                    anchors.bottom              :   redRectPath.bottom
                                    anchors.horizontalCenter    :   parent.horizontalCenter
                                }

                                Image {
                                    id                  :   successIcon
                                    source              :   "/res/img/success-icon.png"
                                    width               :   parent.width * 0.3
                                    fillMode            :   Image.PreserveAspectFit
                                    opacity             :   redChannelChecked ? 1 : 0
                                    anchors.bottom      :   parent.bottom
                                    anchors.left        :   parent.left
                                    Behavior on opacity {
                                        NumberAnimation {
                                            duration    :   200
                                        }
                                    }
                                }
                            }
                        }

                        Rectangle{
                            id              :   redColorSliderTextContainer
                            width           :   parent.width
                            anchors.top     :   redSlider.bottom
                            anchors.bottom  :   parent.bottom

                            Text {
                                id                  :   redColorSliderText
                                text                :   qsTr("Kırmızı")
                                font.pixelSize      :   parent.height * sliderTextFontSize
                                anchors.centerIn    :   parent
                                color               :   "red"
                            }
                        }
                    }


                    Rectangle{
                        id      :   greenSliderContainer
                        width   :   parent.width * 0.33
                        height  :   parent.height


                        Slider{
                            id              :   greenSlider
                            anchors.top     :   greenSliderContainer.top
                            height          :   parent.height * 0.7
                            width           :   parent.width
                            orientation     :   Qt.Vertical
                            from            :   0
                            to              :   255
                            value           :   220
                            onValueChanged  :   {
                                if(gameIsActive && checkColor()){
                                    incrementScore()
                                    nextColor()
                                }

                                if(!gameDisabled)
                                    rgbControl.g(value / 255)
                            }
                            background      :   Item   {

                                Rectangle{
                                    id                          :   greenRectPath
                                    width                       :   greenSlider.handle.width / 3
                                    height                      :   parent.height - greenSlider.handle.height / 2
                                    border.width                :   width * 0.2
                                    border.color                :   "green"
                                    color                       :   "transparent"
                                    radius                      :   width * 0.5
                                    anchors.centerIn            :   parent
                                }

                                Rectangle{
                                    width                       :   greenRectPath.width
                                    height                      :   greenSlider.value / 255 * greenRectPath.height
                                    color                       :   greenRectPath.border.color
                                    radius                      :   greenRectPath.radius
                                    anchors.bottom              :   greenRectPath.bottom
                                    anchors.horizontalCenter    :   parent.horizontalCenter
                                }

                                Image {
                                    id              :   successIcon2
                                    source          :   "/res/img/success-icon.png"
                                    width           :   parent.width * 0.3
                                    fillMode        :   Image.PreserveAspectFit
                                    opacity         :   greenChannelChecked ? 1 : 0
                                    anchors.bottom  :   parent.bottom
                                    Behavior on opacity {
                                        NumberAnimation {
                                            duration    :   200
                                        }
                                    }
                                }
                            }
                        }

                        Rectangle{
                            id              :   greenColorSliderTextContainer
                            width           :   parent.width
                            anchors.top     :   greenSlider.bottom
                            anchors.bottom  :   parent.bottom

                            Text {
                                id                  :   greenColorSliderText
                                text                :   qsTr("Yeşil")
                                font.pixelSize      :   parent.height * sliderTextFontSize
                                anchors.centerIn    :   parent
                                color               :   "green"
                            }
                        }
                    }

                    Rectangle{
                        id      :   blueSliderContainer
                        width   :   parent.width * 0.34
                        height  :   parent.height


                        Slider{
                            id              :   blueSlider
                            anchors.top     :   blueSliderContainer.top
                            height          :   parent.height * 0.7
                            width           :   parent.width
                            orientation     :   Qt.Vertical
                            from            :   0
                            to              :   255
                            value           :   22
                            onValueChanged  :   {
                                if(gameIsActive && checkColor()){
                                    incrementScore()
                                    nextColor()
                                }

                                if(!gameDisabled)
                                    rgbControl.b(value / 255)
                            }
                            background      :   Item   {

                                Rectangle{
                                    id                          :   blueRectPath
                                    width                       :   blueSlider.handle.width / 3
                                    height                      :   parent.height - blueSlider.handle.height / 2
                                    border.width                :   width * 0.2
                                    border.color                :   "blue"
                                    color                       :   "transparent"
                                    radius                      :   width * 0.5
                                    anchors.centerIn            :   parent
                                }

                                Rectangle{
                                    width                       :   blueRectPath.width
                                    height                      :   blueSlider.value / 255 * blueRectPath.height
                                    color                       :   blueRectPath.border.color
                                    radius                      :   blueRectPath.radius
                                    anchors.bottom              :   blueRectPath.bottom
                                    anchors.horizontalCenter    :   parent.horizontalCenter
                                }

                                Image {
                                    id              :   successIcon3
                                    source          :   "/res/img/success-icon.png"
                                    anchors.bottom  :   parent.bottom
                                    width           :   parent.width * 0.3
                                    fillMode        :   Image.PreserveAspectFit
                                    opacity         :   blueChannelCheked ? 1 : 0

                                    Behavior on opacity {

                                        NumberAnimation {
                                            duration    :   100
                                        }
                                    }
                                }
                            }
                        }

                        Rectangle{
                            id              :   blueColorSliderTextContainer
                            width           :   parent.width
                            anchors.top     :   blueSlider.bottom
                            anchors.bottom  :   parent.bottom

                            Text {
                                id                  :   blueColorSliderText
                                text                :   qsTr("Mavi")
                                font.pixelSize      :   parent.height * sliderTextFontSize
                                anchors.centerIn    :   parent
                                color               :   "blue"
                            }
                        }
                    }
                }

                Rectangle{
                    id              :   randomColorContainer
                    anchors.left    :   parent.left
                    anchors.right   :   parent.right
                    height          :   parent.height * 0.2
                    color           :   "transparent"

                    Rectangle{
                        id              :   currentColorLabel
                        width           :   parent.width * currentColorWidth
                        height          :   parent.height * currentColorHeight
                        anchors.centerIn:   parent
                        border.color    :   "black"
                        border.width    :   width * currentColorLabelBorder
                        color           :   Qt.rgba(redSlider.value / redSlider.to , greenSlider.value / greenSlider.to , blueSlider.value / blueSlider.to)
                        radius          :   height * currentColorLabelRadius
                    }
                }

                Rectangle{
                    id              :   controlButtons
                    height          :   parent.height * 0.2
                    anchors.left    :   parent.left
                    anchors.right   :   parent.right
                }
            }
        }
    }

    Timer{

        id              :   analogReadTimer
        running         :   withPot && gameDisabled
        repeat          :   true
        interval        :   50
        onTriggered     :   {
            analogMultiplier = proxy.analogRead(constants.a0) / 1023
            rgbLed.rgb( interactiveContainer.controlsItem.red.value / 255 * analogMultiplier ,
                       interactiveContainer.controlsItem.green.value/ 255 * analogMultiplier ,
                       interactiveContainer.controlsItem.blue.value / 255 * analogMultiplier )
        }
    }

    Timer{

        property int    minAnalog   :   1024
        property int    maxAnalog   :   0

        id              :   withLdrTimer
        running         :   withLdr && gameDisabled
        repeat          :   true
        interval        :   50
        onTriggered     :   {
            var ldrValue = proxy.analogRead(constants.a0)
            if(ldrValue > withLdrTimer.maxAnalog)
                withLdrTimer.maxAnalog = ldrValue

            if(ldrValue < withLdrTimer.minAnalog && ldrValue > 0)
                withLdrTimer.minAnalog = ldrValue

            if(withLdrTimer.maxAnalog - withLdrTimer.minAnalog < 50)
                return


            var brightnessPortion = (ldrValue -  withLdrTimer.minAnalog) / (withLdrTimer.maxAnalog - withLdrTimer.minAnalog)
            if(brightnessPortion < 0)
                return;

            if(brightnessPortion < 0.25)
                brightnessPortion = 0;
            rgbLed.rgb(interactiveContainer.controlsItem.red.value / 255 * brightnessPortion ,
                       interactiveContainer.controlsItem.green.value/ 255 * brightnessPortion ,
                       interactiveContainer.controlsItem.blue.value / 255 * brightnessPortion);
        }
    }

    Item {
        id              :   hardeness
        visible         :   !gameDisabled
        anchors.left    :   interactiveContainer.right
        anchors.top     :   interactiveContainer.top
        anchors.bottom  :   interactiveContainer.bottom
        anchors.right   :   parent.right

        Rectangle{
            color           :   "transparent"
            anchors.fill    :   radioCont
            border.width    :   width * 0.02

            border.color    :   {
                if(easyRadioButton.checked)
                    return "green"
                else if(mediumRadioButton.checked)
                    return "blue"
                else
                    return "red"
            }

            anchors.margins :   -width * 0.1
            radius          :   width * 0.1
        }

        ColumnLayout   {

            id                  :   radioCont
            anchors.centerIn    :   parent

            RoboskopRadioButton{
                id                  :   easyRadioButton
                text                :   "Kolay"
                checked             :   true
                buttonColor         :   "green"
                textColor           :   "green"
                onCheckedChanged    :   tolerance = 50
            }

            RoboskopRadioButton{
                id                  :   mediumRadioButton
                text                :   "Orta"
                buttonColor         :   "blue"
                textColor           :   "blue"
                onCheckedChanged    :   tolerance = 35
            }

            RoboskopRadioButton{
                id                  :   hardRadioButton
                text                :   "Zor"
                buttonColor         :   "red"
                textColor           :   "red"
                onCheckedChanged    :   tolerance = 20
            }
        }
    }
}
