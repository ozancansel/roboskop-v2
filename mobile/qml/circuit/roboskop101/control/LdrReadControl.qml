import QtQuick 2.0
import QtQuick.Controls 2.1
import QtCharts 2.1
import Roboskop 1.0
import "../../../chart"
import "../../../controls"
import "../../../theme"
import "../../template"
import "../../../enum"

Rectangle{

    readonly property real chartWidth               :   0.6

    readonly property real controlButtonHeight      :   0.08
    readonly property real currentStateTextHeight   :   0.05
    readonly property real controlSpacing           :   0.02

    property bool   reading                         :   false

    function    startExample(){
        reading = true;
//        startToRead()
    }

    function    stopExample(){
        reading = false;
//        stopToRead()
    }

    function    startToRead(){
//        bluetooth.send("{s}")
    }

    function    stopToRead(){
//        bluetooth.send("{p}")
    }


    ArduinoProxy{
        id      :   proxy
        socket  :   bluetooth
    }

    ArduinoConstants{
        id      :   constants
    }

    AnalogReadTemplate{
        id                  :   chart
        width               :   parent.width
        anchors.left        :   parent.left
        anchors.top         :   parent.top
        anchors.bottom      :   parent.bottom

        chart.title         :   "Isik Olcum"
        axisY.titleText     :   "Parlaklık (lm)"
        axisY.tickCount     :   10
        splineSeries.name   :   "Light Dependent Resistor"

        Component.onCompleted   :   {
            axisY.applyNiceNumbers()
        }
    }

    Timer{
        id                  :   readTimer
        repeat              :   true
        running             :   reading
        interval            :   50
        onTriggered         :   {
            var value = proxy.analogRead(constants.a0)

            chart.splineSeries.newPoint(value)
//            chart.splineSeries.append()
        }
    }
}
