import QtQuick 2.0
import QtQuick.Controls 2.1
import Roboskop 1.0
import "../../../theme"
import "../../../controls"
import "../../../../"

Rectangle {

    anchors.centerIn        :   parent


    //Margins
    readonly property real slidersContainerMargin       :   0.05

    //Width
    readonly property real slidersContainerWidth        :   0.6
    readonly property real slidersTextLabelWidth        :   0.5

    //Height
    readonly property real controlsContainerHeight      :   0.85
    readonly property real headerTextHeight             :   0.2
    readonly property real applyButtonContainerHeight   :   0.11
    readonly property real applyButtonWidth             :   0.7

    //Spacing
    readonly property real slidersContainerSpacing      :   0.05

    //FontSize
    readonly property real slidersTextLabelFontSize     :   0.3
    readonly property real slidersValueTextFontSize     :   0.3
    readonly property real headerTextFontSize           :   0.3

    //Color
    readonly property color backgroundColor             :   "white"
    readonly property color headerBackgroundColor       :   "#34465e"

    //Pin Conf
    readonly property int redPin                        :   11
    readonly property int yellowPin                     :   10
    readonly property int greenPin                      :   9

    //Variables
    property int redDuration    :   redSpinner.value
    property int yellowDuration :   yellowSpinner.value
    property int greenDuration  :   greenSpinner.value

    color                       :   backgroundColor

    function    startExample(){
        applyDurations()
    }

    function    stopExample(){

    }

    function applyDurations(){
        trafficLightAnimation.restart()
    }

//    onRedDurationChanged    :   if(trafficLightAnimation.running)   trafficLightAnimation.restart()
//    onYellowDurationChanged :   if(trafficLightAnimation.running)   trafficLightAnimation.restart()
//    onGreenDurationChanged  :   if(trafficLightAnimation.running)   trafficLightAnimation.restart()

    ArduinoProxy{
        id      :   proxy
        socket  :   bluetooth
    }

    ArduinoConstants{
        id      :   constants
    }

    Rectangle{

        id                      :   controlsContainer
        width                   :   SizeBinding.size(parent.width * 0.6 , SizeBinding.mm(400) , SizeBinding.mm(10))
        height                  :   parent.height * controlsContainerHeight
        anchors.centerIn        :   parent
        color                   :   "#1bb7ea"

        Rectangle{
            id                  :   headerTextContainer
            height              :   parent.height * headerTextHeight
            width               :   parent.width
            color               :   headerBackgroundColor

            Text {
                id              :   headerText
                text            :   "Trafik Lambası Kontrol"
                font.pixelSize  :   parent.height * headerTextFontSize
                font.family     :   FontCollection.headerFontName
                anchors.centerIn:   parent
                color           :   "white"
            }
        }

        Rectangle {


            id                  :   slidersContainer
            anchors.top         :   headerTextContainer.bottom
            width               :   parent.width
            height              :   parent.height * (1 - headerTextHeight)
            color               :   "transparent"

            Column {

                id                      :   slidersColumn
                anchors.fill            :   parent
                anchors.margins         :   parent.height * slidersContainerMargin
                spacing                 :   height * slidersContainerSpacing

                Rectangle {

                    width       :   parent.width
                    height      :   parent.height * 0.25
                    color       :   "transparent"

                    Rectangle   {
                        id                  :   redDurationTextContainer
                        width               :   parent.width * slidersTextLabelWidth
                        height              :   parent.height
                        color               :   "transparent"
                        Text {
                            id              :   redDurationText
                            anchors.left    :   parent.left
                            text            :   "Kırmızı"
                            anchors.centerIn:   parent
                            color           :   "red"
                            font.pixelSize  :   parent.height * slidersTextLabelFontSize
                            font.family     :   Global.labelFontName
                        }
                    }

                    RoboskopSpinBox{
                        id              :   redSpinner
                        anchors.left    :   redDurationTextContainer.right
                        height          :   parent.height
                        width           :   parent.width * (1 - slidersTextLabelWidth)
                        font.pixelSize  :   height  *   slidersValueTextFontSize
                        from                :   0
                        to                  :   20000
                        value               :   3500
                        stepSize            :   200
                        editable            :   true
                    }
                }

                Rectangle{
                    width       :   parent.width
                    height      :   parent.height * 0.25
                    color       :   "transparent"


                    Rectangle{

                        id                  :   yellowDurationTextContainer
                        width               :   parent.width * slidersTextLabelWidth
                        height              :   parent.height
                        color               :   "transparent"


                        Text {
                            id              :   yelllowDurationText
                            anchors.left    :   parent.left
                            text            :   "Sarı"
                            anchors.centerIn:   parent
                            color           :   "yellow"
                            font.pixelSize  :   parent.height * slidersTextLabelFontSize
                            font.family     :   Global.labelFontName
                        }
                    }

                    RoboskopSpinBox {
                        anchors.left        :   yellowDurationTextContainer.right
                        height              :   parent.height
                        width               :   parent.width * (1 - slidersTextLabelWidth)
                        id                  :   yellowSpinner
                        value               :   800
                        from                :   0
                        to                  :   20000
                        stepSize            :   200
                        font.pixelSize      :   height  *   slidersValueTextFontSize
                        up.indicator.width  :   width * 0.2
                        down.indicator.width: width * 0.2
                    }
                }

                Rectangle{
                    width               :   parent.width
                    height              :   parent.height * 0.25
                    color               :   "transparent"


                    Rectangle{

                        id                  :   greenDurationTextContainer
                        width               :   parent.width * slidersTextLabelWidth
                        height              :   parent.height
                        color               :   "transparent"

                        Text {
                            id              :   greenDurationText
                            text            :   "Yeşil"
                            anchors.centerIn:   parent
                            color           :   "green"
                            font.pixelSize  :   parent.height * slidersTextLabelFontSize
                            font.family     :   Global.labelFontName
                        }
                    }

                    RoboskopSpinBox{
                        anchors.left            :   greenDurationTextContainer.right
                        height                  :   parent.height
                        width                   :   parent.width * (1 - slidersTextLabelWidth)
                        id                      :   greenSpinner
                        value                   :   2000
                        from                    :   0
                        to                      :   20000
                        stepSize                :   200
                        font.pixelSize          :   height  *   slidersValueTextFontSize
                        up.indicator.width      :   width * 0.2
                        down.indicator.width    :   width * 0.2
                    }
                }

                Rectangle{

                    width   :   parent.width
                    height  :   parent.height * applyButtonContainerHeight
                    color   :   "transparent"

                    RoboskopButton{
                        id              :   applyConfigurationsButton
                        anchors.centerIn:   parent
                        height          :   parent.height
                        width           :   parent.width * applyButtonWidth
                        text            :   "Ayarları uygula"
                        onClicked       :   {
                            applyDurations()
                        }
                        controlColor    :   applyConfigurationsButton.pink
                    }
                }
            }
        }

        SequentialAnimation {
            id          :   trafficLightAnimation
            loops       :   Animation.Infinite

            ScriptAction    {
                script  :   {
                    proxy.digitalWrite(redPin , constants.high)
                    proxy.digitalWrite(yellowPin , constants.low)
                    proxy.digitalWrite(greenPin , constants.low)
                }
            }

            PauseAnimation {
                duration    :   redDuration
            }

            ScriptAction{
                script  :   {
                    proxy.digitalWrite(redPin , constants.low)
                    proxy.digitalWrite(yellowPin , constants.high)
                }
            }

            PauseAnimation {
                duration:   yellowDuration
            }

            ScriptAction{
                script  :   {
                    proxy.digitalWrite(yellowPin , constants.low)
                    proxy.digitalWrite(greenPin , constants.high)
                }
            }

            PauseAnimation {
                duration: greenDuration
            }

            ScriptAction{
                script  :   {
                    proxy.digitalWrite(greenPin , constants.low)
                    proxy.digitalWrite(yellowPin , constants.high)
                }
            }


            PauseAnimation {
                duration: yellowDuration
            }
        }
    }
}
