import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real wireThickness    :   getScaledX(3.2) < 1 ? 1 : getScaledX(3.2)

    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(0)

    ArduinoUno{
        id      :   uno
        x       :   getScaledX(0) + slideX
        y       :   getScaledY(320) + slideY
        width   :   getScaledX(380)
        height  :   getScaledY(250)
    }


    BluetoothKit{
        id      :   bluetoothKit
        x       :   getScaledX(490) + slideX
        y       :   getScaledY(10) + slideY
        width   :   getScaledX(150)
        height  :   getScaledY(150 * (40 / 25))
    }

    AcousticKit{
        id      :   acousticKit
        x       :   getScaledX(110) + slideX
        y       :   getScaledY(30) + slideY
        width   :   getScaledX(270)
        height  :   getScaledY(180)
    }

    //Ard gnd -> Acoustic GND

    Wire{
        id          :   ardGndToAcGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(100)
        centerX     :   uno.getDigitalPinPos(14).x
        centerY     :   uno.getDigitalPinPos(14).y
        wireColor   :   Color.black
        reverse     :   true
        thickness   :   wireThickness
    }

    Wire{
        id          :   ardGndToAcGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToAcGnd1.getVerticalDiffAbs(acousticKit.getPinPos(0))
        centerX     :   acousticKit.getPinPos(0).x
        centerY     :   acousticKit.getPinPos(0).y
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   ardGndToAcGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToAcGnd1.getHorizontalDiffAbs(ardGndToAcGnd3.end())
        centerX     :   ardGndToAcGnd1.end().x
        centerY     :   ardGndToAcGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    //Ard gnd -> Acoustic GND bitiş

    //Ard 12 -> Acoustic Echo
    Wire{
        id          :   ard12ToAcEcho1
        alignment   :   Qt.Vertical
        length      :   getScaledY(90)
        centerX     :   uno.getDigitalPinPos(12).x
        centerY     :   uno.getDigitalPinPos(12).y
        wireColor   :   Color.grey
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ard12ToAcEcho3
        alignment   :   Qt.Vertical
        length      :   ard12ToAcEcho1.getVerticalDiffAbs(acousticKit.getPinPos(1))
        centerX     :   acousticKit.getPinPos(1).x
        centerY     :   acousticKit.getPinPos(1).y
        wireColor   :   Color.grey
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard12ToAcEcho2
        alignment   :   Qt.Horizontal
        length      :   ard12ToAcEcho1.getHorizontalDiffAbs(ard12ToAcEcho3.end())
        centerX     :   ard12ToAcEcho1.end().x
        centerY     :   ard12ToAcEcho1.end().y
        wireColor   :   Color.grey
        thickness   :   wireThickness
    }

    //Ard 12 -> Acoustic Echo bitiş

    //Ard 12 -> Acoustic trigger

    Wire{
        id          :   ard11ToAcTrigger1
        alignment   :   Qt.Vertical
        length      :   getScaledY(80)
        centerX     :   uno.getDigitalPinPos(11).x
        centerY     :   uno.getDigitalPinPos(11).y
        wireColor   :   Color.purple
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ard11ToAcTrigger3
        alignment   :   Qt.Vertical
        length      :   ard11ToAcTrigger1.getVerticalDiffAbs(acousticKit.getPinPos(2))
        centerX     :   acousticKit.getPinPos(2).x
        centerY     :   acousticKit.getPinPos(2).y
        wireColor   :   Color.purple
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard11ToAcTrigger2
        alignment   :   Qt.Horizontal
        length      :   ard11ToAcTrigger1.getHorizontalDiffAbs(ard11ToAcTrigger3.end())
        centerX     :   ard11ToAcTrigger1.end().x
        centerY     :   ard11ToAcTrigger1.end().y
        wireColor   :   Color.purple
        thickness   :   wireThickness
    }

    //Ard 12 -> Acoustic trigger bitiş

    //Ard 5v -> Acoustic Vcc

    Wire{
        id          :   ard5vToAc5v1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getPowerPinPos(3).x
        centerY     :   uno.getPowerPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard5vToAc5v2
        alignment   :   Qt.Horizontal
        length      :   getScaledX(220)
        centerX     :   ard5vToAc5v1.end().x
        centerY     :   ard5vToAc5v1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ard5vToAc5v3
        alignment   :   Qt.Vertical
        length      :   getScaledY(385)
        centerX     :   ard5vToAc5v2.end().x
        centerY     :   ard5vToAc5v2.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ard5vToAc5v5
        alignment   :   Qt.Vertical
        length      :   ard5vToAc5v3.getVerticalDiffAbs(acousticKit.getPinPos(3))
        centerX     :   acousticKit.getPinPos(3).x
        centerY     :   acousticKit.getPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard5vToAc5v4
        alignment   :   Qt.Horizontal
        length      :   ard5vToAc5v3.getHorizontalDiffAbs(ard5vToAc5v5.end())
        centerX     :   ard5vToAc5v3.end().x
        centerY     :   ard5vToAc5v3.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }


    //Bt Rx ->Uno Tx

    Wire{
        id          :   ardTxToBtRx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(1).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.green
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx3
        alignment   :   Qt.Vertical
        length      :   ardTxToBtRx1.getVerticalDiffAbs(bluetoothKit.getPinPos(0))
        centerX     :   bluetoothKit.getPinPos(0).x
        centerY     :   bluetoothKit.getPinPos(0).y
        wireColor   :   Color.green
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx2
        alignment   :   Qt.Horizontal
        length      :   ardTxToBtRx1.getHorizontalDiffAbs(ardTxToBtRx3.end())
        centerX     :   ardTxToBtRx1.end().x
        centerY     :   ardTxToBtRx1.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness


    }

    //Bt Rx ->Uno Tx Bitiş

    //Uno Rx -> Bt Tx

    Wire{
        id          :   ardRxToBtTx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getDigitalPinPos(0).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.blue
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx3
        alignment   :   Qt.Vertical
        length      :   ardRxToBtTx1.getVerticalDiffAbs(bluetoothKit.getPinPos(1))
        centerX     :   bluetoothKit.getPinPos(1).x
        centerY     :   bluetoothKit.getPinPos(1).y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx2
        alignment   :   Qt.Horizontal
        length      :   ardRxToBtTx1.getHorizontalDiffAbs(ardRxToBtTx3.end())
        centerX     :   ardRxToBtTx1.end().x
        centerY     :   ardRxToBtTx1.end().y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    //Uno Rx -> Bt Tx bitiş


    //Uno gnd -> Bt gnd

    Wire{
        id          :   ardGndToBtGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(55)
        centerX     :   uno.getPowerPinPos(4).x
        centerY     :   uno.getPowerPinPos(4).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToBtGnd1.getVerticalDiffAbs(bluetoothKit.getPinPos(2))
        centerX     :   bluetoothKit.getPinPos(2).x
        centerY     :   bluetoothKit.getPinPos(2).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToBtGnd1.getHorizontalDiffAbs(ardGndToBtGnd3.end())
        centerX     :   ardGndToBtGnd1.end().x
        centerY     :   ardGndToBtGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    //Uno gnd -> Bt gnd bitiş

    //Uno 3.3 -> Bt güç

    Wire{
        id          :   ard33VToBtPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(65)
        centerX     :   uno.getPowerPinPos(2).x
        centerY     :   uno.getPowerPinPos(2).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard33VToBtPower3
        alignment   :   Qt.Vertical
        length      :   ard33VToBtPower1.getVerticalDiffAbs(bluetoothKit.getPinPos(3))
        centerX     :   bluetoothKit.getPinPos(3).x
        centerY     :   bluetoothKit.getPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard33VToBtPower2
        alignment   :   Qt.Horizontal
        length      :   ard33VToBtPower1.getHorizontalDiffAbs(ard33VToBtPower3.end())
        centerX     :   ard33VToBtPower1.end().x
        centerY     :   ard33VToBtPower1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    //Uno 3.3 -> Bt güç bitiş

}
