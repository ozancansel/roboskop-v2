import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real wireThickness    :   getScaledX(3.2)

    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(0)


    ArduinoUno{
        id      :   uno
        x       :   getScaledX(50) + slideX
        y       :   getScaledY(310) + slideY
        width   :   getScaledX(380)
        height  :   getScaledY(250)
    }

    RgbLedKit{
        id          :   rgbLedKit
        x           :   getScaledX(198) + slideX
        y           :   getScaledY(10) + slideY
        width       :   getScaledX(200)
        height      :   getScaledY(200)
    }

    BluetoothKit{
        id      :   bluetoothKit
        x       :   getScaledX(450) + slideX
        y       :   getScaledY(10) + slideY
        width   :   getScaledX(150)
        height  :   getScaledY(150 * (40 / 25))
    }

    //  Ard 11 -> Rgb Red

    Wire{
        id          :   ard11ToRgbRed1
        alignment   :   Qt.Vertical
        length      :   getScaledY(80)
        centerX     :   uno.getDigitalPinPos(11).x
        centerY     :   uno.getDigitalPinPos(11).y
        wireColor   :   Color.red
        reverse     :   true
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard11ToRgbRed3
        alignment   :   Qt.Vertical
        length      :   ard11ToRgbRed1.getVerticalDiffAbs(rgbLedKit.getPinPos(0))
        centerX     :   rgbLedKit.getPinPos(0).x
        centerY     :   rgbLedKit.getPinPos(0).y
        wireColor   :   Color.red
        thickness   :   wireThickness
        z           :   2
    }

    Wire{
        id          :   ard11ToRgbRed2
        alignment   :   Qt.Horizontal
        length      :   ard11ToRgbRed1.getHorizontalDiffAbs(ard11ToRgbRed3.end())
        centerX     :   ard11ToRgbRed1.end().x
        centerY     :   ard11ToRgbRed1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    //  Ard 11 -> Rgb Red Bitiş

    //  Ard 10 -> Rgb Green

    Wire{
        id          :   ard10ToRgbGreen1
        alignment   :   Qt.Vertical
        length      :   getScaledY(70)
        centerX     :   uno.getDigitalPinPos(10).x
        centerY     :   uno.getDigitalPinPos(10).y
        wireColor   :   Color.green
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard10ToRgbGreen3
        alignment   :   Qt.Vertical
        length      :   ard10ToRgbGreen1.getVerticalDiffAbs(rgbLedKit.getPinPos(1))
        centerX     :   rgbLedKit.getPinPos(1).x
        centerY     :   rgbLedKit.getPinPos(1).y
        wireColor   :   Color.green
        thickness   :   wireThickness
        z           :   2

    }

    Wire{
        id          :   ard10ToRgbGreen2
        alignment   :   Qt.Horizontal
        length      :   ard10ToRgbGreen1.getHorizontalDiffAbs(ard10ToRgbGreen3.end())
        centerX     :   ard10ToRgbGreen1.end().x
        centerY     :   ard10ToRgbGreen1.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness

    }

    //  Ard 10 -> Rgb Green Bitiş

    //  Ard 9 -> Rgb Blue

    Wire{
        id          :   ard9ToRgbBlue1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(9).x
        centerY     :   uno.getDigitalPinPos(9).y
        wireColor   :   Color.blue
        reverse     :   true
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard9ToRgbBlue3
        alignment   :   Qt.Vertical
        length      :   ard9ToRgbBlue1.getVerticalDiffAbs(rgbLedKit.getPinPos(2))
        centerX     :   rgbLedKit.getPinPos(2).x
        centerY     :   rgbLedKit.getPinPos(2).y
        wireColor   :   Color.blue
        thickness   :   wireThickness
        z           :   2

    }

    Wire{
        id          :   ard9ToRgbBlue2
        alignment   :   Qt.Horizontal
        length      :   ard9ToRgbBlue1.getHorizontalDiffAbs(ard9ToRgbBlue3.end())
        centerX     :   ard9ToRgbBlue1.end().x
        centerY     :   ard9ToRgbBlue1.end().y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    //  Ard 10 -> Rgb Green Bitiş

    //  Ard 3.3v -> Rgb Güç

    Wire{
        id          :   ard33vToRgbPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(40)
        centerX     :   uno.getPowerPinPos(2).x
        centerY     :   uno.getPowerPinPos(2).y
        wireColor   :   Color.white
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard33vToRgbPower2
        alignment   :   Qt.Horizontal
        length      :   getScaledX(250)
        centerX     :   ard33vToRgbPower1.end().x
        centerY     :   ard33vToRgbPower1.end().y
        wireColor   :   Color.white
        reverse     :   true
        thickness   :   wireThickness
        z           :   2
    }

    Wire{
        id          :   ard33vToRgbPower3
        alignment   :   Qt.Vertical
        length      :   getScaledY(360)
        centerX     :   ard33vToRgbPower2.end().x
        centerY     :   ard33vToRgbPower2.end().y
        wireColor   :   Color.white
        reverse     :   true
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard33vToRgbPower5
        alignment   :   Qt.Vertical
        length      :   ard33vToRgbPower3.getVerticalDiffAbs(rgbLedKit.getPinPos(3))
        centerX     :   rgbLedKit.getPinPos(3).x
        centerY     :   rgbLedKit.getPinPos(3).y
        wireColor   :   Color.white
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard33vToRgbPower4
        alignment   :   Qt.Horizontal
        length      :   ard33vToRgbPower3.getHorizontalDiffAbs(ard33vToRgbPower5.end())
        centerX     :   ard33vToRgbPower3.end().x
        centerY     :   ard33vToRgbPower3.end().y
        wireColor   :   Color.white
        thickness   :   wireThickness
        z           :   0
    }



    //Bt Rx ->Uno Tx

    Wire{
        id          :   ardTxToBtRx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(1).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.green
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx3
        alignment   :   Qt.Vertical
        length      :   ardTxToBtRx1.getVerticalDiffAbs(bluetoothKit.getPinPos(0))
        centerX     :   bluetoothKit.getPinPos(0).x
        centerY     :   bluetoothKit.getPinPos(0).y
        wireColor   :   Color.green
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx2
        alignment   :   Qt.Horizontal
        length      :   ardTxToBtRx1.getHorizontalDiffAbs(ardTxToBtRx3.end())
        centerX     :   ardTxToBtRx1.end().x
        centerY     :   ardTxToBtRx1.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness


    }

    //Bt Rx ->Uno Tx Bitiş

    //Uno Rx -> Bt Tx

    Wire{
        id          :   ardRxToBtTx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getDigitalPinPos(0).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.blue
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx3
        alignment   :   Qt.Vertical
        length      :   ardRxToBtTx1.getVerticalDiffAbs(bluetoothKit.getPinPos(1))
        centerX     :   bluetoothKit.getPinPos(1).x
        centerY     :   bluetoothKit.getPinPos(1).y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx2
        alignment   :   Qt.Horizontal
        length      :   ardRxToBtTx1.getHorizontalDiffAbs(ardRxToBtTx3.end())
        centerX     :   ardRxToBtTx1.end().x
        centerY     :   ardRxToBtTx1.end().y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    //Uno Rx -> Bt Tx bitiş


    //Uno gnd -> Bt gnd

    Wire{
        id          :   ardGndToBtGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(55)
        centerX     :   uno.getPowerPinPos(4).x
        centerY     :   uno.getPowerPinPos(4).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToBtGnd1.getVerticalDiffAbs(bluetoothKit.getPinPos(2))
        centerX     :   bluetoothKit.getPinPos(2).x
        centerY     :   bluetoothKit.getPinPos(2).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToBtGnd1.getHorizontalDiffAbs(ardGndToBtGnd3.end())
        centerX     :   ardGndToBtGnd1.end().x
        centerY     :   ardGndToBtGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    //Uno gnd -> Bt gnd bitiş

    //Uno 3.3 -> Bt güç

    Wire{
        id          :   ard5VToBtPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(65)
        centerX     :   uno.getPowerPinPos(3).x
        centerY     :   uno.getPowerPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard5VToBtPower3
        alignment   :   Qt.Vertical
        length      :   ard5VToBtPower1.getVerticalDiffAbs(bluetoothKit.getPinPos(3))
        centerX     :   bluetoothKit.getPinPos(3).x
        centerY     :   bluetoothKit.getPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard5VToBtPower2
        alignment   :   Qt.Horizontal
        length      :   ard5VToBtPower1.getHorizontalDiffAbs(ard5VToBtPower3.end())
        centerX     :   ard5VToBtPower1.end().x
        centerY     :   ard5VToBtPower1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    //Uno 3.3 -> Bt güç bitiş


}
