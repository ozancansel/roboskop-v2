import QtQuick 2.0
import "../template"

SketchTemplate{
    Text{
        anchors.centerIn    :   parent
        font.pixelSize      :   getScaledY(50)
        text                :   "Bu ders henüz geliştirme aşamasındadır."
    }
}
