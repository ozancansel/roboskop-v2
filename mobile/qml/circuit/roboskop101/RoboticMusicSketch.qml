import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real wireThickness    :   getScaledX(3.2)

    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(0)

    ArduinoUno{
        id      :   uno
        x       :   getScaledX(0) + slideX
        y       :   getScaledY(320) + slideY
        width   :   getScaledX(380)
        height  :   getScaledY(250)
    }

    BluetoothKit{
        id      :   bluetoothKit
        x       :   getScaledX(490) + slideX
        y       :   getScaledY(10) + slideY
        width   :   getScaledX(150)
        height  :   getScaledY(150 * (40 / 25))
    }

    BuzzerKit{
        id      :   buzzerKit
        x       :   getScaledX(198) + slideX
        y       :   getScaledY(10) + slideY
        width   :   getScaledX(200)
        height  :   getScaledY(200)
    }

    //Ard 8 -> Buzzer 5v

    Wire{
        id          :   ard8ToBuzzer5v1
        alignment   :   Qt.Vertical
        length      :   getScaledY(90)
        centerX     :   uno.getDigitalPinPos(9).x
        centerY     :   uno.getDigitalPinPos(9).y
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
        z           :   5
    }

    Wire{
        id          :   ard8ToBuzzer5v3
        alignment   :   Qt.Vertical
        length      :   ard8ToBuzzer5v1.getVerticalDiffAbs(buzzerKit.getPinPos(0))
        centerX     :   buzzerKit.getPinPos(0).x
        centerY     :   buzzerKit.getPinPos(0).y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   ard8ToBuzzer5v2
        alignment   :   Qt.Horizontal
        length      :   ard8ToBuzzer5v1.getHorizontalDiffAbs(ard8ToBuzzer5v3.end())
        centerX     :   ard8ToBuzzer5v1.end().x
        centerY     :   ard8ToBuzzer5v1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    //Ard 8 -> Buzzer 5v bitiş

    //Ard Gnd -> Buzzer GND

    Wire{
        id          :   ardGndTo5v1
        alignment   :   Qt.Vertical
        length      :   getScaledY(80)
        centerX     :   uno.getDigitalPinPos(14).x
        centerY     :   uno.getDigitalPinPos(14).y
        wireColor   :   Color.black
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   ardGndTo5v3
        alignment   :   Qt.Vertical
        length      :   ardGndTo5v1.getVerticalDiffAbs(buzzerKit.getPinPos(1))
        centerX     :   buzzerKit.getPinPos(1).x
        centerY     :   buzzerKit.getPinPos(1).y
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   ardGndTo5v2
        alignment   :   Qt.Horizontal
        length      :   ardGndTo5v1.getHorizontalDiffAbs(ardGndTo5v3.end())
        centerX     :   ardGndTo5v1.end().x
        centerY     :   ardGndTo5v1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness
        z           :   2
    }

    //Ard Gnd -> Buzzer GND bitiş

    //Bt Rx ->Uno Tx

    Wire{
        id          :   ardTxToBtRx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(1).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.green
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx3
        alignment   :   Qt.Vertical
        length      :   ardTxToBtRx1.getVerticalDiffAbs(bluetoothKit.getPinPos(0))
        centerX     :   bluetoothKit.getPinPos(0).x
        centerY     :   bluetoothKit.getPinPos(0).y
        wireColor   :   Color.green
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx2
        alignment   :   Qt.Horizontal
        length      :   ardTxToBtRx1.getHorizontalDiffAbs(ardTxToBtRx3.end())
        centerX     :   ardTxToBtRx1.end().x
        centerY     :   ardTxToBtRx1.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness


    }

    //Bt Rx ->Uno Tx Bitiş

    //Uno Rx -> Bt Tx

    Wire{
        id          :   ardRxToBtTx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getDigitalPinPos(0).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.blue
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx3
        alignment   :   Qt.Vertical
        length      :   ardRxToBtTx1.getVerticalDiffAbs(bluetoothKit.getPinPos(1))
        centerX     :   bluetoothKit.getPinPos(1).x
        centerY     :   bluetoothKit.getPinPos(1).y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx2
        alignment   :   Qt.Horizontal
        length      :   ardRxToBtTx1.getHorizontalDiffAbs(ardRxToBtTx3.end())
        centerX     :   ardRxToBtTx1.end().x
        centerY     :   ardRxToBtTx1.end().y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    //Uno Rx -> Bt Tx bitiş


    //Uno gnd -> Bt gnd

    Wire{
        id          :   ardGndToBtGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(25)
        centerX     :   uno.getPowerPinPos(4).x
        centerY     :   uno.getPowerPinPos(4).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToBtGnd1.getVerticalDiffAbs(bluetoothKit.getPinPos(2))
        centerX     :   bluetoothKit.getPinPos(2).x
        centerY     :   bluetoothKit.getPinPos(2).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToBtGnd1.getHorizontalDiffAbs(ardGndToBtGnd3.end())
        centerX     :   ardGndToBtGnd1.end().x
        centerY     :   ardGndToBtGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    //Uno gnd -> Bt gnd bitiş

    //Uno 3.3 -> Bt güç

    Wire{
        id          :   ard5VToBtPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(35)
        centerX     :   uno.getPowerPinPos(3).x
        centerY     :   uno.getPowerPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard5VToBtPower3
        alignment   :   Qt.Vertical
        length      :   ard5VToBtPower1.getVerticalDiffAbs(bluetoothKit.getPinPos(3))
        centerX     :   bluetoothKit.getPinPos(3).x
        centerY     :   bluetoothKit.getPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard5VToBtPower2
        alignment   :   Qt.Horizontal
        length      :   ard5VToBtPower1.getHorizontalDiffAbs(ard5VToBtPower3.end())
        centerX     :   ard5VToBtPower1.end().x
        centerY     :   ard5VToBtPower1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    //Uno 3.3 -> Bt güç bitiş

}
