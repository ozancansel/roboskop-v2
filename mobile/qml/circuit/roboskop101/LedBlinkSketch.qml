import QtQuick 2.0

import "../template"
import "../"
import "../../theme"

SketchTemplate {

    readonly property real wireThickness    :   getScaledX(3.2)
    readonly property real slideX           :   getScaledX(150)
    readonly property real slideY           :   getScaledY(20)

    ArduinoUno{
        id      :   uno
        x       :   getScaledX(50) + slideX
        y       :   getScaledY(290) + slideY
        width   :   getScaledX(380)
        height  :   getScaledY(250)
    }

    LedKit{
        id      :   led
        x       :   getScaledX(145) + slideX
        y       :   getScaledY(30) + slideY
        width   :   getScaledX(200)
        height  :   getScaledY(200)
    }

    BluetoothKit{
        id      :   bluetoothKit
        x       :   getScaledX(500) + slideX
        y       :   getScaledY(0) + slideY
        width   :   getScaledX(150)
        height  :   getScaledY(150 * (40 / 25))
    }



    //Uno 8 -> Led Power
    Wire{
        id          :   pin8ToLed1
        alignment   :   Qt.Vertical
        length      :   getScaledY(30)
        centerX     :   uno.getDigitalPinPos(8).x
        centerY     :   uno.getDigitalPinPos(8).y
        z           :   5
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }


    Wire{
        id          :   pin8ToLed3
        alignment   :   Qt.Vertical
        length      :   pin8ToLed1.getVerticalDiffAbs(led.getPinPos(0))
        centerX     :   led.getPinPos(0).x
        centerY     :   led.getPinPos(0).y
        z           :   4
        wireColor   :   Color.red
        thickness   :   wireThickness
    }

    Wire{
        id          :   pin8ToLed2
        alignment   :   Qt.Horizontal
        length      :   pin8ToLed1.getHorizontalDiffAbs(pin8ToLed3.end())
        centerX     :   pin8ToLed1.end().x
        centerY     :   pin8ToLed1.end().y
        z           :   4
        wireColor   :   Color.red
        thickness   :   wireThickness
        reverse     :   true
    }

    //Uno 8 -> Led Power bitiş

    //Uno Gnd -> Led Gnd

    Wire{
        id          :   gndToLed1
        alignment   :   Qt.Vertical
        length      :   getScaledY(55)
        centerX     :   uno.getPowerPinPos(4).x
        centerY     :   uno.getPowerPinPos(4).y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   gndToLed2
        alignment   :   Qt.Horizontal
        length      :   getScaledX(200)
        centerX     :   gndToLed1.end().x
        centerY     :   gndToLed1.end().y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   gndToLed3
        alignment   :   Qt.Vertical
        length      :   getScaledY(315)
        centerX     :   gndToLed2.end().x
        centerY     :   gndToLed2.end().y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
        reverse     :   true
    }

    Wire{
        id          :   gndToLed5
        alignment   :   Qt.Vertical
        length      :   gndToLed3.getVerticalDiffAbs(led.getPinPos(1))
        centerX     :   led.getPinPos(1).x
        centerY     :   led.getPinPos(1).y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
    }

    Wire{
        id          :   gndToLed4
        alignment   :   Qt.Horizontal
        length      :   gndToLed3.getHorizontalDiffAbs(gndToLed5.end())
        centerX     :   gndToLed3.end().x
        centerY     :   gndToLed3.end().y
        z           :   4
        wireColor   :   Color.black
        thickness   :   wireThickness
        reverse     :   true
    }

    //Bt Rx ->Uno Tx

    Wire{
        id          :   ardTxToBtRx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(60)
        centerX     :   uno.getDigitalPinPos(1).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.green
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx3
        alignment   :   Qt.Vertical
        length      :   ardTxToBtRx1.getVerticalDiffAbs(bluetoothKit.getPinPos(0))
        centerX     :   bluetoothKit.getPinPos(0).x
        centerY     :   bluetoothKit.getPinPos(0).y
        wireColor   :   Color.green
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardTxToBtRx2
        alignment   :   Qt.Horizontal
        length      :   ardTxToBtRx1.getHorizontalDiffAbs(ardTxToBtRx3.end())
        centerX     :   ardTxToBtRx1.end().x
        centerY     :   ardTxToBtRx1.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness


    }

    //Bt Rx ->Uno Tx Bitiş

    //Uno Rx -> Bt Tx

    Wire{
        id          :   ardRxToBtTx1
        alignment   :   Qt.Vertical
        length      :   getScaledY(50)
        centerX     :   uno.getDigitalPinPos(0).x
        centerY     :   uno.getDigitalPinPos(1).y
        wireColor   :   Color.blue
        reverse     :   true
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx3
        alignment   :   Qt.Vertical
        length      :   ardRxToBtTx1.getVerticalDiffAbs(bluetoothKit.getPinPos(1))
        centerX     :   bluetoothKit.getPinPos(1).x
        centerY     :   bluetoothKit.getPinPos(1).y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardRxToBtTx2
        alignment   :   Qt.Horizontal
        length      :   ardRxToBtTx1.getHorizontalDiffAbs(ardRxToBtTx3.end())
        centerX     :   ardRxToBtTx1.end().x
        centerY     :   ardRxToBtTx1.end().y
        wireColor   :   Color.blue
        thickness   :   wireThickness

    }

    //Uno Rx -> Bt Tx bitiş


    //Uno gnd -> Bt gnd

    Wire{
        id          :   ardGndToBtGnd1
        alignment   :   Qt.Vertical
        length      :   getScaledY(40)
        centerX     :   uno.getPowerPinPos(5).x
        centerY     :   uno.getPowerPinPos(5).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd3
        alignment   :   Qt.Vertical
        length      :   ardGndToBtGnd1.getVerticalDiffAbs(bluetoothKit.getPinPos(2))
        centerX     :   bluetoothKit.getPinPos(2).x
        centerY     :   bluetoothKit.getPinPos(2).y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    Wire{
        id          :   ardGndToBtGnd2
        alignment   :   Qt.Horizontal
        length      :   ardGndToBtGnd1.getHorizontalDiffAbs(ardGndToBtGnd3.end())
        centerX     :   ardGndToBtGnd1.end().x
        centerY     :   ardGndToBtGnd1.end().y
        wireColor   :   Color.black
        thickness   :   wireThickness

    }

    //Uno gnd -> Bt gnd bitiş

    //Uno 3.3 -> Bt güç

    Wire{
        id          :   ard5VToBtPower1
        alignment   :   Qt.Vertical
        length      :   getScaledY(65)
        centerX     :   uno.getPowerPinPos(3).x
        centerY     :   uno.getPowerPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard5VToBtPower3
        alignment   :   Qt.Vertical
        length      :   ard5VToBtPower1.getVerticalDiffAbs(bluetoothKit.getPinPos(3))
        centerX     :   bluetoothKit.getPinPos(3).x
        centerY     :   bluetoothKit.getPinPos(3).y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    Wire{
        id          :   ard5VToBtPower2
        alignment   :   Qt.Horizontal
        length      :   ard5VToBtPower1.getHorizontalDiffAbs(ard5VToBtPower3.end())
        centerX     :   ard5VToBtPower1.end().x
        centerY     :   ard5VToBtPower1.end().y
        wireColor   :   Color.red
        thickness   :   wireThickness

    }

    //Uno 3.3 -> Bt güç bitiş

}
