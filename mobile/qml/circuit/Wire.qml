import QtQuick 2.0
//import QtGraphicalEffects 1.0
import "../theme"

Rectangle {

    //"#8c0000" (Black fill eski)
    property variant    fillColors      :   [ "#418dd9" , "#cc1414" , "#101010" , "#f7f10c" , "#25cc35" , "#999999" , "#ffffff" , "#ef6100" , "#a37911" , "#33ffc5" , "#8c3b00" , "#ab58a2" , "#fa50e6"]
    property variant    borderColors    :   [ "#1b5bb3" , "#8c0000" , "#000000" , "#d6d63a" , "#00a527" , "#848484" , "#999999" , "#ad6a38" , "#8b691d" , "#15c7c5" , "#6c2710" , "#7a3a73" , "#dc00dc"]

    property int        alignment       :   Qt.Vertical
    property double     thickness       :   5
    property int        wireColor       :   0
    property real       length          :   10
    property real       borderWidth     :   0.26
    property real       contrast        :   0.1
    property real       centerX         :   0
    property real       centerY         :   0
    property bool       reverse         :   false
    property real       centerOffset    :   -thickness * 0.5

    function end(){

        var xOffset = alignment === Qt.Horizontal ? length: 0
        var yOffset = alignment === Qt.Vertical ? length: 0

        if(reverse && alignment == Qt.Horizontal)
            xOffset *= -1

        if(reverse && alignment == Qt.Vertical)
            yOffset *= -1

        return Qt.point(centerX + xOffset , centerY + yOffset)
    }

    function start(){
        return Qt.point(centerX , centerY)
    }

    function getHorizontalDiffAbs(otherComponent){
        //        return Math.abs(end().x - otherComponent.x + (1.5 * centerOffset))
        return Math.abs(end().x - otherComponent.x)
    }

    function getVerticalDiffAbs(otherComponent){
        //        return Math.abs(end().y - otherComponent.y - (1.5 * centerOffset))
        return Math.abs(end().y - otherComponent.y)
    }

    id: wireContainer

    color:  fillColors[wireColor]

    radius: thickness * 0.5
    border.width    :   thickness * borderWidth < 1 ? 1 : thickness * borderWidth
    border.color    :   borderColors[wireColor]
    width: alignment ===  Qt.Horizontal ?   Math.abs(length) + Math.abs(2 * centerOffset)  :   thickness
    height: alignment === Qt.Vertical   ?   Math.abs(length) + Math.abs(2 * centerOffset) :   thickness

    x       :   {
        if(reverse &&  alignment === Qt.Horizontal)
            return centerX - (thickness * 0.5) - length
        else
            return centerX - (thickness * 0.5)
    }

    y       :   {

        if(reverse && alignment === Qt.Vertical)
            centerY - (thickness * 0.5) - length
        else
            centerY - (thickness * 0.5)
    }

    Rectangle {
        id      :   lineStart
        x       :   -thickness * 0.25
        y       :   -thickness * 0.25
        width: thickness * 1.5
        height: thickness * 1.5
        border.width: width * 0.25
        radius: width * 0.5
        color: fillColors[Color.red]
        border.color: borderColors[Color.red]
        opacity: 0.65
    }


    Rectangle{
        id      :   lineEnd
        x       :   alignment === Qt.Horizontal ? length - (thickness * 0.25) : -thickness * 0.25
        y       :   alignment === Qt.Vertical ? length - (thickness * 0.25) : -thickness * 0.25
        width: thickness * 1.5
        height: thickness * 1.5
        border.width: width * 0.25
        radius: width * 0.5
        color: fillColors[Color.red]
        border.color: borderColors[Color.red]
        opacity: 0.65
    }



}
