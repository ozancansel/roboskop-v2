import QtQuick 2.0
import "../theme"

Rectangle {

    //Top Margin
    readonly property real kitVisualContainerTopMargin              :   0.01
    readonly property real pinsTopMargin                            :   0.67


    //Bottom Margin
    readonly property real headerTextBottomMargin                   :   0.03
    readonly property real codeTextBottomMargin                     :   0.25

    //Width
    readonly property real acousticCirclesContainerWidth            :   0.75
    readonly property real acousticCircleWidth                      :   0.38

    //Height
    readonly property real kitVisualContainerHeight                 :   0.6
    readonly property real acousticCirclesContainerHeight           :   0.9
    readonly property real acousticCircleHeight                     :   0.9
    readonly property real headerTextHeight                         :   0.11
    readonly property real codeTextHeight                           :   0.07
    readonly property real pinHeight                                :   0.8
    readonly property real pinContainerHeight                       :   0.23


    //Border
    readonly property real containerBorder                          :   0.01
    readonly property real acousticCircleBorder                     :   0.15

    //spacing
    readonly property real pinSpacing                               :   0.1
    readonly property real pinTextSpacing                           :   0.05

    //Radius
    readonly property real acousticCirclesRadius                    :   0.47

    border.width            :   width * containerBorder

    function    getPinPos(idx){

        var halfPinHeight   =   gndPin.height * 0.5
        var halfPinWidth    =   gndPin.width  * 0.5

        switch(idx){
        case 0:
            return mapToItem(parent , pinsContainer.x + gndPinTextContainer.x + gndPin.x + halfPinWidth
                     , pinsContainer.y + gndPinTextContainer.y + gndPin.y + halfPinHeight)
        case 1:
            return mapToItem(parent , pinsContainer.x + echoPinTextContainer.x + echoPin.x + halfPinWidth
                     , pinsContainer.y + echoPinTextContainer.y + echoPin.y + halfPinHeight)
        case 2:
            return mapToItem(parent , pinsContainer.x + triggerPinTextContainer.x + triggerPin.x + halfPinWidth
                     , pinsContainer.y + triggerPinTextContainer.y + triggerPin.y + halfPinHeight)
        case 3:
            return mapToItem(parent , pinsContainer.x + vccPinTextContainer.x + vccPin.x + halfPinWidth
                     , pinsContainer.y + vccPinTextContainer.y + vccPin.y + halfPinHeight)
        }
    }

    Rectangle{
        id                              :   acousticKitVisualContainer
        anchors.top                     :   parent.top
        anchors.topMargin               :   parent.height * kitVisualContainerTopMargin
        anchors.horizontalCenter        :   parent.horizontalCenter
        width                           :   parent.width * acousticCirclesContainerWidth
        height                          :   parent.height * kitVisualContainerHeight
        color                           :   "transparent"

        Rectangle{
            id                      :   circlesContainer
            width                   :   parent.width
            height                  :   parent.height * acousticCirclesContainerHeight
            color                   :   parent.color

            Rectangle{
                id                      :   leftAcousticCircle
                radius                  :   width * acousticCirclesRadius
                anchors.verticalCenter  :   parent.verticalCenter
                anchors.left            :   parent.left
                width                   :   height
                height                  :   parent.height * acousticCircleHeight
                border.width            :   width * acousticCircleBorder
            }

            Rectangle{
                id                      :   rightAcousticCircle
                radius                  :   width * acousticCirclesRadius
                anchors.verticalCenter  :   parent.verticalCenter
                anchors.right           :   parent.right
                width                   :   height
                height                  :   parent.height * acousticCircleHeight
                border.width            :   (width + height) * acousticCircleBorder * 0.5
            }

            Text {
                id                          :   kitCodeText
                anchors.centerIn            :   parent
                text                        :   "#16p12"
                font.pixelSize              :   parent.height * codeTextHeight
            }


        }

        Text {
            id                          :   headerText
            text                        :   qsTr("Mesafe Sensörü")
            anchors.horizontalCenter    :   parent.horizontalCenter
            anchors.bottom              :   parent.bottom
            font.pixelSize              :   parent.height * headerTextHeight
        }
    }

    Row{
        id                      :   pinsContainer
        anchors.horizontalCenter:   parent.horizontalCenter
        y                       :   parent.height * pinsTopMargin
        height                  :   parent.height * pinContainerHeight
        spacing                 :   height * pinSpacing

        Column{

            id                  :   gndPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   gndPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.black
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   gndPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Toprak"
            }
        }


        Column{

            id                  :   echoPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   echoPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.grey
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   yellowLightPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Echo"
            }
        }

        Column{

            id                  :   triggerPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   triggerPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.purple
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   triggerPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Trig"
            }
        }


        Column{

            id                  :   vccPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   vccPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.red
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   vccPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Güç"
            }
        }
    }
}
