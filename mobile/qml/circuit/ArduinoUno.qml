import QtQuick 2.0

Rectangle {

    //Top Margin
    readonly property real usbTopMargin                     :   0.16
    readonly property real externalPowerTopMargin           :   0.65
    readonly property real switchTopMargin                  :   0.045
    readonly property real digitalPinTopMargin              :   0.026
    readonly property real digitalPinSeperatorLineTopMargin :   0.215
    readonly property real digitalPwmTextTopMargin          :   0.012
    readonly property real arduinoTextTopMargin             :   0.06
    readonly property real chipTopMargin                    :   0.045

    //Left Margin
    readonly property real boardLeftMargin                  :   0.07
    readonly property real externalPowerLeftMargin          :   0.02
    readonly property real switchLeftMargin                 :   0.14
    readonly property real digitalPinLeftStartingMargin     :   0.265
    readonly property real digitalPinSeperatorLineLeftMargin:   0.34
    readonly property real digitalPwmTextLeftMargin         :   0.465
    readonly property real powerPinLeftStartingMargin       :   0.32
    readonly property real analogPinLeftStartingMargin      :   0.7
    readonly property real powerPinSeperatorLineLeftMargin  :   0.40
    readonly property real analogPinSeperatorLineLeftMargin :   0.73
    readonly property real powerPinTextLeftMargin           :   0.4
    readonly property real analogPinTextLeftMargin          :   0.74

    //Bottom Margin
    readonly property real powerTextBottomMargin            :   0.1
    readonly property real powerAnalogPinsSeperatorLineBottomMargin :   0.225
    readonly property real powerAnalogPinTextBottomMargin   :   0.1

    //Border
    readonly property real boardThickness                   :   0.007
    readonly property real usbBorderThickness               :   0.03
    readonly property real switchBorderThickness            :   0.065

    //Width
    readonly property real usbWidth                         :   0.21
    readonly property real externalPowerWidth               :   0.16
    readonly property real switchWidth                      :   0.06
    readonly property real pinWidth                         :   0.043
    readonly property real pinDescriptionTextWidth          :   0.6
    readonly property real digitalPinsSeperatorLineWidth    :   0.54
    readonly property real chipWidth                        :   0.20
    readonly property real powerPinSeperatorLineWidth       :   0.31
    readonly property real analogPinSeperatorLineWidth      :   0.24
    readonly property real powerPinTextWidth                :   0.6

    //Height
    readonly property real usbHeight                        :   0.23
    readonly property real externalPowerHeight              :   0.16
    readonly property real pinsSeperatorLineThickness       :   0.005
    readonly property real pinsDescriptionTextHeight         :   0.8
    readonly property real arduinoTextHeight                :   0.1
    readonly property real chipHeight                       :   0.14

    //Spacing
    readonly property real pinSpacing                       :   0.005

    function    getDigitalPinPos(idx){

        //Çizimde üst tarafta 15 tane pin olduğu için max idx 14 oluyor
        //ve ters olarak indexlendiği için, yani 15.pin d0 olduğu için
        //14 - idx olarak pinin indexini bulabiliyoruz
        var mappedIdx = 14 - idx;

        var pin = digitalPinsRepeater.itemAt(mappedIdx);

        if(pin === "null")
            return Qt.point(0 , 0)

        var centerOffset = boardContainer.width * pinWidth * 0.5

        var globalP = Qt.point(x + digitalPinsRow.x + pin.x + centerOffset, y + digitalPinsRow.y + pin.y + centerOffset)

        return globalP
    }

    function    getAnalogPinPos(idx){
        var pin = analogPinsRepeater.itemAt(idx)

        if(pin === undefined)
            return Qt.point( 0 , 0)

        var centerOffset = boardContainer.width * pinWidth * 0.5
        var globalP = Qt.point(x + analogPinsRow.x + pin.x + centerOffset , y + analogPinsRow.y + pin.y + (analogPinsRow.height - centerOffset))


        return globalP
    }

    function    getPowerPinPos(idx){
        var pin = powerPinsRepeater.itemAt(idx)

        if(pin === undefined)
            return Qt.point( 0 , 0)

        var centerOffset = boardContainer.width * pinWidth * 0.5
        var globalP = Qt.point(x + powerPinsRow.x + pin.x + centerOffset , y + powerPinsRow.y + pin.y + (powerPinsRow.height - centerOffset))

        return globalP
    }

    readonly property variant   digitalPinsDescriptions         :   ["GND" , "13" , "12" , "~11" , "~10" , "~9" , "8" , "7" , "~6" , "~5" , "4" , "~3" , "2" , "TX" , "RX" ]
    readonly property variant   powerPinsDescriptions           :   ["5V" , "RES" , "3.3V" , "5V" , "GND" , "GND" , "VIN"]
    readonly property variant   analogPinsDescriptions          :   ["A0" , "A1" , "A2" , "A3" , "A4" , "A5"]


    id  :   arduinoUnoContainer
    color: "transparent"

    Rectangle{
        id  :   boardContainer
        anchors.left            :   parent.left
        anchors.leftMargin      :   parent.width * boardLeftMargin
        anchors.right           :   parent.right
        anchors.top             :   parent.top
        anchors.bottom          :   parent.bottom
        border.width            :   parent.height * boardThickness
        color                   :   "#107390"
    }

    Rectangle{
        id                      :   usbRect
        width                   :   parent.width * usbWidth
        height                  :   parent.height * usbHeight
        anchors.top             :   parent.top
        anchors.topMargin       :   parent.height * usbTopMargin
        anchors.left            :   parent.left
        border.width            :   height * usbBorderThickness
        color                   :   "#676566"
    }

    Rectangle{
        id                      :   externalPower
        width                   :   parent.width * externalPowerWidth
        height                  :   parent.height * externalPowerHeight
        color                   :   "#000000"
        anchors.left            :   parent.left
        anchors.leftMargin      :   parent.width * externalPowerLeftMargin
        anchors.top             :   parent.top
        anchors.topMargin       :   parent.height * externalPowerTopMargin
    }

    Rectangle{
        width                   :   parent.width * switchWidth
        height                  :   width
        anchors.top             :   parent.top
        anchors.topMargin       :   parent.height * switchTopMargin
        anchors.left            :   parent.left
        anchors.leftMargin      :   parent.width * switchLeftMargin
        border.width            :   width * switchBorderThickness
        color                   :   "white"

        Rectangle{
            anchors.centerIn    :   parent
            width               :   parent.width *   0.65
            height              :   width
            radius              :   width * 0.5
            border.width        :   width * 0.11
            color               :   "#ffa500"
        }
    }

    Row{

        id                      :   digitalPinsRow
        anchors.left            :   boardContainer.left
        anchors.leftMargin      :   boardContainer.width * digitalPinLeftStartingMargin
        anchors.top             :   boardContainer.top
        anchors.topMargin       :   boardContainer.height * digitalPinTopMargin
        spacing                 :   boardContainer.width * pinSpacing

        Repeater{
            id                      :   digitalPinsRepeater
            model : 15
            Rectangle{

                id                  :   pinContainer

                height              :   pin.height + (pinDescriptionText.height * 1.7)
                width               :   pin.width
                color               :   "transparent"

                Pin{
                    id              :   pin
                    width           :   boardContainer.width * pinWidth
                    height          :   width
                }

                Text{
                    id              :   pinDescriptionText
                    text            :   digitalPinsDescriptions[index]
                    rotation        :   -90
                    color           :   "white"
                    font.pixelSize  :   parent.width * pinDescriptionTextWidth
                    anchors.bottom  :   parent.bottom
                    anchors.horizontalCenter:   parent.horizontalCenter
                }
            }
        }
    }

    Rectangle{
        id                          :   digitalPinsSeperatorLine
        width                       :   boardContainer.width * digitalPinsSeperatorLineWidth
        height                      :   boardContainer.height * pinsSeperatorLineThickness < 1 ? 1 : boardContainer.height * pinsSeperatorLineThickness
        anchors.top                 :   boardContainer.top
        anchors.topMargin           :   boardContainer.height * digitalPinSeperatorLineTopMargin
        anchors.left                :   boardContainer.left
        anchors.leftMargin          :   boardContainer.width * digitalPinSeperatorLineLeftMargin
    }

    Text{
        id                          :   digitalPinPwmText
        anchors.top                 :   digitalPinsSeperatorLine.bottom
        anchors.topMargin           :   boardContainer.height * digitalPwmTextTopMargin
        anchors.left                :   boardContainer.left
        anchors.leftMargin          :   boardContainer.width * digitalPwmTextLeftMargin
        height                      :   boardContainer.width * pinWidth * pinsDescriptionTextHeight
        text                        :   "DIGITAL PWM(~)"
        font.pixelSize              :   height
        color                       :   "white"
    }

    Text{
        id                          :   arduinoText
        anchors.top                 :   digitalPinPwmText.bottom
        anchors.topMargin           :   boardContainer.height * arduinoTextTopMargin
        anchors.horizontalCenter    :   digitalPinPwmText.horizontalCenter
        text                        :   ""
        height                      :   boardContainer.height * arduinoTextHeight
        font.pixelSize              :   height
        color                       :   "white"
    }

    Rectangle{
        id                          :   chip
        anchors.top                 :   arduinoText.bottom
        anchors.topMargin           :   boardContainer.height * chipTopMargin
        anchors.horizontalCenter    :   arduinoText.horizontalCenter
        width                       :   boardContainer.width * chipWidth
        height                      :   boardContainer.height * chipHeight
        color                       :   "black"
    }


    Row{

        id                      :   powerPinsRow
        anchors.left            :   boardContainer.left
        anchors.leftMargin      :   boardContainer.width * powerPinLeftStartingMargin
        anchors.bottom          :   boardContainer.bottom
        anchors.bottomMargin    :   boardContainer.height * digitalPinTopMargin
        spacing                 :   boardContainer.width * pinSpacing

        Repeater{
            id                      :   powerPinsRepeater
            model : 7
            Rectangle{

                id                  :   powerPinsContainer

                height              :   powerPin.height + (powerPinDescriptionText.height * 1.7)
                width               :   powerPin.width
                color               :   "transparent"

                Text{
                    id              :   powerPinDescriptionText
                    text            :   powerPinsDescriptions[index]
                    rotation        :   -90
                    color           :   "white"
                    font.pixelSize  :   parent.width * pinDescriptionTextWidth
                    anchors.top     :   parent.top
                    anchors.horizontalCenter:   parent.horizontalCenter
                }

                Pin{
                    id              :   powerPin
                    width           :   boardContainer.width * pinWidth
                    height          :   width
                    anchors.bottom  :   parent.bottom
                }
            }
        }
    }

    Row{

        id                      :   analogPinsRow
        anchors.left            :   boardContainer.left
        anchors.leftMargin      :   boardContainer.width * analogPinLeftStartingMargin
        anchors.bottom          :   boardContainer.bottom
        anchors.bottomMargin    :   boardContainer.height * digitalPinTopMargin
        spacing                 :   boardContainer.width * pinSpacing

        Repeater{
            id                      :   analogPinsRepeater
            model : 6
            Rectangle{

                id                  :   analogPinsContainer
                height              :   analogPin.height + (analogPinDescriptionText.height * 1.7)
                width               :   analogPin.width
                color               :   "transparent"

                Text{
                    id              :   analogPinDescriptionText
                    text            :   analogPinsDescriptions[index]
                    rotation        :   -90
                    color           :   "white"
                    font.pixelSize  :   parent.width * pinDescriptionTextWidth
                    anchors.top     :   parent.top
                    anchors.horizontalCenter:   parent.horizontalCenter
                }

                Pin{
                    id              :   analogPin
                    width           :   boardContainer.width * pinWidth
                    height          :   width
                    anchors.bottom  :   parent.bottom
                }
            }
        }
    }

    Rectangle{
        id                  :   powerPinSeperatorLine
        anchors.left        :   parent.left
        anchors.leftMargin  :   boardContainer.width * powerPinSeperatorLineLeftMargin
        anchors.bottom      :   boardContainer.bottom
        anchors.bottomMargin:   boardContainer.height * powerAnalogPinsSeperatorLineBottomMargin
        width               :   boardContainer.width * powerPinSeperatorLineWidth
        height              :   boardContainer.height * pinsSeperatorLineThickness < 1 ? 1 : boardContainer.height * pinsSeperatorLineThickness
    }

    Rectangle{
        id                  :   analogPinSeperatorLine
        anchors.left        :   parent.left
        anchors.leftMargin  :   parent.width * analogPinSeperatorLineLeftMargin
        anchors.bottom      :   parent.bottom
        anchors.bottomMargin:   parent.height * powerAnalogPinsSeperatorLineBottomMargin
        width               :   parent.width * analogPinSeperatorLineWidth
        height              :   parent.height * pinsSeperatorLineThickness < 1 ? 1 : parent.height * pinsSeperatorLineThickness
    }

    Text{
        id                          :   powerPinText
        anchors.bottom              :   powerPinSeperatorLine.top
        anchors.bottomMargin        :   boardContainer.height * digitalPwmTextTopMargin
        anchors.left                :   boardContainer.left
        anchors.leftMargin          :   boardContainer.width * powerPinTextLeftMargin
        height                      :   boardContainer.width * pinWidth * pinsDescriptionTextHeight
        text                        :   "POWER"
        font.pixelSize              :   height
        color                       :   "white"
    }


    Text{
        id                          :   analogPinText
        anchors.bottom              :   analogPinSeperatorLine.top
        anchors.bottomMargin        :   boardContainer.height * digitalPwmTextTopMargin
        anchors.left                :   boardContainer.left
        anchors.leftMargin          :   boardContainer.width * analogPinTextLeftMargin
        height                      :   boardContainer.width * pinWidth * pinsDescriptionTextHeight
        text                        :   "ANALOG IN"
        font.pixelSize              :   height
        color                       :   "white"
    }

}
