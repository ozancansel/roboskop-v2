import QtQuick 2.7
import "template"
import "../model"
import "../theme"

Template25x25 {

    property double potVisualLineThickness      :   0.04
    property double potCenterLineThickness      :   0.1

    mainText: "RGB"
    subText:  "RENK AYARLI LED"

    component:  Component{
        id : componentVisual

        Image{
            source  :   "/res/img/robologo-512-.png"
            fillMode:   Image.PreserveAspectFit
            scale   :   1
            mipmap  :   true
        }

    }

    code            :   "#16p5"

    pins : ListModel{
        Component.onCompleted: {
            append( {"pinOutColor" : Color.red , "label" : "Kırmızı"} )
            append( {"pinOutColor" : Color.green , "label" : "Yeşil"} )
            append( {"pinOutColor" : Color.blue , "label" : "Mavi"} )
            append( {"pinOutColor" : Color.white , "label" : "Güç"} )
        }
    }

    pinImages   :   [
        "/res/img/fruits/grape.png" ,
        "/res/img/fruits/strawberry.png" ,
        "/res/img/fruits/banana.png" ,
        "/res/img/fruits/apple.png"
    ]

}
