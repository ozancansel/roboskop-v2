import QtQuick 2.0
import "../theme"

Rectangle {

    //Top Margin
    readonly property real lightRepeaterTopMargin           :   0.15
    readonly property real redLightTopMargin                :   0.35
    readonly property real pinsTopMargin                    :   0.82
    readonly property real trafficLightTextTopMargin        :   0.5
    readonly property real kitNameTopTextMargin             :   0.04

    //Left Margin
    readonly property real lightRepeaterLeftMargin          :   0.2

    //Bottom Margin
    readonly property real kitCodeTextBottomMargin          :   0.21

    //Right Margin
    readonly property real kitCodeRightMargin               :   0.05

    //Border
    readonly property real containerBorderThickness         :   0.02
    readonly property real lightBorderThickness             :   0.11
    readonly property real lightsContainerBorderThickness   :   0.014
    readonly property real lightsContainerFootBorderThick   :   0.1

    //Spacing
    readonly property real lightSpacing                     :   0.04
    readonly property real pinSpacing                       :   0.1
    readonly property real pinTextSpacing                   :   0.05

    //Width
    readonly property real lightsContainerWidth             :   0.22
    readonly property real lightsWidth                      :   0.65
    readonly property real lightsContainerFootWidth         :   0.4
    readonly property real textContainerWidth               :   0.4
    readonly property real kitNameWidth                         :   0.7

    //Radius
    readonly property real lightsContainerFootRadius        :   0.35
    readonly property real lightsContainerRadius            :   0.45

    //Height
    readonly property real lightsContainerFootHeight        :   0.55
    readonly property real lightsTextHeight                 :   0.43
    readonly property real pinContainerHeight               :   0.125
    readonly property real codeTextHeight                   :   0.04
    readonly property real pinHeight                        :   0.8
    readonly property real kitNameHeight                    :   0.08

    //Colors
    readonly property color redColor                        :   "#fc0000"
    readonly property color yellowColor                     :   "#ffec00"
    readonly property color greenColor                      :   "#00d628"
    readonly property color lightsContainerFillColor        :   "#3f3f3f"

    id              :   trafficLightKitContainer

    border.width    :   width * containerBorderThickness

    function    getPinPos(idx){

        var halfPinHeight   =   redLightPin.height * 0.5
        var halfPinWidth    =   redLightPin.width  * 0.5

        switch(idx){
        case 0:
            return mapToItem(parent , pinsContainer.x + redLightPinTextContainer.x + redLightPin.x + halfPinWidth
                     , pinsContainer.y + redLightPinTextContainer.y + redLightPin.y + halfPinHeight)
        case 1:
            return mapToItem(parent , pinsContainer.x + yellowLightPinTextContainer.x + yellowLightPin.x + halfPinWidth
                     , pinsContainer.y + yellowLightPinTextContainer.y + yellowLightPin.y + halfPinHeight)
        case 2:
            return mapToItem(parent , pinsContainer.x + greenLightPinTextContainer.x + greenLightPin.x + halfPinWidth
                     , pinsContainer.y + greenLightPinTextContainer.y + greenLightPin.y + halfPinHeight)
        case 3:
            return mapToItem(parent , pinsContainer.x + groundLightPinTextContainer.x + groundLightPin.x + halfPinWidth
                     , pinsContainer.y + groundLightPinTextContainer.y + groundLightPin.y + halfPinHeight)
        }
    }

    //Kit
    Rectangle{
        id                      :   kitNameTextContainer
        anchors.top             :   parent.top
        anchors.topMargin       :   parent.height * kitNameTopTextMargin
        anchors.horizontalCenter:   parent.horizontalCenter
        height                  :   parent.height * kitNameHeight
        width                   :   parent.width * kitNameWidth

        Text {
            id                  :   kitNameText
            text                :   "Trafik Lambası"
            anchors.centerIn    :   parent
            font.pixelSize      :   parent.height
        }
    }

    //Trafik lambası görseli gövde container
    Rectangle{
        id                      :   lightsContainer
        anchors.top             :   parent.top
        anchors.topMargin       :   parent.height * lightRepeaterTopMargin
        anchors.horizontalCenter:   parent.horizontalCenter
        border.width            :   parent.width * lightsContainerBorderThickness
        width                   :   parent.width * lightsContainerWidth
        height                  :   redLight.height * 3 + greenLight.anchors.topMargin * 2 + (2 * redLight.anchors.topMargin)
        radius                  :   width *lightsContainerRadius
        color                   :   lightsContainerFillColor
        border.color            :   "black"
        z                       :   2

        Rectangle{
            id                      :   redLight
            anchors.horizontalCenter:   parent.horizontalCenter
            anchors.top             :   parent.top
            anchors.topMargin       :   height * redLightTopMargin
            width                   :   parent.width * lightsWidth
            height                  :   width
            color                   :   redColor
            radius                  :   width * 0.5
            border.width            :   width * lightBorderThickness
        }

        Rectangle{
            id                      :   yellowLight
            anchors.top             :   redLight.bottom
            anchors.topMargin       :   height * lightSpacing
            anchors.horizontalCenter:   parent.horizontalCenter
            width                   :   parent.width * lightsWidth
            height                  :   width
            color                   :   yellowColor
            radius                  :   width * 0.5
            border.width            :   width * lightBorderThickness
        }

        Rectangle{
            id                      :   greenLight
            anchors.top             :   yellowLight.bottom
            anchors.topMargin       :   height * lightSpacing
            anchors.horizontalCenter:   parent.horizontalCenter
            width                   :   parent.width * lightsWidth
            height                  :   width
            color                   :   greenColor
            radius                  :   width * 0.5
            border.width            :   width * lightBorderThickness
        }
    }

    //Trafik lambası ayak container
    Rectangle{
        id                      :   lightContainerFoot
        anchors.top             :   lightsContainer.bottom
        anchors.topMargin       :   -width * 0.5
        anchors.horizontalCenter:   lightsContainer.horizontalCenter
        width                   :   lightsContainer.width * lightsContainerFootWidth
        height                  :   lightsContainer.height * lightsContainerFootHeight
        z                       :   1
        border.width            :   width * lightsContainerFootBorderThick
        color                   :   lightsContainerFillColor
        radius                  :   width * lightsContainerFootRadius
    }

    //Trafik lambası ışık açıklama kırmızı
    Rectangle{
        id                      :   redLightTextContainer
        y                       :   lightsContainer.y + redLight.y
        anchors.left            :   parent.left
        width                   :   parent.width * textContainerWidth
        height                  :   redLight.height
        color                   :   "transparent"

        Text {
            id                      :   redLightText
            anchors.centerIn        :   parent
            font.pixelSize          :   parent.height * lightsTextHeight
            text                    :   "DUR"
        }
    }

    //Trafik lambası ışık açıklama sarı
    Rectangle{
        id                      :   yellowLightTextContainer
        y                       :   lightsContainer.y + yellowLight.y
        anchors.left            :   parent.left
        width                   :   parent.width * textContainerWidth
        height                  :   yellowLight.height
        color                   :   "transparent"

        Text {
            id                  :   yellowLightText
            anchors.centerIn    :   parent
            text                :   "HAZIR"
            font.pixelSize      :   parent.height * lightsTextHeight
        }

    }

    //Trafik lambası ışık açıklama yeşil
    Rectangle{
        id                      :   greenLightTextContainer
        y                       :   lightsContainer.y + greenLight.y
        anchors.left            :   parent.left
        width                   :   parent.width * textContainerWidth
        height                  :   greenLight.height
        color                   :   "transparent"

        Text {
            id                  :   greenLightText
            anchors.centerIn    :   parent
            text                :   "GEÇ"
            font.pixelSize      :   parent.height * lightsTextHeight
        }

    }

    Text{
        id                      :   kitCodeText
        text                    :   "#16p2"
        anchors.right           :   parent.right
        anchors.rightMargin     :   parent.width * kitCodeRightMargin
        anchors.bottom          :   parent.bottom
        anchors.bottomMargin    :   parent.height * kitCodeTextBottomMargin
        font.pixelSize          :   parent.height * codeTextHeight
    }

    Row{
        id                      :   pinsContainer
        anchors.horizontalCenter:   parent.horizontalCenter
        y                       :   parent.height * pinsTopMargin
        height                  :   parent.height * pinContainerHeight
        spacing                 :   height * pinSpacing

        Column{

            id                  :   redLightPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   redLightPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.red
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   redLightPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Kırmızı"
            }
        }


        Column{

            id                  :   yellowLightPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   yellowLightPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.yellow
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   yellowLightPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Sarı"
            }
        }

        Column{

            id                  :   greenLightPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   greenLightPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.green
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   greenLightPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Yeşil"
            }
        }


        Column{

            id                  :   groundLightPinTextContainer
            height              :   parent.height
            spacing             :   height * pinTextSpacing

            Pin{
                id                  :   groundLightPin
                anchors.horizontalCenter: parent.horizontalCenter
                pinColor            :   Color.black
                height              :   parent.height * pinHeight
                width               :   height
            }

            Text{
                id                  :   groundLightPinText
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize      :   parent.height * (1 - pinHeight)
                text                :   "Toprak"
            }
        }
    }
}

