import QtQuick 2.0
import "../template"
import "../"
import "../../theme"

SketchTemplate {
    id      :   template

    readonly property real  normalWireThickness     :   getScaledX(10)
    readonly property real  bluetoothWriteThickness :   getScaledX(20)

    PinoBoard   {
        id      :   board
        x       :   template.getScaledX(400)
        y       :   template.getScaledY(185)
        width   :   template.getScaledX(380)
        height  :   template.getScaledY(300)
    }


    RgbLedKit{
        id          :   rgbLedKit
        x           :   getScaledX(850)
        y           :   getScaledY(100)
        width       :   getScaledX(200)
        height      :   getScaledY(200)
        preschool   :   true
    }

    BluetoothKit{
        id          :   btKit
        width       :   getScaledX(200)
        height      :   getScaledY(150 * (40 / 25))
        x           :   getScaledX(80)
        y           :   getScaledY(50)
        preschool   :   true
    }

    //Rgb 5v to pino 5v
    Wire{
        id          :   rgbToApple1
        alignment   :   Qt.Horizontal
        length      :   getScaledX(0)
        centerX     :   board.posMapping[board.power3].x
        centerY     :   board.posMapping[board.power3].y
    }

    Wire{
        id          :   rgbToApple3
        alignment   :   Qt.Vertical
        length      :   rgbToApple1.getVerticalDiffAbs(rgbLedKit.getPinPos(3))
        centerX     :   rgbLedKit.getPinPos(3).x
        centerY     :   rgbLedKit.getPinPos(3).y
        thickness   :   normalWireThickness
        color       :   Color.white
    }

    Wire{
        id          :   rgbToApple2
        alignment   :   Qt.Horizontal
        length      :   rgbToApple3.getHorizontalDiffAbs(rgbToApple1.end())
        centerX     :   rgbToApple1.end().x
        centerY     :   rgbToApple1.end().y
        thickness   :   normalWireThickness
        color       :   Color.blue
    }

    //Rgb red to rgb grape
    Wire{
        id          :   rgbRedToPinoGrape1
        alignment   :   Qt.Vertical
        centerX     :   board.posMapping[board.grape].x
        centerY     :   board.posMapping[board.grape].y
        thickness   :   normalWireThickness
        wireColor   :   Color.red
        length      :   getScaledY(65)
    }

    Wire{
        id          :   rgbRedToPinoGrape3
        alignment   :   Qt.Vertical
        centerX     :   rgbLedKit.getPinPos(0).x
        centerY     :   rgbLedKit.getPinPos(0).y
        thickness   :   normalWireThickness
        wireColor   :   Color.red
        length      :   rgbRedToPinoGrape1.getVerticalDiffAbs(rgbLedKit.getPinPos(0))
    }

    Wire{
        id          :   rgbRedToPinoGrape2
        alignment   :   Qt.Horizontal
        centerX     :   rgbRedToPinoGrape1.end().x
        centerY     :   rgbRedToPinoGrape1.end().y
        length      :   rgbRedToPinoGrape1.getHorizontalDiffAbs(rgbRedToPinoGrape3.end())
        wireColor   :   Color.red
        thickness   :   normalWireThickness
    }

    //Rgb green to rgb strawberry
    Wire{
        id          :   rgbGreenToPinoStrawberry1
        alignment   :   Qt.Vertical
        centerX     :   board.posMapping[board.strawberry].x
        centerY     :   board.posMapping[board.strawberry].y
        thickness   :   normalWireThickness
        wireColor   :   Color.green
        length      :   getScaledY(85)
    }

    Wire{
        id          :   rgbGreenToPinoStrawberry3
        alignment   :   Qt.Vertical
        centerX     :   rgbLedKit.getPinPos(1).x
        centerY     :   rgbLedKit.getPinPos(1).y
        thickness   :   normalWireThickness
        wireColor   :   Color.green
        length      :   rgbGreenToPinoStrawberry1.getVerticalDiffAbs(rgbLedKit.getPinPos(1))
    }

    Wire{
        id          :   rgbGreenToPinoStrawberry2
        alignment   :   Qt.Horizontal
        centerX     :   rgbGreenToPinoStrawberry1.end().x
        centerY     :   rgbGreenToPinoStrawberry1.end().y
        thickness   :   normalWireThickness
        wireColor   :   Color.green
        length      :   rgbGreenToPinoStrawberry1.getHorizontalDiffAbs(rgbGreenToPinoStrawberry3.end())
    }

    //Rgb blue to rgb banana
    Wire{
        id          :   rgbBlueToPinoBanana1
        alignment   :   Qt.Vertical
        centerX     :   board.posMapping[board.banana].x
        centerY     :   board.posMapping[board.banana].y
        thickness   :   normalWireThickness
        length      :   getScaledY(105)
        wireColor   :   Color.blue
    }

    Wire{
        id          :   rgbBlueToPinoBanana3
        alignment   :   Qt.Vertical
        centerX     :   rgbLedKit.getPinPos(2).x
        centerY     :   rgbLedKit.getPinPos(2).y
        thickness   :   normalWireThickness
        length      :   rgbBlueToPinoBanana1.getVerticalDiffAbs(rgbLedKit.getPinPos(2))
        wireColor   :   Color.blue
    }

    Wire{
        id          :   rgbBlueToPinoBanana2
        alignment   :   Qt.Horizontal
        centerX     :   rgbBlueToPinoBanana1.end().x
        centerY     :   rgbBlueToPinoBanana1.end().y
        thickness   :   normalWireThickness
        length      :   rgbBlueToPinoBanana1.getHorizontalDiffAbs(rgbBlueToPinoBanana3.end())
        wireColor   :   Color.blue
    }

    //Bt to pino
    Wire    {
        id          :   btToArd5
        alignment   :   Qt.Vertical
        centerX     :   (btKit.getPinPos(1).x + btKit.getPinPos(2).x) / 2
        centerY     :   btKit.getPinPos(1).y
        wireColor   :   Color.blue
        thickness   :   getScaledX(30)
        length      :   btToArd2.getVerticalDiffAbs(Qt.point(centerX , centerY))
    }

    Wire    {
        id          :   btToArd1
        alignment   :   Qt.Horizontal
        centerX     :   board.posMapping[board.lemon].x
        centerY     :   board.posMapping[board.lemon].y
        length      :   getScaledX(120)
        reverse     :   true
        thickness   :   getScaledX(30)
    }

    Wire    {
        id          :   btToArd2
        alignment   :   Qt.Vertical
        centerX     :   btToArd1.end().x
        centerY     :   btToArd1.end().y
        length      :   getScaledY(80)
        thickness   :   getScaledX(30)
    }

    Wire    {
        id          :   btToArd3
        alignment   :   Qt.Horizontal
       centerX      :   btToArd2.end().x
       centerY      :   btToArd2.end().y
       thickness    :   getScaledX(30)
       length       :   btToArd2.getHorizontalDiffAbs(btToArd5.end())
       reverse      :   true
    }
}
