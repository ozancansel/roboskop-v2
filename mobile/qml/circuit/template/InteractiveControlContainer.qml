import QtQuick 2.0
import "../../theme"

Rectangle {

    //Width
    property real       controlWidth            :   0.6

    //Height
    property real       controlHeight           :   0.5
    property real       headerTextHeight        :   0.2

    //Font
    property real       headerTextFontSize      :   0.3

    //Color
    property color      backgroundColor         :   "white"
    property color      headerBackgroundColor   :   "#34465e"


    property alias      controlsComponent       :   controlsLoader.sourceComponent
    property alias      controlsItem            :   controlsLoader.item
    property alias      title                   :   headerText.text

    Rectangle{
        id                  :   headerTextContainer
        height              :   parent.height * headerTextHeight
        width               :   parent.width
        color               :   headerBackgroundColor
        radius              :   15

        Text {
            id              :   headerText
            text            :   "Trafik Lambası Kontrol"
            font.pixelSize  :   parent.height * headerTextFontSize
            font.family     :   FontCollection.headerFontName
            anchors.horizontalCenter    :   parent.horizontalCenter
            color           :   "white"
        }
    }

    Rectangle{
        id                  :   controlsContainer
        width               :   parent.width
        anchors.top         :   headerTextContainer.bottom
        anchors.topMargin   :   -headerTextContainer.radius
        anchors.left        :   parent.left
        anchors.right       :   parent.right
        anchors.bottom      :   parent.bottom

        Loader{
            id              :   controlsLoader
            width           :   parent.width
            height          :   parent.height
        }
    }
}
