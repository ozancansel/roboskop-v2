import QtQuick 2.0
import QtQuick.Window 2.0

import "../../model"
import "../"
import "../../theme"
import "../../controls"

Rectangle {

    readonly property double    borderThickness         :   0.015

    //Top margin
    readonly property double    textTopOffset           :   0.06
    readonly property double    subTextTopOffset        :   0.20
    readonly property double    componentTopOffset      :   0.32
    readonly property double    pinsTopOffset           :   0.74

    //Right margin
    readonly property double    codeTextRightMargin     :   0.06

    //Left margin

    //Bottom margin
    readonly property double    codeTextBottomMargin    :   0.35

    //Height
    readonly property double    mainTextHeight          :   0.12
    readonly property double    subTextHeight           :   0.07
    readonly property double    componentHeight         :   0.35
    readonly property double    codeTextHeight          :   0.06

    //Width
    readonly property double    componentWidth          :   0.50
    readonly property double    pinsHeight              :   0.18
    readonly property double    pinsSpacing             :   0.10
    readonly property double    pinListWidth            :   0.70
    readonly property double    pinLabelWidth           :   0.02

    property alias mainText     :   header.text
    property alias subText      :   subText.text
    property alias component    :   visualComponentLoader.sourceComponent
    property alias pins         :   pinListView.model
    property variant pinImages  :   []
    property bool  preschool    :   false
    property alias code         :   codeTextLabel.text

    function    getPinPos(idx){
        var containerP = Qt.point(pinListView.x , pinListView.y)
        idx += 1;

        var calculatedX = (pinListView.height * (idx - 0.5) ) + (idx - 1) * pinListView.spacing
        var calculatedY = pinListView.y + ( pinListView.height * 0.5)

        if(preschool){
            calculatedY += pinListView.height * 0.75
        }

        var p = Qt.point(containerP.x + calculatedX + x, calculatedY + y)

        return p
    }

    id              :   template25x25Container
    border.width    :   width * borderThickness
    color           :   "transparent"

    Text {
        id: header
        y : parent.height * textTopOffset
        height: parent.height * mainTextHeight
        width: font.pixelSize * text.length
        text: qsTr("Not")
        color: "blue"

        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: height
        font.bold: true
    }

    Text {
        id: subText

        y:  parent.height * subTextTopOffset
        text: qsTr("Not Assigned")
        height: parent.height   *   subTextHeight
        font.pixelSize :  height

        anchors.horizontalCenter: parent.horizontalCenter
    }


    Loader {
        id : visualComponentLoader
        anchors.horizontalCenter: parent.horizontalCenter
        y:  componentTopOffset * template25x25Container.height
        height: componentHeight  *   template25x25Container.height
        width:  componentWidth  *   template25x25Container.width
    }

    Repeater{
        id : pinsRepeater
    }

    ListView {

        id : pinListView
        y : parent.height * pinsTopOffset
        orientation: ListView.Horizontal
        anchors.horizontalCenter: template25x25Container.horizontalCenter
        spacing: height * pinsSpacing

        width: {

            if(template25x25Container.height * pinsHeight * model.count < template25x25Container.width * pinListWidth)
                template25x25Container.height * pinsHeight * model.count
            else
                template25x25Container.width * pinListWidth

        }
        height:  SizeBinding.size(width / model.count , template25x25Container.height * pinsHeight)
        interactive: false

        delegate:Rectangle {

            id: pinListItemContainer

            height: parent.height
            width: height


            Pin {
                id      : pinItem
                pinColor: pinOutColor
                width   : parent.width
                height  : width
                visible : !preschool
            }

            Text {
                id                          :   textLabel
                text                        :   label
                visible                     :   !preschool
                anchors.horizontalCenter    :   pinListItemContainer.horizontalCenter
                anchors.top                 :   pinItem.bottom
                font.pixelSize              :   pinItem.height * 0.2
            }

            Image   {
                id                  :   visualItem
                visible             :   preschool
                source              :   pinImages[index]
                anchors.centerIn    :   pinItem
                width               :   parent.width * 1.4
                fillMode            :   Image.PreserveAspectFit
            }
        }
    }

    Text {
        id: codeTextLabel
        anchors.right: template25x25Container.right
        anchors.rightMargin: template25x25Container.width * codeTextRightMargin
        anchors.bottom: template25x25Container.bottom
        anchors.bottomMargin: template25x25Container.width * codeTextBottomMargin
        text: code
        height: template25x25Container.height * codeTextHeight
        font.pixelSize: height
        color: "black"
    }
}
