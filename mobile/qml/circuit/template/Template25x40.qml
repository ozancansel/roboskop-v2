import QtQuick 2.0
import "../"
import "../../theme"
import "../../controls"

Rectangle {

    readonly property double    borderThickness     :   0.015

    //Top Margin
    readonly property double    headerTextOffset    :   0.06
    readonly property double    subTextOffset       :   0.20
    readonly property double    componentTopOffset  :   0.32
    readonly property double    pinsTopOffset       :   0.80

    //Bottom Margin
    readonly property double    codeTextBottomMargin:   0.35

    //Right Margin
    readonly property double    codeTextRightMargin :   0.06

    //Height
    property double    headerTextHeight    :   0.095
    readonly property double    subTextHeight       :   0.06
    readonly property double    componentHeight     :   0.40
    readonly property double    pinsHeight          :   0.18
    readonly property double    codeTextHeight      :   0.04

    //Width
    readonly property double    componentWidth      :   0.65
    readonly property double    pinListWidth        :   0.70
    readonly property double    pinLabelWidth       :   0.02
    readonly property double    codeTextWidth       :   0.1

    //Max Width
    readonly property double    maxHeaderWidth      :   0.87

    //Spacing
    readonly property double    pinsSpacing         :   0.10

    property alias header       :   header.text
    property alias subText      :   subText.text
    property alias component    :   visualComponentLoader.sourceComponent
    property alias pins         :   pinListView.model
    property alias codeText     :   codeTextLabel.text
    property bool  preschool    :   false


    function    getPinPos(idx){
        var containerP = Qt.point(pinListView.x , pinListView.y)
        idx += 1;
        var calculatedX = (pinListView.height * (idx - 0.5) ) + (idx - 1) * pinListView.spacing ;
        var calculatedY = pinListView.y + ( pinListView.height * 0.5)

        var p = Qt.point( containerP.x + calculatedX + x , calculatedY + y)

        return p
    }

    id              :   template25x40Container
    height          :   width * ( 40 / 25 )
    border.width    :   width * borderThickness

    Text {
        id: header

        y : parent.height * headerTextOffset
        height: parent.height * headerTextHeight
        width: font.pixelSize * text.length
        text: qsTr("Header")
        color: "blue"

        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: height
        font.bold : true
    }

    Text{
        id : subText

        y:  parent.height * subTextOffset
        height: parent.height * subTextHeight
        text: qsTr("Subtext")
        font.pixelSize: height

        anchors.horizontalCenter: template25x40Container.horizontalCenter
    }

    Loader {
        id : visualComponentLoader
        anchors.horizontalCenter: parent.horizontalCenter
        y:  componentTopOffset * template25x40Container.height
        height: componentHeight  *   template25x40Container.height
        width:  componentWidth  *   template25x40Container.width
    }

    ListView {
        id : pinListView
        y : parent.height * pinsTopOffset
        orientation: ListView.Horizontal
        anchors.horizontalCenter: template25x40Container.horizontalCenter
        spacing: height * pinsSpacing
        width: {

            if(template25x40Container.height * pinsHeight * model.count < template25x40Container.width * pinListWidth)
                template25x40Container.height * pinsHeight * model.count
            else
                template25x40Container.width * pinListWidth

        }
        height:  SizeBinding.size(width / model.count , template25x40Container.height * pinsHeight)
        interactive: false

        delegate:Rectangle {

            id: pinListItemContainer
            color: "transparent"

            height: parent.height
            width: height


            Pin {
                id : pinItem
                pinColor: pinOutColor
                width: parent.width
                height: width
            }

            Text {
                id: textLabel
                text: label
                anchors.horizontalCenter: pinListItemContainer.horizontalCenter
                anchors.top : pinItem.bottom
                font.pixelSize: pinItem.height * 0.2
            }
        }
        visible     :   !preschool
    }

    Rectangle{
        id      :   preschoolRect
        visible :   preschool
        anchors.fill    :   pinListView
        radius          :   height * 0.2
        color           :   "#007fff"
        z               :   2
    }

    Text {
        id: codeTextLabel
        anchors.right: template25x40Container.right
        anchors.rightMargin: template25x40Container.width * codeTextRightMargin
        anchors.bottom: template25x40Container.bottom
        anchors.bottomMargin: template25x40Container.width * codeTextBottomMargin
        text: "#16p1"
        height: template25x40Container.height * codeTextHeight
        font.pixelSize: height
        color: "black"
    }

}
