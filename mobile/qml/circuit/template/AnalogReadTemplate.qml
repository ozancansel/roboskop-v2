import QtQuick 2.0
import QtQuick.Controls 2.1
import QtCharts 2.1
import "../../chart"
import "../../controls"
import "../../theme"

Rectangle {

    property real chartWidth                        :   0.7
    property real controlsContainerWidth            :   0.3
    property real controlButtonWidth                :   0.8

    //Height
    property real controlButtonHeight               :   0.08

    //Font
    property real titleTextFontSize                 :   0.05

    property alias chart                            :   analogRead
    property alias splineSeries                     :   analogSplineSeries
    property alias axisY                            :   valueAxisY
    property int    splineColor                     :   Color.pink

    ChartView   {

        id                      :   analogRead
        title                   :   "Pot Sinyal"
        anchors.fill            :   parent
        antialiasing            :   false
        titleFont.pixelSize     :   height * titleTextFontSize
        titleFont.family        :   FontCollection.headerFontName

        ValueAxis {
            id: valueAxisX
            // Hide the value axis; it is only used to map the line series to bar categories axis
            visible: false
            min: 0
            max: 15
        }

        ValueAxis{
            id          : valueAxisY
            min         : 0
            max         : 1023
            titleText   : "Potansiyel Fark (V)" //[&deg;C]
            titleFont.pixelSize : height * titleTextFontSize
            titleFont.family    :   FontCollection.headerFontName
        }

        RealTimeSpline {
            id              :   analogSplineSeries
            axisX           :   valueAxisX
            axisY           :   valueAxisY
            useOpenGL       :   true
            name            :   "Pot Maximum Sinyal seviyesi"
            width           :   5
            color           :   Color.colors[splineColor]
        }
    }
}
