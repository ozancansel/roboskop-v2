import QtQuick 2.0

Flickable {

    //    readonly property real   scaleFactorX       :   width / 1200
    //    readonly property real   scaleFactorY       :   height / 675
    readonly property real   scaleFactorX       :   (contentWidth / 1200) < 0 ? width / 1200 : contentWidth / 1200
    readonly property real   scaleFactorY       :   (contentHeight / 675) < 0 ? height / 675 : contentHeight / 675
    readonly property real   minContentWidth    :   contentWidth * 0.8
    readonly property real   maxContentWidth    :   contentWidth * 2
    property real   centerItemOffsetX           :   0
    property real   centerItemOffsetY           :   0

    property bool   zoomed                      :   false

    id          :   sketchTemplateContainer

    contentHeight       :   height
    contentWidth        :   width

    signal  zoomStatusChanged()

    MouseArea{
        onDoubleClicked     :   {
            var multiplier = zoomed ? 0.5 : 2

            sketchTemplateContainer.contentWidth =  sketchTemplateContainer.contentWidth * multiplier
            sketchTemplateContainer.contentHeight   =   sketchTemplateContainer.contentHeight * multiplier

            zoomed = !zoomed

            zoomStatusChanged()
        }

        anchors.fill        :   parent
    }

    function    getScaledX(val){
        return val * scaleFactorX
    }

    function    getScaledY(val){
        return val * scaleFactorY
    }
}
