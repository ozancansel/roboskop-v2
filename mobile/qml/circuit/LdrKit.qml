import QtQuick 2.0
import "template"
import "../theme"

Template25x25 {

    mainText: "LDR"
    subText: "Işık Sensörü"

    component: Component{
        id : componentVisual


        Image{
//            source:  "/res/img/ldr-circuit.png"
            source  :   "/res/img/robologo-512-.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    code    :   "#16p3"

    pins: ListModel{

        Component.onCompleted: {
            append({"pinOutColor" : Color.red , "label" : "Güç"})
            append({"pinOutColor" : Color.brown , "label" : "Sinyal"})
            append({"pinOutColor" : Color.black , "label" : "Toprak"})
        }
    }
}
