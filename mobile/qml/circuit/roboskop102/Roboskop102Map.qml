import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick 2.0

QtObject {

    readonly property var lessonMapping : [{ } ,
                               /*Lesson 1*/     {  "sketch"     :   "/qml/circuit/roboskop101/LedIntroductionSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop102/control/TemperatureSensor.qml"  },     //"/qml/circuit/roboskop101/control/SpaceshipWithPotGame.qml"},
                                /*Lesson 2*/     {  "sketch"    :   "/qml/circuit/roboskop101/LedBlinkSketch.qml",
                                                    "control"   :   "/qml/circuit/roboskop101/control/LedBlinkControl.qml"       } ,
                                /*Lesson 3*/     {  "sketch"    :   "/qml/circuit/roboskop101/TrafficLightSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/TrafficLightControl.qml" } ,
                                /*Lesson 4*/    {   "sketch"    :   "/qml/circuit/roboskop101/PotSignalSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/PotReadControl.qml"} ,
                                /*Lesson 5*/    {   "sketch"    :   "/qml/circuit/roboskop101/LdrSignalSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/LdrReadControl.qml"  }   ,
                                /*Lesson 6*/    {   "sketch"    :   "/qml/circuit/roboskop101/RgbLedSketch.qml"    ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/RgbControl.qml"  }   ,
                                /*Lesson 7*/    {   "sketch"    :   "/qml/circuit/roboskop101/RgbLedAndPotSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/RgbControl.qml"} ,
                                /*Lesson 8*/    {   "sketch"    :   "/qml/circuit/roboskop101/RgbLedAndLdrSketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/RgbControl.qml"} ,
                                /*Lesson 9*/    {   "sketch"    :   "/qml/circuit/roboskop101/RoboticMusicSketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/PianoControl.qml"  } ,
                                /*Lesson 10*/   {   "sketch"    :   "/qml/circuit/roboskop101/RoboticMusicAndPotSketch.qml"    ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/EmptyControl.qml"} ,
                                /*Lesson 11*/   {   "sketch"    :   "/qml/circuit/roboskop101/AcousticRulerSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/AcousticDistanceControl.qml"} ,
                                /*Lesson 12*/   {   "sketch"    :   "/qml/circuit/roboskop101/ParkSensorSketch.qml"    ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/AcousticDistanceControl.qml"  }   ,
                                /*Lesson 13*/   {   "sketch"    :   "/qml/circuit/roboskop101/EmptySketch.qml",
                                                    "control"   :   "/qml/circuit/roboskop101/control/EmptyControl.qml"  }   ,
                                /*Lesson 14*/   {   "sketch"    :   "/qml/circuit/roboskop101/DigitalMicrophoneSketch.qml" ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/MicrophoneReadControl.qml"  }   ,
                                /*Lesson 15*/   {   "sketch"    :   "/qml/circuit/roboskop101/RgbLedAndMicSketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/MicrophoneReadControl.qml"  }   ,
                                /*Lesson 16*/   {   "sketch"    :   "/qml/circuit/roboskop101/EmptySketch.qml"  ,
                                                    "control"   :   "/qml/circuit/roboskop101/control/EmptyControl.qml"}

    ]

    function    getSketchPath(idx){
        return lessonMapping[idx].sketch
    }

    function    getInteractivePath(idx){
        return lessonMapping[idx].control
    }

}
