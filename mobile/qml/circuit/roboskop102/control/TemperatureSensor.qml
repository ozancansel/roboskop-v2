import QtQuick 2.0
import "../../../controls"

Item {
    TemperatureBar{
        id              :   tempBar
        width           :   parent.width * 0.1
        height          :   parent.height * 0.8
        anchors.centerIn:   parent
    }

    Timer{
        id          :   timer
        interval    :   1000
        running     :   true
        repeat      :   true
        onTriggered :   {
            tempBar.value = Math.random() * 100
        }
    }
}
