import QtQuick 2.0
import "../theme"

Rectangle {

    property  int pinColor      :   Color.black
    property variant    fillColors      :  [ "#1b5bb3" , "#8c0000" , "#000000" , "#d6d63a" , "#00a527" , "#848484" , "#999999" , "#ad6a38" , "#8b691d" , "#15c7c5" , "#6c2710" , "#7a3a73" , "#dc00dc"]

    id                  :   container
    color               :   "white"
    radius              :   width / 2
    border.width        :   width * 0.145
    border.color        :   fillColors[pinColor]

    smooth: true
}
