import QtQuick 2.0
import "template"
import "../theme"

Template25x40 {

    //Override
    headerTextHeight            :   0.085

    component:  Component{
        id : componentVisual

        Image{
//            source:  "/res/img/bluetooth-256x256.png"
            source  :   "/res/img/robologo-512-.png"
            fillMode: Image.PreserveAspectFit
            scale   :   0.8
        }
    }

    pins: ListModel{

        Component.onCompleted: {
            append( {"pinOutColor" : Color.green , "label" : "Rx"} )
            append( {"pinOutColor" : Color.blue , "label" : "Tx"} )
            append( {"pinOutColor" : Color.black , "label" : "Gnd"} )
            append( {"pinOutColor" : Color.red , "label" : "Güç"} )

        }
    }

    header:"BLUETOOTH"
    codeText: "#16p11"
    subText: "Kablosuz İletişim"

}
