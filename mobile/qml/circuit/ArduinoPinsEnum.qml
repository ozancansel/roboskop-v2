pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.0

QtObject {

    readonly property int d0    :   0
    readonly property int d1    :   1
    readonly property int d2    :   2
    readonly property int d3    :   3
    readonly property int d4    :   4
    readonly property int d5    :   5
    readonly property int d6    :   6
    readonly property int d7    :   7
    readonly property int d8    :   8
    readonly property int d9    :   9
    readonly property int d10   :   10
    readonly property int d11   :   11
    readonly property int d12   :   12
    readonly property int d13   :   13

    readonly property int gnd1  :   14
    readonly property int gnd2  :   15
    readonly property int gnd3  :   16

    readonly property int vin   :   17
    readonly property int fiveVolt1     :   18
    readonly property int fiveVolt2     :   19
    readonly property int threeVolt     :   20
    readonly property int resetPin      :   21

    readonly property int a0    :   22
    readonly property int a1    :   23
    readonly property int a2    :   24
    readonly property int a3    :   25
    readonly property int a4    :   26
    readonly property int a5    :   27

}
