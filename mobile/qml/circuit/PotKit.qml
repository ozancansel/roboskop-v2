import QtQuick 2.0
import "template"
import "../model"
import "../theme"

Template25x25{

    property double potVisualLineThickness      :   0.04
    property double potCenterLineThickness      :   0.1

    mainText: "POT"
    subText:  "AYARLI DİRENÇ"

    component:  Component{
        id : componentVisual

        Image{
            source  :   "/res/img/robologo-512-.png"
            fillMode: Image.PreserveAspectFit
        }

    }

    code            :   "#16p5"

    pins : ListModel{
        Component.onCompleted: {
            append( {"pinOutColor" : Color.red , "label" : "Güç"} )
            append( {"pinOutColor" : Color.yellow , "label" : "Sinyal"} )
            append( {"pinOutColor" : Color.black , "label" : "Toprak"} )
        }
    }
}
