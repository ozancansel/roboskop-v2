pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.0
import "../enum"

QtObject {

    id              :   internalNotify

    //Constnats
    readonly property int   bluetoothNotConnectedFlag   :   0
    readonly property int   bluetoothScanFlag           :   1
    readonly property int   bluetoothConnectingFlag     :   2
    readonly property int   bluetoothConnectedFlag      :   3
    readonly property int   bluetoothDisconnectedFlag   :   4
    readonly property int   bluetoothErrorFlag          :   5

    property bool   exampleRunning      :   determineRunning()
    property bool   exampleRunningFlag  :   false

    //Colors
    signal  bluetoothConnectionRequired()
    signal  exampleStartRequired()
    signal  exampleStopRequired()

    function    setRunning(enabled){
        exampleRunningFlag = bluetooth.state == BluetoothStateEnum.connected && enabled
    }

    function    determineRunning(){
        return exampleRunningFlag && bluetooth.state == BluetoothStateEnum.connected
    }

    function    startExample(){
        exampleStartRequired()
    }

    function    stopExample(){
        exampleStopRequired()
    }

    function    connectBluetooth(){
        bluetoothConnectionRequired()
    }

}
