pragma Singleton
import QtQuick 2.0

QtObject {

    readonly property int disconnected  :   0
    readonly property int connecting    :   1
    readonly property int connected     :   2
    readonly property int discovering   :   3
    readonly property int error         :   4
    readonly property int none          :   5

}
