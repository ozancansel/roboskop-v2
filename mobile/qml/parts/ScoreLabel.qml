import QtQuick 2.0
import "../theme"

Item {

    property real   score           :   0
    property int    backgroundColor  :   0
    width               :   Math.max(height , scoreText.width * 1.7)

    Rectangle{
        id                  :   backRect
        width               :   parent.width
        height              :   Math.max(parent.height , scoreText.height * 1.5)
        color               :   Color.colors[backgroundColor]
        radius              :   height * 0.35
        rotation            :   -20
        border.width        :   height * 0.05
        anchors.centerIn    :   parent
    }

    Text {
        id                  :   scoreText
        text                :   score
        font.family         :   FontCollection.labelFontName
        font.pixelSize      :   parent.height * 0.35
        anchors.centerIn    :   parent
        color               :   "white"
    }
}
