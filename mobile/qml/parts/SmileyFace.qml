import QtQuick 2.0
import "../theme"

Item {

    readonly property real  eyeHeight               :   0.07
    readonly property real  eyeWidth                :   0.2
    readonly property real  eyeTopOffset            :   0.37
    readonly property real  mouthWidth              :   0.35
    readonly property real  mouthHeight             :   0.2
    readonly property real  mouthTopOffset          :   0.63
    readonly property real  vibrationDistance       :   0.06
    readonly property int   fine                    :   0
    readonly property int   medium                  :   1
    readonly property int   angry                   :   2
    readonly property int   sleeping                :   3
    readonly property variant   eyeColorMapping     :   [ Color.green , Color.yellow , Color.red , Color.black]
    readonly property variant   eyeRotationMapping  :   [ 4     , -12 , 28   , 2]
    readonly property variant   eyeRadiusMapping    :   [ 0.2   , 0.5 , 0.05 , 0.3]
    readonly property variant   mouthRadiusMapping  :   [ 0.1   , 0.5 , 0    ,   0]
    readonly property variant   mouthWidthMapping   :   [ 0.35  , 0.3 , 0.4  , 0.25]
    readonly property variant   mouthHeightMapping  :   [ 0.05  , 0.1 , 0.16 , 0.05]
    readonly property variant   mouthBorderMapping  :   [ 0.8   , 0.3 , 0.25 , 0.8]

    property int    emotionalState      :   3
    property bool   leftVibration       :   false
    property bool   vibrationEnabled    :   false

    Timer{
        running     :   false
        repeat      :   true
        onTriggered :   {
            emotionalState = Math.floor(Math.random() * 3.95)
        }
    }

    Item {
        id              :   sleepingEffect
        anchors.right   :   parent.right
        anchors.top     :   parent.top
        width           :   parent.width * 0.2
        height          :   width
        visible         :   emotionalState === sleeping

        Text {
            id                  :   sleepingText
            text                :   "zZzZ"
            anchors.centerIn    :   parent
            font.pixelSize      :   parent.height * 0.5
            rotation            :   -20
        }
    }

    Item {
        id              :   organs
        width           :   parent.width
        height          :   parent.height

        Behavior on x{
            SmoothedAnimation{
            }
        }

        Rectangle   {
            anchors.fill    :   parent
            radius          :   parent.width * 0.5
            border.width    :   height * 0.1
            border.color    :   Color.colors[eyeColorMapping[emotionalState]]
        }

        Rectangle   {
            id      :   leftEye
            x       :   (parent.width / 3) - (width / 2)
            y       :   parent.height * eyeTopOffset - height / 2
            width   :   parent.width * eyeWidth
            height  :   parent.height * eyeHeight
            color   :   Color.colors[eyeColorMapping[emotionalState]]
            rotation:   eyeRotationMapping[emotionalState]
            radius  :   height * eyeRadiusMapping[emotionalState]
            Behavior on color{
                ColorAnimation {
                }
            }
            Behavior on rotation{
                SmoothedAnimation{

                }
            }
        }

        Rectangle{
            id      :   rightEye
            x       :   (parent.width * (2 / 3)) - (width / 2)
            y       :   (parent.height * eyeTopOffset) - (height / 2)
            width   :   parent.width * eyeWidth
            height  :   parent.height * eyeHeight
            color   :   Color.colors[eyeColorMapping[emotionalState]]
            rotation:   -eyeRotationMapping[emotionalState]
            radius  :   height * eyeRadiusMapping[emotionalState]
            Behavior on color{
                ColorAnimation {
                }
            }
            Behavior on rotation{
                SmoothedAnimation{
                }
            }
        }

        Rectangle{
            id                          :   mouth
            width                       :   parent.width * mouthWidthMapping[emotionalState]
            height                      :   parent.height * mouthHeightMapping[emotionalState]
            y                           :   parent.height * mouthTopOffset - height / 2
            anchors.horizontalCenter    :   parent.horizontalCenter
            border.width                :   height * mouthBorderMapping[emotionalState]
            border.color                :   Color.colors[eyeColorMapping[emotionalState]]
            radius                      :   height * mouthRadiusMapping[emotionalState]
            Behavior on width {
                SmoothedAnimation{

                }
            }
            Behavior on height {
                SmoothedAnimation{

                }
            }
            Behavior on border.width {
                SmoothedAnimation{

                }
            }
        }
    }


    onEmotionalStateChanged :   {
        if(emotionalState !== angry)
            organs.x    =   0
    }

    Timer   {
        id          :   vibrationTimer
        interval    :   100
        repeat      :   true
        running     :   emotionalState === angry && vibrationEnabled
        onTriggered :   {
            if(leftVibration)
                organs.x    =   parent.width * vibrationDistance
            else
                organs.x    =   -parent.width * vibrationDistance

            leftVibration = !leftVibration
        }
    }

    Timer{
        id          :   mockTimer
        interval    :   1500
        repeat      :   true
        running     :   true
        onTriggered :   {
            var newState = Math.floor(Math.random() * 3)
//            emotionalState = newState
        }
    }
}
