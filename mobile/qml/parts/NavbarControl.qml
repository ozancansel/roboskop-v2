import QtQuick 2.0
import "../theme"


Item{

    property real   textHeight              :   0.9
    property real   iconHeight              :   0.9

    property alias  imgControl               :   image
    property alias  textControl              :   text
    property alias  controlArea              :   area

    scale                   :   area.pressed ? 1.05 : 0.95
    width                   :   image.width + text.width

    Behavior on scale{
        NumberAnimation{
            duration        :   100
        }
    }

    Image {
        fillMode        :   Image.PreserveAspectFit
        id              :   image
        height          :   parent.height * iconHeight
        smooth          :   true
        anchors.left    :   parent.left
    }

    Text {
        id                  :   text
        anchors.verticalCenter      :   parent.verticalCenter
        font.pixelSize      :   parent.height * textHeight
        font.family         :   FontCollection.headerFontName
        color               :   "white"
        anchors.left        :   image.right
    }

    MouseArea{
        id                  :   area
        anchors.fill        :   parent
    }

}
