import QtQuick 2.7

Item {

    property string     type            :   ""
    property string     imgSource       :   ""
    property color      backgroundColor :   "yellow"

    Rectangle{
        anchors.fill    :   parent
        radius          :   width * 0.45
        border.width    :   width * 0.03
        color           :   backgroundColor
    }

    Image {
        source          :   imgSource
        anchors.fill    :   parent
        fillMode        :   Image.PreserveAspectFit
        anchors.margins :   height * 0.2
        smooth          :   true
        mipmap          :   true
    }
}
