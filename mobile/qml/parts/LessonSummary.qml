import QtQuick 2.7
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import Roboskop 1.0
import "../circuit/roboskop101"
import "../theme"
import "../controls"

Item{

    id                                      :   summaryContainer

    property double     headerTextHeight    :   0.5
    property string lessonCode              :   ""
    property string header                  :   ""
    property string imageSrc                :   ""
    property string lessonIdx               :   ""
    property bool   hasGame                 :   false
    property variant labels                 :   [ ]
    property int    colorIdx                :   0
    //    readonly property variant   backgroundColors    :   [ "#33cc33" , "#ffcc33" , "#ff3366" ]
    readonly property variant   backgroundColors    :   [ Color.green , Color.yellow , Color.pink ]
    readonly property variant   fontColors          :   [ "ffcc33" , "#ff3366" , "#f2f1ef"]
    property string headerBackgroundColor       : {
        if(kitCode === 101)
            return "#34465e"
        else if(kitCode === 102)
            return "#EB1600"
    }
    property string headerFontColor             : "white"
    property string descriptionBackgroundColor  : {
        if(kitCode === 101){
            return Color.colors[backgroundColors[(colorIdx % backgroundColors.length)]]
        }
        else if(kitCode === 102)
            return "#00CC49"
    }
    property string imageBackgroundColor        :   {
        if(kitCode === 101)
            return "#ffa96d"
        else if(kitCode === 102)
            return "white"
    }

    function    refreshStatistics(){
        statistics.refresh()
    }

    property int    kitCode                     : 101

    GameStatistics{
        id          :   statistics
        lessonId    :   lessonIdx
    }

    MouseArea{
        id                  :   lessonSummaryArea
        anchors.fill        :   parent
    }

    FontLoader {
        id          :   unicornAwesomeFont
        source      :   "/res/font/JandaCloserToFree.ttf"
    }

    FontLoader{
        id          :   descFont
        source      :   "/res/font/special-labels-font.ttf"
    }


    //Back
    Rectangle{
        id                  :   lessonDescContainerBackground
        anchors.left        :   lessonIdxItem.right
        anchors.leftMargin  :   height * 0.2
        anchors.right       :   parent.right
        anchors.bottom      :   parent.bottom
        anchors.top         :   parent.top
        radius              :   height * 0.2
        opacity             :   0.3
        color               :   Color.colors[backgroundColors[colorIdx % backgroundColors.length]]
    }

    LessonIndexItem{
        id                      :   lessonIdxItem
        width                   :   parent.height * 0.9
        height                  :   parent.height * 0.9
        imgSource               :   imageSrc
        backgroundColor         :   Color.colors[backgroundColors[(colorIdx + 1) % backgroundColors.length]]
        anchors.verticalCenter  :   parent.verticalCenter
    }

    Item{
        id                  :   lessonDescCont
        anchors.fill        :   lessonDescContainerBackground
        z                   :   2

        Text {
            id                      :   name
            text                    :   lessonIdx + ". " + header
            x                       :   parent.height * 0.5
            font.pixelSize          :   parent.height * 0.3
            anchors.verticalCenter  :   parent.verticalCenter
            color                   :   fontColors[colorIdx % fontColors.length]
            font.family             :   unicornAwesomeFont.name
            font.capitalization     :   Font.Capitalize
        }

        Row{
            id                  :   labelsRow
            anchors.right       :   parent.right
            anchors.rightMargin :   parent.height * 0.2
            anchors.top         :   parent.top
            anchors.bottom      :   parent.bottom
            spacing             :   height  *   0.1
            layoutDirection     :   Qt.RightToLeft



            StateLabel  {
                id                      :   stateLabel
                height                  :   parent.height * 0.6
                width                   :   height
                anchors.verticalCenter  :   parent.verticalCenter
                lessonState             :   statistics.state
                hasGame                 :   summaryContainer.hasGame
                backgroundColor         :   backgroundColors[(colorIdx + 1) % backgroundColors.length]
                iconColor               :   backgroundColors[(colorIdx) % backgroundColors.length]
            }

            ScoreLabel{
                id                      :   scoreLabel
                height                  :   parent.height * 0.6
                anchors.verticalCenter  :   parent.verticalCenter
                score                   :   statistics.highestScore
                visible                 :   statistics.highestScore > 0
                backgroundColor         :   backgroundColors[(colorIdx + 1) % backgroundColors.length]
            }
        }
    }

    DropShadow{
        id              :   shadow
        anchors.fill    :   lessonDescContainerBackground
        horizontalOffset:   0
        verticalOffset  :   0
        radius          :   lessonDescContainerBackground.height * 0.1
        samples         :   17
        source          :   lessonDescContainerBackground
        color           :   "#000000"
    }
}

