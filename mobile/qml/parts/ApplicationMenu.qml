import QtQuick 2.7
import QtQuick.Controls 2.0
import "../theme"
import "../controls"

Drawer {

    id          :   applicationMenuContainer

    //Top Margin
    readonly property real menuContainerTopMargin           :   0.02
    readonly property real menuButtonsTopMargin             :   0.03

    readonly property variant urls : ["/qml/form/ExampleForm.qml" , "/qml/form/GezginPovForm.qml" , "" , ""]
    readonly property variant navbarUrls    :   ["/qml/parts/ExampleStateNavbarComponent.qml","/qml/parts/RoboGezginNavbar.qml"]

    signal      menuButtonTriggered(string url, string navbarUrl)

    Rectangle {

        anchors.left        :   parent.left
        anchors.right       :   parent.right
        anchors.top         :   parent.top
        anchors.bottom      :   parent.bottom
        anchors.topMargin   :   parent.height * menuContainerTopMargin
        color               :   "transparent"

        Image{
            id                          :   logoImg
            source                      :   "/res/img/logo-128x.png"
            width                       :   parent.width * 0.5
            fillMode                    :   Image.PreserveAspectFit
            anchors.horizontalCenter    :   parent.horizontalCenter
        }

        Column{
            id                  :   applicationMenuColumn
            anchors.top         :   logoImg.bottom
            anchors.topMargin   :   SizeBinding.size(parent.height * menuButtonsTopMargin , SizeBinding.mm(20) , SizeBinding.mm(5))
            width               :   parent.width
            height              :   parent.height
            y                   :   applicationMenuContainer.topMargin
            spacing             :   SizeBinding.mm(1.5)


            Repeater {
                model       :   ["Örnekler" , "Robug Sür" ,"Çıkış"]
                ExamplesMenuButton{
                    buttonWidth                 :   applicationMenuColumn.width * 0.8
                    buttonHeight                :   SizeBinding.size(parent.height * 0.07 , SizeBinding.mm(12) , SizeBinding.mm(4))
                    anchors.horizontalCenter    :   applicationMenuColumn.horizontalCenter
                    text                        :   modelData
                    iconSource: {
                        if(index == 0)
                            "/res/img/edit-icon-white-48x48.png"
                        else if(index == 1)
                            "/res/img/rc-car-200x200.png"
                        else
                            "/res/img/shutdown-48x48.png"
                    }

                    onButtonClicked: {
                        if(index === 2)
                            Qt.quit()

                        menuButtonTriggered(urls[index] , navbarUrls[index])
                    }
                }
            }
        }
    }

    background              :   Rectangle{
        color               :   "#333333"
        Rectangle{
            x               :   parent.width - 1
            width           :   1
            height          :   parent.height
            color           :   "white"
        }
    }

}
