import QtQuick 2.0

Item {

    property alias      background      :   backRect
    property alias      textItem        :   lessonCompleted

    id              :   itCont
    width           :   lessonCompleted.width * 1.4

    FontLoader{
        id          :   labelFont
        source      :   "/res/font/special-labels-font.ttf"
    }

    Rectangle{
        id          :   backRect
        anchors.fill:   parent
        radius      :   height  *   0.4
    }

    Text {
        id                  :   lessonCompleted
        anchors.centerIn    :   parent
        font.pixelSize      :   parent.height * 0.4
        rotation            :   -itCont.rotation
        font.family         :   labelFont.name
    }
}
