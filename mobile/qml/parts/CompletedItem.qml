import QtQuick 2.0

Item {

    property bool     isCompleted     :   false

    id              :   itCont
    width           :   lessonCompleted.width * 1.4

    FontLoader{
        id          :   labelFont
        source      :   "/res/font/special-labels-font.ttf"
    }

    Rectangle{
        color       :   isCompleted ? "yellow" : "green"
        anchors.fill:   parent
        radius      :   height  *   0.4
    }

    Text {
        id                  :   lessonCompleted
        text                :   isCompleted ?   "Bitti!"   :   "Bekliyor"
        anchors.centerIn    :   parent
        font.pixelSize      :   parent.height * 0.4
        rotation            :   -itCont.rotation
        font.family         :   labelFont.name
    }

    onHeightChanged :   {
        console.log("Height -> " + height)
    }
}
