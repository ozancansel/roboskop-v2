import QtQuick 2.0
import "../theme"

Item {

    property real   score           :   0
    property int    backgroundColor :   0
    property int    iconColor       :   0
    property bool   hasGame         :   false
    property string lessonState           :   "none"
    property variant iconMapping    :   {
        "completed"         :   "/res/img/star-yellow.png",
                "entered"           :   "/res/img/not-finished.png" ,
                "started"           :   "/res/img/not-finished.png",
                "none"              :   ""
    }


    id      :   label

    onStateChanged  :    console.log(lessonState)
    property variant stateMapping   :   {
        "completed"     :   "Tm" ,
                "entered"       :   "?",
                "none"          :   "?"
    }

    function getIcon()  {

        if(lessonState === "entered" || lessonState === "started"){
            if(hasGame)
                return "/res/img/game-not-played.png"
            else
                return "/res/img/not-finished.png"
        }

        return "/res/img/star-" + Color.colorStrings[iconColor] + ".png"
    }

    SequentialAnimation {
        id              :   notPlayedAnimation
        running         :   lessonState === "entered" || lessonState === "none"
        loops           :   Animation.Infinite

        readonly property color fromCol         :   Color.colors[backgroundColor]
        readonly property color lighterCol      :   Qt.lighter(fromCol , 1.2)

        ColorAnimation {
            target      :   backRect
            property    :   "color"
            from        :   notPlayedAnimation.fromCol
            to          :   notPlayedAnimation.lighterCol
            duration    :   400
        }

        PauseAnimation {
            duration    :   250
        }

        ColorAnimation {
            target      :   backRect
            property    :   "color"
            from        :   notPlayedAnimation.lighterCol
            to          :   notPlayedAnimation.fromCol
            duration    :   400
        }
    }

    width               :   Math.max(width , stateText.width * 1.4)

    Rectangle   {
        id                  :   backRect
        width               :   parent.width
        height              :   Math.max(parent.height , stateText.height * 1.4)
        color               :   Color.colors[backgroundColor]
        radius              :   height * 0.35
        rotation            :   -20
        border.width        :   height * 0.05
        anchors.centerIn    :   parent
    }

    Image {
        id                  :   img
        source              :   getIcon()
        anchors.fill        :   parent
        anchors.margins     :   parent.height * 0.2
        anchors.centerIn    :   parent
        visible             :   (lessonState !== "none" && lessonState !== "entered") || hasGame
        fillMode            :   Image.PreserveAspectFit
    }

    Text {
        id                  :   stateText
        text                :   stateMapping[label.lessonState]
        font.family         :   FontCollection.labelFontName
        font.pixelSize      :   parent.height * 0.5
        anchors.centerIn    :   parent
        color               :   "white"
        visible            :   (label.lessonState === "none" || lessonState === "entered") && !hasGame
    }
}
