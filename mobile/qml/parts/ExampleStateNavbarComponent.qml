import QtQuick 2.7
import "../model"
import "../enum"
import "../theme"

Item {

    property    real    lessonIdx   : 1

    //Width
    readonly property real navbarLessonNameContainerWidth       :   0.2
    readonly property real exampleStateTextContainerWidth       :   0.1
    readonly property real bluetoothStateWidth                  :   0.2

    //Height
    readonly property real bluetoothStateIconHeight             :   0.9
    readonly property real exampleStateIconHeight               :   0.9

    //Font
    readonly property real exampleStateTextHeight               :   0.55
    readonly property real bluetoothStateTextHeight             :   0.55

    property bool   exampleRunning                              :   false
    property alias  exampleStartStopControl                     :   exampleStateControl
    property bool   exampleStartStopVisible                     :   true
    property string headerTxt                                   :   ""

    LessonModel{
        id          :   model

        Component.onCompleted: {
            headerTxt = get(lessonIdx - 1).modelHeader
        }
    }


    FontLoader{
        id          :   headerFont
        source      :   "/res/font/special-labels-font.ttf"
    }


    id  :   navbar

    Text {
        id                      :   lessonNameText
        text                    :   headerTxt.toLowerCase()
        font.pixelSize          :   parent.height * 0.6
        color                   :   "white"
        font.family             :   FontCollection.headerFontName
        font.capitalization     :   Font.Capitalize
        anchors.left            :   parent.left
        anchors.verticalCenter  :   parent.verticalCenter
        anchors.right           :   exampleStateControl.left
    }

    Row {

        anchors.left    :   parent.left
        anchors.right   :   parent.right
        anchors.top     :   parent.top
        anchors.bottom  :   parent.bottom
        layoutDirection :   Qt.RightToLeft

        NavbarControl{
            id                      :   bluetoothState
            iconHeight              :   bluetoothStateIconHeight
            textHeight              :   bluetoothStateTextHeight
            height                  :   parent.height

            controlArea.onClicked   :   {
                if(bluetooth.state != BluetoothStateEnum.connected){
                    InternalNotifyEnum.connectBluetooth()
                    return
                }
            }

            controlArea.onPressAndHold  :   {
                if(bluetooth.state == BluetoothStateEnum.connected)
                    bluetooth.closeSocket()
            }

            state   :   "Not Connected"

            states: [
                State{
                    name    :   "connected"; when   :   bluetooth.state === BluetoothStateEnum.connected
                    PropertyChanges {
                        target      :   bluetoothState
                        rotation    :   360
                    }

                    PropertyChanges {
                        target      :   bluetoothState.textControl
                        text        :   "Bağlantı Aktif"
                    }
                    PropertyChanges {
                        target      :   bluetoothState.imgControl
                        source      :   "/res/img/bluetooth-icon-connected.png"
                    }
                },
                State{
                    name    :   "connecting";   when    :   bluetooth.state === BluetoothStateEnum.connecting
                    PropertyChanges {
                        target      :   bluetoothState.textControl
                        color       :   "green"
                        text        :   "Bağlanıyor... "
                    }
                    PropertyChanges {
                        target      :   bluetoothState.imgControl
                        source      :   "/res/img/bluetooth-icon-unconnected.png"
                    }
                },
                State {
                    name    :   "discovering"; when     :   bluetooth.state === BluetoothStateEnum.discovering
                    PropertyChanges {
                        target      :   bluetoothState.textControl
                        text        :   "Aranıyor...   "
                        color       :   Color.colors[Color.white]
                    }
                    PropertyChanges {
                        target      :   bluetoothState.imgControl
                        source      :   "/res/img/bluetooth-icon-unconnected.png"
                    }
                } ,
                State {
                    name    :   "disconnected"; when    :   bluetooth.state === BluetoothStateEnum.disconnected
                    PropertyChanges {
                        target      :   bluetoothState.textControl
                        text        :   "Bağlantı koptu"
                        color       :   Color.colors[Color.red]
                    }

                    PropertyChanges {
                        target          :     navbar
                        exampleRunning  :   false
                    }

                    PropertyChanges {
                        target      :   bluetoothState.imgControl
                        source      :   "/res/img/bluetooth-icon-unconnected.png"
                    }
                },
                State {
                    name    :   "notConnected"; when    :   bluetooth.state === BluetoothStateEnum.none
                    PropertyChanges {
                        target  :   bluetoothState.textControl
                        text    :   "Bağlantı Yok  "
                        color   :   "white"
                    }

                    PropertyChanges {
                        target  :   navbar
                        exampleRunning     :    false

                    }

                    PropertyChanges {
                        target  :   bluetoothState.imgControl
                        source  :   "/res/img/bluetooth-icon-unconnected.png"
                    }
                },
                State {
                    name        :   "error"; when   :   bluetooth.state == BluetoothStateEnum.error
                    PropertyChanges {
                        target  :   bluetoothState.textControl
                        text    :   "Hata"
                        color   :   "red"
                    }
                    PropertyChanges {
                        target  :   bluetoothState.imgControl
                        source  :   "/res/img/bluetooth-icon-unconnected.png"
                    }
                }
            ]

            transitions :   [Transition {
                    from    : ""
                    to      : "connected"
                    reversible  :   true
                    RotationAnimation{
                        target      :   bluetoothState
                        duration    :   200
                    }
                },
                Transition {
                    to      : "connecting"
                    SequentialAnimation{
                        loops: Animation.Infinite

                        ColorAnimation {
                            target  :   bluetoothState.textControl
                            duration: 400
                            from    :   "white"
                            to      :   "green"
                        }

                        ColorAnimation {
                            target  :   bluetoothState.textControl
                            from    : "green"
                            to      : "white"
                            duration: 400
                        }
                    }
                },
                Transition {
                    to      :   "discovering"
                    SequentialAnimation{
                            loops       :   Animation.Infinite
                            ScriptAction{ script : bluetoothState.textControl.text = "Aranıyor.     "}
                            PauseAnimation { duration  : 300}
                            ScriptAction{ script : bluetoothState.textControl.text = "Aranıyor..    "}
                            PauseAnimation { duration  : 300}
                            ScriptAction{ script : bluetoothState.textControl.text = "Aranıyor...   "}
                            PauseAnimation { duration  : 500}
                    }
                }]
        }

        NavbarControl   {
            id                  :   exampleStateControl
            visible             :   exampleStartStopVisible
            imgControl.source   :   InternalNotifyEnum.exampleRunning ?   "/res/img/stop-button-red.png" : "/res/img/play-icon-green-128-.png"
            textControl.text    :   InternalNotifyEnum.exampleRunning ? "Durdur"    :   "Başlat"
            iconHeight          :   exampleStateIconHeight
            textHeight          :   exampleStateTextHeight
            height              :   parent.height

            controlArea.onClicked   :   {

                if(InternalNotifyEnum.exampleRunningFlag)
                    InternalNotifyEnum.stopExample()
                else
                    InternalNotifyEnum.startExample()

            }

            states: [
                State{
                    name    :   "running"; when   :   InternalNotifyEnum.exampleRunning
                    PropertyChanges {
                        target      :   exampleStateControl
                        rotation    :   360
                    }
                }
            ]

            transitions :   [
                Transition {
                    from    : ""
                    to      : "running"
                    RotationAnimation{
                        target      :   exampleStateControl
                        duration    :   200
                    }
                }
            ]
        }
    }

    Connections{
        target          :   globalNotifier
        onBroadcast     :   {
            if(name === "exampleStarted")
                exampleRunning = true

            if(name === "exampleStop")
                exampleRunning = false
        }
    }
}
