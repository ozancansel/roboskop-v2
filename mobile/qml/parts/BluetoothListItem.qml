import QtQuick 2.0
import "../theme"

Rectangle {

    id : container

    property alias btName : name.text
    property alias btAddress : address.text
    property int fontSize : 12
    property string nameColor : "#333333"
    property string addressColor : nameColor
    property string fontColor : "white"

    width: parent.width
    height: parent.height

    Rectangle {
        id : nameContainer
        width:  SizeBinding.size(parent.width * 0.3 , SizeBinding.mm(60) , SizeBinding.mm(40))
        height : container.height
        color: nameColor

        Text {
            id: name
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("text")
            font.pointSize: container.fontSize
            color: fontColor
        }
    }

    Rectangle {

        id : addressContainer
        height: container.height
        anchors.right: container.right
        anchors.left : nameContainer.right
        color: addressColor


        Text {
            id: address
            anchors.fill : parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("text")
            font.pointSize: container.fontSize
            color: fontColor
        }
    }
}
