import QtQuick 2.0
import "../controls"

Item {

    //Header text size
    readonly property real headerTextFontSize       :   0.6

    //Color
    readonly property color headerTextColor          :   "white"

    id          :   container

    Text{
        id                      :   headerText
        text                    :   "RoboGezgin"
        font.pixelSize          :   parent.height * headerTextFontSize
        color                   :   headerTextColor
        anchors.verticalCenter  :   parent.verticalCenter
    }

    Row {
        anchors.fill        :   parent
        layoutDirection     :   Qt.RightToLeft

        BluetoothStateControl{
            height          :   parent.height
            textHeight      :   0.6
        }
    }
}
