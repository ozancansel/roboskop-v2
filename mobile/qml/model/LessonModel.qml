import QtQuick 2.0

ListModel{

    ListElement{
        modelIndex          :   1
        modelCode           :   "Uygulama 1"
        modelHeader         :   "Mikroişlemciyi Öğreniyorum"
        modelImagePath      :   "/res/img/led-1.png"
        modelHasGame        :   false
    }
    ListElement{
        modelIndex          :   2
        modelCode           :   "Uygulama 2"
        modelHeader         :   "Işık Kaynağı Yaptım"
        modelImagePath      :   "/res/img/led-2.png"
        modelHasGame        :   false
    }
    ListElement{
        modelIndex          :   3
        modelCode           :   "Uygulama 3"
        modelHeader         :   "Trafik Lambasını Öğrenelim"
        modelImagePath      :   "/res/img/traffic-light.png"
        modelHasGame        :   false
    }

    ListElement{
        modelIndex          :   4
        modelCode           :   "Uygulama 4"
        modelHeader         :   "Sensörü Oku, Puanları topla"
        modelImagePath      :   "/res/img/voltage-sensor.png"
        modelHasGame        :   true
    }

    ListElement{
        modelIndex          :   5
        modelCode           :   "Uygulama 5"
        modelHeader         :   "Ne kadar ışık var ?"
        modelImagePath      :   "/res/img/ldr-sensor.png"
        modelHasGame        :   false
    }

    ListElement{
        modelIndex          :   6
        modelCode           :   "Uygulama 6"
        modelHeader         :   "Kendi Rengini Bul"
        modelImagePath      :   "/res/img/rgb.png"
        modelHasGame        :   true
    }

    ListElement{
        modelIndex          :   7
        modelCode           :   "Uygulama 7"
        modelHeader         :   "Rengi Analog Ayarlanabilir Led"
        modelImagePath      :   "/res/img/rgb.png"
        modelHasGame        :   false
    }

    ListElement{

        modelIndex          :   8
        modelCode           :   "Uygulama 8"
        modelHeader         :   "Rengi Işıkla Ayarlanabilir Led"
        modelImagePath      :   "/res/img/rgb-with-ldr.png"
        modelHasGame        :   false
    }

    ListElement{
        modelIndex          :   9
        modelCode           :   "Uygulama 9"
        modelHeader         :   "Piano Öğreniyorum"
        modelImagePath      :   "/res/img/piano.png"
        modelHasGame        :   false
    }

    ListElement{
        modelIndex          :   10
        modelCode           :   "Uygulama 10"
        modelHeader         :   "Jingle Bellsi Çal !"
        modelImagePath      :   "/res/img/piano.png"
        modelHasGame        :   true
    }

    ListElement {
        modelIndex          :   11
        modelCode           :   "Uygulama 11"
        modelHeader         :   "Robotik Cetvel Yapalım"
        modelImagePath      :   "/res/img/mobile-sensor.png"
        modelHasGame        :   true
    }

    ListElement{
        modelIndex          :   12
        modelCode           :   "Uygulama 12"
        modelHeader         :   "Kendi Park Sensörünü Yap"
        modelImagePath      :   "/res/img/mobile-sensor.png"
        modelHasGame        :   false
    }

    ListElement{
        modelIndex          :   13
        modelCode           :   "Uygulama 13"
        modelHeader         :   "Biraz da Oyun Oynayalım"
        modelImagePath      :   "/res/img/spaceship-256x.png"
        modelHasGame        :   true
    }

    ListElement{
        modelIndex          :   14
        modelCode           :   "Uygulama 14"
        modelHeader         :   "Ne kadar ses çıkarabilirsin ?"
        modelImagePath      :   "/res/img/mic-read.png"
        modelHasGame        :   false
    }

    ListElement {
        modelIndex          :   15
        modelCode           :   "Uygulama 15"
        modelHeader         :   "Kızdırmak için Bağır"
        modelImagePath      :   "/res/img/angry-icon.png"
        modelHasGame        :   true
    }
}
