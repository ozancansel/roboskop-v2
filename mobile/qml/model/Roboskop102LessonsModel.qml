import QtQuick 2.0

ListModel {
    ListElement{
        modelIndex          :   1
        modelCode           : "Ders 1"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
1.3 Pilin ne olduğunu öğrenir ve kullanır.
1.4 Mikroişlemci tabanlı elektronik devre kurar.
1.5 Arkadaşları ile yardımlaşır.
1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }

    ListElement{
        modelIndex          :   2
        modelCode           : "Ders 2"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
    1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
    1.3 Pilin ne olduğunu öğrenir ve kullanır.
    1.4 Mikroişlemci tabanlı elektronik devre kurar.
    1.5 Arkadaşları ile yardımlaşır.
    1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }

    ListElement{
        modelIndex          :   3
        modelCode           : "Ders 3"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
    1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
    1.3 Pilin ne olduğunu öğrenir ve kullanır.
    1.4 Mikroişlemci tabanlı elektronik devre kurar.
    1.5 Arkadaşları ile yardımlaşır.
    1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }


    ListElement{
        modelIndex          :   4
        modelCode           : "Ders 4"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
    1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
    1.3 Pilin ne olduğunu öğrenir ve kullanır.
    1.4 Mikroişlemci tabanlı elektronik devre kurar.
    1.5 Arkadaşları ile yardımlaşır.
    1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }

    ListElement{
        modelIndex          :   5
        modelCode           : "Ders 5"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
    1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
    1.3 Pilin ne olduğunu öğrenir ve kullanır.
    1.4 Mikroişlemci tabanlı elektronik devre kurar.
    1.5 Arkadaşları ile yardımlaşır.
    1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }

    ListElement{
        modelIndex          :   6
        modelCode           : "Ders 6"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
    1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
    1.3 Pilin ne olduğunu öğrenir ve kullanır.
    1.4 Mikroişlemci tabanlı elektronik devre kurar.
    1.5 Arkadaşları ile yardımlaşır.
    1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }

    ListElement{
        modelIndex          :   7
        modelCode           : "Ders 7"
        modelHeader         : "Tanıtım ve Led Lamba"
        modelDescription    : "1.1 Mikroişlemcinin robotun beyni olduğunu öğrenir.
    1.2 LED ışık kaynağının ne olduğunu öğrenir ve kullanır.
    1.3 Pilin ne olduğunu öğrenir ve kullanır.
    1.4 Mikroişlemci tabanlı elektronik devre kurar.
    1.5 Arkadaşları ile yardımlaşır.
    1.7 Gerçek hayatla bağlantı kurar."
        modelImagePath : "/res/img/robologo-512-.png"
    }
}
