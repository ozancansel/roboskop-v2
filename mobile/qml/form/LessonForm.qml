 import QtQuick 2.7
import QtQuick.Controls 2.0
import Roboskop 1.0
import "../theme"
import "../model"
import "../controls"
import "../circuit"
import "../circuit/roboskop101"
import "../circuit/roboskop102"
import "../dialog"
import "../enum"

Rectangle {

    id                      :   container
    height                  :   parent.height

    property bool   hasInteractive      :   true
    property int    lessonIdx           :   4
    property int    kit                 :   101

    readonly property variant   backgroundColors    :   [ "#33cc33" , "#ffcc33" , "#ff3366" ]
    readonly property variant   highlightColors    :    [ "#39e439" , "#ffdf33" , "#ff436b"]
    readonly property variant   fontColors          :   [ "ffcc33" , "#ff3366" , "#f2f1ef"]

    //Width
    readonly property real navbarLessonNameContainerWidth       :   0.2
    readonly property real exampleStateTextContainerWidth       :   0.1

    function    startExample(){

        var sendIdx = lessonIdx;

        if(sendIdx === 10)
            sendIdx = 9

        bluetooth.send("{S" + sendIdx + "}")

        interactiveLoader.item.startExample()

        InternalNotifyEnum.setRunning(true)
        statistics.started()
    }

    function    stopExample(){
        bluetooth.send("{E}")

        interactiveLoader.item.stopExample()
        InternalNotifyEnum.setRunning(false)
        statistics.stopped()
    }

    function setCurrentIndex(idx){
        lessonFormSwipe.setCurrentIndex(idx)
    }

    function getSketchPath(){
        var lessIdx = lessonIdx
        switch(kit){
        case 101    :   {
                return Roboskop101Map.getSketchPath(lessonIdx)
            }
        case 102    :   {
                return roboskop102.getSketchPath(lessonIdx)
            }
        }
    }

    function getControlPath(){
        switch(kit){
        case 101    :   return Roboskop101Map.getInteractivePath(lessonIdx)
        case 102    :   return roboskop102.getInteractivePath(lessonIdx)
        }
    }

    Roboskop102Map{
        id      :   roboskop102
    }

    GameStatistics{
        id      :   statistics
        lessonId:   Roboskop101Map.getId(lessonIdx)
        onStateChanged  :   console.log("Lesson " + lessonId + " state -> " + state + " score -> " + highestScore)
    }


    SwipeView{

        id                  :   lessonFormSwipe
        currentIndex        :   footerBar.currentIndex
        width               :   parent.width
        height              :   parent.height - footerBar.height
        interactive         :   !sketchLoader.item.zoomed && !InternalNotifyEnum.exampleRunning

        Loader {
            id              :   sketchLoader
            source          :   getSketchPath()
            onLoaded        :   {      }
            height          :   parent.height
        }


        BluetoothDialog{
            id              :   bluetoothDialog
            onConnected     :   {
                lessonFormSwipe.setCurrentIndex(2)
            }
            onDisconnected  :   {
                lessonFormSwipe.setCurrentIndex(1)
            }
        }

        Loader  {
            id              :   interactiveLoader
            source          :   getControlPath()
            onStatusChanged :   {
                if(interactiveLoader.status === Loader.Error)
                    hasInteractive = false
            }
            onLoaded        :   {
                var conf    =   Roboskop101Map.getConf(lessonIdx)
                for(var prop in conf){
                    interactiveLoader.item[prop] = conf[prop]
                }

                Roboskop101Map.getConf(lessonIdx)

                statistics.entered()
            }
        }
    }

    Connections{
        target  :   interactiveLoader.item
        onToastMessageRequired  :   {
            //<message>
            toast.displayMessage(message)
        }
        onNewScore              :   {
            statistics.newScore(score)
        }
    }

    Toast   {
        id              :   toast
        container       :   lessonFormSwipe.currentItem
        fillColor       :   "#191919"
        textColor       :   "white"
    }

    TabBar {
        id              :   footerBar
        currentIndex    :   lessonFormSwipe.currentIndex
        anchors.left    :   parent.left
        anchors.right   :   parent.right
        anchors.bottom  :   parent.bottom
        position        :   TabBar.Footer

        property color  backColor   :   "#2286fb"

        RoboskopTabButton {
            id      :   sketchButton
            text    :   "Çizim"
            height  :   footerBar.height
            backgroundColor :   backgroundColors[0]
            fontColor       :   fontColors[0]
            highlightColor  :   highlightColors[0]
        }

        RoboskopTabButton{
            text    :   "Bluetooth"
            height  :   footerBar.height
            backgroundColor :   backgroundColors[1]
            fontColor       :   fontColors[1]
            highlightColor  :   highlightColors[1]
        }

        RoboskopTabButton{
            id                      :   controlTab
            height                  :   footerBar.height
            text                    :   "Kontrol Et !"
            enabled                 :   hasInteractive
            backgroundColor         :   backgroundColors[2]
            fontColor               :   fontColors[2]
            highlightColor          :   highlightColors[2]
        }
    }

    Connections{
        target      :   InternalNotifyEnum
        onBluetoothConnectionRequired   :   {
            lessonFormSwipe.setCurrentIndex(1)
        }

        onExampleStartRequired          :   {

            if(bluetooth.state ==  BluetoothStateEnum.connected)
                startExample()
            else
                InternalNotifyEnum.connectBluetooth()

            InternalNotifyEnum.setRunning(true)
        }

        onExampleStopRequired           :   {
            stopExample()
            InternalNotifyEnum.setRunning(false)
        }
    }

    Connections{
        target          :   bluetooth
        onStateChanged  :   {
            if(bluetooth.state == BluetoothStateEnum.connected)
                lessonFormSwipe.setCurrentIndex(2)
            else if(bluetooth.state == BluetoothStateEnum.disconnected || bluetooth.state == BluetoothStateEnum.none)
                lessonFormSwipe.setCurrentIndex(1)
        }
    }
}

