import QtQuick 2.0
import "../theme"
import "../model"
import "../controls"
import "../circuit"
import "../circuit/template"

SketchTemplate{

    readonly property real wireThickness :   getScaledX(50)

    Wire{
        id          :   first
        alignment   :   Qt.Horizontal
        length      :   getScaledX(200)
        centerX     :   getScaledX(400)
        centerY     :   getScaledY(310)
        wireColor   :   Color.green
        thickness   :   wireThickness
        visible     :   true
        reverse     :   true
    }

    Wire{
        id          :   second
        alignment   :   Qt.Vertical
        length      :   getScaledY(300)
        centerX     :   first.end().x
        centerY     :   first.end().y
        wireColor   :   Color.green
        thickness   :   wireThickness
        visible     :   true
        reverse     :   true
    }

    Rectangle{
        id          :   firstStartP
        x           :   first.centerX - (width / 2)
        y           :   first.centerY - (height / 2)
        color       :   "black"
        width       :   50
        height      :   50
        z           :   5
        visible     :   true
    }

    Rectangle{
        id          :   firstEndP
        color       :   "black"
        z           :   5
        width       :   50
        height      :   50
        x           :   first.end().x - (width / 2)
        y           :   first.end().y - (height / 2)
        visible     :   true
    }
}
