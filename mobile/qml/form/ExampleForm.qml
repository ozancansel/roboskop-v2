import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import "../controls"
import "../parts"
import "../theme"
import "../model"
import "../dialog"

Rectangle {

    id              :   mainContainer
    color           :   "#936950"
    height          :   parent.height

    readonly property real kitLessonSelectorHeight          :   0.07
    property int    kitCode                                 :   101

    signal  openExampleRequired(int idx , int kitCode)

    onKitCodeChanged    :   {
        fillWith(kitCode)
    }


    function    refreshLessons(){
        model.clear()
        for(var i = 0; i < r101Model.count; i++){
            var el = r101Model.get(i)
            model.append({"modelIndex" : el.modelIndex ,
                             "modelCode" : el.modelCode ,
                             "modelHeader" : el.modelHeader ,
                             "modelImagePath" : el.modelImagePath ,
                            "modelHasGame" : el.modelHasGame})

        }
    }

    function    fillWith(code){
        if(code === 101){
            model.clear()
            for(var i = 0; i < r101Model.count; i++){
                var el = r101Model.get(i)
                model.append({"modelIndex" : el.modelIndex ,
                                 "modelCode" : el.modelCode ,
                                 "modelHeader" : el.modelHeader ,
                                 "modelImagePath" : el.modelImagePath ,
                                 "modelHasGame" : el.modelHasGame})

            }
        }
        else if(code === 102){
            model.clear()
            for(var i = 0; i < r102Model.count; i++){
                var el = r102Model.get(i)
                model.append({"modelIndex" : el.modelIndex ,
                                 "modelCode" : el.modelCode ,
                                 "modelHeader" : el.modelHeader ,
                                 "modelImagePath" : el.modelImagePath})

            }
        }
    }

    LessonModel{
        id                  :   r101Model

        Component.onCompleted   : {
            refreshLessons()
        }
    }

    Roboskop102LessonsModel{
        id                  :   r102Model
    }

    ListModel{
        id                  :   model
    }

//    ConnectingDialog    {
//        id      :   connectingDialog
//        height  :   parent.height
//        width   :   parent.width
//        dialogHeight    :   parent.height * 0.35
//        dialogWidth     :   parent.width * 0.35
//    }

    Timer{
        repeat:true
        interval: 1000
//        onTriggered: connectingDialog.open()
        running: true
    }

    Item {
        id                  :   examplesShow
        anchors.fill        :   parent
        anchors.margins     :   height * 0.1
        anchors.centerIn    :   parent

        ListView {
            id                      :   lessonsListView
            anchors.fill            :   parent
            model                   :   model
            highlightMoveVelocity   :   height * 0.3
            snapMode                :   ListView.SnapOneItem
            highlightRangeMode      :   ListView.StrictlyEnforceRange
            spacing                 :   height * 0.04

            displaced               :   Transition {
                NumberAnimation { properties: "x,y"; to: 0; duration: 1000}
            }


            delegate    :   Item   {

                id          :   summaryContainer
                width       :   lessonsListView.width
                height      :   examplesShow.height * 0.2


                Behavior on scale {                    // added to smooth the scaling
                    NumberAnimation {
                        duration        :   150
                    }
                }

                LessonSummary   {
                    id                  :   lessonSummaryItem
                    anchors.fill        :   parent
                    lessonIdx           :   modelIndex
                    lessonCode          :   modelCode
                    header              :   modelHeader
                    imageSrc            :   modelImagePath
                    hasGame             :   modelHasGame
                    kitCode             :   mainContainer.kitCode
                    colorIdx            :   index
                }

                MouseArea{
                    id              :   lessonArea
                    anchors.fill    :   parent
                    onClicked       :   {
                       lessonsListView.currentIndex = index
                       openExampleRequired(modelIndex , kitCode)
                    }
                }
            }
        }
    }
}

