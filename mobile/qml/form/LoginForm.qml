import QtQuick 2.7
import QtGraphicalEffects 1.0
import "../controls"
import "../theme"

Item {

    readonly property real  colSpacing      :   0.025
    readonly property real  unitHeight      :   0.27
    readonly property real  buttonsHeight   :   0.22
    readonly property real  textBoxFontSize :   0.35
    readonly property color textBoxColors   :   Color.colors[Color.white]
    readonly property color mainContainerBackColor  :   Color.colors[Color.purple]

    id      :   form

    Column{

        spacing                 :   height *    colSpacing
        width                   :   parent.width * 0.9
        anchors.centerIn        :   parent
        z                       :   2

        Item {
            id                  :   userNameArea
            height              :   form.height * unitHeight
            width               :   parent.width

            Item {
                id      :   usernameIconContainer
                height  :   parent.height
                width   :   height

                Image {
                    id              :   usernameIconImg
                    source          :   "/res/img/username-icon.png"
                    anchors.fill    :   parent
                    anchors.margins :   height * 0.1
                    anchors.centerIn:   parent
                    fillMode        :   Image.PreserveAspectFit
                    smooth          :   true
                    mipmap          :   true
                }
            }

            RoboskopTextBox {
                id                  :   txt
                height              :   parent.height
                anchors.left        :   usernameIconContainer.right
                anchors.right       :   parent.right
                anchors.leftMargin  :   height * 0.05
                placeholderText     :   "Isim giriniz..."
                font.pixelSize      :   height * textBoxFontSize
                font.family         :   FontCollection.labelFontName
                backgroundColor     :   textBoxColors
            }
        }

        Item {
            id                  :   passwordArea
            height              :   form.height * unitHeight
            width               :   parent.width

            Item {
                id      :   passwordIconContainer
                height  :   parent.height
                width   :   height

                Image {
                    id              :   passwordIconImg
                    source          :   "/res/img/password-icon.png"
                    anchors.fill    :   parent
                    anchors.margins :   height * 0.1
                    anchors.centerIn:   parent
                    fillMode        :   Image.PreserveAspectFit
                    smooth          :   true
                    mipmap          :   true
                }
            }

            RoboskopTextBox {
                id                  :   passwordTxtBox
                height              :   parent.height
                anchors.left        :   passwordIconContainer.right
                anchors.right       :   parent.right
                anchors.leftMargin  :   height * 0.05
                placeholderText     :   "Şifre giriniz..."
                font.pixelSize      :   height * textBoxFontSize
                font.family         :   FontCollection.labelFontName
                backgroundColor     :   textBoxColors
                echoMode        :   TextInput.Password
            }
        }

        Item {
            id          :   loginContainer
            width       :   parent.width
            height      :   form.height * buttonsHeight


            RoboskopButton{
                id                      :   loginButton
                height                  :   parent.height * 0.95
                width                   :   height * 2.5
                text                    :   "Giriş"
                anchors.bottom          :   parent.bottom
                anchors.right           :   parent.right
                controlColor            :   green
            }
        }
    }

    Rectangle{
        id              :   backRect
        anchors.fill    :   parent
        radius          :   height * 0.1
        color           :   mainContainerBackColor
    }

    DropShadow{
        id              :   shadow
        anchors.fill    :   backRect
        horizontalOffset:   0
        verticalOffset  :   0
        radius          :   backRect.height * 0.05
        samples         :   17
        source          :   backRect
        color           :   "#000000"
    }
}
