import QtQuick 2.7
import QtQuick.Controls 2.0
import GezginBot 1.0
import "../robogezgin"
import "../dialog"
import "../controls"

Item {

    id          :   container
    height      :   parent.height
    readonly property string headerText :   "GezginBot"

    function setCurrentIndex(idx){
        swipe.setCurrentIndex(idx)
    }

    RoboGezginConfig{
        id          :   settings
    }


    SwipeView{
        id          :   swipe
        currentIndex:   footerBar.currentIndex
        anchors.top     :   parent.top
        height      :   parent.height - footerBar.height
        width       :   parent.width

        GezginPov{
            id          :   pov
            config      :   settings
            onToastMessageRequired  :   {
                toast.displayMessage(message)
            }
        }


        LineFollowing{
            id          :   lineFollowing
            onToastMessageRequired  :   {
                toast.displayMessage(message)
            }
        }

        GezginBotSettings{
            config              :   settings
            onSensorAddRequired :   {
                //<sen>
                pov.addSensor(sen)
            }
            onToastMessageRequired  :   {
                toast.displayMessage(message)
            }
        }

        BluetoothDialog {
            id              :   bluetoothDialog
            onConnected     :   {
                swipe.setCurrentIndex(0)
            }
            onDisconnected  :   {
                swipe.setCurrentIndex(3)
            }
        }
    }

    Toast{
        id              :   toast
        container       :   swipe.currentItem
        fillColor       :   "#191919"
        textColor       :   "white"
    }

    TabBar {
        id              :   footerBar
        currentIndex    :   swipe.currentIndex
        anchors.left    :   parent.left
        anchors.right   :   parent.right
        anchors.bottom  :   parent.bottom
        position        :   TabBar.Footer


        TabButton{
            text    :   "Sürüş"
            Rectangle{
                anchors.top :   parent.top
                anchors.left    :   parent.left
                anchors.right   :   parent.right
                height      :   parent.height * 0.1
                color       :   "black"
            }
        }

        TabButton{
            text    :   "Çizgi İzleme"
            Rectangle{
                anchors.top :   parent.top
                anchors.left    :   parent.left
                anchors.right   :   parent.right
                height      :   parent.height * 0.1
                color       :   "black"
            }
        }

        TabButton{
            text    :   "Ayarlar"
            Rectangle{
                anchors.top :   parent.top
                anchors.left    :   parent.left
                anchors.right   :   parent.right
                height      :   parent.height * 0.1
                color       :   "black"
            }
        }

        TabButton{
            text    :   "Bluetooth"
            Rectangle{
                anchors.top :   parent.top
                anchors.left    :   parent.left
                anchors.right   :   parent.right
                height      :   parent.height * 0.1
                color       :   "black"
            }
        }
    }
}
