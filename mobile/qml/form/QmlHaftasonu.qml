import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../theme"

Rectangle {

    id : container

    property var    textSize : 24

    color : "#E0FFFF"

    Image {
        id: lessonImg
        x :SizeBinding.mm(10)
        y : SizeBinding.mm(10)
        source: "/res/svg/lesson/ders-6.svg"
        width : SizeBinding.size(parent.width * 0.27 , SizeBinding.mm(200) , SizeBinding.mm(20))
        height : width * 1.63034188
    }


    Rectangle{


        anchors.left : lessonImg.right
        anchors.leftMargin: SizeBinding.mm(6)
        y : parent.height * 0.1
        width: SizeBinding.size(container.width * 0.5 , SizeBinding.mm(150) , SizeBinding.mm(40))
        height: SizeBinding.mm(50)
        color : "transparent"

        Rectangle{
            id : redSliderContainer
            width:  parent.width
            height: parent.height * 0.33
            color : "transparent"

            Text {
                id: redText
                text: qsTr("Kırmızı Ton : ")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: textSize
                color : "red"
            }

            Slider{
                id : redSlider
                anchors.left : redText.right
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height * 0.6
                width: SizeBinding.size(parent.width * 0.5 , SizeBinding.mm(140) , SizeBinding.mm(45))
                minimumValue: 0
                maximumValue: 100
                style : SliderStyle{
                    handle: Rectangle{
                        anchors.centerIn: parent
                        implicitHeight: redSlider.height
                        implicitWidth:  implicitHeight
                        border.color : "gray"
                        radius: implicitHeight
                    }
                }
                onValueChanged: {
                    bluetooth.send("{61," + parseInt(redSlider.value) + "}")
                }
            }
        }

        Rectangle{

            id : greenSliderContainer
            anchors.top : redSliderContainer.bottom
            width: parent.width
            height: redSliderContainer.height
            color : "transparent"

            Text {
                id: greenText
                text: qsTr("Yeşil Ton    : ")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: textSize
                color : "green"
            }

            Slider{
                id  : greenSlider
                anchors.left : greenText.right
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height * 0.6
                width: SizeBinding.size(parent.width * 0.5 , SizeBinding.mm(140) , SizeBinding.mm(45))
                minimumValue: 0
                maximumValue: 100
                style : SliderStyle{
                    handle: Rectangle{
                        anchors.centerIn: parent
                        implicitHeight: redSlider.height
                        implicitWidth:  implicitHeight
                        border.color : "gray"
                        radius: implicitHeight
                    }
                }
                onValueChanged: {
                    bluetooth.send("{62," + parseInt(greenSlider.value) + "}")
                }
            }

        }

        Rectangle{

            id : blueSliderContainer
            anchors.top : greenSliderContainer.bottom
            width: parent.width
            height: redSliderContainer.height
            color : "transparent"

            Text {
                id: blueText
                text: qsTr("Mavi Ton    : ")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: textSize
                color : "blue"
            }

            Slider{
                id : blueSlider

                anchors.left : blueText.right
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height * 0.6
                width: SizeBinding.size(parent.width * 0.5 , SizeBinding.mm(140) , SizeBinding.mm(45))
                minimumValue: 0
                maximumValue: 100
                style : SliderStyle{
                    handle: Rectangle{
                        anchors.centerIn: parent
                        implicitHeight: redSlider.height
                        implicitWidth:  implicitHeight
                        border.color : "gray"
                        radius: implicitHeight
                    }
                }
                onValueChanged: {
                    bluetooth.send("{63," + parseInt(blueSlider.value) + "}")
                }
            }
        }
    }

}



