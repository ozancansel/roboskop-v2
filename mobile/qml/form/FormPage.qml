import QtQuick 2.0
import "../controls"

Item {

    signal  toastMessageRequired(string message)

    function displayMessage(msg){
        toastMessageRequired(msg)
    }

}
