import QtQuick 2.0
import QtQuick.Controls 2.2

TextField {
    property color  backgroundColor :   "white"
    background  :   Rectangle{
        id          :   back
        radius      :   height * 0.1
        border.width:   1
        color       :   backgroundColor
    }
    color       :   "green"
}
