import QtQuick 2.0
import QtQuick.Controls 2.1
import "../../"

SpinBox {

    readonly property int   green       :   0
    readonly property int   yellow      :   1
    readonly property int   pink        :   2
    readonly property variant   hoverColors         :   [ "#39e439" , "#ffdf33" , "#ff436b"]
    readonly property variant   backgroundColors    :   [ "#33cc33" , "#ffcc33" , "#ff3366"]
    property int    downColor           :   1
    property int    upColor             :   2
    property int    backgroundColor     :   0

    id              :   ctl

    FontLoader{
        id          :   kidFont
        source      :   "/res/font/kid-font.otf"
    }

    background      :   Rectangle{
        id          :   backRect
        radius      :   downItem.backRectRadius
        color       :   backgroundColors[backgroundColor]
    }

    contentItem     :   Item {
        id              :   contentItem
        anchors.left    :   downItem.right
        anchors.right   :   upItem.left
        anchors.top     :   backRect.top
        anchors.bottom  :   backRect.bottom

        Text {
            id                  :   valueText
            text                :   ctl.value
            anchors.centerIn    :   parent
            color               :   "white"
            font.family         :   Global.kidFontName
            font.pixelSize      :   parent.height * 0.4
        }
    }

    down.indicator  :   RoboskopButton {
        id              :   downItem
        implicitWidth   :   ctl.height
        implicitHeight  :   ctl.height
        controlColor    :   downItem.yellow
        text            :   "-"
        onClicked       :   ctl.value -=    stepSize
    }

    up.indicator    :   RoboskopButton {
        id              :   upItem
        width           :   ctl.height
        height          :   ctl.height
        anchors.right   :   ctl.right
        text            :   "+"
        controlColor    :   upItem.pink
        onClicked       :   ctl.value +=    stepSize
    }
}
