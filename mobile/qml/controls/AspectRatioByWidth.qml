import QtQuick 2.0

Item {

    property real           aspectX     :   16
    property real           aspectY     :   9

    readonly property real  ratio       :   aspectY / aspectX

    Component.onCompleted   :   {

        if(parent === 'undefined'){
            console.log("Parent atanmamış")
            return
        }

        parent.height = Qt.binding(function (){
            return parent.width *  ratio
        })
    }
}
