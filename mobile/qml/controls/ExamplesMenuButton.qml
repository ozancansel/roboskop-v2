import QtQuick 2.5
import "../theme"

Rectangle {

    id : buttonContainer

    property double     textHeight  :   0.41
    property alias buttonWidth : buttonContainer.width
    property alias buttonHeight : buttonContainer.height
    property alias text : buttonText.text
    property alias iconSource : icon.source

    signal buttonClicked()

//    color: "#2196f3"
    color           :   "#006ab3"
    radius          : height * 0.2
    border.width    : SizeBinding.size(width * 0.005 , SizeBinding.mm(0.6) , SizeBinding.mm(0.02))


    Rectangle {

        id : iconContainer
        height: parent.height
        width: height * 1.3
        color: "transparent"

        Image{

            id : icon
            source  : "/res/img/edit-icon-white-48x48.png"
            height : parent.height * 0.7
            width: icon.height
            anchors.centerIn: parent

        }

    }

    Text {
        id              :   buttonText
        text            :   qsTr("Atanmadı")
        font.pixelSize  :   buttonContainer.height * textHeight
        clip            :   false
        color           :   "white"
        anchors.left    :   iconContainer.right
        anchors.verticalCenter: buttonContainer.verticalCenter
    }

    MouseArea{
        id : area
        anchors.fill: parent
        onClicked: buttonContainer.buttonClicked()
    }

    Behavior on scale {                    // added to smooth the scaling
        NumberAnimation {
            duration: 100
        }
    }

    states : [
        State{
            name : "pressing"; when: area.pressed
            PropertyChanges {
                target: buttonContainer
                rotation : 5
                color : "red"
                scale : 1.3
            }
        }
    ]

    transitions: [
        Transition {
            from: ""
            to: "pressing"
            ParallelAnimation{

                SequentialAnimation{

                    loops: Animation.Infinite

                    ColorAnimation {
                        duration: 400
                        to : "red"
                    }

                    ColorAnimation {
                        from: "red"
                        to: "#ff9900"
                        duration: 400
                    }
                }
            }
        }
    ]

}
