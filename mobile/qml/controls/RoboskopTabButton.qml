import QtQuick 2.0
import QtQuick.Controls 2.1
import "../effect"
import "../theme"

TabButton {

    property color  backgroundColor         :   "green"
    property color  fontColor               :   "white"
    property color  highlightColor          :   "red"
    id      :   tabButton

    background  :   Item {
        id      :   backItem
        width   :   tabButton.width
        height  :   tabButton.height


        Rectangle   {
            id              :   backRectItem
            anchors.fill    :   parent
            color           :   tabButton.checked ? highlightColor : backgroundColor

            StarEffect  {
                id                      :   checkedFlag
                width                   :   parent.height
                height                  :   parent.height
                visible                 :   tabButton.checked
                x                       :   parent.height
            }
        }

        Text {
            id                  :   buttonText
            text                :   tabButton.text
            anchors.centerIn    :   parent
            font.family         :   FontCollection.labelFontName
            font.pixelSize      :   parent.height * 0.35
            color               :   fontColor
        }
    }

    contentItem :   Item { }
}
