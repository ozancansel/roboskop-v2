import QtQuick 2.5
import "../parts"
import "../enum"

NavbarControl{
    id                      :   bluetoothState
    iconHeight              :   0.9
    textHeight              :   0.9

    controlArea.onClicked   :   {
        if(bluetooth.state != BluetoothStateEnum.connected){
            InternalNotifyEnum.connectBluetooth()
            return
        }
    }

    controlArea.onPressAndHold  :   {
        if(bluetooth.state == BluetoothStateEnum.connected)
            bluetooth.closeSocket()
    }

    state   :   "Not Connected"

    states: [
        State{
            name    :   "connected"; when   :   bluetooth.state === BluetoothStateEnum.connected
            PropertyChanges {
                target      :   bluetoothState
                rotation    :   360
            }

            PropertyChanges {
                target      :   bluetoothState.textControl
                text        :   "Bağlantı Aktif"
            }
            PropertyChanges {
                target      :   bluetoothState.imgControl
                source      :   "/res/img/bluetooth-icon-connected.png"
            }
        },
        State{
            name    :   "connecting";   when    :   bluetooth.state === BluetoothStateEnum.connecting
            PropertyChanges {
                target      :   bluetoothState.textControl
                color       :   "green"
                text        :   "Bağlanıyor... "
            }
            PropertyChanges {
                target      :   bluetoothState.imgControl
                source      :   "/res/img/bluetooth-icon-unconnected.png"
            }
        },
        State {
            name    :   "discovering"; when     :   bluetooth.state === BluetoothStateEnum.discovering
            PropertyChanges {
                target      :   bluetoothState.textControl
                text        :   "Aranıyor...   "
                color       :   "blue"
            }
            PropertyChanges {
                target      :   bluetoothState.imgControl
                source      :   "/res/img/bluetooth-icon-unconnected.png"
            }
        } ,
        State {
            name    :   "disconnected"; when    :   bluetooth.state === BluetoothStateEnum.disconnected
            PropertyChanges {
                target      :   bluetoothState.textControl
                text        :   "Bağlantı koptu"
                color       :   "red"
            }

            PropertyChanges {
                target          :     navbar
                exampleRunning  :   false
            }

            PropertyChanges {
                target      :   bluetoothState.imgControl
                source      :   "/res/img/bluetooth-icon-unconnected.png"
            }
        },
        State {
            name    :   "notConnected"; when    :   bluetooth.state === BluetoothStateEnum.none
            PropertyChanges {
                target  :   bluetoothState.textControl
                text    :   "Bağlantı Yok  "
                color   :   "white"
            }

            PropertyChanges {
                target  :   bluetoothState.imgControl
                source  :   "/res/img/bluetooth-icon-unconnected.png"
            }
        },
        State {
            name        :   "error"; when   :   bluetooth.state == BluetoothStateEnum.error
            PropertyChanges {
                target  :   bluetoothState.textControl
                text    :   "Hata"
                color   :   "red"
            }
            PropertyChanges {
                target  :   bluetoothState.imgControl
                source  :   "/res/img/bluetooth-icon-unconnected.png"
            }
        }
    ]

    transitions :   [Transition {
            from    : ""
            to      : "connected"
            reversible  :   true
            RotationAnimation{
                target      :   bluetoothState
                duration    :   200
            }
        },
        Transition {
            to      : "connecting"
            SequentialAnimation{
                loops: Animation.Infinite

                ColorAnimation {
                    target  :   bluetoothState.textControl
                    duration: 400
                    from    :   "white"
                    to      :   "green"
                }

                ColorAnimation {
                    target  :   bluetoothState.textControl
                    from    : "green"
                    to      : "white"
                    duration: 400
                }
            }
        },
        Transition {
            to      :   "discovering"
            SequentialAnimation{
                loops       :   Animation.Infinite
                ScriptAction{ script : bluetoothState.textControl.text = "Aranıyor.     "}
                PauseAnimation { duration  : 300}
                ScriptAction{ script : bluetoothState.textControl.text = "Aranıyor..    "}
                PauseAnimation { duration  : 300}
                ScriptAction{ script : bluetoothState.textControl.text = "Aranıyor...   "}
                PauseAnimation { duration  : 500}
            }
        }]
}
