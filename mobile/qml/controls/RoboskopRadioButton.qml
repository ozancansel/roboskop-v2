import QtQuick 2.6
import QtQuick.Controls 2.1

RadioButton {

    readonly property real insideArea           :       0.6
    property real   radiusPortion               :       0.2
    property color  buttonColor                 :       "#17a81a"
    property color  textColor                   :       "#17a81a"

    id          :   control
    text        :   "RadioButton"

    states      :   [
        State   {
            name    :   "checked";  when    :   checked

            PropertyChanges {
                target  :   indicatorItem
                rotation:   360
            }
        }
    ]

    transitions :   [

        Transition {
            to  : "checked"
            reversible  :   true
            SequentialAnimation{
                ScaleAnimator{
                    target  :   indicatorItem
                    from    :   1
                    to      :   2
                    duration:   250
                    easing.type :   Easing.OutBounce
                }
                ScaleAnimator{
                    target  :   indicatorItem
                    from    :   2
                    to      :   1
                    duration:   150
                    easing.type :   Easing.OutBounce
                }
            }
        }
    ]

    indicator   :   Rectangle{
        id              :   indicatorItem
        implicitHeight  :   control.height
        implicitWidth   :   control.height
        x               :   control.leftPadding
        y               :   parent.height / 2 - height / 2
        radius          :   height * radiusPortion
        border.color    :   buttonColor

        Rectangle {
            width               :   parent.height * insideArea
            height              :   width
            anchors.centerIn    :   parent
            radius              :   width * radiusPortion
            color               : buttonColor
            visible             : control.checked
        }
    }

    contentItem             :   Text    {
        text                :   control.text
        font                :   control.font
        opacity             :   enabled ? 1.0 : 0.3
        color               :   textColor
        horizontalAlignment :   Text.AlignHCenter
        verticalAlignment   :   Text.AlignVCenter
        leftPadding         :   control.indicator.width + control.spacing
    }
}
