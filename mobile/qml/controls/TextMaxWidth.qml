import QtQuick 2.7

Item {

    property Text target        :   Text {}
    property int    maxWidth    :   0

    FontMetrics{
        id      :   metrics
        font    :   target.font
    }

    Component.onCompleted   :   {
        target.font.pixelSize   =   Qt.binding(function() {
            var preferredWidth = metrics.advanceWidth(target.text)
            if(preferredWidth > maxWidth)
                return maxWidth / preferredWidth * target.font.pixelSize
            return  target.font.pixelSize
        })
    }
}
