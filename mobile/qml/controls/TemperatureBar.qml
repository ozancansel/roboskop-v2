import QtQuick 2.0
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Gauge{

    //Width
    readonly property real valueBarWidth            :   0.8
    readonly property real minorTickMarkWidth       :   0.07
    readonly property real tickmarkWidth            :   0.13

    //Height
    readonly property real minorTickmarkHeight      :   0.1
    readonly property real tickmarkHeight           :   0.12

    //Margins
    //Left
    readonly property real tickmarkRightMargin       :   0.02
    readonly property real minorTickmarkRightMargin  :   0.02


    id                  :   tempGauge
    orientation         :   Qt.Vertical
    maximumValue        :   100
    minimumValue        :   0
    minorTickmarkCount  :   4
    value               :   90

    style       :   GaugeStyle{
        valueBar        :   Rectangle{
            implicitWidth   :   tempGauge.width * valueBarWidth
            color   : {
                var ratio = tempGauge.value / tempGauge.maximumValue
                var greenEnabled = ratio < 0.35
                var blueEnabled = ratio < 0.6 && ratio >= 0.35
                var redEnabled = ratio > 0.5
                var redRatio = redEnabled ? ratio - 0.5 : 0
                var greenRatio = greenEnabled ? (0.35 - ratio) * 4 : 0
                var blueRatio = blueEnabled ? ratio * 1.2 : 0

                Qt.rgba(redRatio, greenRatio,  blueRatio, 1)

            }
            radius  :   width * 0.02

            Text {
                id          :   celcius
                text        :   Math.floor(tempGauge.value) + " °C"
                font.pixelSize  :   Math.min(tempGauge.width , tempGauge.height) * 0.2
                color       :   "white"
                anchors.centerIn    :   parent
            }
        }

        minorTickmark   :   Item{
            implicitWidth   :   tempGauge.width * minorTickMarkWidth
            implicitHeight  :   Math.max(implicitWidth * minorTickmarkHeight , 1)

            Rectangle{
                color       :   "#cccccc"
                anchors.fill:   parent
                anchors.rightMargin :     tempGauge.width * minorTickmarkRightMargin
                anchors.leftMargin  :       anchors.rightMargin
            }
        }

        tickmark        :   Item{
            id              :   tickMarkContainer
            implicitWidth   :   tempGauge.width * tickmarkWidth
            implicitHeight  :   Math.max(implicitWidth * tickmarkHeight , 1)

            Rectangle{
                color       :   "#cccccc"
                anchors.fill:   parent
                anchors.rightMargin :   tempGauge.width * tickmarkRightMargin
                anchors.leftMargin  :   anchors.rightMargin
            }
        }
    }

    Behavior on value{
        SmoothedAnimation{
            velocity    :   height * 0.1
        }
    }
}
