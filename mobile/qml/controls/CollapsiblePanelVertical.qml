import QtQuick 2.0

Item {

    property real   collapsedWidth                      :   0
    property real   expandedWidth                       :   0
    property bool   collapsed                           :   false

    width       :   collapsed ? collapsedWidth : expandedWidth

    Behavior on width{
        SmoothedAnimation{
            velocity    :   width * 0.6
        }
    }
}
