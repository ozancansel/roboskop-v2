import QtQuick 2.0

Rectangle {


    //Width
    readonly property real buttonWidth              :   0.8

    //Height
    readonly property real textHeight               :   0.5
    //Radius
    readonly property real containerRadius          :   0.2

    //Alias
    property alias textColor        :   buttonText.color
    property alias text             :   buttonText.text
    property alias fillColor        :   lessonControlButtonContainer.color
    property alias stroke           :   lessonControlButtonContainer.border

    signal clicked()


    id      :   lessonControlButtonContainer
    color   :   "#ef7b66"
//    color   :   "#33e8ff"
    radius  :   height * containerRadius
    border.width        :   1
    border.color        :   "black"

    Text{
        id                  :   buttonText
        height              :   parent.height * textHeight
        font.pixelSize      :   height
        color               :   "white"
        anchors.centerIn    :   parent
    }

    MouseArea{
        id                  :   buttonArea
        width               :   parent.width
        height              :   parent.height
        onClicked           :   parent.clicked()
    }

    Behavior on scale{
        NumberAnimation{
            duration        :   100
        }
    }

    Behavior on rotation {
        NumberAnimation{
            duration        :   200
        }
    }

    states  :   [
        State {
            name    :   "pressing"
            when    :   buttonArea.pressed
            PropertyChanges {
                target          :   lessonControlButtonContainer
//                scale           :   1.25
                rotation        :   1.2
            }
        }
    ]
}
