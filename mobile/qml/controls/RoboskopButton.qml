import QtQuick 2.0
import QtQuick.Controls 2.1
import "../effect"
import "../theme"

Button {

    property int     controlColor   :   0
    readonly property int   green   :   0
    readonly property int   yellow  :   1
    readonly property int   pink    :   2
    readonly property variant   backgroundColors    :   [ "#33cc33" , "#ffcc33" , "#ff3366"]
    readonly property variant   hoverColors         :   [ "#39e439" , "#ffdf33" , "#ff436b"]

    readonly property alias     backRectRadius      :   backgroundRect.radius

    id          :   ctl

    FontLoader{
        id      :       kidFont
        source  :       "/res/font/kid-font.otf"
    }

    background  :   Item {
        id          :   backIt1
        scale       :   ctl.down ? 1.1 : 1

        Behavior on scale{
            NumberAnimation {
                duration    :   130
            }
        }

        Rectangle{
            id              :   backgroundRect
            anchors.fill    :   parent
            radius          :   height * 0.3
            color           :    ctl.hovered || ctl.pressed ? hoverColors[controlColor] :  backgroundColors[controlColor]
        }

        StarEffect{
            id                      :   checkedFlag2
            width                   :   parent.height
            height                  :   parent.height
            visible                 :   ctl.down || ctl.checked
        }

        Text {
            id              :   txt1
            text            :   ctl.text
            font.family     :   FontCollection.buttonFontName
            font.pixelSize  :   ctl.height * 0.5
            anchors.centerIn:   parent
            color           :   "white"
        }
    }

    contentItem :   Item {
        //Bos
    }
}
