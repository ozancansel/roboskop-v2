import QtQuick 2.0

Item {

    readonly property real iconLinePortion  :   0.1
    readonly property real lineRadius       :   0.4

    property alias area     :   mouseArea

    Column{
        anchors.fill    :   parent
        spacing         :   parent.height * ((1 - (3 * iconLinePortion)) / 3)

        Repeater{
            model   :   3
            delegate    :   Rectangle   {
                id      :   item
                width   :   parent.width
                height  :   parent.height * iconLinePortion
                radius  :   height * lineRadius
            }
        }
    }

    MouseArea{
        id              :   mouseArea
        anchors.fill    :   parent
    }
}
