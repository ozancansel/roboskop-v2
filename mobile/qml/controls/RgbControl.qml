import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "../theme"

Rectangle {

    id : container
    color : "purple"

    Slider{
        id : redSlider
        width: container.width
        height: SizeBinding.mm(20)
        minimumValue: 0
        maximumValue: 255

        Behavior on value {
            NumberAnimation{
                target: redSlider
                properties: "value"
                duration: 200
            }
        }
    }
}
