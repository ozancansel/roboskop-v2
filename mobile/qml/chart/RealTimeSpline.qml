import QtQuick 2.0
import QtCharts 2.1


SplineSeries {
    function    newPoint(newP){

        var maxX = axisX.max

        var lastP = at(count - 1)

        if(count <= maxX){

            if(count === 0)
                append(lastP.x, newP)
            else
                append(lastP.x + 1, newP)

        } else {

            for(var i = 0; i <= count; i++){
                var itP = at(i)
                replace(itP.x , itP.y , itP.x - 1 , itP.y)
            }

            remove(0)
            lastP = at(count - 1)
            append(lastP.x + 1 , newP)

        }
    }
}
