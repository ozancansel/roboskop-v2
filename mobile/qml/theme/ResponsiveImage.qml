pragma singleton
import QtQuick 2.0
import "../theme"

QtObject {

    readonly property real  multiplier  :   determineSize()

    function getImage(image){
        return "/res/img/" + multiplier + "x/" + path
    }

    function determineSize(){
        var size = Math.max(SizeBinding.screenHeight() , SizeBinding.screenWidth())
        if(size <= 480)
            return 1
        else if (size > 480 && size <= 800)
            return 2
        else if (size > 800 && size <= 1280)
            return 4
        else if (size > 1280 && size <= 1920)
            return 8
        else if(size > 1920 && size <= 2560)
            return 16
    }
}
