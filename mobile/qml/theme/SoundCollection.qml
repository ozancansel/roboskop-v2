pragma Singleton
import QtQuick 2.0

Item {

    readonly property string    congratSoundPath    :   "/res/sound/congrat.wav"
    readonly property string    gameOverSoundPath   :   "/res/sound/end-sound.wav"
    readonly property string    timeOverSoundPath   :   "/res/sound/end-sound.wav"
    readonly property string    newPointSoundPath   :   "/res/sound/beep.wav"
    readonly property string    startGameSoundPath  :   ""

}
