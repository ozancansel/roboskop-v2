pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.0

QtObject {

    //Colors
    readonly property int        blue            :   0
    readonly property int        red             :   1
    readonly property int        black           :   2
    readonly property int        yellow          :   3
    readonly property int        green           :   4
    readonly property int        grey            :   5
    readonly property int        white           :   6
    readonly property int        orange          :   7
    readonly property int        ochre           :   8
    readonly property int        cyan            :   9
    readonly property int        brown           :   10
    readonly property int        purple          :   11
    readonly property int        pink            :   12

    readonly property variant   colorStrings    :   ["blue" , "red" , "black" , "yellow" , "green" , "grey" , "white" , "orange" ,
                                                        "ochre" , "cyan" , "brown" , "purple" , "pink"]

    readonly property variant   colors      :   ["#2286fb" , "red" , "black" , "#ffcc33" , "#33cc33" ,
                                                "gray" , "white" , "orange" , "ochre" , "cyan" ,
                                                "brown" , "#940092" , "#ff3366"]
    readonly property variant   contraColors:   ["white" , "white" , "white" , "#ff3366" ,"#ffcc33" ,
                                                 "white" , "black" , "black", "white" , "black" ,
                                                 "white" , "white" , "#f2f1ef"]

}
