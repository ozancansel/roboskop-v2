pragma Singleton
import QtQuick 2.0

Item {

    readonly property string kidFontName        :   kidFont.name
    readonly property string headerFontName     :   jangeCloser.name
    readonly property string labelFontName      :   jangeCloser.name
    readonly property string buttonFontName     :   jangeCloser.name
    readonly property string aclonicaFont       :   aclonica.name
    readonly property string unitedKingdomFont  :   unitedKingdom.name

    FontLoader  {
        id          :   kidFont
        source      :   "/res/font/special-labels-font.ttf"
    }

    FontLoader{
        id          :   jangeCloser
        source      :   "/res/font/JandaCloserToFree.ttf"
    }

    FontLoader{
        id          :   aclonica
        source      :   "/res/font/aclonica.ttf"
    }

    FontLoader{
        id          :   unitedKingdom
        source      :   "/res/font/united-kingdom.otf"
    }
}
