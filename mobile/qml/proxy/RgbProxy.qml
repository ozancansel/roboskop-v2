import QtQuick 2.0
import Roboskop 1.0

Item {
    property ArduinoProxy   arduino : ({})
    property int            redPin  :   -1
    property int            greenPin:   -1
    property int            bluePin :   -1

    id      :   rgbItem

    onArduinoChanged    :   {
        if(!arduino)
            return

        arduino.pinMode(redPin , constants.output)
        arduino.pinMode(greenPin , constants.output)
        arduino.pinMode(bluePin , constants.output)

    }

    function    r(value){
        arduino.analogWrite(redPin , Math.round(255 - (value * 255)))
    }

    function g(value){
        arduino.analogWrite(greenPin , Math.round(255 - (value * 255)))
    }

    function b(value){
        arduino.analogWrite(bluePin , Math.round(255 - (value * 255)))
    }

    function rgb(r , g , b){
        rgbItem.r(r)
        rgbItem.g(g)
        rgbItem.b(b)
    }

    ArduinoConstants{
        id  :   constants
    }

}
