#include "serialcommandinterpreter.h"
#include <QDebug>

SerialCommandInterpreter::SerialCommandInterpreter(QObject *parent) : QObject(parent)
{
    _device = NULL;

    initializeDefaults();
}

SerialCommandInterpreter::SerialCommandInterpreter(QIODevice *device, QObject *parent) : QObject(parent)
{
    _device = device;

    connect(_device , SIGNAL(readyRead()) , this , SLOT(onReadyRead()));

    initializeDefaults();
}

void SerialCommandInterpreter::listen(QIODevice *device){

    //Başka bir socket atanmışsa, onla ilgili signallar detach ediliyor
    if(_device != NULL)
        disconnect(_device , 0 , 0 , 0);

    _device = device;

    connect(_device , SIGNAL(readyRead()) , this , SLOT(onReadyRead()));
}

void SerialCommandInterpreter::onReadyRead(){

    _buffer.append(_device->readAll());

    if(_status == None){

        int startIdx = _buffer.indexOf(_commandStart);

        if(startIdx != -1)
            _status = Start;

        _buffer.remove(0 , startIdx);
    }
    else if(_status == Start){

        int endIdx = _buffer.indexOf(_commandEnd);


        //Eğer ki idx -1 den farklıysa frame bitmiş demektir.
        if(endIdx != -1)
            _status = None;
        else
            return;

        QByteArray  commandBuffer = _buffer.left(endIdx);

        _buffer.remove(0 , endIdx);

        int code =  (int)commandBuffer.at(1);

        commandBuffer.remove(0 , 2);

        QString command = QString::fromUtf8(commandBuffer);

        QStringList arguments = command.split(",");

        emit commandIncome(code , arguments);

        onReadyRead();
    }

}

void SerialCommandInterpreter::initializeDefaults(){
    _commandStart.append("{");
    _commandEnd.append("}");

    _status = None;
}
