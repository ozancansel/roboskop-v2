#include "statistics.h"
#include <QtQml>

void    Statistics::registerQmlType(){
    qmlRegisterType<Statistics>("Roboskop", 1, 0, "RStatistics");
}

Statistics::Statistics(QQuickItem *parent ) : QQuickItem(parent){
}

double Statistics::calculateStdDeviation(QVariantList    _values){

    if(_values.empty())
        return 0;

    int     meanSum = 0, varianceSum = 0;
    double  mean , variance , stdDeviation;

    //Mean hesaplaniyor
    for(int i = 0; i < _values.length(); i++){
        int     val = _values.at(i).toInt();
        meanSum +=  val;
    }

    mean    =   meanSum / (double)_values.length();

    for(int i = 0; i < _values.length(); i++){
        int     val = _values.at(i).toInt();
        varianceSum += (val - mean) * (val - mean);
    }

    variance    =   varianceSum / (double)_values.length();
    stdDeviation=   qSqrt(variance);

    return stdDeviation;
}
