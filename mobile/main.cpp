#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQmlContext>
#include "bluetoothutil.h"
#include "serialcommandinterpreter.h"
#include "globalnotifier.h"
#include "statistics.h"
#include "storage/gamestatistics.h"
#include <plugins.h>
#include "serialcomm.h"
#include "serialport.h"
#include "arduinoproxy.h"
#include "arduinoconstants.h"
#include "serialcomm.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();

    BluetoothUtil               bt;
    SerialCommandInterpreter    interpreter;
    GlobalNotifier              notifier;
    SerialComm*                 comm;

#ifdef Q_OS_ANDROID
    comm = new BluetoothUtil();
#else
    comm = new SerialPort();
#endif

    /*Tipler ekleniyor*/
    Statistics::registerQmlType();
    GameStatistics::registerQmlType();

    notifier.setDebug(true);
    bt.setDebugEnabled(true);

    Plugins plugins;
    plugins.registerTypes("Bacon2D");

    ArduinoProxy::registerQmlType();
    ArduinoConstants::registerQmlType();

    context->setContextProperty("bluetooth" , comm);
    context->setContextProperty("commandInterpreter" , &interpreter);
    context->setContextProperty("globalNotifier" , &notifier);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
