#ifndef BLUETOOTHUTIL_H
#define BLUETOOTHUTIL_H

#include <QObject>
#include <QBluetoothSocket>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothLocalDevice>
#include <QList>
#include <QTimer>
#include <QTime>
#include <QQueue>
#include <QQuickItem>
#include "iroboskopcommandinterpreter.h"
#include "serialcomm.h"

class BluetoothUtil : public  SerialComm
{

    Q_OBJECT

public:

    static void init();
    BluetoothUtil();
    void        setDebugEnabled(bool enabled = true);

public slots:

    void        connectTo(QString address) override;
    void        startScan() override;

private slots:

    void        btConnected();
    void        btDisconnected();
    void        btDeviceDiscovered(const QBluetoothDeviceInfo&);
    void        btScanFinished();
    void        btStateChanged(QBluetoothSocket::SocketState state);

private:

    bool            _debug;
    bool            _commandQueued;

    QList<QBluetoothDeviceInfo>         m_discoveredDevices;
    QMap<QString , QBluetoothDeviceInfo*>       _discoveredDevices;
    QBluetoothDeviceDiscoveryAgent      *_agent;
    QBluetoothLocalDevice               *_localDevice;
    QBluetoothSocket                    *_socket;

    void                    appendIfNotExists(QBluetoothDeviceInfo info);
    QBluetoothDeviceInfo    byAddress(QString addr);

    bool                    debug();

};

#endif // BLUETOOTHUTIL_H
