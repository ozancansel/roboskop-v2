QT += qml quick bluetooth svg charts serialport

QTPLUGIN += qsvg qsvgicon

CONFIG += c++11

SOURCES += main.cpp \
    bluetoothutil.cpp \
    serialcommandinterpreter.cpp \
    iroboskopcommandinterpreter.cpp \
    globalnotifier.cpp \
    statistics.cpp \
    storage/jsonstorage.cpp \
    storage/gamestatistics.cpp \
    serialcomm.cpp \
    serialport.cpp \
    arduinoproxy.cpp \
    arduinoconstants.cpp

RESOURCES += \
    media.qrc \
    qml.qrc

include(../Bacon2D/src/Bacon2D-static.pri)

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    main.qml \
    qml/controls/ExamplesMenuButton.qml \
    qml/form/ExampleForm.qml \
    qml/parts/LessonSummary.qml \
    qml/theme/AppTheme.qml \
    qml/theme/SizeBinding.qml \
    qml/theme/qmldir \
    qml/model/LessonModel.qml \
    qml/dialog/BluetoothDialog.qml \
    qml/model/BluetoothModel.qml \
    qml/parts/BluetoothListItem.qml \
    qml/form/LessonForm.qml \
    qml/circuit/Arduino.qml \
    qml/controls/RgbControl.qml \
    qml/circuit/Pin.qml \
    qml/circuit/LedKit.qml \
    qml/circuit/template/Template25x25.qml \
    qml/circuit/template/qmldir \
    qml/model/KitPinsModel.qml \
    qml/form/QmlHaftasonu.qml \
    qml/circuit/Wire.qml \
    qml/form/LessonDrawingForm.qml \
    qml/circuit/BluetoothKit.qml \
    qml/circuit/template/Template25x40.qml \
    qml/circuit/LdrKit.qml \
    qml/controls/AutoText.qml \
    qml/circuit/template/SketchTemplate.qml \
    qml/theme/Color.qml \
    qml/circuit/ArduinoUno.qml \
    qml/circuit/ArduinoPinsEnum.qml \
    qml/circuit/Battery.qml \
    qml/circuit/TrafficLightKit.qml \
    qml/circuit/PotKit.qml \
    qml/circuit/RgbLedKit.qml \
    qml/circuit/BuzzerKit.qml \
    qml/circuit/AcousticKit.qml \
    qml/circuit/roboskop101/Lesson13Sketch.qml \
    qml/circuit/MicrophoneKit.qml \
    qml/circuit/roboskop101/Lesson16Sketch.qml \
    qml/parts/ApplicationMenu.qml \
    qml/chart/RealTimeSpline.qml \
    qml/controls/LessonControlButton.qml \
    qml/circuit/template/AnalogReadTemplate.qml \
    qml/circuit/template/InteractiveControlContainer.qml \
    qml/circuit/roboskop101/control/PianoControl.qml \
    qml/circuit/roboskop101/Roboskop101Map.qml \
    qml/circuit/roboskop101/qmldir \
    qml/circuit/roboskop101/LedIntroductionSketch.qml \
    qml/circuit/roboskop101/LedBlinkSketch.qml \
    qml/circuit/roboskop101/TrafficLightSketch.qml \
    qml/circuit/roboskop101/PotSignalSketch.qml \
    qml/circuit/roboskop101/LdrSignalSketch.qml \
    qml/circuit/roboskop101/RgbLedSketch.qml \
    qml/circuit/roboskop101/RgbLedAndPotSketch.qml \
    qml/circuit/roboskop101/RgbLedAndLdrSketch.qml \
    qml/circuit/roboskop101/RoboticMusicSketch.qml \
    qml/circuit/roboskop101/RoboticMusicAndPotSketch.qml \
    qml/circuit/roboskop101/AcousticRulerSketch.qml \
    qml/circuit/roboskop101/ParkSensorSketch.qml \
    qml/circuit/roboskop101/DigitalMicrophoneSketch.qml \
    qml/circuit/roboskop101/RgbLedAndMicSketch.qml \
    qml/circuit/roboskop101/control/TrafficLightControl.qml \
    qml/circuit/roboskop101/control/PotReadControl.qml \
    qml/circuit/roboskop101/control/LdrReadControl.qml \
    qml/circuit/roboskop101/control/RgbControl.qml \
    qml/circuit/roboskop101/control/AcousticDistanceControl.qml \
    qml/circuit/roboskop101/control/LedBlinkControl.qml \
    qml/circuit/LedVisual.qml \
    qml/parts/ExampleStateNavbarComponent.qml \
    qml/parts/NavbarControl.qml \
    qml/circuit/roboskop101/control/MicrophoneReadControl.qml \
    qml/enum/InternalNotifyEnum.qml \
    qml/enum/qmldir \
    qml/enum/BluetoothStateEnum.qml \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    qml/theme/ResponsiveImage.qml \
    qml/game/RunAndJumpGameScene.qml    \
    qml/game/bacon2dlib/Boundaries.qml \
    qml/game/bacon2dlib/BoxBody.qml    \
    qml/game/bacon2dlib/ChainBody.qml  \
    qml/game/bacon2dlib/CircleBody.qml \
    qml/game/bacon2dlib/EdgeBody.qml   \
    qml/game/bacon2dlib/EdgeBody.qml   \
    qml/game/bacon2dlib/ImageBoxBody.qml   \
    qml/game/bacon2dlib/InfiniteScrollEntity.qml   \
    qml/game/bacon2dlib/PhysicsEntity.qml  \
    qml/game/bacon2dlib/PolygonBody.qml    \
    qml/game/bacon2dlib/RectangleBoxBody   \
    qml/game/bacon2dlib/qmldir \
    qml/game/runAndJump/Hero.qml \
    qml/game/runAndJump/GroundLine.qml \
    qml/game/runAndJump/Obstacle.qml    \
    qml/game/runAndJump/RocketFlame.qml \
    qml/game/runAndJump/ExplosionSprite.qml \
    qml/circuit/roboskop101/control/SpaceshipWithPotGame.qml \
    qml/circuit/roboskop101/control/EmptyControl.qml \
    qml/circuit/roboskop101/EmptySketch.qml \
    qml/game/runAndJump/SpaceshipBullet.qml \
    qml/enum/CarStatusEnum.qml \
    qml/theme/ResponsiveContainer.qml \
    qml/controls/AspectRatioByWidth.qml \
    qml/controls/FlickableWithCalculator.qml \
    qml/form/GezginPovForm.qml \
    qml/controls/AppMenuIcon.qml \
    qml/utility/NumericHelper.qml \
    qml/utility/qmldir \
    qml/circuit/roboskop102/control/TemperatureSensor.qml \
    qml/circuit/roboskop102/Roboskop102Map.qml \
    qml/circuit/roboskop102/circuit/TemperatureSketch.qml \
    qml/controls/TemperatureBar.qml \
    qml/model/Roboskop102LessonsModel.qml \
    qml/controls/RoboskopRadioButton.qml \
    qml/controls/BluetoothStateControl.qml \
    qml/controls/Toast.qml \
    qml/controls/ToastComponent.qml \
    Global.qml \
    qmldir \
    qml/form/FormPage.qml \
    qml/music/NoteStream.qml \
    qml/music/NoteSymbols.qml \
    qml/music/Melody.qml \
    qml/music/notearchive/JingleBells.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    qml/controls/TextMaxWidth.qml \
    qml/parts/LessonIndexItem.qml \
    qml/parts/CompletedItem.qml \
    qml/parts/SpecialLabel.qml \
    qml/controls/RoboskopCheckButton.qml \
    qml/controls/RoboskopButton.qml \
    qml/controls/RoboskopSpinBox.qml \
    qml/theme/FontCollection.qml \
    qml/theme/SoundCollection.qml \
    qml/parts/SmileyFace.qml \
    qml/controls/RoboskopTabButton.qml \
    qml/effect/StarEffect.qml \
    qml/parts/ScoreLabel.qml \
    qml/parts/StateLabel.qml \
    qml/form/LoginForm.qml \
    qml/controls/RoboskopTextBox.qml \
    qml/dialog/LoginDialog.qml \
    qml/circuit/PinoBoard.qml \
    qml/circuit/preschool/RgbLedSketch.qml \
    background.js \
    qml/proxy/RgbProxy.qml \
    qml/proxy/AcousticProximityProxy.qml \
    qml/dialog/GameEndDialog.qml \
    qml/dialog/ConnectingDialog.qml

HEADERS += \
    bluetoothutil.h \
    serialcommandinterpreter.h \
    iroboskopcommandinterpreter.h \
    globalnotifier.h \
    statistics.h \
    storage/jsonstorage.h \
    storage/gamestatistics.h \
    serialcomm.h \
    serialport.h \
    arduinoproxy.h \
    arduinoconstants.h

#ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

