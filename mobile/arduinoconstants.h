#ifndef ARDUINOCONSTANTS_H
#define ARDUINOCONSTANTS_H

#include <QQuickItem>

class ArduinoConstants : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(int a0 READ a0 NOTIFY a0Changed)
    Q_PROPERTY(int a1 READ a1 NOTIFY a1Changed)
    Q_PROPERTY(int a2 READ a2 NOTIFY a2Changed)
    Q_PROPERTY(int a3 READ a3 NOTIFY a3Changed)
    Q_PROPERTY(int a4 READ a4 NOTIFY a4Changed)
    Q_PROPERTY(int a5 READ a5 NOTIFY a5Changed)
    Q_PROPERTY(int high READ high NOTIFY highChanged)
    Q_PROPERTY(int low READ low NOTIFY lowChanged)
    Q_PROPERTY(int output READ output NOTIFY outputChanged)
    Q_PROPERTY(int input READ input NOTIFY inputChanged)

public:

    static void     registerQmlType();
    ArduinoConstants(QQuickItem* parent = nullptr);
    int     a0();
    int     a1();
    int     a2();
    int     a3();
    int     a4();
    int     a5();
    int     high();
    int     low();
    int     output();
    int     input();

signals:

    void    a0Changed();
    void    a1Changed();
    void    a2Changed();
    void    a3Changed();
    void    a4Changed();
    void    a5Changed();
    void    highChanged();
    void    lowChanged();
    void    outputChanged();
    void    inputChanged();

};

#endif // ARDUINOCONSTANTS_H
