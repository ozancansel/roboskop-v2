#ifndef ARDUINOPROXY_H
#define ARDUINOPROXY_H

#include <QQuickItem>
#include <QIODevice>
#include <QRegularExpression>
#include "serialcomm.h"

#define DIGITAL_WRITE       "!"
#define DIGITAL_READ        "\""
#define ANALOG_WRITE        "$"
#define ANALOG_READ         "%"
#define DELAY_MS            "&"
#define DELAY_US            "'"
#define PULSE_IN            "("
#define PIN_MODE            ")"
#define TONE                "+"
#define NO_TONE             "-"
#define ATTACH_ACOUSTIC     "."
#define READ_DISTANCE       "/"

#define A0                  14
#define A1                  15
#define A2                  16
#define A3                  17
#define A4                  18
#define A5                  19
#define A6                  20

#define HIGH                1
#define LOW                 0
#define OUTPUT              1
#define INPUT               0

class ArduinoProxy : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(SerialComm* socket READ socket WRITE setSocket NOTIFY socketChanged)

public:

    static void registerQmlType();
    ArduinoProxy(QQuickItem* parent = nullptr);
    SerialComm*  socket();
    void        setSocket(SerialComm* socket);

    struct     CommandBlock{
        int         code;
        QStringList params;
    };

public slots:

    bool        connectionOpen();
    void        digitalWrite(int pin , int type);
    bool        digitalRead(int pin);
    void        analogWrite(int pin , int type);
    int         analogRead(int pin);
    void        pinMode(int pin , int type);
    void        delay(int duration);
    void        delayMicroseconds(int duration);
    void        tone(int pin , int frequency);
    void        noTone(int pin);
    int         attachAcoustic(int triggerPin , int echoPin);
    int         readDistance(int id , int timeout);

signals:

    void        socketChanged();

private:

    QList<CommandBlock> m_parameters;
    SerialComm*         m_comm;
    QIODevice*          m_socket;
    QRegularExpression  m_waitMessageExpression;

    bool        wait(int timeout = 1000);
    bool        checkSocket();

};

#endif // ARDUINOPROXY_H
