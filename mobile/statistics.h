#ifndef STATISTICS_H
#define STATISTICS_H

#include <QQuickItem>
#include <QList>
#include <QtMath>
#include <QVariant>

class Statistics : public QQuickItem
{
    Q_OBJECT
public:

    static void     registerQmlType();
    Statistics(QQuickItem *parent = Q_NULLPTR);
public slots:
    double      calculateStdDeviation(QVariantList    list);
};

#endif // STATISTICS_H
