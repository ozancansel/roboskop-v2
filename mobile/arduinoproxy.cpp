#include "arduinoproxy.h"
#include <QRegularExpression>
#include <QTime>

void ArduinoProxy::registerQmlType(){
    qmlRegisterType<ArduinoProxy>("Roboskop" , 1 , 0 , "ArduinoProxy");
}

ArduinoProxy::ArduinoProxy(QQuickItem* parent) :
    QQuickItem(parent) ,
    m_socket(nullptr)
{
    m_waitMessageExpression = QRegularExpression("{(.)([^{}]*)}");
}

SerialComm* ArduinoProxy::socket(){
    return m_comm;
}

void ArduinoProxy::setSocket(SerialComm *socket){
    m_comm = socket;
    m_socket = m_comm->device();
    emit socketChanged();
}

bool ArduinoProxy::wait(int timeout){

    if(m_socket == nullptr || !m_socket->isOpen()){
        return false;
    }

    //Parametreler temizleniyor
    m_parameters.clear();

    QTime t;
    t.start();
    int started = t.elapsed();

    QString income;

    while(timeout == -1 || t.elapsed() - started < timeout){
        bool ready = m_socket->bytesAvailable() || m_socket->waitForReadyRead(10);

        if(ready){
            income.append(QString(m_socket->readAll()));
            QRegularExpressionMatchIterator iter = m_waitMessageExpression.globalMatch(income);

            while(iter.hasNext()){
                //Eslesen aliniyor
                QRegularExpressionMatch match = iter.next();

                int code = (int)match.captured(1).toLatin1().at(0);

                //Ok message received
                if(code == 35){
                    return true;
                }else if(code == 42){
                    return false;
                }

                QString paramsStr = match.captured(2);

                CommandBlock    block;

                block.code = code;

                //Eger parametreler varsa
                if(!paramsStr.isEmpty()){
                    QStringList paramList = paramsStr.split(",");
                    block.params = paramList;
                }

                m_parameters.append(block);
            }
        }
    }

    return false;
}



//Functions
void ArduinoProxy::digitalWrite(int pin, int type){
    if(!checkSocket()) return;
    m_comm->send(QString("{%0%1,%2}")
                    .arg(DIGITAL_WRITE)
                    .arg(pin)
                    .arg(type));

    bool result = wait();

    if(!result)
        qDebug() << "digitalWrite fail";
}

bool ArduinoProxy::digitalRead(int pin){
    return false;
}

void ArduinoProxy::analogWrite(int pin, int type){
    if(!checkSocket()) return;
    m_comm->send(QString("{%0%1,%2}")
                    .arg(ANALOG_WRITE)
                    .arg(pin)
                    .arg(type));

    wait();
}

int ArduinoProxy::analogRead(int pin){
    if(!checkSocket()) return -1;
    QString command = QString("{%0%1}")
            .arg(ANALOG_READ)
            .arg(pin);

    m_comm->send(command);

    bool result = wait();

    if(!result){
    } else  {
        if(m_parameters.length() < 1)
            return -1;
        QString valueStr = m_parameters.at(0).params.at(0);
        return valueStr.toInt();
    }

    return -1;
}

void ArduinoProxy::pinMode(int pin, int type){
    if(!checkSocket())  return;
    m_comm->send(QString("{%0%1,%2}")
                    .arg(PIN_MODE)
                    .arg(pin)
                    .arg(type));

    wait();
}

void ArduinoProxy::delay(int duration){
    if(!checkSocket()) return;
    QString command = QString("{%0%1}")
            .arg(DELAY_MS)
            .arg(duration);
    m_comm->send(command);

    bool result = wait(duration > 1000 ? duration * 2 : 1000);
}

void ArduinoProxy::delayMicroseconds(int duration){
    if(!checkSocket()) return;
    QString command = QString("{%0%1}")
            .arg(DELAY_MS)
            .arg(duration);

    m_comm->send(command);

    bool result = wait(-1);
}

void ArduinoProxy::tone(int pin, int frequency){
    if(!checkSocket())  return;

    QString command = QString("{%0%1,%2}")
                        .arg(TONE)
                        .arg(pin)
                        .arg(frequency);

    m_comm->send(command);

    wait(-1);
}

void ArduinoProxy::noTone(int pin){
    if(!checkSocket())  return;

    QString command = QString("{%0%1}")
                            .arg(NO_TONE)
                            .arg(pin);

    m_comm->send(command);

    wait(-1);
}


bool ArduinoProxy::checkSocket(){
    if(m_socket == nullptr) return false;
    if(!m_socket->isOpen()) return false;

    return true;
}

int ArduinoProxy::attachAcoustic(int triggerPin, int echoPin){
    if(!checkSocket())  return - 1;
    QString command = QString("{%0%1,%2}")
                        .arg(ATTACH_ACOUSTIC)
                        .arg(triggerPin)
                        .arg(echoPin);

    m_comm->send(command);

    bool result = wait();

    if(result){
        int id = m_parameters.at(0).params.at(0).toInt();
        return id;
    } else {
        return -1;
    }
}

int ArduinoProxy::readDistance(int id , int timeout){
    if(!checkSocket())  return -1;

    QString command = QString("{%0%1,%2}")
                        .arg(READ_DISTANCE)
                        .arg(id)
                        .arg(timeout);

    m_comm->send(command);

    bool result = wait();

    if(result){
        int distance = m_parameters.at(0).params.at(0).toInt();
        return distance;
    } else {
        return -1;
    }

}

bool ArduinoProxy::connectionOpen(){
    return m_socket != nullptr && m_socket->isOpen();
}
