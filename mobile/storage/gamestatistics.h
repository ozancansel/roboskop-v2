#ifndef GAMESTATISTICS_H
#define GAMESTATISTICS_H

#include "jsonstorage.h"

#define LESSON_STATE_LABEL          "state"
#define LESSON_COMPLETED_VAL        "completed"
#define LESSON_ENTERED_VAL          "entered"
#define LESSON_STARTED_VAL          "started"
#define LESSON_STATE_NONE_VAL       "none"
#define LESSON_HIGHEST_SCORE_LABEL  "score"
#define STATISTIC_FILE_PREFIX       "game-statistics-"

class GameStatistics : public JsonStorage
{

    Q_OBJECT
    Q_PROPERTY(int lessonId READ lessonId WRITE setLessonId NOTIFY lessonIdChanged)
    Q_PROPERTY(QString state READ state NOTIFY stateChanged)
    Q_PROPERTY(int  highestScore READ highestScore NOTIFY highestScoreChanged)

public:

    static void registerQmlType();
    GameStatistics(QObject *parent = Q_NULLPTR);

    int         lessonId();
    QString     state();
    int         highestScore();
    void        setLessonId(int val);

public slots:

    void        started();
    void        stopped();
    void        entered();
    void        newScore(int score);
    void        refresh();

signals:

    void        lessonIdChanged();
    void        highestScoreChanged();
    void        stateChanged();

private:

    bool        isCompleted();
    bool        hasScore();
    void        setHighestScore(int val);
    void        setState(QString state);


    int         _lessonId;
    int         _highestScore;

};

#endif // GAMESTATISTICS_H
