#include "gamestatistics.h"
#include <QtQml>

void GameStatistics::registerQmlType(){
    qmlRegisterType<GameStatistics>("Roboskop" , 1 , 0 , "GameStatistics");
}

GameStatistics::GameStatistics(QObject *parent) : JsonStorage(parent)
{
    _lessonId = -1;
}

void GameStatistics::started(){
    qDebug() << "Lesson " << _lessonId << " baslatildi.";
    if(state() == LESSON_STATE_NONE_VAL){
        setState(LESSON_STARTED_VAL);
    }
}

void GameStatistics::stopped(){
    qDebug() << "Lesson " << _lessonId << " tamamlandi.";
    if(state() != LESSON_COMPLETED_VAL)
        setState(LESSON_COMPLETED_VAL);
}

void GameStatistics::setLessonId(int val){
    _lessonId   =   val;
    setFile(QString(STATISTIC_FILE_PREFIX).append(QString::number(val)));

    refresh();
}

int GameStatistics::lessonId(){
    return _lessonId;
}

QString GameStatistics::state(){
    QVariant    lessonState = get(LESSON_STATE_LABEL);

    if(lessonState.isNull())
        return LESSON_STATE_NONE_VAL;

    return lessonState.toString();
}

int GameStatistics::highestScore(){
    QVariant    highestScore = get(LESSON_HIGHEST_SCORE_LABEL);

    if(highestScore.isNull())
        return -1;

    return highestScore.toInt();
}

void GameStatistics::newScore(int score){
    int     lastScore = highestScore();

    qDebug() << "Lesson " << _lessonId << " yeni skor -> " << score;

    setState(LESSON_COMPLETED_VAL);
    //Son skor buyukse, en yuksek puan degistiriliyor
    if(score > lastScore){
        setHighestScore(score);
    }
}


bool GameStatistics::isCompleted(){

    QVariant    lessonState = get(LESSON_STATE_LABEL);

    //Girilmemisse false donduruluyor
    if(lessonState.isNull())
        return false;


    return lessonState.toString() == LESSON_COMPLETED_VAL;
}

bool GameStatistics::hasScore(){
    QVariant    lessonScore = get(LESSON_HIGHEST_SCORE_LABEL);

    //Girilmemisse false olarak geri donduruluyor
    if(lessonScore.isNull())
        return false;

    return true;
}

void GameStatistics::setHighestScore(int val){
    set(LESSON_HIGHEST_SCORE_LABEL , QVariant(val));

    save();

    emit highestScoreChanged();
}

void GameStatistics::setState(QString state){
    set(LESSON_STATE_LABEL , state);

    save();

    emit stateChanged();
}

void GameStatistics::entered(){
    qDebug() << "Lesson " << _lessonId << " giris yapildi.";
    setState(LESSON_ENTERED_VAL);
}

void GameStatistics::refresh(){
    //Cache temizleniyor
    clearCache();

    load();     //Ders ayarlari yukleniyor

    emit lessonIdChanged();
    emit stateChanged();
    emit highestScoreChanged();
}
